/*
Navicat MySQL Data Transfer

Source Server         : php
Source Server Version : 50520
Source Host           : localhost:3306
Source Database       : sf

Target Server Type    : MYSQL
Target Server Version : 50520
File Encoding         : 65001

Date: 2015-08-18 13:20:26
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sf_action
-- ----------------------------
DROP TABLE IF EXISTS `sf_action`;
CREATE TABLE `sf_action` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `name` char(30) NOT NULL DEFAULT '' COMMENT '行為唯壹標識',
  `title` char(80) NOT NULL DEFAULT '' COMMENT '行為說明',
  `remark` char(140) NOT NULL DEFAULT '' COMMENT '行為描述',
  `rule` text NOT NULL COMMENT '行為規則',
  `log` text NOT NULL COMMENT '日誌規則',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '類型',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '狀態',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '修改時間',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='系統行為表';

-- ----------------------------
-- Records of sf_action
-- ----------------------------
INSERT INTO `sf_action` VALUES ('1', 'user_login', '用戶登錄', '積分+10，每天壹次', 'table:member|field:score|condition:uid={$self} AND status>-1|rule:score+10|cycle:24|max:1;', '[user|get_nickname]在[time|time_format]登錄了後臺', '1', '1', '1387181220');
INSERT INTO `sf_action` VALUES ('2', 'add_article', '發布文章', '積分+5，每天上限5次', 'table:member|field:score|condition:uid={$self}|rule:score+5|cycle:24|max:5', '', '2', '0', '1380173180');
INSERT INTO `sf_action` VALUES ('3', 'review', '評論', '評論積分+1，無限制', 'table:member|field:score|condition:uid={$self}|rule:score+1', '', '2', '1', '1383285646');
INSERT INTO `sf_action` VALUES ('4', 'add_document', '發表文檔', '積分+10，每天上限5次', 'table:member|field:score|condition:uid={$self}|rule:score+10|cycle:24|max:5', '[user|get_nickname]在[time|time_format]發表了壹篇文章。\r\n表[model]，記錄編號[record]。', '2', '0', '1386139726');
INSERT INTO `sf_action` VALUES ('5', 'add_document_topic', '發表討論', '積分+5，每天上限10次', 'table:member|field:score|condition:uid={$self}|rule:score+5|cycle:24|max:10', '', '2', '0', '1383285551');
INSERT INTO `sf_action` VALUES ('6', 'update_config', '更新配置', '新增或修改或刪除配置', '', '', '1', '1', '1383294988');
INSERT INTO `sf_action` VALUES ('7', 'update_model', '更新模型', '新增或修改模型', '', '', '1', '1', '1383295057');
INSERT INTO `sf_action` VALUES ('8', 'update_attribute', '更新屬性', '新增或更新或刪除屬性', '', '', '1', '1', '1383295963');
INSERT INTO `sf_action` VALUES ('9', 'update_channel', '更新導航', '新增或修改或刪除導航', '', '', '1', '1', '1383296301');
INSERT INTO `sf_action` VALUES ('10', 'update_menu', '更新菜單', '新增或修改或刪除菜單', '', '', '1', '1', '1383296392');
INSERT INTO `sf_action` VALUES ('11', 'update_category', '更新分類', '新增或修改或刪除分類', '', '', '1', '1', '1383296765');

-- ----------------------------
-- Table structure for sf_action_log
-- ----------------------------
DROP TABLE IF EXISTS `sf_action_log`;
CREATE TABLE `sf_action_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `action_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '行為id',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '執行用戶id',
  `action_ip` bigint(20) NOT NULL COMMENT '執行行為者ip',
  `model` varchar(50) NOT NULL DEFAULT '' COMMENT '觸發行為的表',
  `record_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '觸發行為的數據id',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '日誌備註',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '狀態',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '執行行為的時間',
  PRIMARY KEY (`id`),
  KEY `action_ip_ix` (`action_ip`),
  KEY `action_id_ix` (`action_id`),
  KEY `user_id_ix` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=632 DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='行為日誌表';

-- ----------------------------
-- Records of sf_action_log
-- ----------------------------
INSERT INTO `sf_action_log` VALUES ('325', '7', '1', '0', 'model', '9', '操作url：/sf/index.php?s=/Admin/Model/update.html', '1', '1430296560');
INSERT INTO `sf_action_log` VALUES ('326', '1', '1', '0', 'member', '1', 'admin在2015-04-29 16:43登錄了後臺', '1', '1430297036');
INSERT INTO `sf_action_log` VALUES ('327', '1', '1', '0', 'member', '1', 'admin在2015-04-29 16:49登錄了後臺', '1', '1430297351');
INSERT INTO `sf_action_log` VALUES ('328', '1', '1', '0', 'member', '1', 'admin在2015-05-03 09:44登錄了後臺', '1', '1430617475');
INSERT INTO `sf_action_log` VALUES ('329', '7', '1', '0', 'model', '2', '操作url：/sf/index.php?s=/Admin/Model/update.html', '1', '1430617860');
INSERT INTO `sf_action_log` VALUES ('330', '1', '1', '0', 'member', '1', 'admin在2015-05-03 09:55登錄了後臺', '1', '1430618156');
INSERT INTO `sf_action_log` VALUES ('331', '6', '1', '0', 'config', '20', '操作url：/sf/index.php?s=/Admin/Config/edit.html', '1', '1430618182');
INSERT INTO `sf_action_log` VALUES ('332', '7', '1', '0', 'model', '11', '操作url：/sf/index.php?s=/Admin/Model/update.html', '1', '1430623827');
INSERT INTO `sf_action_log` VALUES ('333', '1', '1', '0', 'member', '1', 'admin在2015-05-03 14:34登錄了後臺', '1', '1430634885');
INSERT INTO `sf_action_log` VALUES ('334', '1', '1', '0', 'member', '1', 'admin在2015-05-04 10:18登錄了後臺', '1', '1430705908');
INSERT INTO `sf_action_log` VALUES ('335', '10', '1', '0', 'Menu', '2', '操作url：/sf/index.php?s=/Admin/Menu/edit.html', '1', '1430706407');
INSERT INTO `sf_action_log` VALUES ('336', '10', '1', '0', 'Menu', '2', '操作url：/sf/index.php?s=/Admin/Menu/edit.html', '1', '1430706417');
INSERT INTO `sf_action_log` VALUES ('337', '1', '1', '0', 'member', '1', 'admin在2015-05-04 11:37登錄了後臺', '1', '1430710627');
INSERT INTO `sf_action_log` VALUES ('338', '1', '1', '0', 'member', '1', 'admin在2015-05-04 11:37登錄了後臺', '1', '1430710660');
INSERT INTO `sf_action_log` VALUES ('339', '11', '1', '0', 'category', '53', '操作url：/sf/index.php?s=/Admin/Category/edit.html', '1', '1430710795');
INSERT INTO `sf_action_log` VALUES ('340', '11', '1', '0', 'category', '53', '操作url：/sf/index.php?s=/Admin/Category/edit.html', '1', '1430710839');
INSERT INTO `sf_action_log` VALUES ('341', '1', '1', '0', 'member', '1', 'admin在2015-05-04 11:45登錄了後臺', '1', '1430711139');
INSERT INTO `sf_action_log` VALUES ('342', '1', '1', '0', 'member', '1', 'admin在2015-05-04 11:47登錄了後臺', '1', '1430711241');
INSERT INTO `sf_action_log` VALUES ('343', '7', '1', '0', 'model', '5', '操作url：/sf/index.php?s=/Admin/Model/update.html', '1', '1430724688');
INSERT INTO `sf_action_log` VALUES ('344', '1', '1', '0', 'member', '1', 'admin在2015-05-05 09:20登錄了後臺', '1', '1430788838');
INSERT INTO `sf_action_log` VALUES ('345', '7', '1', '0', 'model', '6', '操作url：/sf/index.php?s=/Admin/Model/update.html', '1', '1430788912');
INSERT INTO `sf_action_log` VALUES ('346', '1', '1', '0', 'member', '1', 'admin在2015-05-05 13:49登錄了後臺', '1', '1430804982');
INSERT INTO `sf_action_log` VALUES ('347', '8', '1', '0', 'attribute', '94', '操作url：/sf/index.php?s=/Admin/Attribute/update.html', '1', '1430805051');
INSERT INTO `sf_action_log` VALUES ('348', '8', '1', '0', 'attribute', '95', '操作url：/sf/index.php?s=/Admin/Attribute/update.html', '1', '1430805255');
INSERT INTO `sf_action_log` VALUES ('349', '8', '1', '0', 'attribute', '96', '操作url：/sf/index.php?s=/Admin/Attribute/update.html', '1', '1430805271');
INSERT INTO `sf_action_log` VALUES ('350', '8', '1', '0', 'attribute', '97', '操作url：/sf/index.php?s=/Admin/Attribute/update.html', '1', '1430805312');
INSERT INTO `sf_action_log` VALUES ('351', '8', '1', '0', 'attribute', '98', '操作url：/sf/index.php?s=/Admin/Attribute/update.html', '1', '1430805324');
INSERT INTO `sf_action_log` VALUES ('352', '8', '1', '0', 'attribute', '98', '操作url：/sf/index.php?s=/Admin/Attribute/update.html', '1', '1430805342');
INSERT INTO `sf_action_log` VALUES ('353', '8', '1', '0', 'attribute', '97', '操作url：/sf/index.php?s=/Admin/Attribute/update.html', '1', '1430805351');
INSERT INTO `sf_action_log` VALUES ('354', '8', '1', '0', 'attribute', '95', '操作url：/sf/index.php?s=/Admin/Attribute/update.html', '1', '1430805376');
INSERT INTO `sf_action_log` VALUES ('355', '8', '1', '0', 'attribute', '96', '操作url：/sf/index.php?s=/Admin/Attribute/update.html', '1', '1430805389');
INSERT INTO `sf_action_log` VALUES ('356', '10', '1', '0', 'Menu', '2', '操作url：/sf/index.php?s=/Admin/Menu/edit.html', '1', '1430805794');
INSERT INTO `sf_action_log` VALUES ('357', '1', '1', '0', 'member', '1', 'admin在2015-05-05 14:22登錄了後臺', '1', '1430806975');
INSERT INTO `sf_action_log` VALUES ('358', '11', '1', '0', 'category', '62', '操作url：/sf/index.php?s=/Admin/Category/edit.html', '1', '1430813696');
INSERT INTO `sf_action_log` VALUES ('359', '1', '1', '0', 'member', '1', 'admin在2015-05-05 17:38登錄了後臺', '1', '1430818730');
INSERT INTO `sf_action_log` VALUES ('360', '1', '1', '0', 'member', '1', 'admin在2015-05-06 09:34登錄了後臺', '1', '1430876058');
INSERT INTO `sf_action_log` VALUES ('361', '1', '1', '0', 'member', '1', 'admin在2015-05-06 13:32登錄了後臺', '1', '1430890343');
INSERT INTO `sf_action_log` VALUES ('362', '1', '1', '0', 'member', '1', 'admin在2015-05-06 16:19登錄了後臺', '1', '1430900367');
INSERT INTO `sf_action_log` VALUES ('363', '7', '1', '0', 'model', '11', '操作url：/sf/index.php?s=/Admin/Model/update.html', '1', '1430906690');
INSERT INTO `sf_action_log` VALUES ('364', '1', '1', '0', 'member', '1', 'admin在2015-05-07 09:25登錄了後臺', '1', '1430961923');
INSERT INTO `sf_action_log` VALUES ('365', '7', '1', '0', 'model', '11', '操作url：/sf/index.php?s=/Admin/Model/update.html', '1', '1430961938');
INSERT INTO `sf_action_log` VALUES ('366', '1', '1', '0', 'member', '1', 'admin在2015-05-07 09:44登錄了後臺', '1', '1430963083');
INSERT INTO `sf_action_log` VALUES ('367', '1', '1', '0', 'member', '1', 'admin在2015-05-07 09:49登錄了後臺', '1', '1430963343');
INSERT INTO `sf_action_log` VALUES ('368', '1', '1', '0', 'member', '1', 'admin在2015-05-07 09:49登錄了後臺', '1', '1430963351');
INSERT INTO `sf_action_log` VALUES ('369', '1', '1', '0', 'member', '1', 'admin在2015-05-07 09:49登錄了後臺', '1', '1430963399');
INSERT INTO `sf_action_log` VALUES ('370', '1', '1', '0', 'member', '1', 'admin在2015-05-07 10:19登錄了後臺', '1', '1430965148');
INSERT INTO `sf_action_log` VALUES ('371', '7', '1', '0', 'model', '6', '操作url：/sf/index.php?s=/Admin/Model/update.html', '1', '1430967529');
INSERT INTO `sf_action_log` VALUES ('372', '1', '1', '0', 'member', '1', 'admin在2015-05-07 14:20登錄了後臺', '1', '1430979610');
INSERT INTO `sf_action_log` VALUES ('373', '11', '1', '0', 'category', '42', '操作url：/sf/index.php?s=/Admin/Category/edit.html', '1', '1430986367');
INSERT INTO `sf_action_log` VALUES ('374', '11', '1', '0', 'category', '43', '操作url：/sf/index.php?s=/Admin/Category/edit.html', '1', '1430986371');
INSERT INTO `sf_action_log` VALUES ('375', '1', '1', '0', 'member', '1', 'admin在2015-05-07 16:16登錄了後臺', '1', '1430986597');
INSERT INTO `sf_action_log` VALUES ('376', '1', '2', '0', 'member', '2', 'user在2015-05-07 16:17登錄了後臺', '1', '1430986668');
INSERT INTO `sf_action_log` VALUES ('377', '1', '1', '0', 'member', '1', 'admin在2015-05-07 16:18登錄了後臺', '1', '1430986690');
INSERT INTO `sf_action_log` VALUES ('378', '1', '2', '0', 'member', '2', 'user在2015-05-07 16:18登錄了後臺', '1', '1430986729');
INSERT INTO `sf_action_log` VALUES ('379', '1', '1', '0', 'member', '1', 'admin在2015-05-07 16:19登錄了後臺', '1', '1430986741');
INSERT INTO `sf_action_log` VALUES ('380', '1', '2', '0', 'member', '2', 'user在2015-05-07 16:19登錄了後臺', '1', '1430986797');
INSERT INTO `sf_action_log` VALUES ('381', '1', '1', '0', 'member', '1', 'admin在2015-05-07 16:20登錄了後臺', '1', '1430986810');
INSERT INTO `sf_action_log` VALUES ('382', '1', '3', '0', 'member', '3', 'user2在2015-05-07 16:21登錄了後臺', '1', '1430986861');
INSERT INTO `sf_action_log` VALUES ('383', '1', '1', '0', 'member', '1', 'admin在2015-05-07 16:21登錄了後臺', '1', '1430986874');
INSERT INTO `sf_action_log` VALUES ('384', '1', '2', '0', 'member', '2', 'user在2015-05-07 16:22登錄了後臺', '1', '1430986960');
INSERT INTO `sf_action_log` VALUES ('385', '1', '1', '0', 'member', '1', 'admin在2015-05-07 16:23登錄了後臺', '1', '1430986981');
INSERT INTO `sf_action_log` VALUES ('386', '1', '3', '0', 'member', '3', 'user2在2015-05-07 17:21登錄了後臺', '1', '1430990493');
INSERT INTO `sf_action_log` VALUES ('387', '1', '1', '0', 'member', '1', 'admin在2015-05-07 17:21登錄了後臺', '1', '1430990510');
INSERT INTO `sf_action_log` VALUES ('388', '1', '3', '0', 'member', '3', 'user2在2015-05-07 17:22登錄了後臺', '1', '1430990556');
INSERT INTO `sf_action_log` VALUES ('389', '1', '3', '0', 'member', '3', 'user2在2015-05-07 17:35登錄了後臺', '1', '1430991349');
INSERT INTO `sf_action_log` VALUES ('390', '1', '1', '0', 'member', '1', 'admin在2015-05-07 17:36登錄了後臺', '1', '1430991362');
INSERT INTO `sf_action_log` VALUES ('391', '1', '1', '0', 'member', '1', 'admin在2015-05-07 18:03登錄了後臺', '1', '1430992991');
INSERT INTO `sf_action_log` VALUES ('392', '1', '1', '0', 'member', '1', 'admin在2015-05-08 09:25登錄了後臺', '1', '1431048355');
INSERT INTO `sf_action_log` VALUES ('393', '7', '1', '0', 'model', '11', '操作url：/sf/index.php?s=/Admin/Model/update.html', '1', '1431048438');
INSERT INTO `sf_action_log` VALUES ('394', '7', '1', '0', 'model', '12', '操作url：/sf/index.php?s=/Admin/Model/update.html', '1', '1431050352');
INSERT INTO `sf_action_log` VALUES ('395', '8', '1', '0', 'attribute', '99', '操作url：/sf/index.php?s=/Admin/Attribute/update.html', '1', '1431050375');
INSERT INTO `sf_action_log` VALUES ('396', '7', '1', '0', 'model', '12', '操作url：/sf/index.php?s=/Admin/Model/update.html', '1', '1431050396');
INSERT INTO `sf_action_log` VALUES ('397', '7', '1', '0', 'model', '12', '操作url：/sf/index.php?s=/Admin/Model/update.html', '1', '1431050445');
INSERT INTO `sf_action_log` VALUES ('398', '8', '1', '0', 'attribute', '12', '操作url：/sf/index.php?s=/Admin/Attribute/update.html', '1', '1431050464');
INSERT INTO `sf_action_log` VALUES ('399', '7', '1', '0', 'model', '12', '操作url：/sf/index.php?s=/Admin/Model/update.html', '1', '1431050479');
INSERT INTO `sf_action_log` VALUES ('400', '11', '1', '0', 'category', '65', '操作url：/sf/index.php?s=/Admin/Category/add.html', '1', '1431050514');
INSERT INTO `sf_action_log` VALUES ('401', '11', '1', '0', 'category', '65', '操作url：/sf/index.php?s=/Admin/Category/edit.html', '1', '1431050943');
INSERT INTO `sf_action_log` VALUES ('402', '7', '1', '0', 'model', '12', '操作url：/sf/index.php?s=/Admin/Model/update.html', '1', '1431051485');
INSERT INTO `sf_action_log` VALUES ('403', '11', '1', '0', 'category', '65', '操作url：/sf/index.php?s=/Admin/Category/edit.html', '1', '1431051595');
INSERT INTO `sf_action_log` VALUES ('404', '11', '1', '0', 'category', '65', '操作url：/sf/index.php?s=/Admin/Category/remove/id/65.html', '1', '1431051619');
INSERT INTO `sf_action_log` VALUES ('405', '11', '1', '0', 'category', '66', '操作url：/sf/index.php?s=/Admin/Category/add.html', '1', '1431051636');
INSERT INTO `sf_action_log` VALUES ('406', '11', '1', '0', 'category', '66', '操作url：/sf/index.php?s=/Admin/Category/edit.html', '1', '1431051717');
INSERT INTO `sf_action_log` VALUES ('407', '1', '1', '0', 'member', '1', 'admin在2015-05-08 10:26登錄了後臺', '1', '1431051970');
INSERT INTO `sf_action_log` VALUES ('408', '1', '1', '0', 'member', '1', 'admin在2015-05-08 10:28登錄了後臺', '1', '1431052118');
INSERT INTO `sf_action_log` VALUES ('409', '1', '1', '0', 'member', '1', 'admin在2015-05-08 10:28登錄了後臺', '1', '1431052124');
INSERT INTO `sf_action_log` VALUES ('410', '1', '1', '0', 'member', '1', 'admin在2015-05-08 10:30登錄了後臺', '1', '1431052202');
INSERT INTO `sf_action_log` VALUES ('411', '1', '1', '0', 'member', '1', 'admin在2015-05-08 10:33登錄了後臺', '1', '1431052383');
INSERT INTO `sf_action_log` VALUES ('412', '1', '1', '0', 'member', '1', 'admin在2015-05-08 11:40登錄了後臺', '1', '1431056442');
INSERT INTO `sf_action_log` VALUES ('413', '1', '1', '0', 'member', '1', 'admin在2015-05-08 11:40登錄了後臺', '1', '1431056449');
INSERT INTO `sf_action_log` VALUES ('414', '1', '1', '0', 'member', '1', 'admin在2015-05-08 11:44登錄了後臺', '1', '1431056666');
INSERT INTO `sf_action_log` VALUES ('415', '11', '1', '0', 'category', '1', '操作url：/sf/index.php?s=/Admin/Category/edit.html', '1', '1431063129');
INSERT INTO `sf_action_log` VALUES ('416', '11', '1', '0', 'category', '2', '操作url：/sf/index.php?s=/Admin/Category/edit.html', '1', '1431063138');
INSERT INTO `sf_action_log` VALUES ('417', '11', '1', '0', 'category', '44', '操作url：/sf/index.php?s=/Admin/Category/edit.html', '1', '1431063169');
INSERT INTO `sf_action_log` VALUES ('418', '11', '1', '0', 'category', '61', '操作url：/sf/index.php?s=/Admin/Category/remove/id/61.html', '1', '1431063209');
INSERT INTO `sf_action_log` VALUES ('419', '11', '1', '0', 'category', '54', '操作url：/sf/index.php?s=/Admin/Category/remove/id/54.html', '1', '1431063214');
INSERT INTO `sf_action_log` VALUES ('420', '1', '1', '0', 'member', '1', 'admin在2015-05-08 13:34登錄了後臺', '1', '1431063286');
INSERT INTO `sf_action_log` VALUES ('421', '1', '1', '0', 'member', '1', 'admin在2015-05-08 13:34登錄了後臺', '1', '1431063295');
INSERT INTO `sf_action_log` VALUES ('422', '1', '1', '0', 'member', '1', 'admin在2015-05-08 13:56登錄了後臺', '1', '1431064606');
INSERT INTO `sf_action_log` VALUES ('423', '8', '1', '0', 'attribute', '63', '操作url：/sf/index.php?s=/Admin/Attribute/remove/id/63.html', '1', '1431065334');
INSERT INTO `sf_action_log` VALUES ('424', '8', '1', '0', 'attribute', '64', '操作url：/sf/index.php?s=/Admin/Attribute/remove/id/64.html', '1', '1431065337');
INSERT INTO `sf_action_log` VALUES ('425', '8', '1', '0', 'attribute', '100', '操作url：/sf/index.php?s=/Admin/Attribute/update.html', '1', '1431065461');
INSERT INTO `sf_action_log` VALUES ('426', '8', '1', '0', 'attribute', '101', '操作url：/sf/index.php?s=/Admin/Attribute/update.html', '1', '1431065504');
INSERT INTO `sf_action_log` VALUES ('427', '8', '1', '0', 'attribute', '102', '操作url：/sf/index.php?s=/Admin/Attribute/update.html', '1', '1431065554');
INSERT INTO `sf_action_log` VALUES ('428', '7', '1', '0', 'model', '2', '操作url：/sf/index.php?s=/Admin/Model/update.html', '1', '1431065568');
INSERT INTO `sf_action_log` VALUES ('429', '7', '1', '0', 'model', '3', '操作url：/sf/index.php?s=/Admin/Model/update.html', '1', '1431065579');
INSERT INTO `sf_action_log` VALUES ('430', '7', '1', '0', 'model', '4', '操作url：/sf/index.php?s=/Admin/Model/update.html', '1', '1431065628');
INSERT INTO `sf_action_log` VALUES ('431', '7', '1', '0', 'model', '5', '操作url：/sf/index.php?s=/Admin/Model/update.html', '1', '1431065636');
INSERT INTO `sf_action_log` VALUES ('432', '7', '1', '0', 'model', '10', '操作url：/sf/index.php?s=/Admin/Model/update.html', '1', '1431065663');
INSERT INTO `sf_action_log` VALUES ('433', '7', '1', '0', 'model', '11', '操作url：/sf/index.php?s=/Admin/Model/update.html', '1', '1431065670');
INSERT INTO `sf_action_log` VALUES ('434', '7', '1', '0', 'model', '12', '操作url：/sf/index.php?s=/Admin/Model/update.html', '1', '1431065677');
INSERT INTO `sf_action_log` VALUES ('435', '11', '1', '0', 'category', '45', '操作url：/sf/index.php?s=/Admin/Category/remove/id/45.html', '1', '1431065725');
INSERT INTO `sf_action_log` VALUES ('436', '11', '1', '0', 'category', '46', '操作url：/sf/index.php?s=/Admin/Category/remove/id/46.html', '1', '1431065729');
INSERT INTO `sf_action_log` VALUES ('437', '11', '1', '0', 'category', '52', '操作url：/sf/index.php?s=/Admin/Category/remove/id/52.html', '1', '1431065732');
INSERT INTO `sf_action_log` VALUES ('438', '11', '1', '0', 'category', '44', '操作url：/sf/index.php?s=/Admin/Category/remove/id/44.html', '1', '1431065736');
INSERT INTO `sf_action_log` VALUES ('439', '11', '1', '0', 'category', '67', '操作url：/sf/index.php?s=/Admin/Category/add.html', '1', '1431065771');
INSERT INTO `sf_action_log` VALUES ('440', '7', '1', '0', 'model', '6', '操作url：/sf/index.php?s=/Admin/Model/update.html', '1', '1431065799');
INSERT INTO `sf_action_log` VALUES ('441', '7', '1', '0', 'model', '6', '操作url：/sf/index.php?s=/Admin/Model/update.html', '1', '1431065830');
INSERT INTO `sf_action_log` VALUES ('442', '1', '1', '0', 'member', '1', 'admin在2015-05-08 14:26登錄了後臺', '1', '1431066392');
INSERT INTO `sf_action_log` VALUES ('443', '7', '1', '0', 'model', '13', '操作url：/sf/index.php?s=/Admin/Model/update.html', '1', '1431072271');
INSERT INTO `sf_action_log` VALUES ('444', '8', '1', '0', 'attribute', '103', '操作url：/sf/index.php?s=/Admin/Attribute/update.html', '1', '1431072291');
INSERT INTO `sf_action_log` VALUES ('445', '7', '1', '0', 'model', '13', '操作url：/sf/index.php?s=/Admin/Model/update.html', '1', '1431072306');
INSERT INTO `sf_action_log` VALUES ('446', '7', '1', '0', 'model', '13', '操作url：/sf/index.php?s=/Admin/Model/update.html', '1', '1431072332');
INSERT INTO `sf_action_log` VALUES ('447', '11', '1', '0', 'category', '68', '操作url：/sf/index.php?s=/Admin/Category/add.html', '1', '1431072384');
INSERT INTO `sf_action_log` VALUES ('448', '11', '1', '0', 'category', '68', '操作url：/sf/index.php?s=/Admin/Category/edit.html', '1', '1431072413');
INSERT INTO `sf_action_log` VALUES ('449', '1', '1', '0', 'member', '1', 'admin在2015-05-08 16:14登錄了後臺', '1', '1431072868');
INSERT INTO `sf_action_log` VALUES ('450', '1', '1', '0', 'member', '1', 'admin在2015-05-09 09:18登錄了後臺', '1', '1431134294');
INSERT INTO `sf_action_log` VALUES ('451', '7', '1', '0', 'model', '13', '操作url：/sf/index.php?s=/Admin/Model/update.html', '1', '1431134356');
INSERT INTO `sf_action_log` VALUES ('452', '1', '1', '0', 'member', '1', 'admin在2015-05-09 09:45登錄了後臺', '1', '1431135917');
INSERT INTO `sf_action_log` VALUES ('453', '1', '1', '0', 'member', '1', 'admin在2015-05-09 10:56登錄了後臺', '1', '1431140215');
INSERT INTO `sf_action_log` VALUES ('454', '1', '1', '0', 'member', '1', 'admin在2015-05-09 17:00登錄了後臺', '1', '1431162026');
INSERT INTO `sf_action_log` VALUES ('455', '1', '1', '0', 'member', '1', 'admin在2015-05-12 10:43登錄了後臺', '1', '1431398620');
INSERT INTO `sf_action_log` VALUES ('456', '8', '1', '0', 'attribute', '24', '操作url：/sf/index.php?s=/Admin/Attribute/update.html', '1', '1431417542');
INSERT INTO `sf_action_log` VALUES ('457', '8', '1', '0', 'attribute', '24', '操作url：/sf/index.php?s=/Admin/Attribute/remove/id/24.html', '1', '1431417575');
INSERT INTO `sf_action_log` VALUES ('458', '8', '1', '0', 'attribute', '104', '操作url：/sf/index.php?s=/Admin/Attribute/update.html', '1', '1431417619');
INSERT INTO `sf_action_log` VALUES ('459', '7', '1', '0', 'model', '2', '操作url：/sf/index.php?s=/Admin/Model/update.html', '1', '1431417753');
INSERT INTO `sf_action_log` VALUES ('460', '1', '1', '0', 'member', '1', 'admin在2015-05-12 16:20登錄了後臺', '1', '1431418830');
INSERT INTO `sf_action_log` VALUES ('461', '1', '1', '0', 'member', '1', 'admin在2015-05-12 16:21登錄了後臺', '1', '1431418874');
INSERT INTO `sf_action_log` VALUES ('462', '1', '1', '0', 'member', '1', 'admin在2015-05-12 17:27登錄了後臺', '1', '1431422856');
INSERT INTO `sf_action_log` VALUES ('463', '1', '1', '0', 'member', '1', 'admin在2015-05-13 09:32登錄了後臺', '1', '1431480768');
INSERT INTO `sf_action_log` VALUES ('464', '1', '1', '0', 'member', '1', 'admin在2015-05-13 15:05登錄了後臺', '1', '1431500757');
INSERT INTO `sf_action_log` VALUES ('465', '1', '1', '0', 'member', '1', 'admin在2015-05-13 15:54登錄了後臺', '1', '1431503670');
INSERT INTO `sf_action_log` VALUES ('466', '1', '1', '0', 'member', '1', 'admin在2015-05-13 16:31登錄了後臺', '1', '1431505865');
INSERT INTO `sf_action_log` VALUES ('467', '8', '1', '0', 'attribute', '105', '操作url：/sf/index.php?s=/Admin/Attribute/update.html', '1', '1431507241');
INSERT INTO `sf_action_log` VALUES ('468', '1', '1', '0', 'member', '1', 'admin在2015-05-13 17:30登錄了後臺', '1', '1431509454');
INSERT INTO `sf_action_log` VALUES ('469', '1', '1', '0', 'member', '1', 'admin在2015-05-13 17:32登錄了後臺', '1', '1431509563');
INSERT INTO `sf_action_log` VALUES ('470', '1', '1', '0', 'member', '1', 'admin在2015-05-14 10:03登錄了後臺', '1', '1431568992');
INSERT INTO `sf_action_log` VALUES ('471', '1', '1', '0', 'member', '1', 'admin在2015-05-14 10:03登錄了後臺', '1', '1431569001');
INSERT INTO `sf_action_log` VALUES ('472', '1', '1', '0', 'member', '1', 'admin在2015-05-14 10:03登錄了後臺', '1', '1431569026');
INSERT INTO `sf_action_log` VALUES ('473', '1', '1', '0', 'member', '1', 'admin在2015-05-14 10:13登錄了後臺', '1', '1431569592');
INSERT INTO `sf_action_log` VALUES ('474', '1', '1', '0', 'member', '1', 'admin在2015-05-14 11:26登錄了後臺', '1', '1431573961');
INSERT INTO `sf_action_log` VALUES ('475', '1', '1', '0', 'member', '1', 'admin在2015-05-14 14:13登錄了後臺', '1', '1431584007');
INSERT INTO `sf_action_log` VALUES ('476', '1', '1', '0', 'member', '1', 'admin在2015-05-20 10:50登錄了後臺', '1', '1432090214');
INSERT INTO `sf_action_log` VALUES ('477', '7', '1', '0', 'model', '2', '操作url：/sf/Admin/Model/update.html', '1', '1432091589');
INSERT INTO `sf_action_log` VALUES ('478', '8', '1', '0', 'attribute', '104', '操作url：/sf/Admin/Attribute/remove/id/104.html', '1', '1432091631');
INSERT INTO `sf_action_log` VALUES ('479', '7', '1', '0', 'model', '2', '操作url：/sf/Admin/Model/update.html', '1', '1432091700');
INSERT INTO `sf_action_log` VALUES ('480', '7', '1', '0', 'model', '2', '操作url：/sf/Admin/Model/update.html', '1', '1432091836');
INSERT INTO `sf_action_log` VALUES ('481', '8', '1', '0', 'attribute', '25', '操作url：/sf/Admin/Attribute/remove/id/25.html', '1', '1432091889');
INSERT INTO `sf_action_log` VALUES ('482', '8', '1', '0', 'attribute', '26', '操作url：/sf/Admin/Attribute/remove/id/26.html', '1', '1432091893');
INSERT INTO `sf_action_log` VALUES ('483', '8', '1', '0', 'attribute', '23', '操作url：/sf/Admin/Attribute/remove/id/23.html', '1', '1432091896');
INSERT INTO `sf_action_log` VALUES ('484', '7', '1', '0', 'model', '2', '操作url：/sf/Admin/Model/update.html', '1', '1432091911');
INSERT INTO `sf_action_log` VALUES ('485', '7', '1', '0', 'model', '2', '操作url：/sf/Admin/Model/update.html', '1', '1432091931');
INSERT INTO `sf_action_log` VALUES ('486', '7', '1', '0', 'model', '1', '操作url：/sf/Admin/Model/update.html', '1', '1432106155');
INSERT INTO `sf_action_log` VALUES ('487', '8', '1', '0', 'attribute', '106', '操作url：/sf/Admin/Attribute/update.html', '1', '1432106765');
INSERT INTO `sf_action_log` VALUES ('488', '8', '1', '0', 'attribute', '107', '操作url：/sf/Admin/Attribute/update.html', '1', '1432106788');
INSERT INTO `sf_action_log` VALUES ('489', '1', '1', '0', 'member', '1', 'admin在2015-05-20 15:33登錄了後臺', '1', '1432107214');
INSERT INTO `sf_action_log` VALUES ('490', '1', '1', '0', 'member', '1', 'admin在2015-05-20 15:34登錄了後臺', '1', '1432107245');
INSERT INTO `sf_action_log` VALUES ('491', '1', '1', '0', 'member', '1', 'admin在2015-05-20 15:34登錄了後臺', '1', '1432107268');
INSERT INTO `sf_action_log` VALUES ('492', '1', '1', '0', 'member', '1', 'admin在2015-05-20 15:34登錄了後臺', '1', '1432107289');
INSERT INTO `sf_action_log` VALUES ('493', '1', '1', '0', 'member', '1', 'admin在2015-05-20 16:17登錄了後臺', '1', '1432109861');
INSERT INTO `sf_action_log` VALUES ('494', '6', '1', '0', 'config', '49', '操作url：/sf/Admin/Config/edit.html', '1', '1432110295');
INSERT INTO `sf_action_log` VALUES ('495', '1', '1', '0', 'member', '1', 'admin在2015-05-22 14:10登錄了後臺', '1', '1432275043');
INSERT INTO `sf_action_log` VALUES ('496', '1', '1', '0', 'member', '1', 'admin在2015-05-22 14:54登錄了後臺', '1', '1432277648');
INSERT INTO `sf_action_log` VALUES ('497', '1', '1', '0', 'member', '1', 'admin在2015-05-27 16:29登錄了後臺', '1', '1432715399');
INSERT INTO `sf_action_log` VALUES ('498', '1', '2', '0', 'member', '2', 'user在2015-05-27 17:29登錄了後臺', '1', '1432718978');
INSERT INTO `sf_action_log` VALUES ('499', '1', '1', '0', 'member', '1', 'admin在2015-05-27 17:30登錄了後臺', '1', '1432719002');
INSERT INTO `sf_action_log` VALUES ('500', '1', '2', '0', 'member', '2', 'user在2015-05-27 17:31登錄了後臺', '1', '1432719088');
INSERT INTO `sf_action_log` VALUES ('501', '1', '1', '0', 'member', '1', 'admin在2015-05-27 17:34登錄了後臺', '1', '1432719253');
INSERT INTO `sf_action_log` VALUES ('502', '1', '2', '0', 'member', '2', 'user在2015-05-27 17:38登錄了後臺', '1', '1432719535');
INSERT INTO `sf_action_log` VALUES ('503', '1', '2', '0', 'member', '2', 'user在2015-05-27 17:40登錄了後臺', '1', '1432719605');
INSERT INTO `sf_action_log` VALUES ('504', '1', '2', '0', 'member', '2', 'user在2015-05-27 17:40登錄了後臺', '1', '1432719629');
INSERT INTO `sf_action_log` VALUES ('505', '1', '1', '0', 'member', '1', 'admin在2015-05-27 17:41登錄了後臺', '1', '1432719673');
INSERT INTO `sf_action_log` VALUES ('506', '1', '2', '0', 'member', '2', 'user在2015-05-27 17:41登錄了後臺', '1', '1432719686');
INSERT INTO `sf_action_log` VALUES ('507', '1', '1', '0', 'member', '1', 'admin在2015-05-27 17:41登錄了後臺', '1', '1432719707');
INSERT INTO `sf_action_log` VALUES ('508', '1', '2', '0', 'member', '2', 'user在2015-05-27 17:42登錄了後臺', '1', '1432719746');
INSERT INTO `sf_action_log` VALUES ('509', '1', '1', '0', 'member', '1', 'admin在2015-05-27 17:43登錄了後臺', '1', '1432719790');
INSERT INTO `sf_action_log` VALUES ('510', '1', '1', '0', 'member', '1', 'admin在2015-06-01 10:33登錄了後臺', '1', '1433126036');
INSERT INTO `sf_action_log` VALUES ('511', '1', '1', '0', 'member', '1', 'admin在2015-06-23 15:36登錄了後臺', '1', '1435044965');
INSERT INTO `sf_action_log` VALUES ('512', '7', '1', '0', 'model', '14', '操作url：/sf/Admin/Model/update.html', '1', '1435045077');
INSERT INTO `sf_action_log` VALUES ('513', '8', '1', '0', 'attribute', '108', '操作url：/sf/Admin/Attribute/update.html', '1', '1435045160');
INSERT INTO `sf_action_log` VALUES ('514', '7', '1', '0', 'model', '14', '操作url：/sf/Admin/Model/update.html', '1', '1435045181');
INSERT INTO `sf_action_log` VALUES ('515', '7', '1', '0', 'model', '14', '操作url：/sf/Admin/Model/update.html', '1', '1435045187');
INSERT INTO `sf_action_log` VALUES ('516', '7', '1', '0', 'model', '14', '操作url：/sf/Admin/Model/update.html', '1', '1435045210');
INSERT INTO `sf_action_log` VALUES ('517', '11', '1', '0', 'category', '69', '操作url：/sf/Admin/Category/add.html', '1', '1435045230');
INSERT INTO `sf_action_log` VALUES ('518', '11', '1', '0', 'category', '69', '操作url：/sf/Admin/Category/remove/id/69.html', '1', '1435045526');
INSERT INTO `sf_action_log` VALUES ('519', '11', '1', '0', 'category', '70', '操作url：/sf/Admin/Category/add.html', '1', '1435045538');
INSERT INTO `sf_action_log` VALUES ('520', '1', '1', '1947419216', 'member', '1', 'admin在2015-06-24 15:30登錄了後臺', '1', '1435131026');
INSERT INTO `sf_action_log` VALUES ('521', '1', '1', '1947419216', 'member', '1', 'admin在2015-06-24 15:50登錄了後臺', '1', '1435132240');
INSERT INTO `sf_action_log` VALUES ('522', '1', '1', '3073441505', 'member', '1', 'admin在2015-06-29 14:51登錄了後臺', '1', '1435560714');
INSERT INTO `sf_action_log` VALUES ('523', '1', '1', '3073441505', 'member', '1', 'admin在2015-06-29 14:53登錄了後臺', '1', '1435560814');
INSERT INTO `sf_action_log` VALUES ('524', '1', '1', '3073441505', 'member', '1', 'admin在2015-06-29 16:26登錄了後臺', '1', '1435566387');
INSERT INTO `sf_action_log` VALUES ('525', '1', '1', '3073441505', 'member', '1', 'admin在2015-06-29 17:06登錄了後臺', '1', '1435568784');
INSERT INTO `sf_action_log` VALUES ('526', '1', '1', '2053435365', 'member', '1', 'admin在2015-06-29 17:19登錄了後臺', '1', '1435569594');
INSERT INTO `sf_action_log` VALUES ('527', '1', '1', '3073441505', 'member', '1', 'admin在2015-06-29 18:09登錄了後臺', '1', '1435572557');
INSERT INTO `sf_action_log` VALUES ('528', '1', '1', '1947418629', 'member', '1', 'admin在2015-06-30 14:19登錄了後臺', '1', '1435645184');
INSERT INTO `sf_action_log` VALUES ('529', '1', '1', '242992429', 'member', '1', 'admin在2015-07-01 16:57登錄了後臺', '1', '1435741044');
INSERT INTO `sf_action_log` VALUES ('530', '1', '1', '242993482', 'member', '1', 'admin在2015-07-06 15:41登錄了後臺', '1', '1436168467');
INSERT INTO `sf_action_log` VALUES ('531', '1', '1', '460161144', 'member', '1', 'admin在2015-07-06 15:41登錄了後臺', '1', '1436168480');
INSERT INTO `sf_action_log` VALUES ('532', '1', '1', '460161144', 'member', '1', 'admin在2015-07-06 15:46登錄了後臺', '1', '1436168774');
INSERT INTO `sf_action_log` VALUES ('533', '1', '1', '460161144', 'member', '1', 'admin在2015-07-06 16:03登錄了後臺', '1', '1436169806');
INSERT INTO `sf_action_log` VALUES ('534', '1', '1', '1900867162', 'member', '1', 'admin在2015-07-07 10:52登錄了後臺', '1', '1436237545');
INSERT INTO `sf_action_log` VALUES ('535', '1', '1', '460161144', 'member', '1', 'admin在2015-07-07 12:50登錄了後臺', '1', '1436244659');
INSERT INTO `sf_action_log` VALUES ('536', '1', '1', '460161144', 'member', '1', 'admin在2015-07-08 18:10登錄了後臺', '1', '1436350242');
INSERT INTO `sf_action_log` VALUES ('537', '1', '1', '460161144', 'member', '1', 'admin在2015-07-14 14:29登錄了後臺', '1', '1436855348');
INSERT INTO `sf_action_log` VALUES ('538', '1', '1', '460174487', 'member', '1', 'admin在2015-07-15 11:09登錄了後臺', '1', '1436929766');
INSERT INTO `sf_action_log` VALUES ('539', '1', '4', '460174487', 'member', '4', '123在2015-07-15 11:22登錄了後臺', '1', '1436930553');
INSERT INTO `sf_action_log` VALUES ('540', '1', '1', '460174487', 'member', '1', 'admin在2015-07-15 11:23登錄了後臺', '1', '1436930599');
INSERT INTO `sf_action_log` VALUES ('541', '1', '1', '3400479377', 'member', '1', 'admin在2015-07-15 12:03登錄了後臺', '1', '1436933013');
INSERT INTO `sf_action_log` VALUES ('542', '1', '1', '3400479389', 'member', '1', 'admin在2015-07-15 12:15登錄了後臺', '1', '1436933701');
INSERT INTO `sf_action_log` VALUES ('543', '1', '4', '3400479379', 'member', '4', '123在2015-07-15 12:22登錄了後臺', '1', '1436934171');
INSERT INTO `sf_action_log` VALUES ('544', '1', '1', '3400479381', 'member', '1', 'admin在2015-07-15 12:24登錄了後臺', '1', '1436934252');
INSERT INTO `sf_action_log` VALUES ('545', '1', '1', '3400479377', 'member', '1', 'admin在2015-07-15 12:29登錄了後臺', '1', '1436934569');
INSERT INTO `sf_action_log` VALUES ('546', '1', '1', '1947419400', 'member', '1', 'admin在2015-07-15 14:42登錄了後臺', '1', '1436942552');
INSERT INTO `sf_action_log` VALUES ('547', '1', '1', '3073741281', 'member', '1', 'admin在2015-07-31 11:09登錄了後臺', '1', '1438312177');
INSERT INTO `sf_action_log` VALUES ('548', '1', '1', '3073741281', 'member', '1', 'admin在2015-07-31 11:24登錄了後臺', '1', '1438313069');
INSERT INTO `sf_action_log` VALUES ('549', '1', '5', '3073741281', 'member', '5', 'bing在2015-07-31 12:21登錄了後臺', '1', '1438316470');
INSERT INTO `sf_action_log` VALUES ('550', '1', '1', '3073741281', 'member', '1', 'admin在2015-07-31 12:22登錄了後臺', '1', '1438316554');
INSERT INTO `sf_action_log` VALUES ('551', '1', '1', '3073741281', 'member', '1', 'admin在2015-07-31 12:36登錄了後臺', '1', '1438317385');
INSERT INTO `sf_action_log` VALUES ('552', '1', '5', '3073741281', 'member', '5', 'bing在2015-07-31 13:02登錄了後臺', '1', '1438318926');
INSERT INTO `sf_action_log` VALUES ('553', '1', '5', '3073741281', 'member', '5', 'bing在2015-07-31 13:02登錄了後臺', '1', '1438318965');
INSERT INTO `sf_action_log` VALUES ('554', '1', '1', '3073741281', 'member', '1', 'admin在2015-07-31 13:03登錄了後臺', '1', '1438319031');
INSERT INTO `sf_action_log` VALUES ('555', '1', '1', '3073741281', 'member', '1', 'admin在2015-07-31 16:46登錄了後臺', '1', '1438332410');
INSERT INTO `sf_action_log` VALUES ('556', '1', '1', '1900866638', 'member', '1', 'admin在2015-08-03 13:49登錄了後臺', '1', '1438580973');
INSERT INTO `sf_action_log` VALUES ('557', '1', '1', '460161998', 'member', '1', 'admin在2015-08-03 15:38登錄了後臺', '1', '1438587487');
INSERT INTO `sf_action_log` VALUES ('558', '1', '1', '2130706433', 'member', '1', 'admin在2015-08-03 17:09登錄了後臺', '1', '1438592967');
INSERT INTO `sf_action_log` VALUES ('559', '8', '1', '2130706433', 'attribute', '109', '操作url：/sf/Admin/Attribute/update.html', '1', '1438593592');
INSERT INTO `sf_action_log` VALUES ('560', '7', '1', '2130706433', 'model', '14', '操作url：/sf/Admin/Model/update.html', '1', '1438593600');
INSERT INTO `sf_action_log` VALUES ('561', '7', '1', '2130706433', 'model', '13', '操作url：/sf/Admin/Model/update.html', '1', '1438593606');
INSERT INTO `sf_action_log` VALUES ('562', '7', '1', '2130706433', 'model', '13', '操作url：/sf/Admin/Model/update.html', '1', '1438593633');
INSERT INTO `sf_action_log` VALUES ('563', '7', '1', '2130706433', 'model', '12', '操作url：/sf/Admin/Model/update.html', '1', '1438593640');
INSERT INTO `sf_action_log` VALUES ('564', '7', '1', '2130706433', 'model', '11', '操作url：/sf/Admin/Model/update.html', '1', '1438593651');
INSERT INTO `sf_action_log` VALUES ('565', '7', '1', '2130706433', 'model', '10', '操作url：/sf/Admin/Model/update.html', '1', '1438593665');
INSERT INTO `sf_action_log` VALUES ('566', '7', '1', '2130706433', 'model', '9', '操作url：/sf/Admin/Model/update.html', '1', '1438593672');
INSERT INTO `sf_action_log` VALUES ('567', '7', '1', '2130706433', 'model', '6', '操作url：/sf/Admin/Model/update.html', '1', '1438593679');
INSERT INTO `sf_action_log` VALUES ('568', '7', '1', '2130706433', 'model', '4', '操作url：/sf/Admin/Model/update.html', '1', '1438593708');
INSERT INTO `sf_action_log` VALUES ('569', '7', '1', '2130706433', 'model', '5', '操作url：/sf/Admin/Model/update.html', '1', '1438593718');
INSERT INTO `sf_action_log` VALUES ('570', '7', '1', '2130706433', 'model', '3', '操作url：/sf/Admin/Model/update.html', '1', '1438593740');
INSERT INTO `sf_action_log` VALUES ('571', '7', '1', '2130706433', 'model', '2', '操作url：/sf/Admin/Model/update.html', '1', '1438593750');
INSERT INTO `sf_action_log` VALUES ('572', '7', '1', '2130706433', 'model', '15', '操作url：/sf/Admin/Model/update.html', '1', '1438595051');
INSERT INTO `sf_action_log` VALUES ('573', '8', '1', '2130706433', 'attribute', '110', '操作url：/sf/Admin/Attribute/update.html', '1', '1438595095');
INSERT INTO `sf_action_log` VALUES ('574', '7', '1', '2130706433', 'model', '15', '操作url：/sf/Admin/Model/update.html', '1', '1438595131');
INSERT INTO `sf_action_log` VALUES ('575', '7', '1', '2130706433', 'model', '15', '操作url：/sf/Admin/Model/update.html', '1', '1438595167');
INSERT INTO `sf_action_log` VALUES ('576', '7', '1', '2130706433', 'model', '11', '操作url：/sf/Admin/Model/update.html', '1', '1438595214');
INSERT INTO `sf_action_log` VALUES ('577', '11', '1', '2130706433', 'category', '71', '操作url：/sf/Admin/Category/add.html', '1', '1438595234');
INSERT INTO `sf_action_log` VALUES ('578', '8', '1', '2130706433', 'attribute', '110', '操作url：/sf/Admin/Attribute/update.html', '1', '1438595252');
INSERT INTO `sf_action_log` VALUES ('579', '8', '1', '2130706433', 'attribute', '111', '操作url：/sf/Admin/Attribute/update.html', '1', '1438596209');
INSERT INTO `sf_action_log` VALUES ('580', '8', '1', '2130706433', 'attribute', '112', '操作url：/sf/Admin/Attribute/update.html', '1', '1438596226');
INSERT INTO `sf_action_log` VALUES ('581', '8', '1', '2130706433', 'attribute', '113', '操作url：/sf/Admin/Attribute/update.html', '1', '1438596241');
INSERT INTO `sf_action_log` VALUES ('582', '8', '1', '2130706433', 'attribute', '114', '操作url：/sf/Admin/Attribute/update.html', '1', '1438596263');
INSERT INTO `sf_action_log` VALUES ('583', '7', '1', '2130706433', 'model', '15', '操作url：/sf/Admin/Model/update.html', '1', '1438596301');
INSERT INTO `sf_action_log` VALUES ('584', '8', '1', '2130706433', 'attribute', '110', '操作url：/sf/Admin/Attribute/update.html', '1', '1438596339');
INSERT INTO `sf_action_log` VALUES ('585', '8', '1', '2130706433', 'attribute', '110', '操作url：/sf/Admin/Attribute/update.html', '1', '1438596507');
INSERT INTO `sf_action_log` VALUES ('586', '8', '1', '2130706433', 'attribute', '110', '操作url：/sf/Admin/Attribute/remove/id/110.html', '1', '1438597202');
INSERT INTO `sf_action_log` VALUES ('587', '7', '1', '2130706433', 'model', '15', '操作url：/sf/Admin/Model/update.html', '1', '1438597211');
INSERT INTO `sf_action_log` VALUES ('588', '1', '1', '2130706433', 'member', '1', 'admin在2015-08-04 13:59登錄了後臺', '1', '1438667972');
INSERT INTO `sf_action_log` VALUES ('589', '1', '1', '2130706433', 'member', '1', 'admin在2015-08-04 14:22登錄了後臺', '1', '1438669354');
INSERT INTO `sf_action_log` VALUES ('590', '1', '6', '2130706433', 'member', '6', 'admin2在2015-08-04 15:00登錄了後臺', '1', '1438671600');
INSERT INTO `sf_action_log` VALUES ('591', '1', '1', '2130706433', 'member', '1', 'admin在2015-08-04 15:01登錄了後臺', '1', '1438671660');
INSERT INTO `sf_action_log` VALUES ('592', '11', '1', '2130706433', 'category', '71', '操作url：/sf/Admin/Category/edit.html', '1', '1438671878');
INSERT INTO `sf_action_log` VALUES ('593', '11', '1', '2130706433', 'category', '62', '操作url：/sf/Admin/Category/edit.html', '1', '1438671895');
INSERT INTO `sf_action_log` VALUES ('594', '11', '1', '2130706433', 'category', '67', '操作url：/sf/Admin/Category/edit.html', '1', '1438671904');
INSERT INTO `sf_action_log` VALUES ('595', '11', '1', '2130706433', 'category', '70', '操作url：/sf/Admin/Category/edit.html', '1', '1438671914');
INSERT INTO `sf_action_log` VALUES ('596', '7', '1', '2130706433', 'model', '14', '操作url：/sf/Admin/Model/update.html', '1', '1438672086');
INSERT INTO `sf_action_log` VALUES ('597', '7', '1', '2130706433', 'model', '14', '操作url：/sf/Admin/Model/update.html', '1', '1438672209');
INSERT INTO `sf_action_log` VALUES ('598', '10', '1', '0', 'Menu', '122', '操作url：/Project/sf/Admin/Menu/add.html', '1', '1439525089');
INSERT INTO `sf_action_log` VALUES ('599', '11', '1', '0', 'category', '74', '操作url：/Project/sf/Admin/Category/remove/id/74.html', '1', '1439525359');
INSERT INTO `sf_action_log` VALUES ('600', '11', '1', '0', 'category', '75', '操作url：/Project/sf/Admin/Category/add.html', '1', '1439526285');
INSERT INTO `sf_action_log` VALUES ('601', '11', '1', '0', 'category', '75', '操作url：/Project/sf/Admin/Category/remove/id/75.html', '1', '1439539987');
INSERT INTO `sf_action_log` VALUES ('602', '11', '1', '0', 'category', '76', '操作url：/Project/sf/Admin/Category/add.html', '1', '1439546970');
INSERT INTO `sf_action_log` VALUES ('603', '1', '7', '0', 'member', '7', 'aaa在2015-08-14 18:34登錄了後臺', '1', '1439548458');
INSERT INTO `sf_action_log` VALUES ('604', '1', '1', '0', 'member', '1', 'admin在2015-08-14 18:34登錄了後臺', '1', '1439548497');
INSERT INTO `sf_action_log` VALUES ('605', '11', '1', '0', 'category', '76', '操作url：/Project/sf/Admin/Category/remove/id/76.html', '1', '1439548606');
INSERT INTO `sf_action_log` VALUES ('606', '11', '1', '0', 'category', '77', '操作url：/Project/sf/Admin/Category/add.html', '1', '1439548615');
INSERT INTO `sf_action_log` VALUES ('607', '11', '1', '0', 'category', '78', '操作url：/Project/sf/Admin/Category/add.html', '1', '1439548775');
INSERT INTO `sf_action_log` VALUES ('608', '1', '7', '0', 'member', '7', 'aaa在2015-08-14 18:40登錄了後臺', '1', '1439548800');
INSERT INTO `sf_action_log` VALUES ('609', '1', '1', '0', 'member', '1', 'admin在2015-08-14 18:41登錄了後臺', '1', '1439548916');
INSERT INTO `sf_action_log` VALUES ('610', '1', '1', '0', 'member', '1', 'admin在2015-08-17 09:59登錄了後臺', '1', '1439776775');
INSERT INTO `sf_action_log` VALUES ('611', '1', '5', '0', 'member', '5', 'bing在2015-08-17 10:17登錄了後臺', '1', '1439777871');
INSERT INTO `sf_action_log` VALUES ('612', '1', '1', '0', 'member', '1', 'admin在2015-08-17 10:21登錄了後臺', '1', '1439778076');
INSERT INTO `sf_action_log` VALUES ('613', '1', '5', '0', 'member', '5', 'bing在2015-08-17 10:22登錄了後臺', '1', '1439778163');
INSERT INTO `sf_action_log` VALUES ('614', '1', '7', '0', 'member', '7', 'aaa在2015-08-17 10:24登錄了後臺', '1', '1439778277');
INSERT INTO `sf_action_log` VALUES ('615', '1', '1', '0', 'member', '1', 'admin在2015-08-17 10:25登錄了後臺', '1', '1439778302');
INSERT INTO `sf_action_log` VALUES ('616', '1', '7', '0', 'member', '7', 'aaa在2015-08-17 10:26登錄了後臺', '1', '1439778411');
INSERT INTO `sf_action_log` VALUES ('617', '1', '1', '0', 'member', '1', 'admin在2015-08-17 10:27登錄了後臺', '1', '1439778458');
INSERT INTO `sf_action_log` VALUES ('618', '1', '7', '0', 'member', '7', 'aaa在2015-08-17 10:29登錄了後臺', '1', '1439778556');
INSERT INTO `sf_action_log` VALUES ('619', '1', '1', '0', 'member', '1', 'admin在2015-08-17 10:29登錄了後臺', '1', '1439778594');
INSERT INTO `sf_action_log` VALUES ('620', '11', '1', '0', 'category', '77', '操作url：/Project/sf/Admin/Category/edit.html', '1', '1439778892');
INSERT INTO `sf_action_log` VALUES ('621', '11', '1', '0', 'category', '77', '操作url：/Project/sf/Admin/Category/remove/id/77.html', '1', '1439780323');
INSERT INTO `sf_action_log` VALUES ('622', '11', '1', '0', 'category', '78', '操作url：/Project/sf/Admin/Category/remove/id/78.html', '1', '1439780329');
INSERT INTO `sf_action_log` VALUES ('623', '10', '1', '0', 'Menu', '123', '操作url：/Project/sf/Admin/Menu/add.html', '1', '1439780446');
INSERT INTO `sf_action_log` VALUES ('624', '10', '1', '0', 'Menu', '123', '操作url：/Project/sf/Admin/Menu/edit.html', '1', '1439781234');
INSERT INTO `sf_action_log` VALUES ('625', '10', '1', '0', 'Menu', '123', '操作url：/Project/sf/Admin/Menu/edit.html', '1', '1439781486');
INSERT INTO `sf_action_log` VALUES ('626', '10', '1', '0', 'Menu', '0', '操作url：/Project/sf/Admin/Menu/del/id/123.html', '1', '1439785572');
INSERT INTO `sf_action_log` VALUES ('627', '11', '1', '0', 'category', '79', '操作url：/Project/sf/Admin/Category/add.html', '1', '1439785847');
INSERT INTO `sf_action_log` VALUES ('628', '1', '1', '0', 'member', '1', 'admin在2015-08-17 12:31登錄了後臺', '1', '1439785907');
INSERT INTO `sf_action_log` VALUES ('629', '1', '7', '0', 'member', '7', 'aaa在2015-08-17 16:13登錄了後臺', '1', '1439799213');
INSERT INTO `sf_action_log` VALUES ('630', '1', '1', '0', 'member', '1', 'admin在2015-08-18 12:46登錄了後臺', '1', '1439873162');
INSERT INTO `sf_action_log` VALUES ('631', '1', '8', '0', 'member', '8', 'AAAA在2015-08-18 13:16登錄了後臺', '1', '1439875013');

-- ----------------------------
-- Table structure for sf_addons
-- ----------------------------
DROP TABLE IF EXISTS `sf_addons`;
CREATE TABLE `sf_addons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `name` varchar(40) NOT NULL COMMENT '插件名或標識',
  `title` varchar(20) NOT NULL DEFAULT '' COMMENT '中文名',
  `description` text COMMENT '插件描述',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '狀態',
  `config` text COMMENT '配置',
  `author` varchar(40) DEFAULT '' COMMENT '作者',
  `version` varchar(20) DEFAULT '' COMMENT '版本號',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '安裝時間',
  `has_adminlist` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否有後臺列表',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='插件表';

-- ----------------------------
-- Records of sf_addons
-- ----------------------------
INSERT INTO `sf_addons` VALUES ('2', 'SiteStat', '站點統計信息', '統計站點的基礎信息', '1', '{\"title\":\"\\u7cfb\\u7edf\\u4fe1\\u606f\",\"width\":\"1\",\"display\":\"1\",\"status\":\"0\"}', 'thinkphp', '0.1', '1379512015', '0');
INSERT INTO `sf_addons` VALUES ('3', 'DevTeam', '開發團隊信息', '開發團隊成員信息', '1', '{\"title\":\"OneThink\\u5f00\\u53d1\\u56e2\\u961f\",\"width\":\"2\",\"display\":\"1\"}', 'thinkphp', '0.1', '1379512022', '0');
INSERT INTO `sf_addons` VALUES ('4', 'SystemInfo', '系統環境信息', '用於顯示壹些服務器的信息', '1', '{\"title\":\"\\u7cfb\\u7edf\\u4fe1\\u606f\",\"width\":\"2\",\"display\":\"1\"}', 'thinkphp', '0.1', '1379512036', '0');
INSERT INTO `sf_addons` VALUES ('5', 'Editor', '前臺編輯器', '用於增強整站長文本的輸入和顯示', '1', '{\"editor_type\":\"2\",\"editor_wysiwyg\":\"1\",\"editor_height\":\"300px\",\"editor_resize_type\":\"1\"}', 'thinkphp', '0.1', '1379830910', '0');
INSERT INTO `sf_addons` VALUES ('6', 'Attachment', '附件', '用於文檔模型上傳附件', '1', 'null', 'thinkphp', '0.1', '1379842319', '1');
INSERT INTO `sf_addons` VALUES ('9', 'SocialComment', '通用社交化評論', '集成了各種社交化評論插件，輕松集成到系統中。', '1', '{\"comment_type\":\"1\",\"comment_uid_youyan\":\"\",\"comment_short_name_duoshuo\":\"\",\"comment_data_list_duoshuo\":\"\"}', 'thinkphp', '0.1', '1380273962', '0');
INSERT INTO `sf_addons` VALUES ('15', 'EditorForAdmin', '後臺編輯器', '用於增強整站長文本的輸入和顯示', '1', '{\"editor_type\":\"2\",\"editor_wysiwyg\":\"1\",\"editor_height\":\"500px\",\"editor_resize_type\":\"1\"}', 'thinkphp', '0.1', '1383126253', '0');
INSERT INTO `sf_addons` VALUES ('18', 'UploadImages', '多圖上傳', '多圖上傳', '1', 'null', '木梁大囧', '1.2', '1428721906', '0');

-- ----------------------------
-- Table structure for sf_attachment
-- ----------------------------
DROP TABLE IF EXISTS `sf_attachment`;
CREATE TABLE `sf_attachment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用戶ID',
  `title` char(30) NOT NULL DEFAULT '' COMMENT '附件顯示名',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '附件類型',
  `source` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '資源ID',
  `record_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '關聯記錄ID',
  `download` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '下載次數',
  `size` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '附件大小',
  `dir` int(12) unsigned NOT NULL DEFAULT '0' COMMENT '上級目錄ID',
  `sort` int(8) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '狀態',
  PRIMARY KEY (`id`),
  KEY `idx_record_status` (`record_id`,`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='附件表';

-- ----------------------------
-- Records of sf_attachment
-- ----------------------------

-- ----------------------------
-- Table structure for sf_attribute
-- ----------------------------
DROP TABLE IF EXISTS `sf_attribute`;
CREATE TABLE `sf_attribute` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '字段名',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '字段註釋',
  `field` varchar(100) NOT NULL DEFAULT '' COMMENT '字段定義',
  `type` varchar(20) NOT NULL DEFAULT '' COMMENT '數據類型',
  `value` varchar(100) NOT NULL DEFAULT '' COMMENT '字段默認值',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '備註',
  `is_show` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否顯示',
  `extra` varchar(255) NOT NULL DEFAULT '' COMMENT '參數',
  `model_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '模型id',
  `is_must` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否必填',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '狀態',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  `validate_rule` varchar(255) NOT NULL,
  `validate_time` tinyint(1) unsigned NOT NULL,
  `error_info` varchar(100) NOT NULL,
  `validate_type` varchar(25) NOT NULL,
  `auto_rule` varchar(100) NOT NULL,
  `auto_time` tinyint(1) unsigned NOT NULL,
  `auto_type` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `model_id` (`model_id`)
) ENGINE=MyISAM AUTO_INCREMENT=115 DEFAULT CHARSET=utf8 COMMENT='模型屬性表';

-- ----------------------------
-- Records of sf_attribute
-- ----------------------------
INSERT INTO `sf_attribute` VALUES ('1', 'uid', '用戶ID', 'int(10) unsigned NOT NULL ', 'num', '0', '', '0', '', '1', '0', '1', '1384508362', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `sf_attribute` VALUES ('2', 'name', '標識', 'char(40) NOT NULL ', 'string', '', '同壹根節點下標識不重復', '1', '', '1', '0', '1', '1383894743', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `sf_attribute` VALUES ('3', 'title', '標題', 'char(80) NOT NULL ', 'string', '', '文檔標題', '1', '', '1', '0', '1', '1383894778', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `sf_attribute` VALUES ('4', 'category_id', '所屬分類', 'int(10) unsigned NOT NULL ', 'string', '', '', '0', '', '1', '0', '1', '1384508336', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `sf_attribute` VALUES ('5', 'description', '描述', 'text NOT NULL ', 'textarea', '', '', '1', '', '1', '0', '1', '1430272757', '1383891233', '', '0', '', 'regex', '', '0', 'function');
INSERT INTO `sf_attribute` VALUES ('6', 'root', '根節點', 'int(10) unsigned NOT NULL ', 'num', '0', '該文檔的頂級文檔編號', '0', '', '1', '0', '1', '1384508323', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `sf_attribute` VALUES ('7', 'pid', '所屬ID', 'int(10) unsigned NOT NULL ', 'num', '0', '父文檔編號', '0', '', '1', '0', '1', '1384508543', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `sf_attribute` VALUES ('8', 'model_id', '內容模型ID', 'tinyint(3) unsigned NOT NULL ', 'num', '0', '該文檔所對應的模型', '0', '', '1', '0', '1', '1384508350', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `sf_attribute` VALUES ('9', 'type', '內容類型', 'tinyint(3) unsigned NOT NULL ', 'select', '2', '', '1', '1:目錄\r\n2:主題\r\n3:段落', '1', '0', '1', '1384511157', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `sf_attribute` VALUES ('10', 'position', '推薦位', 'smallint(5) unsigned NOT NULL ', 'checkbox', '0', '多個推薦則將其推薦值相加', '1', '1:列表推薦\r\n2:頻道頁推薦\r\n4:首頁推薦', '1', '0', '1', '1383895640', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `sf_attribute` VALUES ('11', 'link_id', '外鏈', 'int(10) unsigned NOT NULL ', 'num', '0', '0-非外鏈，大於0-外鏈ID,需要函數進行鏈接與編號的轉換', '1', '', '1', '0', '1', '1383895757', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `sf_attribute` VALUES ('12', 'cover_id', '封面', 'int(10) unsigned NOT NULL ', 'pictures', '0', '0-無封面，大於0-封面圖片ID，需要函數處理', '1', '', '1', '0', '1', '1431050464', '1383891233', '', '0', '', 'regex', '', '0', 'function');
INSERT INTO `sf_attribute` VALUES ('13', 'display', '可見性', 'tinyint(3) unsigned NOT NULL ', 'radio', '1', '', '1', '0:不可見\r\n1:所有人可見', '1', '0', '1', '1386662271', '1383891233', '', '0', '', 'regex', '', '0', 'function');
INSERT INTO `sf_attribute` VALUES ('14', 'deadline', '截至時間', 'int(10) unsigned NOT NULL ', 'datetime', '0', '0-永久有效', '1', '', '1', '0', '1', '1387163248', '1383891233', '', '0', '', 'regex', '', '0', 'function');
INSERT INTO `sf_attribute` VALUES ('15', 'attach', '附件數量', 'tinyint(3) unsigned NOT NULL ', 'num', '0', '', '0', '', '1', '0', '1', '1387260355', '1383891233', '', '0', '', 'regex', '', '0', 'function');
INSERT INTO `sf_attribute` VALUES ('16', 'view', '瀏覽量', 'int(10) unsigned NOT NULL ', 'num', '0', '', '1', '', '1', '0', '1', '1383895835', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `sf_attribute` VALUES ('17', 'comment', '評論數', 'int(10) unsigned NOT NULL ', 'num', '0', '', '1', '', '1', '0', '1', '1383895846', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `sf_attribute` VALUES ('18', 'extend', '擴展統計字段', 'int(10) unsigned NOT NULL ', 'num', '0', '根據需求自行使用', '0', '', '1', '0', '1', '1384508264', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `sf_attribute` VALUES ('19', 'level', '優先級', 'int(10) unsigned NOT NULL ', 'num', '0', '越高排序越靠前', '1', '', '1', '0', '1', '1383895894', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `sf_attribute` VALUES ('20', 'create_time', '創建時間', 'int(10) unsigned NOT NULL ', 'datetime', '0', '', '1', '', '1', '0', '1', '1383895903', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `sf_attribute` VALUES ('21', 'update_time', '更新時間', 'int(10) unsigned NOT NULL ', 'datetime', '0', '', '0', '', '1', '0', '1', '1384508277', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `sf_attribute` VALUES ('22', 'status', '數據狀態', 'tinyint(4) NOT NULL ', 'radio', '0', '', '0', '-1:刪除\r\n0:禁用\r\n1:正常\r\n2:待審核\r\n3:草稿', '1', '0', '1', '1384508496', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `sf_attribute` VALUES ('27', 'parse', '內容解析類型', 'tinyint(3) unsigned NOT NULL ', 'select', '0', '', '0', '0:html\r\n1:ubb\r\n2:markdown', '3', '0', '1', '1387260461', '1383891252', '', '0', '', 'regex', '', '0', 'function');
INSERT INTO `sf_attribute` VALUES ('28', 'content', '下載詳細描述', 'text NOT NULL ', 'editor', '', '', '1', '', '3', '0', '1', '1383896438', '1383891252', '', '0', '', '', '', '0', '');
INSERT INTO `sf_attribute` VALUES ('29', 'template', '詳情頁顯示模板', 'varchar(100) NOT NULL ', 'string', '', '', '1', '', '3', '0', '1', '1383896429', '1383891252', '', '0', '', '', '', '0', '');
INSERT INTO `sf_attribute` VALUES ('30', 'file_id', '文件ID', 'int(10) unsigned NOT NULL ', 'file', '0', '需要函數處理', '1', '', '3', '0', '1', '1383896415', '1383891252', '', '0', '', '', '', '0', '');
INSERT INTO `sf_attribute` VALUES ('31', 'download', '下載次數', 'int(10) unsigned NOT NULL ', 'num', '0', '', '1', '', '3', '0', '1', '1383896380', '1383891252', '', '0', '', '', '', '0', '');
INSERT INTO `sf_attribute` VALUES ('32', 'size', '文件大小', 'bigint(20) unsigned NOT NULL ', 'num', '0', '單位bit', '1', '', '3', '0', '1', '1383896371', '1383891252', '', '0', '', '', '', '0', '');
INSERT INTO `sf_attribute` VALUES ('33', 'ImageS', ' 上傳多圖', 'varchar(255) NOT NULL', 'pictures', '', '', '1', '', '2', '0', '1', '1428718841', '1428718841', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('35', 'pay', '首付', 'int(10) UNSIGNED NOT NULL', 'num', '', '萬起', '1', '', '4', '0', '1', '1428736974', '1428736974', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('36', 'size', '尺寸', 'varchar(255) NOT NULL', 'string', '', '平方尺', '1', '', '4', '0', '1', '1428737031', '1428737031', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('37', 'fee', '交易費', 'int(10) UNSIGNED NOT NULL', 'num', '', '萬', '1', '', '4', '0', '1', '1428737152', '1428737152', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('38', 'types', '房型', 'varchar(255) NOT NULL', 'string', '', '', '1', '', '4', '0', '1', '1428830602', '1428737356', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('39', 'attribute', '屬性', 'varchar(255) NOT NULL', 'string', '', '', '1', '', '4', '0', '1', '1428737768', '1428737768', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('40', 'tel', '電話', 'varchar(255) NOT NULL', 'string', '', '', '1', '', '4', '0', '1', '1428737862', '1428737862', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('41', 'photo', '實景圖', 'varchar(255) NOT NULL', 'pictures', '', '', '1', '', '4', '0', '1', '1428824185', '1428737890', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('43', 'tag', '標簽', 'varchar(255) NOT NULL', 'string', '', '每個標簽用逗號(,)分開', '1', '', '4', '0', '1', '1428739561', '1428739208', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('44', 'names', '物業名稱', 'text NOT NULL', 'string', '', '', '1', '', '4', '0', '1', '1428824351', '1428824103', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('45', 'photo2', '外景圖', 'varchar(255) NOT NULL', 'pictures', '', '', '1', '', '4', '0', '1', '1428824203', '1428824203', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('46', 'title2', '標題2', 'varchar(255) NOT NULL', 'string', '', '', '1', '', '4', '0', '1', '1428827328', '1428827328', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('47', 'hot', '是否熱賣', 'int(10) UNSIGNED NOT NULL', 'num', '0', '是填1，否填0', '1', '', '4', '0', '1', '1428827630', '1428827581', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('48', 'hots', '是否熱賣', 'int(10) UNSIGNED NOT NULL', 'num', '0', '填0否，填1是', '1', '', '1', '0', '1', '1428828494', '1428828494', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('52', 'edit', '作者', 'varchar(255) NOT NULL', 'string', '', '', '1', '', '2', '0', '1', '1429596670', '1429596670', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('53', 'come', '來源', 'varchar(255) NOT NULL', 'string', '', '', '1', '', '2', '0', '1', '1429596680', '1429596680', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('55', 'content', '詳細內容', 'text NOT NULL', 'editor', '', '', '1', '', '1', '0', '1', '1429669704', '1429669704', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('56', 'cid', '標識', 'int(10) UNSIGNED NOT NULL', 'num', '', '', '1', '', '5', '0', '1', '1429669946', '1429669946', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('57', 'name', '條件', 'varchar(255) NOT NULL', 'string', '', '', '1', '', '6', '0', '1', '1430292764', '1429758571', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('59', 'add', '所在地', 'varchar(255) NOT NULL', 'string', '', '', '1', '', '1', '0', '1', '1429841986', '1429841986', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('60', 'housestype', '物業類型', 'varchar(255) NOT NULL', 'string', '', '', '1', '', '1', '0', '1', '1429842063', '1429842063', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('61', 'sell', '銷售類型', 'varchar(255) NOT NULL', 'radio', '', '', '1', '出售\r\n出租', '1', '0', '1', '1429842295', '1429842123', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('62', 'houses_prices', '房屋價格', 'int(10) UNSIGNED NOT NULL', 'num', '', '(物業價格)', '1', '', '1', '0', '1', '1429842503', '1429842164', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('70', 'cc', 'cc', 'varchar(255) NOT NULL', 'string', '1', '', '1', '', '9', '0', '1', '1429860355', '1429860332', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('72', 'price', '價格', 'int(10) UNSIGNED NOT NULL', 'num', '1000', '元/壹平方米', '1', '', '1', '0', '1', '1429861751', '1429861751', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('73', 'street', '街道', 'varchar(255) NOT NULL', 'string', '', '', '1', '', '10', '0', '1', '1430200705', '1430200705', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('74', 'range', '區域', 'varchar(255) NOT NULL', 'string', '', '', '1', '', '10', '0', '1', '1430200731', '1430200731', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('75', 'types', '類型', 'varchar(255) NOT NULL', 'string', '', '', '1', '', '10', '0', '1', '1430203111', '1430200745', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('76', 'floor', '樓層', 'varchar(100) NOT NULL', 'string', '', '', '1', '', '10', '0', '1', '1430200914', '1430200914', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('77', 'house_type', '護型', 'varchar(255) NOT NULL', 'string', '', '', '1', '', '10', '0', '1', '1430200954', '1430200954', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('78', 'rantaltime', '租期', 'varchar(255) NOT NULL', 'string', '', '', '1', '', '10', '0', '1', '1430201000', '1430201000', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('79', 'statused', '現狀', 'varchar(255) NOT NULL', 'string', '', '', '1', '', '10', '0', '1', '1430209293', '1430201042', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('80', 'fee', '管理費', 'varchar(50) NOT NULL', 'string', '', '', '1', '', '10', '0', '1', '1430201236', '1430201236', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('81', 'address', '物業地址', 'varchar(255) NOT NULL', 'string', '', '', '1', '', '10', '0', '1', '1430208444', '1430201253', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('82', 'finish', '裝修程度', 'varchar(255) NOT NULL', 'string', '', '', '1', '', '10', '0', '1', '1430201343', '1430201343', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('83', 'device', '配套設備', 'varchar(255) NOT NULL', 'string', '', '', '1', '', '10', '0', '1', '1430201373', '1430201373', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('84', 'foregift', '訂金要求', 'varchar(255) NOT NULL', 'string', '', '', '1', '', '10', '0', '1', '1430201581', '1430201581', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('85', 'photo', '物業圖片', 'varchar(255) NOT NULL', 'pictures', '', '', '1', '', '10', '0', '1', '1430201629', '1430201629', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('86', 'wname', '委託人姓名', 'varchar(255) NOT NULL', 'string', '', '', '1', '', '10', '0', '1', '1430208531', '1430201658', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('87', 'tel', '委託人電話', 'varchar(100) NOT NULL', 'string', '', '', '1', '', '10', '0', '1', '1430201676', '1430201676', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('88', 'email', '委託人郵箱', 'varchar(255) NOT NULL', 'string', '', '', '1', '', '10', '0', '1', '1430201691', '1430201691', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('89', 'adds', '委託人地址', 'varchar(255) NOT NULL', 'string', '', '', '1', '', '10', '0', '1', '1430201709', '1430201709', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('90', 'area', '面積', 'varchar(255) NOT NULL', 'string', '', '平方米', '1', '', '10', '0', '1', '1430212517', '1430203874', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('91', 'name', '姓名', 'varchar(255) NOT NULL', 'string', '', '', '1', '', '11', '0', '1', '1430278117', '1430278117', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('92', 'tel', '電話', 'varchar(100) NOT NULL', 'string', '', '', '1', '', '11', '0', '1', '1430278138', '1430278138', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('93', 'email', '電子郵箱', 'varchar(255) NOT NULL', 'string', '', '', '1', '', '11', '0', '1', '1430278158', '1430278158', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('94', 'place', '地理位置', 'varchar(255) NOT NULL', 'string', '', '', '1', '', '4', '0', '1', '1430805051', '1430805051', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('95', 'shi', '室', 'varchar(50) NOT NULL', 'num', '', '', '1', '', '4', '0', '1', '1430805376', '1430805255', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('96', 'ting', '廳', 'varchar(50) NOT NULL', 'num', '', '', '1', '', '4', '0', '1', '1430805389', '1430805271', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('97', 'wei', '衛', 'varchar(50) NOT NULL', 'num', '', '', '1', '', '4', '0', '1', '1430805351', '1430805312', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('98', 'che', '車庫', 'varchar(50) NOT NULL', 'num', '', '', '1', '', '4', '0', '1', '1430805342', '1430805324', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('99', 'url', 'url', 'varchar(255) NOT NULL', 'string', '1', '', '1', '', '12', '0', '1', '1431050376', '1431050376', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('100', 'search_add', '所在地', 'varchar(255) NOT NULL', 'string', '', '每個條件用,號分開', '1', '', '1', '0', '1', '1431065461', '1431065461', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('101', 'search_type', '房屋類型', 'varchar(255) NOT NULL', 'string', '', '每個條件用,號分開', '1', '', '1', '0', '1', '1431065504', '1431065504', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('102', 'search_prices', '價格', 'varchar(255) NOT NULL', 'string', '', '每個條件用,號分開，價格範圍用-號分開', '1', '', '1', '0', '1', '1431065554', '1431065554', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('103', 'url', 'url', 'varchar(255) NOT NULL', 'string', '', '', '1', '', '13', '0', '1', '1431072291', '1431072291', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('105', 'P1', 'P1', 'int(10) UNSIGNED NOT NULL', 'picture', '', '', '1', '', '13', '0', '1', '1431507241', '1431507241', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('106', 'unit', '單位（貨幣）', 'varchar(255) NOT NULL', 'string', '', '', '1', '', '4', '0', '1', '1432106765', '1432106765', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('107', 'units', '單位（面積單位）', 'varchar(255) NOT NULL', 'string', '', '', '1', '', '4', '0', '1', '1432106788', '1432106788', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('108', 'bgpic', '背景圖', 'varchar(255) NOT NULL', 'pictures', '', '', '1', '', '14', '0', '1', '1435045160', '1435045160', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('109', 'read_status', '閱讀狀態', 'varchar(255) NOT NULL', 'string', '未讀', '', '1', '', '1', '0', '1', '1438593592', '1438593592', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('111', 'tel', '電話', 'varchar(255) NOT NULL', 'string', '', '', '1', '', '15', '0', '1', '1438596209', '1438596209', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('112', 'email', '電子郵箱', 'varchar(255) NOT NULL', 'string', '', '', '1', '', '15', '0', '1', '1438596226', '1438596226', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('113', 'adds', '地址', 'varchar(255) NOT NULL', 'string', '', '', '1', '', '15', '0', '1', '1438596241', '1438596241', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `sf_attribute` VALUES ('114', 'homepage', '主頁', 'varchar(255) NOT NULL', 'string', '', '', '1', '', '15', '0', '1', '1438596263', '1438596263', '', '3', '', 'regex', '', '3', 'function');

-- ----------------------------
-- Table structure for sf_auth_extend
-- ----------------------------
DROP TABLE IF EXISTS `sf_auth_extend`;
CREATE TABLE `sf_auth_extend` (
  `group_id` mediumint(10) unsigned NOT NULL COMMENT '用戶id',
  `extend_id` mediumint(8) unsigned NOT NULL COMMENT '擴展表中數據的id',
  `type` tinyint(1) unsigned NOT NULL COMMENT '擴展類型標識 1:欄目分類權限;2:模型權限',
  UNIQUE KEY `group_extend_type` (`group_id`,`extend_id`,`type`),
  KEY `uid` (`group_id`),
  KEY `group_id` (`extend_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用戶組與分類的對應關系表';

-- ----------------------------
-- Records of sf_auth_extend
-- ----------------------------
INSERT INTO `sf_auth_extend` VALUES ('1', '1', '1');
INSERT INTO `sf_auth_extend` VALUES ('1', '1', '2');
INSERT INTO `sf_auth_extend` VALUES ('1', '2', '1');
INSERT INTO `sf_auth_extend` VALUES ('1', '2', '2');
INSERT INTO `sf_auth_extend` VALUES ('1', '3', '2');
INSERT INTO `sf_auth_extend` VALUES ('1', '42', '1');
INSERT INTO `sf_auth_extend` VALUES ('1', '43', '1');
INSERT INTO `sf_auth_extend` VALUES ('1', '53', '1');
INSERT INTO `sf_auth_extend` VALUES ('1', '55', '1');
INSERT INTO `sf_auth_extend` VALUES ('1', '58', '1');
INSERT INTO `sf_auth_extend` VALUES ('1', '59', '1');
INSERT INTO `sf_auth_extend` VALUES ('1', '60', '1');
INSERT INTO `sf_auth_extend` VALUES ('1', '62', '1');
INSERT INTO `sf_auth_extend` VALUES ('1', '63', '1');
INSERT INTO `sf_auth_extend` VALUES ('1', '66', '1');
INSERT INTO `sf_auth_extend` VALUES ('1', '67', '1');
INSERT INTO `sf_auth_extend` VALUES ('1', '68', '1');
INSERT INTO `sf_auth_extend` VALUES ('3', '1', '1');
INSERT INTO `sf_auth_extend` VALUES ('3', '2', '1');
INSERT INTO `sf_auth_extend` VALUES ('3', '42', '1');
INSERT INTO `sf_auth_extend` VALUES ('3', '43', '1');
INSERT INTO `sf_auth_extend` VALUES ('3', '53', '1');
INSERT INTO `sf_auth_extend` VALUES ('3', '55', '1');
INSERT INTO `sf_auth_extend` VALUES ('3', '58', '1');
INSERT INTO `sf_auth_extend` VALUES ('3', '59', '1');
INSERT INTO `sf_auth_extend` VALUES ('3', '60', '1');
INSERT INTO `sf_auth_extend` VALUES ('3', '62', '1');
INSERT INTO `sf_auth_extend` VALUES ('3', '63', '1');
INSERT INTO `sf_auth_extend` VALUES ('3', '66', '1');
INSERT INTO `sf_auth_extend` VALUES ('3', '67', '1');
INSERT INTO `sf_auth_extend` VALUES ('3', '68', '1');
INSERT INTO `sf_auth_extend` VALUES ('3', '70', '1');
INSERT INTO `sf_auth_extend` VALUES ('3', '71', '1');
INSERT INTO `sf_auth_extend` VALUES ('4', '1', '1');
INSERT INTO `sf_auth_extend` VALUES ('4', '2', '1');
INSERT INTO `sf_auth_extend` VALUES ('4', '42', '1');
INSERT INTO `sf_auth_extend` VALUES ('4', '43', '1');
INSERT INTO `sf_auth_extend` VALUES ('4', '53', '1');
INSERT INTO `sf_auth_extend` VALUES ('4', '55', '1');
INSERT INTO `sf_auth_extend` VALUES ('4', '58', '1');
INSERT INTO `sf_auth_extend` VALUES ('4', '59', '1');
INSERT INTO `sf_auth_extend` VALUES ('4', '60', '1');
INSERT INTO `sf_auth_extend` VALUES ('4', '62', '1');
INSERT INTO `sf_auth_extend` VALUES ('4', '63', '1');
INSERT INTO `sf_auth_extend` VALUES ('4', '66', '1');
INSERT INTO `sf_auth_extend` VALUES ('4', '67', '1');
INSERT INTO `sf_auth_extend` VALUES ('4', '68', '1');
INSERT INTO `sf_auth_extend` VALUES ('4', '70', '1');
INSERT INTO `sf_auth_extend` VALUES ('4', '71', '1');
INSERT INTO `sf_auth_extend` VALUES ('4', '79', '1');
INSERT INTO `sf_auth_extend` VALUES ('5', '1', '1');
INSERT INTO `sf_auth_extend` VALUES ('5', '2', '1');
INSERT INTO `sf_auth_extend` VALUES ('5', '42', '1');
INSERT INTO `sf_auth_extend` VALUES ('5', '43', '1');
INSERT INTO `sf_auth_extend` VALUES ('5', '53', '1');
INSERT INTO `sf_auth_extend` VALUES ('5', '55', '1');
INSERT INTO `sf_auth_extend` VALUES ('5', '58', '1');
INSERT INTO `sf_auth_extend` VALUES ('5', '59', '1');
INSERT INTO `sf_auth_extend` VALUES ('5', '60', '1');
INSERT INTO `sf_auth_extend` VALUES ('5', '62', '1');
INSERT INTO `sf_auth_extend` VALUES ('5', '63', '1');
INSERT INTO `sf_auth_extend` VALUES ('5', '66', '1');
INSERT INTO `sf_auth_extend` VALUES ('5', '67', '1');
INSERT INTO `sf_auth_extend` VALUES ('5', '68', '1');
INSERT INTO `sf_auth_extend` VALUES ('5', '70', '1');
INSERT INTO `sf_auth_extend` VALUES ('5', '71', '1');
INSERT INTO `sf_auth_extend` VALUES ('5', '79', '1');

-- ----------------------------
-- Table structure for sf_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `sf_auth_group`;
CREATE TABLE `sf_auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '用戶組id,自增主鍵',
  `module` varchar(20) NOT NULL COMMENT '用戶組所屬模塊',
  `type` tinyint(4) NOT NULL COMMENT '組類型',
  `title` char(20) NOT NULL DEFAULT '' COMMENT '用戶組中文名稱',
  `description` varchar(80) NOT NULL DEFAULT '' COMMENT '描述信息',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '用戶組狀態：為1正常，為0禁用,-1為刪除',
  `rules` varchar(500) NOT NULL DEFAULT '' COMMENT '用戶組擁有的規則id，多個規則 , 隔開',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sf_auth_group
-- ----------------------------
INSERT INTO `sf_auth_group` VALUES ('1', 'admin', '1', '默認用戶組', '', '-1', '3,4,5,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,79,80,81,82,83,84,86,87,88,89,90,91,92,93,94,95,100,102,103,107,108,109,110,195,205,206,207,208,209,210,211,212,213,214,215,216,217');
INSERT INTO `sf_auth_group` VALUES ('2', 'admin', '1', '測試用戶', '測試用戶', '-1', '7,8,9,10,11,12,13,14,15,16,17,18,65,79,195,211,217');
INSERT INTO `sf_auth_group` VALUES ('3', 'admin', '1', '普通管理員', '', '-1', '3,4,5,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,79,80,81,82,83,84,86,87,88,89,90,91,92,93,94,95,100,102,103,107,108,109,110,195,205,206,207,208,209,210,211,212,213,214,215,216,217');
INSERT INTO `sf_auth_group` VALUES ('4', 'admin', '1', '管理员', '', '1', '3,4,5,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,79,80,81,82,83,84,86,87,88,89,90,91,92,93,94,95,100,102,103,107,108,109,110,195,205,206,207,208,209,210,211,212,213,214,215,216,217,218');
INSERT INTO `sf_auth_group` VALUES ('5', 'admin', '1', '客戶', '', '1', '3,7,8,9,10,11,12,13,14,15,16,17,18,26,79,88,108,109,211,217');

-- ----------------------------
-- Table structure for sf_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `sf_auth_group_access`;
CREATE TABLE `sf_auth_group_access` (
  `uid` int(10) unsigned NOT NULL COMMENT '用戶id',
  `group_id` mediumint(8) unsigned NOT NULL COMMENT '用戶組id',
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sf_auth_group_access
-- ----------------------------
INSERT INTO `sf_auth_group_access` VALUES ('2', '3');
INSERT INTO `sf_auth_group_access` VALUES ('2', '4');
INSERT INTO `sf_auth_group_access` VALUES ('3', '3');
INSERT INTO `sf_auth_group_access` VALUES ('3', '4');
INSERT INTO `sf_auth_group_access` VALUES ('4', '4');
INSERT INTO `sf_auth_group_access` VALUES ('5', '3');
INSERT INTO `sf_auth_group_access` VALUES ('5', '4');
INSERT INTO `sf_auth_group_access` VALUES ('6', '3');
INSERT INTO `sf_auth_group_access` VALUES ('6', '4');
INSERT INTO `sf_auth_group_access` VALUES ('7', '5');
INSERT INTO `sf_auth_group_access` VALUES ('8', '4');

-- ----------------------------
-- Table structure for sf_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `sf_auth_rule`;
CREATE TABLE `sf_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '規則id,自增主鍵',
  `module` varchar(20) NOT NULL COMMENT '規則所屬module',
  `type` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1-url;2-主菜單',
  `name` char(80) NOT NULL DEFAULT '' COMMENT '規則唯壹英文標識',
  `title` char(20) NOT NULL DEFAULT '' COMMENT '規則中文描述',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否有效(0:無效,1:有效)',
  `condition` varchar(300) NOT NULL DEFAULT '' COMMENT '規則附加條件',
  PRIMARY KEY (`id`),
  KEY `module` (`module`,`status`,`type`)
) ENGINE=MyISAM AUTO_INCREMENT=219 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sf_auth_rule
-- ----------------------------
INSERT INTO `sf_auth_rule` VALUES ('1', 'admin', '2', 'Admin/Index/index', '首頁', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('2', 'admin', '2', 'Admin/Article/mydocument', '網站管理', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('3', 'admin', '2', 'Admin/User/index', '用戶', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('4', 'admin', '2', 'Admin/Addons/index', '擴展', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('5', 'admin', '2', 'Admin/Config/group', '系統管理', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('7', 'admin', '1', 'Admin/article/add', '新增', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('8', 'admin', '1', 'Admin/article/edit', '編輯', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('9', 'admin', '1', 'Admin/article/setStatus', '改變狀態', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('10', 'admin', '1', 'Admin/article/update', '保存', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('11', 'admin', '1', 'Admin/article/autoSave', '保存草稿', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('12', 'admin', '1', 'Admin/article/move', '移動', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('13', 'admin', '1', 'Admin/article/copy', '復制', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('14', 'admin', '1', 'Admin/article/paste', '粘貼', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('15', 'admin', '1', 'Admin/article/permit', '還原', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('16', 'admin', '1', 'Admin/article/clear', '清空', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('17', 'admin', '1', 'Admin/article/index', '文檔列表', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('18', 'admin', '1', 'Admin/article/recycle', '回收站', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('19', 'admin', '1', 'Admin/User/addaction', '新增用戶行為', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('20', 'admin', '1', 'Admin/User/editaction', '編輯用戶行為', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('21', 'admin', '1', 'Admin/User/saveAction', '保存用戶行為', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('22', 'admin', '1', 'Admin/User/setStatus', '變更行為狀態', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('23', 'admin', '1', 'Admin/User/changeStatus?method=forbidUser', '禁用會員', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('24', 'admin', '1', 'Admin/User/changeStatus?method=resumeUser', '啟用會員', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('25', 'admin', '1', 'Admin/User/changeStatus?method=deleteUser', '刪除會員', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('26', 'admin', '1', 'Admin/User/index', '用戶信息', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('27', 'admin', '1', 'Admin/User/action', '用戶行為', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('28', 'admin', '1', 'Admin/AuthManager/changeStatus?method=deleteGroup', '刪除', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('29', 'admin', '1', 'Admin/AuthManager/changeStatus?method=forbidGroup', '禁用', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('30', 'admin', '1', 'Admin/AuthManager/changeStatus?method=resumeGroup', '恢復', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('31', 'admin', '1', 'Admin/AuthManager/createGroup', '新增', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('32', 'admin', '1', 'Admin/AuthManager/editGroup', '編輯', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('33', 'admin', '1', 'Admin/AuthManager/writeGroup', '保存用戶組', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('34', 'admin', '1', 'Admin/AuthManager/group', '授權', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('35', 'admin', '1', 'Admin/AuthManager/access', '訪問授權', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('36', 'admin', '1', 'Admin/AuthManager/user', '成員授權', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('37', 'admin', '1', 'Admin/AuthManager/removeFromGroup', '解除授權', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('38', 'admin', '1', 'Admin/AuthManager/addToGroup', '保存成員授權', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('39', 'admin', '1', 'Admin/AuthManager/category', '分類授權', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('40', 'admin', '1', 'Admin/AuthManager/addToCategory', '保存分類授權', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('41', 'admin', '1', 'Admin/AuthManager/index', '權限管理', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('42', 'admin', '1', 'Admin/Addons/create', '創建', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('43', 'admin', '1', 'Admin/Addons/checkForm', '檢測創建', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('44', 'admin', '1', 'Admin/Addons/preview', '預覽', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('45', 'admin', '1', 'Admin/Addons/build', '快速生成插件', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('46', 'admin', '1', 'Admin/Addons/config', '設置', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('47', 'admin', '1', 'Admin/Addons/disable', '禁用', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('48', 'admin', '1', 'Admin/Addons/enable', '啟用', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('49', 'admin', '1', 'Admin/Addons/install', '安裝', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('50', 'admin', '1', 'Admin/Addons/uninstall', '卸載', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('51', 'admin', '1', 'Admin/Addons/saveconfig', '更新配置', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('52', 'admin', '1', 'Admin/Addons/adminList', '插件後臺列表', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('53', 'admin', '1', 'Admin/Addons/execute', 'URL方式訪問插件', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('54', 'admin', '1', 'Admin/Addons/index', '插件管理', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('55', 'admin', '1', 'Admin/Addons/hooks', '鉤子管理', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('56', 'admin', '1', 'Admin/model/add', '新增', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('57', 'admin', '1', 'Admin/model/edit', '編輯', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('58', 'admin', '1', 'Admin/model/setStatus', '改變狀態', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('59', 'admin', '1', 'Admin/model/update', '保存數據', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('60', 'admin', '1', 'Admin/Model/index', '模型管理', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('61', 'admin', '1', 'Admin/Config/edit', '編輯', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('62', 'admin', '1', 'Admin/Config/del', '刪除', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('63', 'admin', '1', 'Admin/Config/add', '新增', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('64', 'admin', '1', 'Admin/Config/save', '保存', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('65', 'admin', '1', 'Admin/Config/group', '網站設置', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('66', 'admin', '1', 'Admin/Config/index', '配置管理', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('67', 'admin', '1', 'Admin/Channel/add', '新增', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('68', 'admin', '1', 'Admin/Channel/edit', '編輯', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('69', 'admin', '1', 'Admin/Channel/del', '刪除', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('70', 'admin', '1', 'Admin/Channel/index', '導航管理', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('71', 'admin', '1', 'Admin/Category/edit', '編輯', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('72', 'admin', '1', 'Admin/Category/add', '新增', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('73', 'admin', '1', 'Admin/Category/remove', '刪除', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('74', 'admin', '1', 'Admin/Category/index', '分類管理', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('75', 'admin', '1', 'Admin/file/upload', '上傳控件', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('76', 'admin', '1', 'Admin/file/uploadPicture', '上傳圖片', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('77', 'admin', '1', 'Admin/file/download', '下載', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('79', 'admin', '1', 'Admin/article/batchOperate', '導入', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('80', 'admin', '1', 'Admin/Database/index?type=export', '備份數據庫', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('81', 'admin', '1', 'Admin/Database/index?type=import', '還原數據庫', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('82', 'admin', '1', 'Admin/Database/export', '備份', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('83', 'admin', '1', 'Admin/Database/optimize', '優化表', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('84', 'admin', '1', 'Admin/Database/repair', '修復表', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('86', 'admin', '1', 'Admin/Database/import', '恢復', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('87', 'admin', '1', 'Admin/Database/del', '刪除', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('88', 'admin', '1', 'Admin/User/add', '新增用戶', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('89', 'admin', '1', 'Admin/Attribute/index', '屬性管理', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('90', 'admin', '1', 'Admin/Attribute/add', '新增', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('91', 'admin', '1', 'Admin/Attribute/edit', '編輯', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('92', 'admin', '1', 'Admin/Attribute/setStatus', '改變狀態', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('93', 'admin', '1', 'Admin/Attribute/update', '保存數據', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('94', 'admin', '1', 'Admin/AuthManager/modelauth', '模型授權', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('95', 'admin', '1', 'Admin/AuthManager/addToModel', '保存模型授權', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('96', 'admin', '1', 'Admin/Category/move', '移動', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('97', 'admin', '1', 'Admin/Category/merge', '合並', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('98', 'admin', '1', 'Admin/Config/menu', '後臺菜單管理', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('99', 'admin', '1', 'Admin/Article/mydocument', '內容', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('100', 'admin', '1', 'Admin/Menu/index', '菜單管理', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('101', 'admin', '1', 'Admin/other', '其他', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('102', 'admin', '1', 'Admin/Menu/add', '新增', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('103', 'admin', '1', 'Admin/Menu/edit', '編輯', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('104', 'admin', '1', 'Admin/Think/lists?model=article', '文章管理', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('105', 'admin', '1', 'Admin/Think/lists?model=download', '下載管理', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('106', 'admin', '1', 'Admin/Think/lists?model=config', '配置管理', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('107', 'admin', '1', 'Admin/Action/actionlog', '行為日誌', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('108', 'admin', '1', 'Admin/User/updatePassword', '修改密碼', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('109', 'admin', '1', 'Admin/User/updateNickname', '修改昵稱', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('110', 'admin', '1', 'Admin/action/edit', '查看行為日誌', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('111', 'admin', '2', 'Admin/article/index', '文檔列表', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('112', 'admin', '2', 'Admin/article/add', '新增', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('113', 'admin', '2', 'Admin/article/edit', '編輯', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('114', 'admin', '2', 'Admin/article/setStatus', '改變狀態', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('115', 'admin', '2', 'Admin/article/update', '保存', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('116', 'admin', '2', 'Admin/article/autoSave', '保存草稿', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('117', 'admin', '2', 'Admin/article/move', '移動', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('118', 'admin', '2', 'Admin/article/copy', '復制', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('119', 'admin', '2', 'Admin/article/paste', '粘貼', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('120', 'admin', '2', 'Admin/article/batchOperate', '導入', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('121', 'admin', '2', 'Admin/article/recycle', '回收站', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('122', 'admin', '2', 'Admin/article/permit', '還原', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('123', 'admin', '2', 'Admin/article/clear', '清空', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('124', 'admin', '2', 'Admin/User/add', '新增用戶', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('125', 'admin', '2', 'Admin/User/action', '用戶行為', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('126', 'admin', '2', 'Admin/User/addAction', '新增用戶行為', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('127', 'admin', '2', 'Admin/User/editAction', '編輯用戶行為', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('128', 'admin', '2', 'Admin/User/saveAction', '保存用戶行為', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('129', 'admin', '2', 'Admin/User/setStatus', '變更行為狀態', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('130', 'admin', '2', 'Admin/User/changeStatus?method=forbidUser', '禁用會員', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('131', 'admin', '2', 'Admin/User/changeStatus?method=resumeUser', '啟用會員', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('132', 'admin', '2', 'Admin/User/changeStatus?method=deleteUser', '刪除會員', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('133', 'admin', '2', 'Admin/AuthManager/index', '權限管理', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('134', 'admin', '2', 'Admin/AuthManager/changeStatus?method=deleteGroup', '刪除', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('135', 'admin', '2', 'Admin/AuthManager/changeStatus?method=forbidGroup', '禁用', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('136', 'admin', '2', 'Admin/AuthManager/changeStatus?method=resumeGroup', '恢復', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('137', 'admin', '2', 'Admin/AuthManager/createGroup', '新增', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('138', 'admin', '2', 'Admin/AuthManager/editGroup', '編輯', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('139', 'admin', '2', 'Admin/AuthManager/writeGroup', '保存用戶組', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('140', 'admin', '2', 'Admin/AuthManager/group', '授權', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('141', 'admin', '2', 'Admin/AuthManager/access', '訪問授權', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('142', 'admin', '2', 'Admin/AuthManager/user', '成員授權', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('143', 'admin', '2', 'Admin/AuthManager/removeFromGroup', '解除授權', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('144', 'admin', '2', 'Admin/AuthManager/addToGroup', '保存成員授權', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('145', 'admin', '2', 'Admin/AuthManager/category', '分類授權', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('146', 'admin', '2', 'Admin/AuthManager/addToCategory', '保存分類授權', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('147', 'admin', '2', 'Admin/AuthManager/modelauth', '模型授權', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('148', 'admin', '2', 'Admin/AuthManager/addToModel', '保存模型授權', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('149', 'admin', '2', 'Admin/Addons/create', '創建', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('150', 'admin', '2', 'Admin/Addons/checkForm', '檢測創建', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('151', 'admin', '2', 'Admin/Addons/preview', '預覽', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('152', 'admin', '2', 'Admin/Addons/build', '快速生成插件', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('153', 'admin', '2', 'Admin/Addons/config', '設置', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('154', 'admin', '2', 'Admin/Addons/disable', '禁用', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('155', 'admin', '2', 'Admin/Addons/enable', '啟用', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('156', 'admin', '2', 'Admin/Addons/install', '安裝', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('157', 'admin', '2', 'Admin/Addons/uninstall', '卸載', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('158', 'admin', '2', 'Admin/Addons/saveconfig', '更新配置', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('159', 'admin', '2', 'Admin/Addons/adminList', '插件後臺列表', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('160', 'admin', '2', 'Admin/Addons/execute', 'URL方式訪問插件', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('161', 'admin', '2', 'Admin/Addons/hooks', '鉤子管理', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('162', 'admin', '2', 'Admin/Model/index', '模型管理', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('163', 'admin', '2', 'Admin/model/add', '新增', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('164', 'admin', '2', 'Admin/model/edit', '編輯', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('165', 'admin', '2', 'Admin/model/setStatus', '改變狀態', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('166', 'admin', '2', 'Admin/model/update', '保存數據', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('167', 'admin', '2', 'Admin/Attribute/index', '屬性管理', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('168', 'admin', '2', 'Admin/Attribute/add', '新增', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('169', 'admin', '2', 'Admin/Attribute/edit', '編輯', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('170', 'admin', '2', 'Admin/Attribute/setStatus', '改變狀態', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('171', 'admin', '2', 'Admin/Attribute/update', '保存數據', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('172', 'admin', '2', 'Admin/Config/index', '配置管理', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('173', 'admin', '2', 'Admin/Config/edit', '編輯', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('174', 'admin', '2', 'Admin/Config/del', '刪除', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('175', 'admin', '2', 'Admin/Config/add', '新增', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('176', 'admin', '2', 'Admin/Config/save', '保存', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('177', 'admin', '2', 'Admin/Menu/index', '菜單管理', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('178', 'admin', '2', 'Admin/Channel/index', '導航管理', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('179', 'admin', '2', 'Admin/Channel/add', '新增', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('180', 'admin', '2', 'Admin/Channel/edit', '編輯', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('181', 'admin', '2', 'Admin/Channel/del', '刪除', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('182', 'admin', '2', 'Admin/Category/index', '分類管理', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('183', 'admin', '2', 'Admin/Category/edit', '編輯', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('184', 'admin', '2', 'Admin/Category/add', '新增', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('185', 'admin', '2', 'Admin/Category/remove', '刪除', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('186', 'admin', '2', 'Admin/Category/move', '移動', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('187', 'admin', '2', 'Admin/Category/merge', '合並', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('188', 'admin', '2', 'Admin/Database/index?type=export', '備份數據庫', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('189', 'admin', '2', 'Admin/Database/export', '備份', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('190', 'admin', '2', 'Admin/Database/optimize', '優化表', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('191', 'admin', '2', 'Admin/Database/repair', '修復表', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('192', 'admin', '2', 'Admin/Database/index?type=import', '還原數據庫', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('193', 'admin', '2', 'Admin/Database/import', '恢復', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('194', 'admin', '2', 'Admin/Database/del', '刪除', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('195', 'admin', '2', 'Admin/other', '其他', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('196', 'admin', '2', 'Admin/Menu/add', '新增', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('197', 'admin', '2', 'Admin/Menu/edit', '編輯', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('198', 'admin', '2', 'Admin/Think/lists?model=article', '應用', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('199', 'admin', '2', 'Admin/Think/lists?model=download', '下載管理', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('200', 'admin', '2', 'Admin/Think/lists?model=config', '應用', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('201', 'admin', '2', 'Admin/Action/actionlog', '行為日誌', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('202', 'admin', '2', 'Admin/User/updatePassword', '修改密碼', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('203', 'admin', '2', 'Admin/User/updateNickname', '修改昵稱', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('204', 'admin', '2', 'Admin/action/edit', '查看行為日誌', '-1', '');
INSERT INTO `sf_auth_rule` VALUES ('205', 'admin', '1', 'Admin/think/add', '新增數據', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('206', 'admin', '1', 'Admin/think/edit', '編輯數據', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('207', 'admin', '1', 'Admin/Menu/import', '導入', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('208', 'admin', '1', 'Admin/Model/generate', '生成', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('209', 'admin', '1', 'Admin/Addons/addHook', '新增鉤子', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('210', 'admin', '1', 'Admin/Addons/edithook', '編輯鉤子', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('211', 'admin', '1', 'Admin/Article/sort', '文檔排序', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('212', 'admin', '1', 'Admin/Config/sort', '排序', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('213', 'admin', '1', 'Admin/Menu/sort', '排序', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('214', 'admin', '1', 'Admin/Channel/sort', '排序', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('215', 'admin', '1', 'Admin/Category/operate/type/move', '移動', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('216', 'admin', '1', 'Admin/Category/operate/type/merge', '合並', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('217', 'admin', '2', 'Admin/Article/houseslist', '網站管理', '1', '');
INSERT INTO `sf_auth_rule` VALUES ('218', 'admin', '1', 'Admin/Article/screening', '委託篩選', '-1', '');

-- ----------------------------
-- Table structure for sf_category
-- ----------------------------
DROP TABLE IF EXISTS `sf_category`;
CREATE TABLE `sf_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '分類ID',
  `name` varchar(30) NOT NULL COMMENT '標誌',
  `title` varchar(50) NOT NULL COMMENT '標題',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上級分類ID',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序（同級有效）',
  `list_row` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '列表每頁行數',
  `meta_title` varchar(50) NOT NULL DEFAULT '' COMMENT 'SEO的網頁標題',
  `keywords` varchar(255) NOT NULL DEFAULT '' COMMENT '關鍵字',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  `template_index` varchar(100) NOT NULL COMMENT '頻道頁模板',
  `template_lists` varchar(100) NOT NULL COMMENT '列表頁模板',
  `template_detail` varchar(100) NOT NULL COMMENT '詳情頁模板',
  `template_edit` varchar(100) NOT NULL COMMENT '編輯頁模板',
  `model` varchar(100) NOT NULL DEFAULT '' COMMENT '關聯模型',
  `type` varchar(100) NOT NULL DEFAULT '' COMMENT '允許發布的內容類型',
  `link_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '外鏈',
  `allow_publish` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否允許發布內容',
  `display` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '可見性',
  `reply` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否允許回復',
  `check` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '發布的文章是否需要審核',
  `reply_model` varchar(100) NOT NULL DEFAULT '',
  `extend` text NOT NULL COMMENT '擴展設置',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '數據狀態',
  `icon` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '分類圖標',
  `displays` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`name`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=80 DEFAULT CHARSET=utf8 COMMENT='分類表';

-- ----------------------------
-- Records of sf_category
-- ----------------------------
INSERT INTO `sf_category` VALUES ('1', 'blog', '市場動態', '0', '0', '10', '', '', '', '', '', '', '', '2', '2,1', '0', '0', '1', '0', '0', '1', '', '1379474947', '1431063129', '1', '0', '1');
INSERT INTO `sf_category` VALUES ('2', 'default_blog', '列表', '1', '1', '10', '', '', '', '', '', '', '', '2', '2,1,3', '0', '1', '1', '0', '1', '1', '', '1379475028', '1431063138', '1', '31', '0');
INSERT INTO `sf_category` VALUES ('42', 'clause', '買賣須知', '0', '0', '10', '', '', '', '', '', '', '', '5', '2', '0', '0', '1', '1', '0', '', '', '1429600374', '1430986367', '1', '0', '0');
INSERT INTO `sf_category` VALUES ('43', 'Clauses', ' 列表', '42', '0', '10', '', '', '', '', '', '', '', '5', '2', '0', '1', '1', '1', '0', '', '', '1429600397', '1430986371', '1', '0', '0');
INSERT INTO `sf_category` VALUES ('53', 'newbuild', '最新樓盤', '0', '0', '10', '', '', '', '', '', '', '', '', '2', '0', '0', '1', '1', '0', '', '', '1430199520', '1430710839', '1', '0', '1');
INSERT INTO `sf_category` VALUES ('55', 'sell', '委託租售', '0', '0', '10', '', '', '', '', '', '', '', '', '', '0', '0', '1', '1', '0', '', '', '1430201746', '1430201746', '1', '0', '1');
INSERT INTO `sf_category` VALUES ('58', 'rental', '樓盤出租', '55', '0', '10', '', '', '', '', '', '', '', '10', '2', '0', '1', '1', '1', '0', '', '', '1430202308', '1430202881', '1', '0', '1');
INSERT INTO `sf_category` VALUES ('59', 'sell2', '樓盤出售', '55', '0', '10', '', '', '', '', '', '', '', '10', '', '0', '1', '1', '1', '0', '', '', '1430213435', '1430213435', '1', '0', '0');
INSERT INTO `sf_category` VALUES ('60', 'h1', '樓盤列表', '53', '0', '10', '', '', '', '', '', '', '', '4', '2', '0', '1', '1', '1', '0', '', '', '1430272517', '1430272517', '1', '0', '1');
INSERT INTO `sf_category` VALUES ('62', 'c1', '留言板', '0', '7', '10', '', '', '', '', '', '', '', '', '', '0', '0', '1', '1', '0', '', '', '1430278255', '1438671895', '1', '0', '1');
INSERT INTO `sf_category` VALUES ('63', 'c', '列表', '62', '0', '10', '', '', '', '', '', '', '', '11', '', '0', '1', '1', '1', '0', '', '', '1430278472', '1430278472', '1', '0', '0');
INSERT INTO `sf_category` VALUES ('66', 'logos', 'logo設置', '0', '5', '10', '', '', '', '', '', '', '', '12', '', '0', '1', '1', '1', '0', '', '', '1431051636', '1431051717', '1', '0', '1');
INSERT INTO `sf_category` VALUES ('67', 'searchs', '條件篩選', '0', '8', '10', '', '', '', '', '', '', '', '6', '', '0', '1', '1', '1', '0', '', '', '1431065771', '1438671904', '1', '0', '0');
INSERT INTO `sf_category` VALUES ('68', 'ad', '側邊廣告', '0', '6', '10', '', '', '', '', '', '', '', '13', '', '0', '1', '1', '1', '0', '', '', '1431072384', '1431072413', '1', '0', '0');
INSERT INTO `sf_category` VALUES ('70', 'bg', '背景圖', '0', '9', '10', '', '', '', '', '', '', '', '14', '', '0', '1', '1', '1', '0', '', '', '1435045538', '1438671914', '1', '0', '0');
INSERT INTO `sf_category` VALUES ('71', 'cs', '聯繫我們', '0', '4', '10', '', '', '', '', '', '', '', '15', '', '0', '1', '1', '1', '0', '', '', '1438595234', '1438671878', '1', '0', '0');
INSERT INTO `sf_category` VALUES ('79', 'screening', '委託篩選', '55', '0', '10', '', '', '', '', '', '', '', '', '', '0', '1', '1', '1', '0', '', '', '1439785847', '1439785847', '1', '0', '0');

-- ----------------------------
-- Table structure for sf_channel
-- ----------------------------
DROP TABLE IF EXISTS `sf_channel`;
CREATE TABLE `sf_channel` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '頻道ID',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上級頻道ID',
  `title` char(30) NOT NULL COMMENT '頻道標題',
  `url` char(100) NOT NULL COMMENT '頻道連接',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '導航排序',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '狀態',
  `target` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '新窗口打開',
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sf_channel
-- ----------------------------
INSERT INTO `sf_channel` VALUES ('1', '0', '首頁', 'Index/index', '1', '1379475111', '1379923177', '1', '0');
INSERT INTO `sf_channel` VALUES ('2', '0', '博客', 'Article/index?category=blog', '2', '1379475131', '1379483713', '1', '0');
INSERT INTO `sf_channel` VALUES ('3', '0', '官網', 'http://www.onethink.cn', '3', '1379475154', '1387163458', '1', '0');

-- ----------------------------
-- Table structure for sf_config
-- ----------------------------
DROP TABLE IF EXISTS `sf_config`;
CREATE TABLE `sf_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '配置ID',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '配置名稱',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '配置類型',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '配置說明',
  `group` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '配置分組',
  `extra` varchar(255) NOT NULL DEFAULT '' COMMENT '配置值',
  `remark` varchar(100) NOT NULL COMMENT '配置說明',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '狀態',
  `value` text NOT NULL COMMENT '配置值',
  `sort` smallint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`name`),
  KEY `type` (`type`),
  KEY `group` (`group`)
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sf_config
-- ----------------------------
INSERT INTO `sf_config` VALUES ('1', 'WEB_SITE_TITLE', '1', '網站標題', '1', '', '順發地產', '1378898976', '1428830860', '1', '順發地產', '0');
INSERT INTO `sf_config` VALUES ('2', 'WEB_SITE_DESCRIPTION', '2', '網站描述', '1', '', '網站搜索引擎描述', '1378898976', '1379235841', '1', '順發地產自開業以來，專營澳門各區住宅，鋪位，車位，寫字樓買賣租賃等業務，為市民大眾提供最多最好的選擇。', '1');
INSERT INTO `sf_config` VALUES ('3', 'WEB_SITE_KEYWORD', '2', '網站關鍵字', '1', '', '網站搜索引擎關鍵字', '1378898976', '1381390100', '1', 'ThinkPHP,OneThink', '8');
INSERT INTO `sf_config` VALUES ('4', 'WEB_SITE_CLOSE', '4', '關閉站點', '1', '0:關閉,1:開啟', '站點關閉後其他用戶不能訪問，管理員可以正常訪問', '1378898976', '1379235296', '1', '1', '1');
INSERT INTO `sf_config` VALUES ('9', 'CONFIG_TYPE_LIST', '3', '配置類型列表', '4', '', '主要用於數據解析和頁面表單的生成', '1378898976', '1379235348', '1', '0:數字\r\n1:字符\r\n2:文本\r\n3:數組\r\n4:枚舉', '2');
INSERT INTO `sf_config` VALUES ('10', 'WEB_SITE_ICP', '1', '網站備案號', '1', '', '設置在網站底部顯示的備案號，如“滬ICP備12007941號-2', '1378900335', '1379235859', '1', '', '9');
INSERT INTO `sf_config` VALUES ('11', 'DOCUMENT_POSITION', '3', '文檔推薦位', '2', '', '文檔推薦位，推薦到多個位置KEY值相加即可', '1379053380', '1379235329', '1', '1:列表頁推薦\r\n2:頻道頁推薦\r\n4:網站首頁推薦', '3');
INSERT INTO `sf_config` VALUES ('12', 'DOCUMENT_DISPLAY', '3', '文檔可見性', '2', '', '文章可見性僅影響前臺顯示，後臺不收影響', '1379056370', '1379235322', '1', '0:所有人可見\r\n1:僅註冊會員可見\r\n2:僅管理員可見', '4');
INSERT INTO `sf_config` VALUES ('13', 'COLOR_STYLE', '4', '後臺色系', '1', 'default_color:默認\r\nblue_color:紫羅蘭', '後臺顏色風格', '1379122533', '1379235904', '1', 'default_color', '10');
INSERT INTO `sf_config` VALUES ('20', 'CONFIG_GROUP_LIST', '3', '配置分組', '4', '', '配置分組', '1379228036', '1430618182', '1', '1:基本\r\n2:內容\r\n3:用戶\r\n4:系統\r\n5:郵箱', '5');
INSERT INTO `sf_config` VALUES ('21', 'HOOKS_TYPE', '3', '鉤子的類型', '4', '', '類型 1-用於擴展顯示內容，2-用於擴展業務處理', '1379313397', '1379313407', '1', '1:視圖\r\n2:控制器', '6');
INSERT INTO `sf_config` VALUES ('22', 'AUTH_CONFIG', '3', 'Auth配置', '4', '', '自定義Auth.class.php類配置', '1379409310', '1379409564', '1', 'AUTH_ON:1\r\nAUTH_TYPE:2', '8');
INSERT INTO `sf_config` VALUES ('23', 'OPEN_DRAFTBOX', '4', '是否開啟草稿功能', '2', '0:關閉草稿功能\r\n1:開啟草稿功能\r\n', '新增文章時的草稿功能配置', '1379484332', '1379484591', '1', '1', '1');
INSERT INTO `sf_config` VALUES ('24', 'DRAFT_AOTOSAVE_INTERVAL', '0', '自動保存草稿時間', '2', '', '自動保存草稿的時間間隔，單位：秒', '1379484574', '1386143323', '1', '60', '2');
INSERT INTO `sf_config` VALUES ('25', 'LIST_ROWS', '0', '後臺每頁記錄數', '2', '', '後臺數據每頁顯示記錄數', '1379503896', '1380427745', '1', '10', '10');
INSERT INTO `sf_config` VALUES ('26', 'USER_ALLOW_REGISTER', '4', '是否允許用戶註冊', '3', '0:關閉註冊\r\n1:允許註冊', '是否開放用戶註冊', '1379504487', '1379504580', '1', '1', '3');
INSERT INTO `sf_config` VALUES ('27', 'CODEMIRROR_THEME', '4', '預覽插件的CodeMirror主題', '4', '3024-day:3024 day\r\n3024-night:3024 night\r\nambiance:ambiance\r\nbase16-dark:base16 dark\r\nbase16-light:base16 light\r\nblackboard:blackboard\r\ncobalt:cobalt\r\neclipse:eclipse\r\nelegant:elegant\r\nerlang-dark:erlang-dark\r\nlesser-dark:lesser-dark\r\nmidnight:midnight', '詳情見CodeMirror官網', '1379814385', '1384740813', '1', 'ambiance', '3');
INSERT INTO `sf_config` VALUES ('28', 'DATA_BACKUP_PATH', '1', '數據庫備份根路徑', '4', '', '路徑必須以 / 結尾', '1381482411', '1381482411', '1', './Data/', '5');
INSERT INTO `sf_config` VALUES ('29', 'DATA_BACKUP_PART_SIZE', '0', '數據庫備份卷大小', '4', '', '該值用於限制壓縮後的分卷最大長度。單位：B；建議設置20M', '1381482488', '1381729564', '1', '20971520', '7');
INSERT INTO `sf_config` VALUES ('30', 'DATA_BACKUP_COMPRESS', '4', '數據庫備份文件是否啟用壓縮', '4', '0:不壓縮\r\n1:啟用壓縮', '壓縮備份文件需要PHP環境支持gzopen,gzwrite函數', '1381713345', '1381729544', '1', '1', '9');
INSERT INTO `sf_config` VALUES ('31', 'DATA_BACKUP_COMPRESS_LEVEL', '4', '數據庫備份文件壓縮級別', '4', '1:普通\r\n4:壹般\r\n9:最高', '數據庫備份文件的壓縮級別，該配置在開啟壓縮時生效', '1381713408', '1381713408', '1', '9', '10');
INSERT INTO `sf_config` VALUES ('32', 'DEVELOP_MODE', '4', '開啟開發者模式', '4', '0:關閉\r\n1:開啟', '是否開啟開發者模式', '1383105995', '1383291877', '1', '0', '11');
INSERT INTO `sf_config` VALUES ('33', 'ALLOW_VISIT', '3', '不受限控制器方法', '0', '', '', '1386644047', '1386644741', '1', '0:article/draftbox\r\n1:article/mydocument\r\n2:Category/tree\r\n3:Index/verify\r\n4:file/upload\r\n5:file/download\r\n6:user/updatePassword\r\n7:user/updateNickname\r\n8:user/submitPassword\r\n9:user/submitNickname\r\n10:file/uploadpicture', '0');
INSERT INTO `sf_config` VALUES ('34', 'DENY_VISIT', '3', '超管專限控制器方法', '0', '', '僅超級管理員可訪問的控制器方法', '1386644141', '1386644659', '1', '0:Addons/addhook\r\n1:Addons/edithook\r\n2:Addons/delhook\r\n3:Addons/updateHook\r\n4:Admin/getMenus\r\n5:Admin/recordList\r\n6:AuthManager/updateRules\r\n7:AuthManager/tree', '0');
INSERT INTO `sf_config` VALUES ('35', 'REPLY_LIST_ROWS', '0', '回復列表每頁條數', '2', '', '', '1386645376', '1387178083', '1', '10', '0');
INSERT INTO `sf_config` VALUES ('36', 'ADMIN_ALLOW_IP', '2', '後臺允許訪問IP', '4', '', '多個用逗號分隔，如果不配置表示不限制IP訪問', '1387165454', '1387165553', '1', '', '12');
INSERT INTO `sf_config` VALUES ('37', 'SHOW_PAGE_TRACE', '4', '是否顯示頁面Trace', '4', '0:關閉\r\n1:開啟', '是否顯示頁面Trace信息', '1387165685', '1387165685', '1', '0', '1');
INSERT INTO `sf_config` VALUES ('38', 'MAIL_HOST', '1', 'smtp服務器的名稱', '5', '', 'smtp服務器的名稱', '1429521189', '1429522827', '1', 'smtp.163.com', '0');
INSERT INTO `sf_config` VALUES ('39', 'MAIL_USERNAME', '1', '妳的郵箱名', '5', '', '妳的郵箱名', '1429521241', '1429522835', '1', '794025348@qq.com', '0');
INSERT INTO `sf_config` VALUES ('40', 'MAIL_FROMNAME', '1', '發件人姓名', '5', '', '發件人姓名', '1429522107', '1429522841', '1', '順發地產', '0');
INSERT INTO `sf_config` VALUES ('41', 'MAIL_PASSWORD', '1', '郵箱密碼', '5', '', '郵箱密碼', '1429522136', '1429522846', '1', 'zkefislsoooemhle', '0');
INSERT INTO `sf_config` VALUES ('42', 'WEB_SITE_TEL', '1', '聯繫電話', '1', '', '聯繫電話', '1429602846', '1429602933', '1', '6633 3903 /6623 7250', '0');
INSERT INTO `sf_config` VALUES ('43', 'WEB_SITE_EMAIL', '1', '電子郵箱', '1', '', '電子郵箱', '1429602869', '1429602940', '1', 'sf@sfhousing.net', '0');
INSERT INTO `sf_config` VALUES ('44', 'WEB_SITE_ADD', '1', '聯繫地址', '1', '', '聯繫地址', '1429602893', '1429602999', '1', '澳門田畔街7號F嘉輝大廈地下', '0');
INSERT INTO `sf_config` VALUES ('45', 'WEB_SITE_INDEX', '1', '主頁', '1', '', '主頁', '1429602909', '1429603004', '1', 'www.sfhousing.net', '0');
INSERT INTO `sf_config` VALUES ('46', 'MAIL_FROMTITLE', '1', '郵件標題', '5', '', '郵件標題', '1430288138', '1430288272', '1', '順發地產聯繫我們', '0');
INSERT INTO `sf_config` VALUES ('47', 'MAIL_FROMU', '1', '接收人郵箱', '5', '', '郵箱地址', '1430288221', '1430288221', '1', '794025348@qq.com', '0');
INSERT INTO `sf_config` VALUES ('48', 'MAIL_CONTENT', '2', '內容', '5', '', '內容', '1430288237', '1430288266', '1', '順發地產', '0');
INSERT INTO `sf_config` VALUES ('49', 'MAPSITE', '1', '坐標', '1', '', '地圖坐標', '1432110266', '1432110295', '1', '22.1984624664,115.43818974071', '0');
INSERT INTO `sf_config` VALUES ('50', 'ANALYTICS', '1', 'GOOGLE ANALYTICS設置', '1', '', '', '1433126591', '1433126591', '1', '', '0');
INSERT INTO `sf_config` VALUES ('51', 'COPYRIGHT', '1', '版權設置', '1', '', '版權設置', '1438670999', '1438670999', '1', 'Copynight @ 2014Soufun Holdings Limited,All Right Rwserved順發地產 版權所有', '0');

-- ----------------------------
-- Table structure for sf_document
-- ----------------------------
DROP TABLE IF EXISTS `sf_document`;
CREATE TABLE `sf_document` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文檔ID',
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用戶ID',
  `name` char(40) NOT NULL DEFAULT '' COMMENT '標識',
  `title` char(80) NOT NULL DEFAULT '' COMMENT '標題',
  `category_id` int(10) unsigned NOT NULL COMMENT '所屬分類',
  `description` text NOT NULL COMMENT '描述',
  `root` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '根節點',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '所屬ID',
  `model_id` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '內容模型ID',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '2' COMMENT '內容類型',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '推薦位',
  `link_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '外鏈',
  `cover_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '封面',
  `display` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '可見性',
  `deadline` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '截至時間',
  `attach` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '附件數量',
  `view` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '瀏覽量',
  `comment` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '評論數',
  `extend` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '擴展統計字段',
  `level` int(10) NOT NULL DEFAULT '0' COMMENT '優先級',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '數據狀態',
  `hots` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '是否熱賣',
  `content` text NOT NULL COMMENT '詳細內容',
  `add` varchar(255) NOT NULL COMMENT '所在地',
  `housestype` varchar(255) NOT NULL COMMENT '物業類型',
  `sell` varchar(255) NOT NULL COMMENT '銷售類型',
  `houses_prices` int(10) unsigned NOT NULL COMMENT '房屋價格',
  `price` int(10) unsigned NOT NULL DEFAULT '1000' COMMENT '價格',
  `search_add` varchar(255) NOT NULL COMMENT '所在地',
  `search_type` varchar(255) NOT NULL COMMENT '房屋類型',
  `search_prices` varchar(255) NOT NULL COMMENT '價格',
  `read_status` varchar(255) NOT NULL DEFAULT '未讀' COMMENT '閱讀狀態',
  PRIMARY KEY (`id`),
  KEY `idx_category_status` (`category_id`,`status`),
  KEY `idx_status_type_pid` (`status`,`uid`,`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=313 DEFAULT CHARSET=utf8 COMMENT='文檔模型基礎表';

-- ----------------------------
-- Records of sf_document
-- ----------------------------
INSERT INTO `sf_document` VALUES ('294', '1', '', '人行半年3度降息經濟不妙？房產專家難樂觀', '2', '中國人民銀行半年來第三度降息，10日宣布金融機構人民幣1年期存貸款基準利率各調降1碼(0.25%)。中國大陸全面寬鬆的貨幣政策，是否也意味大陸經濟真的不妙？也加深了市場的疑慮。', '0', '0', '2', '2', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '1438597140', '1438669431', '1', '0', '<p>\r\n	中國人民銀行半年來第三度降息，10日宣布金融機構人民幣1年期存貸款基準利率各調降1碼(0.25%)。中國大陸全面寬鬆的貨幣政策，是否也意味大陸經濟真的不妙？也加深了市場的疑慮。\r\n</p>\r\n<p>\r\n	中國人民銀行半年來第三度降息，10日宣布金融機構人民幣1年期存貸款基準利率各調降1碼(0.25%)。中國大陸全面寬鬆的貨幣政策，是否也意味大陸經濟真的不妙？也加深了市場的疑慮。\r\n</p>\r\n<p>\r\n	中國人民銀行半年來第三度降息，10日宣布金融機構人民幣1年期存貸款基準利率各調降1碼(0.25%)。中國大陸全面寬鬆的貨幣政策，是否也意味大陸經濟真的不妙？也加深了市場的疑慮。\r\n</p>\r\n<p>\r\n	<br />\r\n</p>', '', '', '0', '0', '1000', '', '', '', '未讀');
INSERT INTO `sf_document` VALUES ('295', '1', '', '聯繫我們', '71', '', '0', '0', '15', '2', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '1438597140', '1438669196', '1', '0', '<p>\r\n	<span style=\"font-family:SimHei;font-size:18px;line-height:30px;\">順發地產的空間費看得見的看風景費看的空間富 士康空間打開就監控就看見看的監控費看就空間 看得見啊搜房卡監控的風景看覺得費監控的反恐 精英卡機的的減肥卡機的費看撒嬌帶飯監控就就 看的境況艱苦奮鬥就監控風景闊景發件就</span> \r\n</p>\r\n<p>\r\n	<br />\r\n</p>', '', '', '0', '0', '1000', '', '', '', '未讀');
INSERT INTO `sf_document` VALUES ('296', '1', '', '廣告', '68', '', '0', '0', '13', '2', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '1438598194', '1438598194', '3', '0', '', '', '', '0', '0', '1000', '', '', '', '未讀');
INSERT INTO `sf_document` VALUES ('297', '1', '', '側欄廣告', '68', '', '0', '0', '13', '2', '0', '0', '21', '1', '0', '0', '0', '0', '0', '0', '1438668000', '1438668000', '1', '0', '', '', '', '0', '0', '1000', '', '', '', '未讀');
INSERT INTO `sf_document` VALUES ('298', '1', '', '首頁', '70', '', '0', '0', '14', '2', '0', '0', '0', '1', '0', '0', '0', '0', '0', '5', '1438668180', '1438668635', '1', '0', '', '', '', '0', '0', '1000', '', '', '', '未讀');
INSERT INTO `sf_document` VALUES ('299', '1', '', '最新樓盤', '70', '', '0', '0', '14', '2', '0', '0', '0', '1', '0', '0', '0', '0', '0', '4', '1438668660', '1438668833', '1', '0', '', '', '', '0', '0', '1000', '', '', '', '未讀');
INSERT INTO `sf_document` VALUES ('300', '1', '', '市場動態', '70', '', '0', '0', '14', '2', '0', '0', '0', '1', '0', '0', '0', '0', '0', '3', '1438668840', '1438668881', '1', '0', '', '', '', '0', '0', '1000', '', '', '', '未讀');
INSERT INTO `sf_document` VALUES ('301', '1', '', '買賣須知', '70', '', '0', '0', '14', '2', '0', '0', '0', '1', '0', '0', '0', '0', '0', '2', '1438668928', '1438668928', '1', '0', '', '', '', '0', '0', '1000', '', '', '', '未讀');
INSERT INTO `sf_document` VALUES ('302', '1', '', '買賣須知', '43', '', '0', '0', '5', '2', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '1438668960', '1438668978', '1', '0', '<span style=\"color:#FFFFFF;\">1.賣雙方的名稱或者姓名和住所;</span><br />\r\n<span style=\"color:#FFFFFF;\">2.標的物名稱、規格、型號、生產廠商、產地、數量及價款；</span><br />\r\n<span style=\"color:#FFFFFF;\">3.質量要求；</span><br />\r\n<span style=\"color:#FFFFFF;\">4.包裝方式；</span><br />\r\n<span style=\"color:#FFFFFF;\">5.交貨的時間、地點、方式；</span><br />\r\n<span style=\"color:#FFFFFF;\">6.檢驗標準、時間、方法；</span><br />\r\n<span style=\"color:#FFFFFF;\">7.結算方式；</span><br />\r\n<span style=\"color:#FFFFFF;\">8.違約責任；</span><br />\r\n<span style=\"color:#FFFFFF;\">9.解決爭議的方法；</span><br />\r\n<span style=\"color:#FFFFFF;\">10.其他條款(如國際貨物買賣合同使用的文字及其效力、質量保證期等) 買賣合同賣方必須是標的物的所有人或者有權處分該物的人。 買賣合同應當對買賣的標的物進行詳細的描述，做到明確具體壹目了然。 買賣合同應對貨物質量重點加以約定，以便於驗收，避免糾紛。賣方應當按照約定的質量要求交付標的物，賣方提供有關標的物質量說明的，交付的標的物應當符合該說明的質量要求。 貨物的包裝方式對於貨物的安全運送非常重要，包裝不善就可能發生貨損，侵害買方的利益。對於包裝方式無約定的應當按照通用的方式包裝，沒有通用方式的，應當采取足以保護標的物的包裝方式。 交貨的時間、地點和方式是交易的關鍵內容，它涉及雙方的利益實現和標的物毀損滅失風險承擔問題。壹般情況下，標的物的所有權自交付時轉移，風險承擔隨之轉移。因此對交付的相關內容壹定要在合同中予以明確。 標的物的檢驗是指買方在收到賣方交付的標的物時,對其等級、質量、重量、包裝、規格等情況的查驗、測試或者鑒定。合同中應當規定檢驗的時間、地點、標準和方法、買方發現質量問題提出異議的時間及賣方答復的時間、發生質量爭議的鑒定機構。買方收到標的物後應當在合同約定的檢驗期間內對標的物進行檢驗，如發現貨物的數量或質量不符合約定應在檢驗期內通知賣方，買方怠於通知的，視為所交貨物符合約定。合同沒有約定檢驗期間的，買方應及時檢驗，並在發現問題的合理期間內通知賣方。 買賣合同中買方的義務還有按照約定的數額、地點、時間支付價款等，賣方的義務還有按照合同約定或者交易習慣相關單證和資料、按照約定的時間地點交付標的物等。如果雙方違反了應盡的義務，應當按照合同約定承擔違約責任，合同沒有約定的按照法律規定承擔違約責任。買方的違約的情形大致有毀約、未按照約定提貨、付款等，賣方的違約情形大致有毀約、未按期交貨、交付的標的物不符合約定等。對違約行為的懲處，合同雙方應在合同違約責任條款中加以詳細描述。</span>', '', '', '0', '0', '1000', '', '', '', '未讀');
INSERT INTO `sf_document` VALUES ('303', '1', '', '聯繫我們', '70', '', '0', '0', '14', '2', '0', '0', '0', '1', '0', '0', '0', '0', '0', '1', '1438669020', '1438673056', '1', '0', '', '', '', '0', '0', '1000', '', '', '', '未讀');
INSERT INTO `sf_document` VALUES ('304', '1', '', '準備中', '60', '準備中', '0', '0', '4', '2', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '1438669493', '1438669493', '1', '0', '', '所在地', '房屋類型', '', '10000', '8000', '', '', '', '未讀');
INSERT INTO `sf_document` VALUES ('305', '1', '', '準備中', '60', '準備中', '0', '0', '4', '2', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '1438669479', '1438669479', '1', '0', '', '所在地', '房屋類型', '1', '5000', '5000', '', '', '', '未讀');
INSERT INTO `sf_document` VALUES ('306', '1', '', '準備中', '60', '準備中', '0', '0', '4', '2', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '1438669545', '1438669545', '1', '1', '', '所在地', '房屋類型', '1', '5611', '1220', '', '', '', '未讀');
INSERT INTO `sf_document` VALUES ('307', '1', '', '準備中', '60', '準備中', '0', '0', '4', '2', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '1438669588', '1438669588', '1', '0', '', '所在地', '房屋類型', '1', '45646', '546', '', '', '', '未讀');
INSERT INTO `sf_document` VALUES ('308', '1', '', '準備中', '60', '準備中', '0', '0', '4', '2', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '1438669737', '1438669737', '1', '0', '', '所在地', '房屋類型', '0', '1124', '500', '', '', '', '未讀');
INSERT INTO `sf_document` VALUES ('309', '1', '', 'logo設置', '66', '', '0', '0', '12', '2', '0', '0', '63', '1', '0', '0', '0', '0', '0', '0', '1438671383', '1438671383', '1', '0', '', '', '', '0', '0', '1000', '', '', '', '未讀');
INSERT INTO `sf_document` VALUES ('310', '1', '', 'aaa', '67', '', '0', '0', '6', '2', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '1439781323', '1439781323', '1', '0', '', '', '', '0', '0', '1000', 'aa', 'aa', 'aa', '未讀');
INSERT INTO `sf_document` VALUES ('311', '1', '', '拱北吉屋豪宅出租', '58', '4456', '0', '0', '10', '2', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '1439794881', '1439794881', '1', '0', '', '', '', '', '0', '1000', '', '', '', '未讀');
INSERT INTO `sf_document` VALUES ('312', '1', '', 'aaa', '60', '', '0', '0', '4', '2', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '1439798446', '1439798446', '1', '0', '', 'aa', 'aa', '0', '0', '0', '', '', '', '未讀');

-- ----------------------------
-- Table structure for sf_document_ad
-- ----------------------------
DROP TABLE IF EXISTS `sf_document_ad`;
CREATE TABLE `sf_document_ad` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `url` varchar(255) NOT NULL COMMENT 'url',
  `P1` int(10) unsigned NOT NULL COMMENT 'P1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=298 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sf_document_ad
-- ----------------------------
INSERT INTO `sf_document_ad` VALUES ('246', '', '0');
INSERT INTO `sf_document_ad` VALUES ('296', '', '0');
INSERT INTO `sf_document_ad` VALUES ('297', '', '0');

-- ----------------------------
-- Table structure for sf_document_article
-- ----------------------------
DROP TABLE IF EXISTS `sf_document_article`;
CREATE TABLE `sf_document_article` (
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文檔ID',
  `ImageS` varchar(255) NOT NULL COMMENT ' 上傳多圖',
  `edit` varchar(255) NOT NULL COMMENT '作者',
  `come` varchar(255) NOT NULL COMMENT '來源',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文檔模型文章表';

-- ----------------------------
-- Records of sf_document_article
-- ----------------------------
INSERT INTO `sf_document_article` VALUES ('48', '15', 'admin', '沈陽日報');
INSERT INTO `sf_document_article` VALUES ('208', '15', 'admin', '沈陽日報');
INSERT INTO `sf_document_article` VALUES ('256', '65', '', '');
INSERT INTO `sf_document_article` VALUES ('257', '65', '', '');
INSERT INTO `sf_document_article` VALUES ('294', '79', '', '');

-- ----------------------------
-- Table structure for sf_document_bg
-- ----------------------------
DROP TABLE IF EXISTS `sf_document_bg`;
CREATE TABLE `sf_document_bg` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `bgpic` varchar(255) NOT NULL COMMENT '背景圖',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=304 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sf_document_bg
-- ----------------------------
INSERT INTO `sf_document_bg` VALUES ('263', '78');
INSERT INTO `sf_document_bg` VALUES ('264', '78');
INSERT INTO `sf_document_bg` VALUES ('265', '75');
INSERT INTO `sf_document_bg` VALUES ('266', '76');
INSERT INTO `sf_document_bg` VALUES ('267', '77');
INSERT INTO `sf_document_bg` VALUES ('298', '73');
INSERT INTO `sf_document_bg` VALUES ('299', '74');
INSERT INTO `sf_document_bg` VALUES ('300', '74');
INSERT INTO `sf_document_bg` VALUES ('301', '76');
INSERT INTO `sf_document_bg` VALUES ('303', '74');

-- ----------------------------
-- Table structure for sf_document_clause
-- ----------------------------
DROP TABLE IF EXISTS `sf_document_clause`;
CREATE TABLE `sf_document_clause` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `cid` int(10) unsigned NOT NULL COMMENT '標識',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=303 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sf_document_clause
-- ----------------------------
INSERT INTO `sf_document_clause` VALUES ('66', '0');
INSERT INTO `sf_document_clause` VALUES ('302', '0');

-- ----------------------------
-- Table structure for sf_document_contract
-- ----------------------------
DROP TABLE IF EXISTS `sf_document_contract`;
CREATE TABLE `sf_document_contract` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `name` varchar(255) NOT NULL COMMENT '姓名',
  `tel` varchar(100) NOT NULL COMMENT '電話',
  `email` varchar(255) NOT NULL COMMENT '電子郵箱',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=286 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sf_document_contract
-- ----------------------------
INSERT INTO `sf_document_contract` VALUES ('268', 'dsa', 'dsa', 'dsa');
INSERT INTO `sf_document_contract` VALUES ('272', '冰', '15345456', '4145545456');
INSERT INTO `sf_document_contract` VALUES ('273', '冰', '15345456', '4145545456');
INSERT INTO `sf_document_contract` VALUES ('274', '冰', '145645645', '54564561512@qq.com');
INSERT INTO `sf_document_contract` VALUES ('279', '123', '123', '123');
INSERT INTO `sf_document_contract` VALUES ('282', 'bing', '4684684411', '1546841351@qq.com');
INSERT INTO `sf_document_contract` VALUES ('284', '姓名 Name', '電話 Phone', '郵箱 Email');
INSERT INTO `sf_document_contract` VALUES ('285', '姓名 Name', '電話 Phone', '郵箱 Email');

-- ----------------------------
-- Table structure for sf_document_cus
-- ----------------------------
DROP TABLE IF EXISTS `sf_document_cus`;
CREATE TABLE `sf_document_cus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `tel` varchar(255) NOT NULL COMMENT '電話',
  `email` varchar(255) NOT NULL COMMENT '電子郵箱',
  `adds` varchar(255) NOT NULL COMMENT '地址',
  `homepage` varchar(255) NOT NULL COMMENT '主頁',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=296 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sf_document_cus
-- ----------------------------
INSERT INTO `sf_document_cus` VALUES ('293', 'dsa', 'sad', 'dsa', 'dsa');
INSERT INTO `sf_document_cus` VALUES ('295', '6633 3903 /6623 7250', 'sf@sfhousing.net', '澳門田畔街7號F嘉輝大廈地下', ' www.sfhousing.net');

-- ----------------------------
-- Table structure for sf_document_download
-- ----------------------------
DROP TABLE IF EXISTS `sf_document_download`;
CREATE TABLE `sf_document_download` (
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文檔ID',
  `parse` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '內容解析類型',
  `content` text NOT NULL COMMENT '下載詳細描述',
  `template` varchar(100) NOT NULL DEFAULT '' COMMENT '詳情頁顯示模板',
  `file_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件ID',
  `download` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '下載次數',
  `size` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文檔模型下載表';

-- ----------------------------
-- Records of sf_document_download
-- ----------------------------

-- ----------------------------
-- Table structure for sf_document_houses
-- ----------------------------
DROP TABLE IF EXISTS `sf_document_houses`;
CREATE TABLE `sf_document_houses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `pay` int(10) unsigned NOT NULL COMMENT '首付',
  `size` varchar(255) NOT NULL COMMENT '尺寸',
  `fee` int(10) unsigned NOT NULL COMMENT '交易費',
  `types` varchar(255) NOT NULL COMMENT '房型',
  `attribute` varchar(255) NOT NULL COMMENT '屬性',
  `tel` varchar(255) NOT NULL COMMENT '電話',
  `photo` varchar(255) NOT NULL COMMENT '實景圖',
  `tag` varchar(255) NOT NULL COMMENT '標簽',
  `names` text NOT NULL COMMENT '物業名稱',
  `photo2` varchar(255) NOT NULL COMMENT '外景圖',
  `title2` varchar(255) NOT NULL COMMENT '標題2',
  `hot` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '是否熱賣',
  `place` varchar(255) NOT NULL COMMENT '地理位置',
  `shi` varchar(50) NOT NULL COMMENT '室',
  `ting` varchar(50) NOT NULL COMMENT '廳',
  `wei` varchar(50) NOT NULL COMMENT '衛',
  `che` varchar(50) NOT NULL COMMENT '車庫',
  `unit` varchar(255) NOT NULL COMMENT '單位（貨幣）',
  `units` varchar(255) NOT NULL COMMENT '單位（面積單位）',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=313 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sf_document_houses
-- ----------------------------
INSERT INTO `sf_document_houses` VALUES ('201', '30', '80', '10', '1 室　 4 臥 　4 衛　 2 車庫', '準備中', '準備中', '72', '普通公寓,板塔結合,70年房權,限購', '華發水岸', '72', '準備中', '0', '', '1', '2', '2', '1', '$', '平方呎');
INSERT INTO `sf_document_houses` VALUES ('202', '30', '80', '10', '1 室　 4 臥 　4 衛　 2 車庫', '準備中', '準備中', '72', '70年房權,隔音墻,木地板,防水', '華發水岸', '72', '準備中', '0', '', '2', '2', '2', '2', '$', '平方呎');
INSERT INTO `sf_document_houses` VALUES ('203', '30', '80', '10', '1 室　 4 臥 　4 衛　 2 車庫', '準備中', '準備中', '72', '普通公寓,板塔結合,70年房權,限購', '華發水岸', '72', '準備中', '0', '', '2', '2', '2', '2', '$', '平方呎');
INSERT INTO `sf_document_houses` VALUES ('205', '30', '80', '10', '1 室　 4 臥 　4 衛　 2 車庫', '準備中', '準備中', '72', '地鋪', '富景華庭', '72', '金銀島', '0', '22.2063911443,113.5531185891', '2', '2', '1', '1', '$', '平方呎');
INSERT INTO `sf_document_houses` VALUES ('280', '12454', '1354', '12111', '', '朝南', '', '72,53', '', '', '72,17', '', '0', '22.1990535830,113.5450722396', '2', '3', '2', '1', '', '');
INSERT INTO `sf_document_houses` VALUES ('228', '50', '50', '500', '', '準備中', '準備中', '72', '', '', '71', '準備中', '0', '', '5', '5', '5', '5', '$', '平方呎');
INSERT INTO `sf_document_houses` VALUES ('304', '5000', '50', '50', '', '準備中', '準備中', '71', '', '', '72', '準備中', '0', '', '1', '2', '3', '5', '$', '平方呎');
INSERT INTO `sf_document_houses` VALUES ('305', '5000', '5000', '5000', '', '準備中', '準備中', '71', '70年房權,普通公寓', '', '72', '準備中', '0', '', '1', '2', '2', '3', '$', '平方呎');
INSERT INTO `sf_document_houses` VALUES ('306', '50', '200', '50', '', '準備中', '準備中', '71', '', '', '72', '準備中', '0', '', '1', '1', '3', '5', '$', '平方呎');
INSERT INTO `sf_document_houses` VALUES ('307', '50', '45646', '456454', '', '準備中', '準備中', '71', '70年房權,普通公寓,隔音墻', '', '72', '準備中', '0', '', '1', '2', '3', '1', '$', '平方呎');
INSERT INTO `sf_document_houses` VALUES ('308', '50', '50', '120', '', '準備中', '準備中', '71', '普通公寓,隔音墻,70年房權', '', '72', '準備中', '0', '', '1', '1', '1', '1', '$', '平方呎');
INSERT INTO `sf_document_houses` VALUES ('312', '0', '', '0', '', '', '', '', '隔音墻,70年房權,普通公寓', '', '', 'qqq', '0', '', '', '', '', '', '', '');

-- ----------------------------
-- Table structure for sf_document_logo
-- ----------------------------
DROP TABLE IF EXISTS `sf_document_logo`;
CREATE TABLE `sf_document_logo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `url` varchar(255) NOT NULL DEFAULT '1' COMMENT 'url',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=310 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sf_document_logo
-- ----------------------------
INSERT INTO `sf_document_logo` VALUES ('241', '1');
INSERT INTO `sf_document_logo` VALUES ('309', '1');

-- ----------------------------
-- Table structure for sf_document_ranges
-- ----------------------------
DROP TABLE IF EXISTS `sf_document_ranges`;
CREATE TABLE `sf_document_ranges` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `cc` varchar(255) NOT NULL DEFAULT '1' COMMENT 'cc',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=234 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sf_document_ranges
-- ----------------------------

-- ----------------------------
-- Table structure for sf_document_search
-- ----------------------------
DROP TABLE IF EXISTS `sf_document_search`;
CREATE TABLE `sf_document_search` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `name` varchar(255) NOT NULL COMMENT '條件',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=311 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sf_document_search
-- ----------------------------
INSERT INTO `sf_document_search` VALUES ('243', '');
INSERT INTO `sf_document_search` VALUES ('310', '');

-- ----------------------------
-- Table structure for sf_document_sellrantal
-- ----------------------------
DROP TABLE IF EXISTS `sf_document_sellrantal`;
CREATE TABLE `sf_document_sellrantal` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `street` varchar(255) NOT NULL COMMENT '街道',
  `range` varchar(255) NOT NULL COMMENT '區域',
  `types` varchar(255) NOT NULL COMMENT '類型',
  `floor` varchar(100) NOT NULL COMMENT '樓層',
  `house_type` varchar(255) NOT NULL COMMENT '護型',
  `rantaltime` varchar(255) NOT NULL COMMENT '租期',
  `statused` varchar(255) NOT NULL COMMENT '現狀',
  `fee` varchar(50) NOT NULL COMMENT '管理費',
  `address` varchar(255) NOT NULL COMMENT '物業地址',
  `finish` varchar(255) NOT NULL COMMENT '裝修程度',
  `device` varchar(255) NOT NULL COMMENT '配套設備',
  `foregift` varchar(255) NOT NULL COMMENT '訂金要求',
  `photo` varchar(255) NOT NULL COMMENT '物業圖片',
  `wname` varchar(255) NOT NULL COMMENT '委託人姓名',
  `tel` varchar(100) NOT NULL COMMENT '委託人電話',
  `email` varchar(255) NOT NULL COMMENT '委託人郵箱',
  `adds` varchar(255) NOT NULL COMMENT '委託人地址',
  `area` varchar(255) NOT NULL COMMENT '面積',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=312 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sf_document_sellrantal
-- ----------------------------
INSERT INTO `sf_document_sellrantal` VALUES ('195', '測試', '前山', '經濟這用房', '第四層', '1室1廳1廁', '出售不填', '出售不填', '100', '測試', '無裝修', '無設備', '橙色是', '', '的撒', '的撒', 'dsa1', '橙色踩踩踩', '100');
INSERT INTO `sf_document_sellrantal` VALUES ('196', '的颯颯', '井岸', '公寓', '第三層', '1室1廳1廁', '出售不填', '出售不填', '100', '的是的撒的', '無裝修', '無設備', '的薩達', '15,16', '的颯颯', '的撒', '的撒打算', 'ds的撒', '150');
INSERT INTO `sf_document_sellrantal` VALUES ('231', '珠海大橋東', '華發', '公寓', '第三層', '1室1廳1廁', '二年', '自用', '200', '珠海大橋東側物業大樓C座', '中檔裝修', '單人床,衣櫃,洗衣機', '沒要求', '19,17,14', '陳先生', '13523654982', '125555@qq.com', '珠海大橋東側物業大樓C座', '150');
INSERT INTO `sf_document_sellrantal` VALUES ('254', '', '前山', '豪宅', '第二層', '1室1廳1廁', '出售不填', '出售不填', '100mop', '', '無裝修', '無設備', '', '', '', '', '', '', '100');
INSERT INTO `sf_document_sellrantal` VALUES ('255', '', '11', '11', '11', '1室1廳1廁', '出售不填', '出售不填', '100mop', '', '無裝修', '無設備', '', '', '', '', '', '', '11');
INSERT INTO `sf_document_sellrantal` VALUES ('269', '', '請選擇', '請選擇', '請選擇', '1室1廳1廁', '一年', '吉屋', '100mop', '', '無裝修', '無設備', '', '', '', '', '', '', '請選擇');
INSERT INTO `sf_document_sellrantal` VALUES ('270', '', '請選擇', '請選擇', '請選擇', '1室1廳1廁', '一年', '吉屋', '100mop', '', '無裝修', '無設備', '', '', '', '', '', '', '請選擇');
INSERT INTO `sf_document_sellrantal` VALUES ('271', 'dsfvdgv', '請選擇', '請選擇', '請選擇', '1室1廳1廁', '一年', '吉屋', '100mop', '', '無裝修', '無設備', '', '', '', '', '', '', '請選擇');
INSERT INTO `sf_document_sellrantal` VALUES ('275', '', '請選擇', '請選擇', '請選擇', '1室1廳1廁', '一年', '吉屋', '100mop', '', '無裝修', '無設備', '', '', '', '', '', '', '請選擇');
INSERT INTO `sf_document_sellrantal` VALUES ('276', '', '請選擇', '請選擇', '請選擇', '1室1廳1廁', '一年', '吉屋', '100mop', '', '無裝修', '無設備', '', '', '', '', '', '', '請選擇');
INSERT INTO `sf_document_sellrantal` VALUES ('277', '', '請選擇', '請選擇', '請選擇', '1室1廳1廁', '一年', '吉屋', '100mop', '', '無裝修', '無設備', '', '', '', '', '', '', '請選擇');
INSERT INTO `sf_document_sellrantal` VALUES ('278', '123313', '澳門', '其他', '請選擇', '1室1廳1廁', '出售不填', '出售不填', '100mop', '123123123', '無裝修', '無設備', '123', '', '123123', '123123123', '123123123', '123123', '請選擇');
INSERT INTO `sf_document_sellrantal` VALUES ('281', '拱北', '拱北', '經濟適用房', '第二層', '2室2廳2廁', '一年', '吉屋', '100mop', '拱北名門大廈', '無裝修', '冰箱,單人床,冷氣', '押一付一', '17', 'bing', '1534564561', '4654651531@qq.com', '拱北', '500平方呎');
INSERT INTO `sf_document_sellrantal` VALUES ('283', '', '請選擇', '請選擇', '請選擇', '1室1廳1廁', '一年', '吉屋', '100mop', '', '無裝修', '無設備', '', '', '', '', '', '', '請選擇');
INSERT INTO `sf_document_sellrantal` VALUES ('286', '', '請選擇', '請選擇', '請選擇', '1室1廳1廁', '一年', '吉屋', '100mop', '', '無裝修', '無設備', '', '', '', '', '', '', '請選擇');
INSERT INTO `sf_document_sellrantal` VALUES ('287', '', '華發', '請選擇', '請選擇', '1室1廳1廁', '五年', '吉屋', '100mop', '', '無裝修', '電視機,冰箱,單人床,雙人床', '', '80', '', '', '', '', '請選擇');
INSERT INTO `sf_document_sellrantal` VALUES ('288', '', '請選擇', '請選擇', '請選擇', '1室1廳1廁', '出售不填', '出售不填', '100mop', '', '無裝修', '無設備', '', '', '', '', '', '', '請選擇');
INSERT INTO `sf_document_sellrantal` VALUES ('289', '', '請選擇', '請選擇', '請選擇', '1室1廳1廁', '出售不填', '出售不填', '100mop', '', '無裝修', '無設備', '', '', '', '', '', '', '請選擇');
INSERT INTO `sf_document_sellrantal` VALUES ('290', '', '請選擇', '請選擇', '請選擇', '1室1廳1廁', '出售不填', '出售不填', '100mop', '', '無裝修', '無設備', '', '', '', '', '', '', '請選擇');
INSERT INTO `sf_document_sellrantal` VALUES ('291', 'DVC', '橫琴', '請選擇', '請選擇', '1室1廳1廁', '一年', '吉屋', '100mop', '', '無裝修', '無設備', '', '', '', '', '', '', '50平方米');
INSERT INTO `sf_document_sellrantal` VALUES ('311', 'asdasd', '拱北', '豪宅', '第一層', '1室1廳1廁', '一年', '吉屋', '100mop', 'asdsa', '無裝修', '冰箱', '5456', '', '', '', '', '', '50平方米');

-- ----------------------------
-- Table structure for sf_file
-- ----------------------------
DROP TABLE IF EXISTS `sf_file`;
CREATE TABLE `sf_file` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文件ID',
  `name` char(30) NOT NULL DEFAULT '' COMMENT '原始文件名',
  `savename` char(20) NOT NULL DEFAULT '' COMMENT '保存名稱',
  `savepath` char(30) NOT NULL DEFAULT '' COMMENT '文件保存路徑',
  `ext` char(5) NOT NULL DEFAULT '' COMMENT '文件後綴',
  `mime` char(40) NOT NULL DEFAULT '' COMMENT '文件mime類型',
  `size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `md5` char(32) NOT NULL DEFAULT '' COMMENT '文件md5',
  `sha1` char(40) NOT NULL DEFAULT '' COMMENT '文件 sha1編碼',
  `location` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '文件保存位置',
  `create_time` int(10) unsigned NOT NULL COMMENT '上傳時間',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_md5` (`md5`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文件表';

-- ----------------------------
-- Records of sf_file
-- ----------------------------

-- ----------------------------
-- Table structure for sf_hooks
-- ----------------------------
DROP TABLE IF EXISTS `sf_hooks`;
CREATE TABLE `sf_hooks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `name` varchar(40) NOT NULL DEFAULT '' COMMENT '鉤子名稱',
  `description` text NOT NULL COMMENT '描述',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '類型',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `addons` varchar(255) NOT NULL DEFAULT '' COMMENT '鉤子掛載的插件 ''，''分割',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sf_hooks
-- ----------------------------
INSERT INTO `sf_hooks` VALUES ('1', 'pageHeader', '頁面header鉤子，壹般用於加載插件CSS文件和代碼', '1', '0', '');
INSERT INTO `sf_hooks` VALUES ('2', 'pageFooter', '頁面footer鉤子，壹般用於加載插件JS文件和JS代碼', '1', '0', 'ReturnTop');
INSERT INTO `sf_hooks` VALUES ('3', 'documentEditForm', '添加編輯表單的 擴展內容鉤子', '1', '0', 'Attachment');
INSERT INTO `sf_hooks` VALUES ('4', 'documentDetailAfter', '文檔末尾顯示', '1', '0', 'Attachment,SocialComment');
INSERT INTO `sf_hooks` VALUES ('5', 'documentDetailBefore', '頁面內容前顯示用鉤子', '1', '0', '');
INSERT INTO `sf_hooks` VALUES ('6', 'documentSaveComplete', '保存文檔數據後的擴展鉤子', '2', '0', 'Attachment');
INSERT INTO `sf_hooks` VALUES ('7', 'documentEditFormContent', '添加編輯表單的內容顯示鉤子', '1', '0', 'Editor');
INSERT INTO `sf_hooks` VALUES ('8', 'adminArticleEdit', '後臺內容編輯頁編輯器', '1', '1378982734', 'EditorForAdmin');
INSERT INTO `sf_hooks` VALUES ('13', 'AdminIndex', '首頁小格子個性化顯示', '1', '1382596073', 'SiteStat,SystemInfo,DevTeam');
INSERT INTO `sf_hooks` VALUES ('14', 'topicComment', '評論提交方式擴展鉤子。', '1', '1380163518', 'Editor');
INSERT INTO `sf_hooks` VALUES ('16', 'app_begin', '應用開始', '2', '1384481614', '');
INSERT INTO `sf_hooks` VALUES ('17', 'UploadImages', '多圖上傳\r\n', '1', '1428718510', 'UploadImages');

-- ----------------------------
-- Table structure for sf_member
-- ----------------------------
DROP TABLE IF EXISTS `sf_member`;
CREATE TABLE `sf_member` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用戶ID',
  `nickname` char(16) NOT NULL DEFAULT '' COMMENT '昵稱',
  `sex` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '性別',
  `birthday` date NOT NULL DEFAULT '0000-00-00' COMMENT '生日',
  `qq` char(10) NOT NULL DEFAULT '' COMMENT 'qq號',
  `score` mediumint(8) NOT NULL DEFAULT '0' COMMENT '用戶積分',
  `login` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '登錄次數',
  `reg_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '註冊IP',
  `reg_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '註冊時間',
  `last_login_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '最後登錄IP',
  `last_login_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最後登錄時間',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '會員狀態',
  PRIMARY KEY (`uid`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='會員表';

-- ----------------------------
-- Records of sf_member
-- ----------------------------
INSERT INTO `sf_member` VALUES ('1', 'admin', '0', '0000-00-00', '', '240', '205', '0', '1428718406', '0', '1439873162', '1');
INSERT INTO `sf_member` VALUES ('2', 'user', '0', '0000-00-00', '', '20', '11', '0', '0', '0', '1432719746', '1');
INSERT INTO `sf_member` VALUES ('3', 'user2', '0', '0000-00-00', '', '10', '4', '0', '0', '0', '1430991349', '1');
INSERT INTO `sf_member` VALUES ('4', '123', '0', '0000-00-00', '', '10', '2', '0', '0', '3400479379', '1436934171', '1');
INSERT INTO `sf_member` VALUES ('5', 'bing', '0', '0000-00-00', '', '20', '5', '0', '0', '0', '1439778163', '1');
INSERT INTO `sf_member` VALUES ('6', 'admin2', '0', '0000-00-00', '', '10', '1', '0', '0', '2130706433', '1438671600', '1');
INSERT INTO `sf_member` VALUES ('7', 'aaa', '0', '0000-00-00', '', '20', '6', '0', '0', '0', '1439799213', '1');
INSERT INTO `sf_member` VALUES ('8', 'AAAA', '0', '0000-00-00', '', '10', '1', '0', '0', '0', '1439875013', '1');

-- ----------------------------
-- Table structure for sf_menu
-- ----------------------------
DROP TABLE IF EXISTS `sf_menu`;
CREATE TABLE `sf_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文檔ID',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '標題',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上級分類ID',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序（同級有效）',
  `url` char(255) NOT NULL DEFAULT '' COMMENT '鏈接地址',
  `hide` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否隱藏',
  `tip` varchar(255) NOT NULL DEFAULT '' COMMENT '提示',
  `group` varchar(50) DEFAULT '' COMMENT '分組',
  `is_dev` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否僅開發者模式可見',
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=124 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sf_menu
-- ----------------------------
INSERT INTO `sf_menu` VALUES ('2', '網站管理', '0', '2', 'Article/houseslist', '0', '', '', '0');
INSERT INTO `sf_menu` VALUES ('3', '文檔列表', '2', '0', 'article/index', '1', '', '內容', '0');
INSERT INTO `sf_menu` VALUES ('4', '新增', '3', '0', 'article/add', '0', '', '', '0');
INSERT INTO `sf_menu` VALUES ('5', '編輯', '3', '0', 'article/edit', '0', '', '', '0');
INSERT INTO `sf_menu` VALUES ('6', '改變狀態', '3', '0', 'article/setStatus', '0', '', '', '0');
INSERT INTO `sf_menu` VALUES ('7', '保存', '3', '0', 'article/update', '0', '', '', '0');
INSERT INTO `sf_menu` VALUES ('8', '保存草稿', '3', '0', 'article/autoSave', '0', '', '', '0');
INSERT INTO `sf_menu` VALUES ('9', '移動', '3', '0', 'article/move', '0', '', '', '0');
INSERT INTO `sf_menu` VALUES ('10', '復制', '3', '0', 'article/copy', '0', '', '', '0');
INSERT INTO `sf_menu` VALUES ('11', '粘貼', '3', '0', 'article/paste', '0', '', '', '0');
INSERT INTO `sf_menu` VALUES ('12', '導入', '3', '0', 'article/batchOperate', '0', '', '', '0');
INSERT INTO `sf_menu` VALUES ('13', '回收站', '2', '0', 'article/recycle', '1', '', '內容', '0');
INSERT INTO `sf_menu` VALUES ('14', '還原', '13', '0', 'article/permit', '0', '', '', '0');
INSERT INTO `sf_menu` VALUES ('15', '清空', '13', '0', 'article/clear', '0', '', '', '0');
INSERT INTO `sf_menu` VALUES ('16', '用戶', '0', '3', 'User/index', '0', '', '', '0');
INSERT INTO `sf_menu` VALUES ('17', '用戶信息', '16', '0', 'User/index', '0', '', '用戶管理', '0');
INSERT INTO `sf_menu` VALUES ('18', '新增用戶', '17', '0', 'User/add', '0', '添加新用戶', '', '0');
INSERT INTO `sf_menu` VALUES ('19', '用戶行為', '16', '0', 'User/action', '0', '', '行為管理', '0');
INSERT INTO `sf_menu` VALUES ('20', '新增用戶行為', '19', '0', 'User/addaction', '0', '', '', '0');
INSERT INTO `sf_menu` VALUES ('21', '編輯用戶行為', '19', '0', 'User/editaction', '0', '', '', '0');
INSERT INTO `sf_menu` VALUES ('22', '保存用戶行為', '19', '0', 'User/saveAction', '0', '\"用戶->用戶行為\"保存編輯和新增的用戶行為', '', '0');
INSERT INTO `sf_menu` VALUES ('23', '變更行為狀態', '19', '0', 'User/setStatus', '0', '\"用戶->用戶行為\"中的啟用,禁用和刪除權限', '', '0');
INSERT INTO `sf_menu` VALUES ('24', '禁用會員', '19', '0', 'User/changeStatus?method=forbidUser', '0', '\"用戶->用戶信息\"中的禁用', '', '0');
INSERT INTO `sf_menu` VALUES ('25', '啟用會員', '19', '0', 'User/changeStatus?method=resumeUser', '0', '\"用戶->用戶信息\"中的啟用', '', '0');
INSERT INTO `sf_menu` VALUES ('26', '刪除會員', '19', '0', 'User/changeStatus?method=deleteUser', '0', '\"用戶->用戶信息\"中的刪除', '', '0');
INSERT INTO `sf_menu` VALUES ('27', '權限管理', '16', '0', 'AuthManager/index', '0', '', '用戶管理', '0');
INSERT INTO `sf_menu` VALUES ('28', '刪除', '27', '0', 'AuthManager/changeStatus?method=deleteGroup', '0', '刪除用戶組', '', '0');
INSERT INTO `sf_menu` VALUES ('29', '禁用', '27', '0', 'AuthManager/changeStatus?method=forbidGroup', '0', '禁用用戶組', '', '0');
INSERT INTO `sf_menu` VALUES ('30', '恢復', '27', '0', 'AuthManager/changeStatus?method=resumeGroup', '0', '恢復已禁用的用戶組', '', '0');
INSERT INTO `sf_menu` VALUES ('31', '新增', '27', '0', 'AuthManager/createGroup', '0', '創建新的用戶組', '', '0');
INSERT INTO `sf_menu` VALUES ('32', '編輯', '27', '0', 'AuthManager/editGroup', '0', '編輯用戶組名稱和描述', '', '0');
INSERT INTO `sf_menu` VALUES ('33', '保存用戶組', '27', '0', 'AuthManager/writeGroup', '0', '新增和編輯用戶組的\"保存\"按鈕', '', '0');
INSERT INTO `sf_menu` VALUES ('34', '授權', '27', '0', 'AuthManager/group', '0', '\"後臺 \\ 用戶 \\ 用戶信息\"列表頁的\"授權\"操作按鈕,用於設置用戶所屬用戶組', '', '0');
INSERT INTO `sf_menu` VALUES ('35', '訪問授權', '27', '0', 'AuthManager/access', '0', '\"後臺 \\ 用戶 \\ 權限管理\"列表頁的\"訪問授權\"操作按鈕', '', '0');
INSERT INTO `sf_menu` VALUES ('36', '成員授權', '27', '0', 'AuthManager/user', '0', '\"後臺 \\ 用戶 \\ 權限管理\"列表頁的\"成員授權\"操作按鈕', '', '0');
INSERT INTO `sf_menu` VALUES ('37', '解除授權', '27', '0', 'AuthManager/removeFromGroup', '0', '\"成員授權\"列表頁內的解除授權操作按鈕', '', '0');
INSERT INTO `sf_menu` VALUES ('38', '保存成員授權', '27', '0', 'AuthManager/addToGroup', '0', '\"用戶信息\"列表頁\"授權\"時的\"保存\"按鈕和\"成員授權\"裏右上角的\"添加\"按鈕)', '', '0');
INSERT INTO `sf_menu` VALUES ('39', '分類授權', '27', '0', 'AuthManager/category', '0', '\"後臺 \\ 用戶 \\ 權限管理\"列表頁的\"分類授權\"操作按鈕', '', '0');
INSERT INTO `sf_menu` VALUES ('40', '保存分類授權', '27', '0', 'AuthManager/addToCategory', '0', '\"分類授權\"頁面的\"保存\"按鈕', '', '0');
INSERT INTO `sf_menu` VALUES ('41', '模型授權', '27', '0', 'AuthManager/modelauth', '0', '\"後臺 \\ 用戶 \\ 權限管理\"列表頁的\"模型授權\"操作按鈕', '', '0');
INSERT INTO `sf_menu` VALUES ('42', '保存模型授權', '27', '0', 'AuthManager/addToModel', '0', '\"分類授權\"頁面的\"保存\"按鈕', '', '0');
INSERT INTO `sf_menu` VALUES ('43', '擴展', '0', '7', 'Addons/index', '0', '', '', '0');
INSERT INTO `sf_menu` VALUES ('44', '插件管理', '43', '1', 'Addons/index', '0', '', '擴展', '0');
INSERT INTO `sf_menu` VALUES ('45', '創建', '44', '0', 'Addons/create', '0', '服務器上創建插件結構向導', '', '0');
INSERT INTO `sf_menu` VALUES ('46', '檢測創建', '44', '0', 'Addons/checkForm', '0', '檢測插件是否可以創建', '', '0');
INSERT INTO `sf_menu` VALUES ('47', '預覽', '44', '0', 'Addons/preview', '0', '預覽插件定義類文件', '', '0');
INSERT INTO `sf_menu` VALUES ('48', '快速生成插件', '44', '0', 'Addons/build', '0', '開始生成插件結構', '', '0');
INSERT INTO `sf_menu` VALUES ('49', '設置', '44', '0', 'Addons/config', '0', '設置插件配置', '', '0');
INSERT INTO `sf_menu` VALUES ('50', '禁用', '44', '0', 'Addons/disable', '0', '禁用插件', '', '0');
INSERT INTO `sf_menu` VALUES ('51', '啟用', '44', '0', 'Addons/enable', '0', '啟用插件', '', '0');
INSERT INTO `sf_menu` VALUES ('52', '安裝', '44', '0', 'Addons/install', '0', '安裝插件', '', '0');
INSERT INTO `sf_menu` VALUES ('53', '卸載', '44', '0', 'Addons/uninstall', '0', '卸載插件', '', '0');
INSERT INTO `sf_menu` VALUES ('54', '更新配置', '44', '0', 'Addons/saveconfig', '0', '更新插件配置處理', '', '0');
INSERT INTO `sf_menu` VALUES ('55', '插件後臺列表', '44', '0', 'Addons/adminList', '0', '', '', '0');
INSERT INTO `sf_menu` VALUES ('56', 'URL方式訪問插件', '44', '0', 'Addons/execute', '0', '控制是否有權限通過url訪問插件控制器方法', '', '0');
INSERT INTO `sf_menu` VALUES ('57', '鉤子管理', '43', '2', 'Addons/hooks', '0', '', '擴展', '0');
INSERT INTO `sf_menu` VALUES ('58', '模型管理', '68', '3', 'Model/index', '0', '', '系統設置', '0');
INSERT INTO `sf_menu` VALUES ('59', '新增', '58', '0', 'model/add', '0', '', '', '0');
INSERT INTO `sf_menu` VALUES ('60', '編輯', '58', '0', 'model/edit', '0', '', '', '0');
INSERT INTO `sf_menu` VALUES ('61', '改變狀態', '58', '0', 'model/setStatus', '0', '', '', '0');
INSERT INTO `sf_menu` VALUES ('62', '保存數據', '58', '0', 'model/update', '0', '', '', '0');
INSERT INTO `sf_menu` VALUES ('63', '屬性管理', '68', '0', 'Attribute/index', '1', '網站屬性配置。', '', '0');
INSERT INTO `sf_menu` VALUES ('64', '新增', '63', '0', 'Attribute/add', '0', '', '', '0');
INSERT INTO `sf_menu` VALUES ('65', '編輯', '63', '0', 'Attribute/edit', '0', '', '', '0');
INSERT INTO `sf_menu` VALUES ('66', '改變狀態', '63', '0', 'Attribute/setStatus', '0', '', '', '0');
INSERT INTO `sf_menu` VALUES ('67', '保存數據', '63', '0', 'Attribute/update', '0', '', '', '0');
INSERT INTO `sf_menu` VALUES ('68', '系統管理', '0', '4', 'Config/group', '0', '', '', '0');
INSERT INTO `sf_menu` VALUES ('69', '網站設置', '68', '1', 'Config/group', '0', '', '系統設置', '0');
INSERT INTO `sf_menu` VALUES ('70', '配置管理', '68', '4', 'Config/index', '0', '', '系統設置', '0');
INSERT INTO `sf_menu` VALUES ('71', '編輯', '70', '0', 'Config/edit', '0', '新增編輯和保存配置', '', '0');
INSERT INTO `sf_menu` VALUES ('72', '刪除', '70', '0', 'Config/del', '0', '刪除配置', '', '0');
INSERT INTO `sf_menu` VALUES ('73', '新增', '70', '0', 'Config/add', '0', '新增配置', '', '0');
INSERT INTO `sf_menu` VALUES ('74', '保存', '70', '0', 'Config/save', '0', '保存配置', '', '0');
INSERT INTO `sf_menu` VALUES ('75', '菜單管理', '68', '5', 'Menu/index', '0', '', '系統設置', '0');
INSERT INTO `sf_menu` VALUES ('76', '導航管理', '68', '6', 'Channel/index', '0', '', '系統設置', '0');
INSERT INTO `sf_menu` VALUES ('77', '新增', '76', '0', 'Channel/add', '0', '', '', '0');
INSERT INTO `sf_menu` VALUES ('78', '編輯', '76', '0', 'Channel/edit', '0', '', '', '0');
INSERT INTO `sf_menu` VALUES ('79', '刪除', '76', '0', 'Channel/del', '0', '', '', '0');
INSERT INTO `sf_menu` VALUES ('80', '分類管理', '68', '2', 'Category/index', '0', '', '系統設置', '0');
INSERT INTO `sf_menu` VALUES ('81', '編輯', '80', '0', 'Category/edit', '0', '編輯和保存欄目分類', '', '0');
INSERT INTO `sf_menu` VALUES ('82', '新增', '80', '0', 'Category/add', '0', '新增欄目分類', '', '0');
INSERT INTO `sf_menu` VALUES ('83', '刪除', '80', '0', 'Category/remove', '0', '刪除欄目分類', '', '0');
INSERT INTO `sf_menu` VALUES ('84', '移動', '80', '0', 'Category/operate/type/move', '0', '移動欄目分類', '', '0');
INSERT INTO `sf_menu` VALUES ('85', '合並', '80', '0', 'Category/operate/type/merge', '0', '合並欄目分類', '', '0');
INSERT INTO `sf_menu` VALUES ('86', '備份數據庫', '68', '0', 'Database/index?type=export', '0', '', '數據備份', '0');
INSERT INTO `sf_menu` VALUES ('87', '備份', '86', '0', 'Database/export', '0', '備份數據庫', '', '0');
INSERT INTO `sf_menu` VALUES ('88', '優化表', '86', '0', 'Database/optimize', '0', '優化數據表', '', '0');
INSERT INTO `sf_menu` VALUES ('89', '修復表', '86', '0', 'Database/repair', '0', '修復數據表', '', '0');
INSERT INTO `sf_menu` VALUES ('90', '還原數據庫', '68', '0', 'Database/index?type=import', '0', '', '數據備份', '0');
INSERT INTO `sf_menu` VALUES ('91', '恢復', '90', '0', 'Database/import', '0', '數據庫恢復', '', '0');
INSERT INTO `sf_menu` VALUES ('92', '刪除', '90', '0', 'Database/del', '0', '刪除備份文件', '', '0');
INSERT INTO `sf_menu` VALUES ('93', '其他', '0', '5', 'other', '1', '', '', '0');
INSERT INTO `sf_menu` VALUES ('96', '新增', '75', '0', 'Menu/add', '0', '', '系統設置', '0');
INSERT INTO `sf_menu` VALUES ('98', '編輯', '75', '0', 'Menu/edit', '0', '', '', '0');
INSERT INTO `sf_menu` VALUES ('104', '下載管理', '102', '0', 'Think/lists?model=download', '0', '', '', '0');
INSERT INTO `sf_menu` VALUES ('105', '配置管理', '102', '0', 'Think/lists?model=config', '0', '', '', '0');
INSERT INTO `sf_menu` VALUES ('106', '行為日誌', '16', '0', 'Action/actionlog', '0', '', '行為管理', '0');
INSERT INTO `sf_menu` VALUES ('108', '修改密碼', '16', '0', 'User/updatePassword', '1', '', '', '0');
INSERT INTO `sf_menu` VALUES ('109', '修改昵稱', '16', '0', 'User/updateNickname', '1', '', '', '0');
INSERT INTO `sf_menu` VALUES ('110', '查看行為日誌', '106', '0', 'action/edit', '1', '', '', '0');
INSERT INTO `sf_menu` VALUES ('112', '新增數據', '58', '0', 'think/add', '1', '', '', '0');
INSERT INTO `sf_menu` VALUES ('113', '編輯數據', '58', '0', 'think/edit', '1', '', '', '0');
INSERT INTO `sf_menu` VALUES ('114', '導入', '75', '0', 'Menu/import', '0', '', '', '0');
INSERT INTO `sf_menu` VALUES ('115', '生成', '58', '0', 'Model/generate', '0', '', '', '0');
INSERT INTO `sf_menu` VALUES ('116', '新增鉤子', '57', '0', 'Addons/addHook', '0', '', '', '0');
INSERT INTO `sf_menu` VALUES ('117', '編輯鉤子', '57', '0', 'Addons/edithook', '0', '', '', '0');
INSERT INTO `sf_menu` VALUES ('118', '文檔排序', '3', '0', 'Article/sort', '1', '', '', '0');
INSERT INTO `sf_menu` VALUES ('119', '排序', '70', '0', 'Config/sort', '1', '', '', '0');
INSERT INTO `sf_menu` VALUES ('120', '排序', '75', '0', 'Menu/sort', '1', '', '', '0');
INSERT INTO `sf_menu` VALUES ('121', '排序', '76', '0', 'Channel/sort', '1', '', '', '0');

-- ----------------------------
-- Table structure for sf_model
-- ----------------------------
DROP TABLE IF EXISTS `sf_model`;
CREATE TABLE `sf_model` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '模型ID',
  `name` char(30) NOT NULL DEFAULT '' COMMENT '模型標識',
  `title` char(30) NOT NULL DEFAULT '' COMMENT '模型名稱',
  `extend` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '繼承的模型',
  `relation` varchar(30) NOT NULL DEFAULT '' COMMENT '繼承與被繼承模型的關聯字段',
  `need_pk` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '新建表時是否需要主鍵字段',
  `field_sort` text NOT NULL COMMENT '表單字段排序',
  `field_group` varchar(255) NOT NULL DEFAULT '1:基礎' COMMENT '字段分組',
  `attribute_list` text NOT NULL COMMENT '屬性列表（表的字段）',
  `template_list` varchar(100) NOT NULL DEFAULT '' COMMENT '列表模板',
  `template_add` varchar(100) NOT NULL DEFAULT '' COMMENT '新增模板',
  `template_edit` varchar(100) NOT NULL DEFAULT '' COMMENT '編輯模板',
  `list_grid` text NOT NULL COMMENT '列表定義',
  `list_row` smallint(2) unsigned NOT NULL DEFAULT '10' COMMENT '列表數據長度',
  `search_key` varchar(50) NOT NULL DEFAULT '' COMMENT '默認搜索字段',
  `search_list` varchar(255) NOT NULL DEFAULT '' COMMENT '高級搜索的字段',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '狀態',
  `engine_type` varchar(25) NOT NULL DEFAULT 'MyISAM' COMMENT '數據庫引擎',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='文檔模型表';

-- ----------------------------
-- Records of sf_model
-- ----------------------------
INSERT INTO `sf_model` VALUES ('1', 'document', '基礎文檔', '0', '', '1', '{\"1\":[\"102\",\"100\",\"101\",\"72\",\"62\",\"61\",\"60\",\"59\",\"55\",\"48\",\"2\",\"3\",\"5\",\"9\",\"10\",\"11\",\"12\",\"13\",\"14\",\"16\",\"17\",\"19\",\"20\"]}', '1:基礎', '', '', '', '', 'id:編號\r\ntitle:標題:[EDIT]&cate_id=[category_id]\r\nupdate_time|time_format:最後更新\r\nid:操作:[EDIT]&cate_id=[category_id]|編輯,article/setstatus?status=-1&ids=[id]|刪除', '0', '', '', '1383891233', '1432106155', '1', 'MyISAM');
INSERT INTO `sf_model` VALUES ('2', 'article', '文章', '1', '', '1', '{\"1\":[\"3\",\"53\",\"52\",\"5\",\"33\",\"20\",\"55\"],\"2\":[\"9\",\"109\",\"72\",\"12\",\"100\",\"101\",\"102\",\"13\",\"19\",\"10\",\"60\",\"59\",\"61\",\"16\",\"17\",\"62\",\"2\",\"48\",\"14\",\"11\"]}', '1:基礎,2:擴展', '', '', '', '', 'id:編號\r\n', '0', '', '', '1383891243', '1438593750', '1', 'MyISAM');
INSERT INTO `sf_model` VALUES ('3', 'download', '下載', '1', '', '1', '{\"1\":[\"3\",\"28\",\"30\",\"32\",\"2\",\"5\",\"31\"],\"2\":[\"13\",\"109\",\"10\",\"9\",\"48\",\"59\",\"61\",\"60\",\"55\",\"62\",\"101\",\"100\",\"72\",\"102\",\"12\",\"16\",\"17\",\"19\",\"11\",\"20\",\"14\",\"29\"]}', '1:基礎,2:擴展', '', '', '', '', 'id:編號\r\ntitle:標題', '0', '', '', '1383891252', '1438593740', '1', 'MyISAM');
INSERT INTO `sf_model` VALUES ('4', 'houses', '樓盤', '1', '', '1', '{\"1\":[\"107\",\"106\",\"98\",\"97\",\"96\",\"95\",\"94\",\"43\",\"3\",\"46\",\"44\",\"59\",\"72\",\"62\",\"61\",\"60\",\"35\",\"36\",\"37\",\"48\",\"38\",\"39\",\"40\",\"5\",\"41\",\"45\",\"20\"],\"2\":[\"109\",\"17\",\"19\",\"47\",\"16\",\"102\",\"100\",\"55\",\"101\",\"2\",\"14\",\"11\",\"10\",\"13\",\"12\",\"9\"]}', '1:基礎;2:擴展', '', '', '', '', 'id:編號', '10', '', '', '1428736788', '1438593708', '1', 'MyISAM');
INSERT INTO `sf_model` VALUES ('5', 'clause', '條款', '1', '', '1', '{\"1\":[\"3\",\"55\",\"20\"],\"2\":[\"60\",\"109\",\"101\",\"100\",\"102\",\"59\",\"17\",\"72\",\"61\",\"2\",\"62\",\"56\",\"19\",\"48\",\"16\",\"13\",\"14\",\"12\",\"9\",\"11\",\"10\",\"5\"]}', '1:基礎;2:擴展', '', '', '', '', 'id:編號', '10', '', '', '1429600236', '1438593718', '1', 'MyISAM');
INSERT INTO `sf_model` VALUES ('6', 'search', '條件搜索', '1', '', '1', '{\"1\":[\"3\",\"100\",\"101\",\"102\"],\"2\":[\"2\",\"109\",\"57\",\"5\",\"72\",\"20\",\"60\",\"59\",\"16\",\"62\",\"61\",\"55\",\"48\",\"19\",\"17\",\"13\",\"12\",\"14\",\"10\",\"9\",\"11\"]}', '1:基礎;2:擴展', '', '', '', '', '編號：id', '10', '', '', '1429758404', '1438593679', '1', 'MyISAM');
INSERT INTO `sf_model` VALUES ('9', 'ranges', '範圍', '1', '', '1', '{\"1\":[\"102\",\"101\",\"100\",\"3\"],\"2\":[\"2\",\"109\",\"55\",\"62\",\"11\",\"48\",\"61\",\"60\",\"72\",\"59\",\"17\",\"20\",\"19\",\"16\",\"13\",\"14\",\"70\",\"12\",\"10\",\"9\",\"5\"]}', '1:基礎;2:擴展', '', '', '', '', 'id:編號', '10', '', '', '1429860289', '1438593672', '1', 'MyISAM');
INSERT INTO `sf_model` VALUES ('10', 'sellrantal', '收集樓盤', '1', '', '1', '{\"1\":[\"3\",\"73\",\"74\",\"75\",\"76\",\"77\",\"90\",\"78\",\"79\",\"80\",\"81\",\"82\",\"83\",\"84\",\"85\",\"86\",\"87\",\"88\",\"89\",\"5\",\"20\"],\"2\":[\"62\",\"109\",\"101\",\"100\",\"102\",\"61\",\"60\",\"55\",\"2\",\"10\",\"9\",\"12\",\"11\",\"13\",\"16\",\"14\",\"72\",\"17\",\"19\",\"48\",\"59\"]}', '1:基礎;2:擴展', '', '', '', '', '編號:id', '10', '', '', '1430200599', '1438593665', '1', 'MyISAM');
INSERT INTO `sf_model` VALUES ('11', 'contract', '留言板', '1', '', '1', '{\"1\":[\"3\",\"91\",\"92\",\"93\",\"5\"],\"2\":[\"2\",\"109\",\"101\",\"100\",\"102\",\"9\",\"10\",\"11\",\"72\",\"62\",\"55\",\"61\",\"60\",\"59\",\"48\",\"19\",\"12\",\"20\",\"17\",\"16\",\"14\",\"13\"]}', '1:基礎;2:擴展', '', '', '', '', 'id:編號', '10', '', '', '1430278090', '1438595214', '1', 'MyISAM');
INSERT INTO `sf_model` VALUES ('12', 'logo', '商標', '1', '', '1', '{\"1\":[\"3\",\"12\"],\"2\":[\"101\",\"109\",\"99\",\"100\",\"102\",\"5\",\"13\",\"72\",\"16\",\"61\",\"62\",\"60\",\"59\",\"55\",\"48\",\"20\",\"19\",\"17\",\"14\",\"9\",\"11\",\"10\",\"2\"]}', '1:基礎;2:扩展', '', '', '', '', 'id:編號', '10', '', '', '1431050352', '1438593640', '1', 'MyISAM');
INSERT INTO `sf_model` VALUES ('13', 'ad', '廣告', '1', '', '1', '{\"1\":[\"3\",\"12\"],\"2\":[\"2\",\"105\",\"109\",\"5\",\"103\",\"11\",\"55\",\"72\",\"102\",\"100\",\"101\",\"62\",\"61\",\"60\",\"48\",\"59\",\"16\",\"20\",\"9\",\"19\",\"14\",\"17\",\"13\",\"10\"]}', '1:基礎;2:扩展', '', '', '', '', 'id:編號', '10', '', '', '1431072271', '1438593633', '1', 'MyISAM');
INSERT INTO `sf_model` VALUES ('14', 'bg', '背景圖', '1', '', '1', '{\"1\":[\"3\",\"108\"],\"2\":[\"2\",\"5\",\"72\",\"109\",\"102\",\"59\",\"100\",\"101\",\"62\",\"61\",\"20\",\"60\",\"55\",\"17\",\"48\",\"16\",\"19\",\"14\",\"12\",\"13\",\"10\",\"11\",\"9\"]}', '1:基礎;2:擴展', '', 'lists', '', '', 'id:編號', '10', '', '', '1435045077', '1438672209', '1', 'MyISAM');
INSERT INTO `sf_model` VALUES ('15', 'cus', '聯繫我們', '1', '', '1', '{\"1\":[\"3\",\"111\",\"113\",\"112\",\"114\",\"55\"],\"2\":[\"2\",\"9\",\"48\",\"102\",\"109\",\"101\",\"100\",\"72\",\"62\",\"61\",\"60\",\"59\",\"19\",\"20\",\"11\",\"17\",\"16\",\"14\",\"13\",\"12\",\"5\",\"10\"]}', '1:基礎;2:擴展', '', '', '', '', 'id:編號 \r\n', '10', '', '', '1438595051', '1438597211', '1', 'MyISAM');

-- ----------------------------
-- Table structure for sf_picture
-- ----------------------------
DROP TABLE IF EXISTS `sf_picture`;
CREATE TABLE `sf_picture` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵id自增',
  `path` varchar(255) NOT NULL DEFAULT '' COMMENT '路徑',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '圖片鏈接',
  `md5` char(32) NOT NULL DEFAULT '' COMMENT '文件md5',
  `sha1` char(40) NOT NULL DEFAULT '' COMMENT '文件 sha1編碼',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '狀態',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=83 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sf_picture
-- ----------------------------
INSERT INTO `sf_picture` VALUES ('14', '/Uploads/Picture/2015-04-21/5535b45c3a20d.jpg', '', '902073e494c3e74b39dfa457cacdabf7', '18a766fc01095c2181ec7edf80596243f02d1ec1', '1', '1429582940');
INSERT INTO `sf_picture` VALUES ('15', '/Uploads/Picture/2015-04-21/5535b45c576d4.jpg', '', '8140686d26d1ef8bb08f531c06d177af', 'aeb9f62bb384d87be03eb5dc2418225d520e29b6', '1', '1429582940');
INSERT INTO `sf_picture` VALUES ('16', '/Uploads/Picture/2015-04-21/5535b45c70162.jpg', '', '01f713e404e8e3773ffa101fd90b52af', 'c39eb5641a028791efbcc24227280e43f5deddbd', '1', '1429582940');
INSERT INTO `sf_picture` VALUES ('17', '/Uploads/Picture/2015-04-21/5535b6a5edfca.jpg', '', 'c2b047be9727b937e94ac12908710a68', '7da4ca3ec6105964c27f9a602e13992eb3c245ef', '1', '1429583525');
INSERT INTO `sf_picture` VALUES ('18', '/Uploads/Picture/2015-04-21/5535b6a62aad5.jpg', '', '1d93cab1f14cad26ca82e6f0ee32471d', 'f0ec174edf34bb04218b7afab12d520db5d89b42', '1', '1429583526');
INSERT INTO `sf_picture` VALUES ('19', '/Uploads/Picture/2015-04-21/5535c374d834f.jpg', '', '545da52e5561fc0e1394e780fa74fdaa', '44c3de9512f46dea6c00590ae8fc9ca05b6f00d4', '1', '1429586804');
INSERT INTO `sf_picture` VALUES ('20', '/Uploads/Picture/2015-04-21/55361a20e71e0.jpg', '', 'b1b918fffc066b0bffd84e296d296de7', '7214604323fb3ccd1a0fb63faa4e01a2bd0cf0b0', '1', '1429608992');
INSERT INTO `sf_picture` VALUES ('21', '/Uploads/Picture/2015-04-21/55361a2113eff.jpg', '', 'ebbd5687fe4286cb6d745e945151f060', '9861bea36c16b173c426e8e46fd6f6e0432063a0', '1', '1429608993');
INSERT INTO `sf_picture` VALUES ('22', '2015-04-28/553ee364734e2.png', '', 'f794da20a79b40306479e6f047cf2354', 'eceda53fc5e51d349dfd83a21c6bc5796ed9f0a6', '1', '1430184804');
INSERT INTO `sf_picture` VALUES ('38', '2015-04-28/553eecb082993.png', '', 'cd6175455dfd4a06de725e547373e8cb', 'ce88bc19d29f2a44bd81818f6ec04a2627198d94', '1', '1430187184');
INSERT INTO `sf_picture` VALUES ('41', '2015-04-28/553eecb0c4082.png', '', 'e3f95bee8734636a1d61c92e2c8cad29', '8f5291d9100631839cf45cabd4d7d7b8af025159', '1', '1430187184');
INSERT INTO `sf_picture` VALUES ('50', '/Uploads/Picture/2015-04-28/553ef88cbb924.jpg', '', 'fb743c5e60eb7983e2783232fa598a16', '025491339ba2109a3df35ebd60ac4f85b889c7fd', '1', '1430190220');
INSERT INTO `sf_picture` VALUES ('51', '/Uploads/Picture/2015-04-28/553ef8e46eabb.jpg', '', 'af2a5338312d248603698130c2798ae9', 'd8e6434f4ee16f457917eb193a44c47d8c9bfa30', '1', '1430190308');
INSERT INTO `sf_picture` VALUES ('52', '/Uploads/Picture/2015-04-28/553efa1e750dc.jpg', '', '971f4c4304f1ce291b27fc8dba239161', '7612ff1ed788bf911e345ab4f5352ceaaf1959c2', '1', '1430190622');
INSERT INTO `sf_picture` VALUES ('53', '/Uploads/Picture/2015-04-28/553efa1e921bb.jpg', '', '750db576b14de016d3d74717819d2a81', '5201392e306ee19686b5f48e45e752f00763a9a3', '1', '1430190622');
INSERT INTO `sf_picture` VALUES ('54', '/Uploads/Picture/2015-04-28/553f00f98ae1e.png', '', 'd3d13315fbfd7d6cae1874e6147b3953', '2878005eb78fa8f2f52788e78432ebc8ac0d9318', '1', '1430192377');
INSERT INTO `sf_picture` VALUES ('55', '/Uploads/Picture/2015-04-28/553f027b4512c.gif', '', 'c0bd48e2273d6398f7209b36b498c876', 'f6f372727801369e2fe70112592c28795da8ceaf', '1', '1430192763');
INSERT INTO `sf_picture` VALUES ('56', '/Uploads/Picture/2015-04-28/553f027b581e0.jpg', '', 'd185e9cd39358651d53ce76cb6c46cf8', '880a9dbee95a956aab180fb3c709a2e7ebe0e28a', '1', '1430192763');
INSERT INTO `sf_picture` VALUES ('57', '/Uploads/Picture/2015-04-28/553f027b7049e.jpg', '', 'a98ec51fae545d4fb92bdf89817e1a06', '6d0b683678b65fa7aacf5db6dd6d8207e5bc6921', '1', '1430192763');
INSERT INTO `sf_picture` VALUES ('58', '/Uploads/Picture/2015-04-28/553f027b82d82.jpg', '', 'dc7105fbf87aa26c0c82797358cfaf82', 'e024e33da17db2fe9e3c4d61f00edadaac7b41ee', '1', '1430192763');
INSERT INTO `sf_picture` VALUES ('59', '/Uploads/Picture/2015-04-28/553f380c60935.jpg', '', '0ef1f2247ff1117fcb97f691234e37b9', '3a1ea410d8e562b20500f12b503459c459238ae9', '1', '1430206476');
INSERT INTO `sf_picture` VALUES ('60', '/Uploads/Picture/2015-04-28/553f380c810c5.jpg', '', 'ec98f305a581b70763e478c24d0abcf0', '33d2ee4c3d44c6f9e1e0cba04eca9342cda0286d', '1', '1430206476');
INSERT INTO `sf_picture` VALUES ('61', '/Uploads/Picture/2015-05-04/55473a8969739.gif', '', 'd6ba8fea49535aead03c83e388222be7', 'ee3e39ded20fc5900ab90f687e0e32b1b8b43122', '1', '1430731401');
INSERT INTO `sf_picture` VALUES ('62', '/Uploads/Picture/2015-05-05/5548695e03bef.jpg', '', '85d93009346bfd81006a3917aba26914', 'f0e077800be426237591365f781debc78223fdb6', '1', '1430808925');
INSERT INTO `sf_picture` VALUES ('63', '/Uploads/Picture/2015-05-08/554c1b435fb19.png', '', '688208f5364448a674cb1dcbafc62dc1', 'dc7d3cc9080a1a92195be513ec76e22bebcb648c', '1', '1431051075');
INSERT INTO `sf_picture` VALUES ('64', '/Uploads/Picture/2015-05-14/555416dbe9b29.gif', '', '888ce560d41a7e353e634bb6aff86f57', 'b30cbc841862d4f7d006fd89002b4cf969361420', '1', '1431574235');
INSERT INTO `sf_picture` VALUES ('65', '/Uploads/Picture/2015-05-22/555ec8cba0b97.jpg', '', 'e32f40258012b6fd5888457165475b23', 'f655c8e75503645bfb31e9b244179ad6aa84d64a', '1', '1432275147');
INSERT INTO `sf_picture` VALUES ('73', '/Uploads/Picture/2015-07-06/559a3abeeb1c3.jpg', '', '2aea46992fce2c477714028b0d38d499', '6183f6a7fc3c5c592b7917b8849b421564e0befd', '1', '1436170938');
INSERT INTO `sf_picture` VALUES ('74', '/Uploads/Picture/2015-07-06/559a3b84104c0.jpg', '', '76bd76fd79d3b2186ec3210d7aa41504', '652e85b49c95dc2ab0dabc1a65157dd67021a737', '1', '1436171131');
INSERT INTO `sf_picture` VALUES ('76', '/Uploads/Picture/2015-07-06/559a3c4e1c109.jpg', '', '3e9358e6fed30f0e086d70eb48789b59', '33c1f5ed97c8dea2ca0f6ccdc29ebb00936f4170', '1', '1436171338');
INSERT INTO `sf_picture` VALUES ('75', '/Uploads/Picture/2015-07-06/559a3c2502c99.jpg', '', '88fe4b9dbb80d52357c4c2f3542eee65', '8f377cb40f63fb2d3924b5e7cfa812d60835f0b7', '1', '1436171298');
INSERT INTO `sf_picture` VALUES ('77', '/Uploads/Picture/2015-07-06/559a3c9416410.jpg', '', 'c78bbf816c7877e77d06113015c831be', '45b31124454370b65e84509e5219c544b5667883', '1', '1436171406');
INSERT INTO `sf_picture` VALUES ('71', '/Uploads/Picture/2015-07-06/559a366784633.jpg', '', '8118c164d088ad776025cabc8c177f3d', '5c045a4f83e4b6971f16e72bb864188720b9f050', '1', '1436169831');
INSERT INTO `sf_picture` VALUES ('72', '/Uploads/Picture/2015-07-06/559a370d0e151.jpg', '', '570daee3949805d05c602ed37923f317', 'c419e3916e533ec02321941cad9d6faaa668d672', '1', '1436169996');
INSERT INTO `sf_picture` VALUES ('78', '/Uploads/Picture/2015-07-06/559a4abb602ca.jpg', '', '1086c27fc82b1f181d93d20d8318a3e1', 'a32254353605c43d18cdf475935e93cde06ff16d', '1', '1436175034');
INSERT INTO `sf_picture` VALUES ('79', '/Uploads/Picture/2015-07-06/559a4c323d858.jpg', '', '1a24f94c54723893920252147f082e8c', '7a76d6a045d6980dc9077dac1dca92eef605f1a6', '1', '1436175410');
INSERT INTO `sf_picture` VALUES ('80', '/Uploads/Picture/2015-07-31/55bafd294e76a.jpg', '', '464929085ba7e316673e59e59fccea75', '29eb3a0ab426f0b186e5a02d1263be05c37a48a9', '1', '1438317859');
INSERT INTO `sf_picture` VALUES ('81', '/Uploads/Picture/2015-07-31/55bafd938a3be.png', '', '8567fe4490610d91539dfcaf0891b1f8', '57f64d34df0092621453adcb73c442e44b7ad18d', '1', '1438317970');
INSERT INTO `sf_picture` VALUES ('82', '/Uploads/Picture/2015-07-31/55bafdaddf79a.jpg', '', '0777a37dc178b4f7a4e9b3b99f65c1dd', '48320e4dc6e7907764164d833605e8b2dc062565', '1', '1438317994');

-- ----------------------------
-- Table structure for sf_screening
-- ----------------------------
DROP TABLE IF EXISTS `sf_screening`;
CREATE TABLE `sf_screening` (
  `id` tinyint(11) unsigned NOT NULL AUTO_INCREMENT,
  `area` varchar(255) NOT NULL COMMENT '區域',
  `widespread` varchar(255) NOT NULL COMMENT '面積',
  `type` varchar(255) NOT NULL COMMENT '類型',
  `floor` varchar(255) NOT NULL COMMENT '樓層',
  `price` varchar(255) NOT NULL COMMENT '價格',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sf_screening
-- ----------------------------
INSERT INTO `sf_screening` VALUES ('1', '拱北,澳門', '50平方米,100平方米', '豪宅,公寓', '第一層,第二層', '100,200,300');

-- ----------------------------
-- Table structure for sf_tag
-- ----------------------------
DROP TABLE IF EXISTS `sf_tag`;
CREATE TABLE `sf_tag` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sf_tag
-- ----------------------------
INSERT INTO `sf_tag` VALUES ('2', '隔音墻');
INSERT INTO `sf_tag` VALUES ('10', '70年房權');
INSERT INTO `sf_tag` VALUES ('32', '普通公寓');

-- ----------------------------
-- Table structure for sf_ucenter_admin
-- ----------------------------
DROP TABLE IF EXISTS `sf_ucenter_admin`;
CREATE TABLE `sf_ucenter_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '管理員ID',
  `member_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '管理員用戶ID',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '管理員狀態',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='管理員表';

-- ----------------------------
-- Records of sf_ucenter_admin
-- ----------------------------

-- ----------------------------
-- Table structure for sf_ucenter_app
-- ----------------------------
DROP TABLE IF EXISTS `sf_ucenter_app`;
CREATE TABLE `sf_ucenter_app` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '應用ID',
  `title` varchar(30) NOT NULL COMMENT '應用名稱',
  `url` varchar(100) NOT NULL COMMENT '應用URL',
  `ip` char(15) NOT NULL COMMENT '應用IP',
  `auth_key` varchar(100) NOT NULL COMMENT '加密KEY',
  `sys_login` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '同步登陸',
  `allow_ip` varchar(255) NOT NULL COMMENT '允許訪問的IP',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '應用狀態',
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='應用表';

-- ----------------------------
-- Records of sf_ucenter_app
-- ----------------------------

-- ----------------------------
-- Table structure for sf_ucenter_member
-- ----------------------------
DROP TABLE IF EXISTS `sf_ucenter_member`;
CREATE TABLE `sf_ucenter_member` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用戶ID',
  `username` char(16) NOT NULL COMMENT '用戶名',
  `password` char(32) NOT NULL COMMENT '密碼',
  `email` char(32) NOT NULL COMMENT '用戶郵箱',
  `mobile` char(15) NOT NULL COMMENT '用戶手機',
  `reg_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '註冊時間',
  `reg_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '註冊IP',
  `last_login_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最後登錄時間',
  `last_login_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '最後登錄IP',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `status` tinyint(4) DEFAULT '0' COMMENT '用戶狀態',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='用戶表';

-- ----------------------------
-- Records of sf_ucenter_member
-- ----------------------------
INSERT INTO `sf_ucenter_member` VALUES ('1', 'admin', '71b7f89d19b9b394b772f92692576e78', '54546@qq.com', '', '1428718406', '0', '1439873162', '0', '1428718406', '1');
INSERT INTO `sf_ucenter_member` VALUES ('2', 'user', 'aa4964dbc657248d266963c1b74b186d', '123456@qq.com', '', '1430618223', '0', '1432719746', '0', '1430618223', '1');
INSERT INTO `sf_ucenter_member` VALUES ('3', 'user2', 'aa4964dbc657248d266963c1b74b186d', 'dsa@qq.com', '', '1430986834', '0', '1430991349', '0', '1430986834', '1');
INSERT INTO `sf_ucenter_member` VALUES ('4', '123', 'f8527d27dc10037602aea10a4d9a8fea', '123@hotmail.com', '', '1436930442', '460174487', '1436934171', '3400479379', '1436930442', '1');
INSERT INTO `sf_ucenter_member` VALUES ('5', 'bing', '71b7f89d19b9b394b772f92692576e78', '794025348@qq.com', '', '1438316449', '3073741281', '1439778163', '0', '1438316449', '1');
INSERT INTO `sf_ucenter_member` VALUES ('6', 'admin2', '0ca3cb85f77f2f81d701c84415b58b85', '45644@qq.com', '', '1438671567', '2130706433', '1438671600', '2130706433', '1438671567', '1');
INSERT INTO `sf_ucenter_member` VALUES ('7', 'aaa', '90d41020b54fcddf3435509a0eeeabe8', '1555554654@qq.com', '', '1439548445', '0', '1439799213', '0', '1439548445', '1');
INSERT INTO `sf_ucenter_member` VALUES ('8', 'AAAA', 'd7095449c9b908dc1bc540d7ef3bf3f6', '4654654@QQ.COM', '', '1439874993', '0', '1439875013', '0', '1439874993', '1');

-- ----------------------------
-- Table structure for sf_ucenter_setting
-- ----------------------------
DROP TABLE IF EXISTS `sf_ucenter_setting`;
CREATE TABLE `sf_ucenter_setting` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '設置ID',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '配置類型（1-用戶配置）',
  `value` text NOT NULL COMMENT '配置數據',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='設置表';

-- ----------------------------
-- Records of sf_ucenter_setting
-- ----------------------------

-- ----------------------------
-- Table structure for sf_url
-- ----------------------------
DROP TABLE IF EXISTS `sf_url`;
CREATE TABLE `sf_url` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '鏈接唯壹標識',
  `url` char(255) NOT NULL DEFAULT '' COMMENT '鏈接地址',
  `short` char(100) NOT NULL DEFAULT '' COMMENT '短網址',
  `status` tinyint(2) NOT NULL DEFAULT '2' COMMENT '狀態',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_url` (`url`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='鏈接表';

-- ----------------------------
-- Records of sf_url
-- ----------------------------

-- ----------------------------
-- Table structure for sf_user
-- ----------------------------
DROP TABLE IF EXISTS `sf_user`;
CREATE TABLE `sf_user` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `pwd` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sf_user
-- ----------------------------
INSERT INTO `sf_user` VALUES ('1', 'sb', 'sb');

-- ----------------------------
-- Table structure for sf_userdata
-- ----------------------------
DROP TABLE IF EXISTS `sf_userdata`;
CREATE TABLE `sf_userdata` (
  `uid` int(10) unsigned NOT NULL COMMENT '用戶id',
  `type` tinyint(3) unsigned NOT NULL COMMENT '類型標識',
  `target_id` int(10) unsigned NOT NULL COMMENT '目標id',
  UNIQUE KEY `uid` (`uid`,`type`,`target_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sf_userdata
-- ----------------------------
