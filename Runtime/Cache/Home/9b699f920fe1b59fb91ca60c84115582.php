<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head profile="http://gmpg.org/xfn/11" xmlns:addthis="http://www.addthis.com/help/api-spec">
<meta charset="UTF-8">
<title><?php echo C('WEB_SITE_TITLE');?></title>
<meta http-equiv="X-UA-Compatible">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" type="text/css" href="/sf/Public/Index/css/bootstrap.min.css"/>
<link href="/sf/Public/Index/dist/css/swiper.min.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="/sf/Public/Index/css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto[gallery2] main stylesheet" charset="utf-8" />
<link rel="stylesheet" type="text/css" href="/sf/Public/Index/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="/sf/Public/Index/css/sf_style.css" >
<meta property="og:title" content="<?php echo ($list["title"]); ?>">
<meta property="og:description" content="<?php echo ($list["description"]); ?>">
<meta property="og:url" content="http://sfhousing.net/index.php?s=/Home/Property/detail/id/<?php echo ($list["id"]); ?>.html#">
<style>
 #jiathis_webchat img{ width:200px; height: 200px}   
</style>

</head>
<body>

<!--top-->
<!--<div class="top_background"><img src="images/top.png" /></div>-->
<!-- 导航条
================================================== -->
<div class="top">
  <div class="container">
    <div class="logo"><img src="/sf/Public/Index/images/Logo.png" /></div>
    <div class="nav_top">
      <div  class="phone_nav"><span class="phone_title">選單</span> <i class="fa fa-angle-down phone_ico"></i></div>
      <ul>
        <li class="cur"><a href="<?php echo U('Index/Index');?>">首頁</a></li>
        <li><a href="<?php echo U('Property/Index');?>">最新樓盤</a></li>
        <li><a href="<?php echo U('News/Index');?>">市場動態</a></li>
        <li><a href="<?php echo U('Rantal/Index');?>">委託租售</a></li>
        <li><a href="<?php echo U('Regulations/Index');?>">買賣須知</a></li>
        <li><a href="<?php echo U('Contact/Index');?>">聯繫我們</a></li>
      </ul>
        <div class="curBg"></div>
    </div>
  </div>
</div>

<!--top end--> 
<!--search-->
<div class="details_search fleft gallery clearfix">
  <div class="form-group search_li">
       <form action="<?php echo U('search');?>" method="post">
    <input type="text" name='title'  class="form-control" placeholder="請輸入房源，地區">
    <button type="submit" class="btn btn-default">搜索<i class="fa fa-search search_ico"></i></button>
       </form>
  </div>
</div>
<!--search end-->
<div class="details_property gallery clearfix">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="swiper-container swiper1"> 
          
          <!-- Carousel items -->
          <div class="swiper-wrapper">
              <?php if(is_array($photo)): foreach($photo as $key=>$data): if($key == 0): ?><div class="swiper-slide coll_img c-e-i"><a href="#"><img src="/sf<?php echo get_cover($data,$field = path);?>" /></a>
              <div class="hover_img"><a href="/sf<?php echo get_cover($data,$field = path);?>"  rel="prettyPhoto[gallery2]"><i class="fa fa-picture-o img_ico"></i></a></div>
            </div>
  <?php else: ?>  <div class="swiper-slide coll_img c-e-i"><a href="#"><img src="/sf<?php echo get_cover($data,$field = path);?>" /></a>
               <div class="hover_img"><a href="/sf<?php echo get_cover($data,$field = path);?>"  rel="prettyPhoto[gallery2]"><i class="fa fa-picture-o img_ico"></i></a></div>
            </div><?php endif; endforeach; endif; ?>
          </div>
          <!-- Carousel nav --> 
          <div class="swiper-button-prev swiper-button-prev1"><i class="fa fa-angle-left c-s-i"></i></div>
        <div class="swiper-button-next swiper-button-next1"><i class="fa fa-angle-right c-s-i"></i></div>
        </div>
        <div class="details_property_bottom">
          <div><a class="scroll col-mw" href="#picture_top">外景圖 </a></div>
          <div><a class="scroll" href="#picture_top">實景圖</a></div>
          <div><a class="scroll" href="#map">交通圖 </a></div>
          <div class="fron_right"><a class="scroll" href="#products">戶型圖</a></div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="new_property_title"><a href="#"><?php echo ($list["title"]); ?></a></div>
        

        <div class="new_property_btn"> <a href="Details_Property.html">
         <?php if(is_array($tags)): foreach($tags as $key=>$vo): ?><input type="button" value="<?php echo ($vo); ?>" class="btn-<?php echo ($key+1); ?> btn" />
          </a> <a href="#"><?php endforeach; endif; ?>
        </div>
        <div class="new_property_font">
        <div class="price_title">價格：</div>
          <div class="price"><?php echo ($list["price"]); ?></div>
          <div class="price_right">元/平方尺</div>
 
          <div class="price_bottom">首付<?php echo ($list["pay"]); ?>萬起　<?php echo ($list["size"]); echo ($list["units"]); ?></div>
  

        </div>
        <div class="right_font_content"> 
          房屋價格：<?php echo ($list["unit"]); echo ($price); ?> 　　   預估交易費：　 约<?php echo ($list["unit"]); ?> <?php echo ($list["fee"]); ?><br />
          　　房型：<?php echo ($list["shi"]); ?> 室　 <?php echo ($list["ting"]); ?> 廳 　<?php echo ($list["wei"]); ?> 衛　 <?php echo ($list["che"]); ?> 車庫<br />
          房屋屬性：<?php echo ($list["attribute"]); ?><br />

<div class="jiathis_style_32x32">
       <a class="jiathis_button_weixin"><i class="fa fa-weixin ey-ico"></i></a>
        <a href="javascript:;" onclick="jiathis_sendto('fb');return false;" class="jiatitle"><span class="jtico jtico_fb"><i class="fa fa-facebook ey-ico"></i></span></a>
	
</div>
<script type="text/javascript" src="http://v3.jiathis.com/code/jia.js" charset="utf-8"></script>

        </div>
        <div class="call_ico"> <i class="fa fa-phone"></i> </div>
        <div class="call_num"> <?php echo ($list["tel"]); ?> </div>

      </div>

    </div>

    <div class="row information_li" id="products">
      <ul class="col-md-6 col-xs-12">
        <li class="col-md-4 information col-xs-4"><a class="scroll" href="#information_top">詳細信息</a></li>
        <li class="col-md-4 information col-xs-4"><a class="scroll" href="#picture_top">樓盤相冊</a></li>
        <li class="col-md-4 information col-xs-4"><a class="scroll" href="#map">地理位置</a></li>
      </ul>
    </div>
    <div class="row dtd">
      <div class="information_title col-md-12 col-mw" id="information_top">物業特色</div>
      <div class="information_content col-md-12"><?php echo ($list["description"]); ?></div>
    </div>
    <div class="dtd">
    <div class="row col-img-li" id="picture_top">
      <div class="col-md-12 col-img-title col-mw">外景圖</div>
      <?php if(is_array($photo)): foreach($photo as $key=>$vo): ?><div class="col-md-3 col-md-offset-1 col_img c-e-i"><img src="/sf<?php echo get_cover($vo,$field = path);?>" />
        <div class="hover_img"><a href="/sf<?php echo get_cover($vo,$field = path);?>"  rel="prettyPhoto[gallery2]"><i class="fa fa-picture-o img_ico"></i></a></div>
     </div><?php endforeach; endif; ?>

    </div>
    <div class="row col-img-li">
      <div class="col-md-12 col-img-title col-mw">實景圖</div>
             <?php if(is_array($photo2)): foreach($photo2 as $key=>$data2): ?><div class="col-md-3 col-md-offset-1 col_img c-e-i"><img src="/sf<?php echo get_cover($data2,$field = path);?>" />
        <div class="hover_img"><a href="/sf<?php echo get_cover($data2,$field = path);?>"  rel="prettyPhoto[gallery2]"><i class="fa fa-picture-o img_ico"></i></a></div>
      </div><?php endforeach; endif; ?>
    </div>
    </div>
    <div class="row dtd">
      <div class="col-md-12 col-img-title col-mw">地理位置</div>
      <div id="map" class="col-md-11 col-md-offset-1">
        <div class="map_copy"></div>
       <div class="map_forn btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
           <form id="form2"  method='post' action="<?php echo U('Property/gmap');?>" target="test">
               <input type="text" name="place" style="display: none" value="<?php echo ($list["place"]); ?>">
           </form>
           <a id='place'>查看地圖</a></div>
      </div>
    </div>
  </div>
</div>
 <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
              <iframe nme='iframe1' id='iframe1' name="test" style="width:900px; height:500px; border:none; overflow:hidden" ></iframe>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal end-->
    
    
    


    <!-- 底部
    ================================================== -->
<div class="footer">
  <div class="footer_content">
   
      <div class="footer_top">
        <div class="footer_line"></div>
        <div class="footer_top_title">順發優勢</div>
        <div class="footer_line"></div>
      </div>
      <div class="footer_bottom">
        <div class="footer_bottom_content">
          <div class="footer_ico ico footer_ico"></div>
          <div class="footer_bottom_title">真實</div>
        </div>
        <div class="footer_bottom_content">
          <div class="footer_ico2 ico footer_ico"></div>
          <div class="footer_bottom_title">及時</div>
        </div>
        <div class="footer_bottom_content">
          <div class="footer_ico3 ico footer_ico"></div>
          <div class="footer_bottom_title">專業</div>
        </div>
      </div>
      <div class="copynight">Copynight @ 2014Soufun Holdings Limited,All Right Rwserved順發地產 版權所有</div>
    </div>

</div>
<!--footer end--> 
<!--goto top--> 
<a class="go_top" href="javascript:goTop();"><i class="fa fa-caret-up"></i></a> 

<!--goto top end--> 
 <?php echo C('ANALYTICS');?>
<script src="/sf/Public/Index/js/jquery.min.js"></script> 
<script src="/sf/Public/Index/js/bootstrap.min.js"></script> 
<script src="/sf/Public/Index/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script> 
<script src="/sf/Public/Index/dist/js/swiper.js" type="text/javascript"></script>
<script src="/sf/Public/Index/js/sf_js.js"></script>
<script src="http://www.osblog.net/res/js/html5shiv.min.js"></script> 
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55507f3d67ac0b77" async="async"></script>

<!-- 用于加载js代码 -->
<!-- 页面footer钩子，一般用于加载插件JS文件和JS代码 -->
<?php echo hook('pageFooter', 'widget');?>
<div class="hidden"><!-- 用于加载统计代码等隐藏元素 -->
	
</div>

<script>
     $('.nav_top li').removeClass('cur');
    $(".nav_top li:eq(1)").addClass('cur');
    $('#place').click(function(){
      
        $('#form2').submit();
    });
    </script>
</body>
</html>