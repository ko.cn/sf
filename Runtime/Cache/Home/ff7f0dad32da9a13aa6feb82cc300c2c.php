<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php echo C('WEB_SITE_TITLE');?></title>
<meta http-equiv="X-UA-Compatible">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" type="text/css" href="/sf/Public/Index/css/bootstrap.min.css"/>
<link href="/sf/Public/Index/dist/css/swiper.min.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="/sf/Public/Index/css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto[gallery2] main stylesheet" charset="utf-8" />
<link rel="stylesheet" type="text/css" href="/sf/Public/Index/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="/sf/Public/Index/css/sf_style.css" >


</head>
<body>
<!--top-->
<!-- 导航条
================================================== -->
<div class="top">
  <div class="container">
    <div class="logo"><img src="/sf/Public/Index/images/Logo.png" /></div>
    <div class="nav_top">
      <div  class="phone_nav"><span class="phone_title">選單</span> <i class="fa fa-angle-down phone_ico"></i></div>
      <ul>
        <li class="cur"><a href="<?php echo U('Index/Index');?>">首頁</a></li>
        <li><a href="<?php echo U('Property/Index');?>">最新樓盤</a></li>
        <li><a href="<?php echo U('News/Index');?>">市場動態</a></li>
        <li><a href="<?php echo U('Rantal/Index');?>">委託租售</a></li>
        <li><a href="<?php echo U('Regulations/Index');?>">買賣須知</a></li>
        <li><a href="<?php echo U('Contact/Index');?>">聯繫我們</a></li>
      </ul>
        <div class="curBg"></div>
    </div>
  </div>
</div>
<!--top end-->

<div class="contact  fleft gallery clearfix" style=" background:url(/sf<?php echo get_cover($bg['bgpic'],$field = path);?>) no-repeat;background-size:cover;">
<div class="col-bg btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal" data-site="<?php echo C('MAPSITE');?>"><a></a></div>
  <div class="container">
    <div class="contact_title">關於我們</div>
    <div class="col-md-12 col-contact-bg">
    <div class="col-md-7 col-md-left">
      <div class="col-title">關於順發</div>
      <p><?php echo ($cus["content"]); ?></p>
    </div>
    <div class="col-md-4 col-md-offset-1 col-md-left">
      <div class="col-title">聯繫我們</div>
      <div class="phone_num_ico"></div>
      <div class="contact_right_font"><?php echo ($cus["tel"]); ?></div>
      <div class="clear"></div>
      <div class="mail_ico"></div>
      <div class="contact_right_font"><?php echo ($cus["email"]); ?></div>
      <div class="clear"></div>
      <div class="adre_ico"></div>
      <div class="contact_right_font"><?php echo ($cus["adds"]); ?></div>
      <div class="clear"></div>
      <div class="inter_ico"></div>
      <div class="contact_right_font"><a href="<?php echo C('WEB_SITE_INDEX');?>"><?php echo ($cus["homepage"]); ?></a></div>
    </div>
    </div>
  </div>
     <div class="message_content">
                <form action="<?php echo U('Contact/update');?>" method="post" id="form_message">               
                    <input class="message_input" type="text" name="contacts" onfocus="if (value == '姓名 Name') {
                                value = ''
                            }" onblur="if (value == '') {
                                        value = '姓名 Name'
                                    }" value="姓名 Name" />
                    <input class="message_input" type="text" name="phone" onfocus="if (value == '電話 Phone') {
                                value = ''
                            }" onblur="if (value == '') {
                                        value = '電話 Phone'
                                    }" value="電話 Phone" />
                    <input class="message_input" type="text" name="email" onfocus="if (value == '郵箱 Email') {
                                value = ''
                            }" onblur="if (value == '') {
                                        value = '郵箱 Email'
                                    }" value="郵箱 Email" />
                    <textarea class="message_text_area" name="content" onfocus="if (value == '請留言  message...') {
                                value = ''
                            }" onblur="if (value == '') {
                                        value = '請留言  message...'
                                    }">請留言  message...</textarea>
                    <div class="col_message_button">
                    <input class="message_button" type="submit" value="發送 Send" />
                   </div>
                </form>
            </div>

</div>
  <!--footer-->
  <!-- Modal -->
    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
           <div class="gmap3"></div>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal end-->
	
    <!-- 底部
    ================================================== -->
<div class="footer">
  <div class="footer_content">
   
      <div class="footer_top">
        <div class="footer_line"></div>
        <div class="footer_top_title">順發優勢</div>
        <div class="footer_line"></div>
      </div>
      <div class="footer_bottom">
        <div class="footer_bottom_content">
          <div class="footer_ico ico footer_ico"></div>
          <div class="footer_bottom_title">真實</div>
        </div>
        <div class="footer_bottom_content">
          <div class="footer_ico2 ico footer_ico"></div>
          <div class="footer_bottom_title">及時</div>
        </div>
        <div class="footer_bottom_content">
          <div class="footer_ico3 ico footer_ico"></div>
          <div class="footer_bottom_title">專業</div>
        </div>
      </div>
      <div class="copynight"><?php echo C('COPYRIGHT');?></div>
    </div>

</div>
<!--footer end--> 
<!--goto top--> 
<a class="go_top" href="javascript:goTop();"><i class="fa fa-caret-up"></i></a> 

<!--goto top end--> 
 <?php echo C('ANALYTICS');?>
<script src="/sf/Public/Index/js/jquery.min.js"></script> 
<script src="/sf/Public/Index/js/bootstrap.min.js"></script> 
<script src="/sf/Public/Index/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script> 
<script src="/sf/Public/Index/dist/js/swiper.js" type="text/javascript"></script>
<script src="/sf/Public/Index/js/sf_js.js"></script>
<script src="http://www.osblog.net/res/js/html5shiv.min.js"></script> 
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55507f3d67ac0b77" async="async"></script>

<!-- 用于加载js代码 -->
<!-- 页面footer钩子，一般用于加载插件JS文件和JS代码 -->
<?php echo hook('pageFooter', 'widget');?>
<div class="hidden"><!-- 用于加载统计代码等隐藏元素 -->
	
</div>


</body>
<script>
    $('.nav_top li').removeClass('cur');
    $(".nav_top li:eq(5)").addClass('cur');
    </script>
<script src="http://maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script>
<script type="text/javascript" src="/sf/Public/Index/js/gmap3.min.js"></script> 
<script type="text/javascript">
  
          $('.btn').click(function(){
              $('.modal').show();
              
          var mapsite=$(this).attr('data-site').split(',');
         
                $(document).ready(function () {

        $(".gmap3").gmap3({
        map:{
        options:{
        center:[mapsite[0],mapsite[1]],
                zoom: 14
        }
        },
		marker: {
        latLng:[mapsite[0],mapsite[1]],
	 
        }
        })
        });
        });
    </script>
</html>