<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<title><?php echo C('WEB_SITE_TITLE');?></title>
<meta http-equiv="X-UA-Compatible">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" type="text/css" href="/sf/Public/Index/css/bootstrap.min.css"/>
<link href="/sf/Public/Index/dist/css/swiper.min.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="/sf/Public/Index/css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto[gallery2] main stylesheet" charset="utf-8" />
<link rel="stylesheet" type="text/css" href="/sf/Public/Index/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="/sf/Public/Index/css/sf_style.css" >


    <style>
           .uploadify{background:#ff8041; color: #fff; border-radius: 5px;  font-weight:bold; font-size: 15px; text-align: center}
                .swfupload{ top:0; left: 124px} 
       </style>
       <script src="/sf/Public/Index/js/jquery.min.js"></script> 
         <script type="text/javascript" src="/sf/Public/static/uploadify/jquery.uploadify.min.js"></script>
</head>

<body>
<!--top-->
<!-- 导航条
================================================== -->
<div class="top">
  <div class="container">
    <div class="logo"><img src="/sf/Public/Index/images/Logo.png" /></div>
    <div class="nav_top">
      <div  class="phone_nav"><span class="phone_title">選單</span> <i class="fa fa-angle-down phone_ico"></i></div>
      <ul>
        <li class="cur"><a href="<?php echo U('Index/Index');?>">首頁</a></li>
        <li><a href="<?php echo U('Property/Index');?>">最新樓盤</a></li>
        <li><a href="<?php echo U('News/Index');?>">市場動態</a></li>
        <li><a href="<?php echo U('Rantal/Index');?>">委託租售</a></li>
        <li><a href="<?php echo U('Regulations/Index');?>">買賣須知</a></li>
        <li><a href="<?php echo U('Contact/Index');?>">聯繫我們</a></li>
      </ul>
        <div class="curBg"></div>
    </div>
  </div>
</div>

<!--top end-->
<div class="rantal">
  <div class="container">     
      <form id='form1' method='post'  action="<?php echo U('Rantal/update');?>">
    <div class="row">
      <div class="col-md-6 col-md-offset-3 btn_li col-xs-12"> <a href="<?php echo U('Rantal/Index');?>" class="rent_btn col-xs-6 col-md-3 col-md-offset-3">出租委託</a> <a href="<?php echo U('Sell/Index');?>" class="col-xs-6 sell_btn col-md-3">出售委託</a> </div>
    </div>
    <div class="row place clear">
      <div class="col-md-6">
     
        <label>街道 :</label>
        <input name="street" type="text">
      </div>
      <div class="col-md-6 col-xs-12">
        <div class="row">
          <div class="col-md-6 col-xs-6">
            <label>區域：</label>
            <div class="rantal_select">
                <p class="select_title">
                    <input name="range" id='place' value="請選擇" onfocus="if (value == '請選擇') {
                                value = ''
                            }" onblur="if (value == '') {
                                        value = '請選擇'
                                    }"><i class="fa fa-angle-down search_ico"></i></p>
              
              <ul>

                <li onClick="$('#place').val('橫琴')">橫琴</li>
                <li onClick="$('#place').val('華發')">華發</li>
                <li onClick="$('#place').val('前山')">前山</li>
                <li onClick="$('#place').val('拱北')">拱北</li>
                <li onClick="$('#place').val('井岸')">井岸</li>
              </ul>
            </div>
          </div>
          <div class="col-md-6 col-xs-6">
            <label>面積：</label>
            <div class="rantal_select">
              <p class="select_title"><input name="area" id='mianji' value="請選擇" onfocus="if (value == '請選擇') {
                                value = ''
                            }" onblur="if (value == '') {
                                        value = '請選擇'
                                    }"><i class="fa fa-angle-down search_ico"></i></p>
        
              <ul>

                <li onClick="$('#mianji').val('50')">50平方米</li>
                <li onClick="$('#mianji').val('100')">100平方米</li>
                <li onClick="$('#mianji').val('150')">150平方米</li>
                <li onClick="$('#mianji').val('200')">200平方米</li>
                <li onClick="$('#mianji').val('250')">250平方米</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row clear">
      <div class="col-md-6 col-xs-12">
        <label>戶型 :</label>
        <div class="house">
          <div class="rantal_select">
            <p class="select_title"><input name="huxing" id='huxing' value="1" onfocus="if (value == '1') {
                                value = ''
                            }" onblur="if (value == '') {
                                        value = '1'
                                    }"><i class="fa fa-angle-down search_ico"></i></p>
            
            <ul>
              <li onClick="$('#huxing').val('1')">1</li>
              <li onClick="$('#huxing').val('2')">2</li>
              <li onClick="$('#huxing').val('3')">3</li>
            </ul>
          </div>
          <label>室</label>
        </div>
        <div class="house">
          <div class="rantal_select">
            <p class="select_title"><input name="ting" id='ting' value="1" onfocus="if (value == '1') {
                                value = ''
                            }" onblur="if (value == '') {
                                        value = '1'
                                    }"><i class="fa fa-angle-down search_ico"></i></p>
        
            <ul>
              <li onClick="$('#ting').val('1')">1</li>
              <li onClick="$('#ting').val('2')">2</li>
              <li onClick="$('#ting').val('3')">3</li>
            </ul>
          </div>
          <label>廳</label>
        </div>
        <div class="house">
          <div class="rantal_select">
            <p class="select_title"><input name="ce" id='ce' value="1" onfocus="if (value == '1') {
                                value = ''
                            }" onblur="if (value == '') {
                                        value = '1'
                                    }"><i class="fa fa-angle-down search_ico"></i></p>
           
            <ul>
              <li onClick="$('#ce').val('1')">1</li>
              <li onClick="$('#ce').val('2')">2</li>
              <li onClick="$('#ce').val('3')">3</li>
            </ul>
          </div>
          <label>廁</label>
        </div>
      </div>
      <div class="col-md-6 col-xs-12">
        <div class="row">
          <div class="col-md-6 col-xs-6">
            <label>類型：</label>
            <div class="rantal_select">
              <p class="select_title"><input name="types" id='leixing' value="請選擇" onfocus="if (value == '請選擇') {
                                value = ''
                            }" onblur="if (value == '') {
                                        value = '請選擇'
                                    }"><i class="fa fa-angle-down search_ico"></i></p>
              
              <ul>
         
                <li onClick="$('#leixing').val('豪宅')">豪宅</li>
                <li onClick="$('#leixing').val('公寓')">公寓</li>
                <li onClick="$('#leixing').val('經濟適用房')">經濟適用房</li>
                <li onClick="$('#leixing').val('其它')">其他</li>
              </ul>
            </div>
          </div>
          <div class="col-md-6 col-xs-6">
            <label>樓層：</label>
            <div class="rantal_select">
              <p class="select_title"><input name="floors" id='lc' value="請選擇" onfocus="if (value == '請選擇') {
                                value = ''
                            }" onblur="if (value == '') {
                                        value = '請選擇'
                                    }"><i class="fa fa-angle-down search_ico"></i></p>
         
              <ul>
                <li onClick="$('#lc').val('第一層')">第一層</li>
                <li onClick="$('#lc').val('第二層')">第二層</li>
                <li onClick="$('#lc').val('第三層')">第三層</li>
                <li onClick="$('#lc').val('第四層')">第四層</li>
                <li onClick="$('#lc').val('第五層')">第五層</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row clear">
      <div class="col-md-12 clear">
        <label>出租：</label>
        <div class="col-lease">
          <label class="phone_clear">租期：</label>
             <input name="time" type="radio" value='一年' checked='checked'/>
          <label>一年</label>
        <input name="time" type="radio" value='二年'/>
          <label>二年</label>
         <input name="time" type="radio" value='五年'/>
          <label>五年</label>
          
          <label class="phone_clear col-fz">現狀：</label>
          <input name="status" type="radio" value='吉屋' checked='checked'/>
          <label>吉屋</label>
          <input name="status" type="radio" value='自用'/>
          <label>自用</label>
            <input name="status" type="radio" value='帶租約'/>
          <label>帶租約</label>
        
        </div>
      </div>
      <div class="col-md-12 rantal_price clear">
        <label>價格：</label>
        <div class="rantal_select">
          <p class="select_title"><input name="fee" id='jg' value="100mop" onfocus="if (value == '100mop') {
                                value = ''
                            }" onblur="if (value == '') {
                                        value = '100mop'
                                    }"><i class="fa fa-angle-down search_ico"></i></p>
         
          <ul>
           
            <li onClick="$('#jg').val('100')">100mop</li>
            <li onClick="$('#jg').val('200')">200mop</li>
            <li onClick="$('#jg').val('300')">300mop</li>
          </ul>
        </div>
        <label class="price_label">管理費</label>
        <input name="price" type="checkbox"  checked/>
      </div>
      <div class="col-md-12 place clear">
        <label>物業地址：</label>
        <input type="text" name="add" />
      </div>
      <div class="col-md-12 clear">
        <label>裝修程度：</label>
        <div class="col-lease">
         
          <input name="finish" type="radio" value='無裝修' checked='checked'/>
          <label>無裝修</label>
          <input name="finish" type="radio" value='簡單裝修'/>
          <label>簡單裝修</label>
          <input name="finish" type="radio" value='中檔裝修' class="phone_clear"/>
          <label>中檔裝修</label>
          <input name="finish" type="radio" value='豪華裝修'/>
          <label>豪華裝修</label>
          <input name="finish" type="radio" value='精裝修'/>
          <label>精裝修</label>
         
        </div>
      </div>
      <div class="col-md-12 clear">
        <label>配套設施：</label>
        <input class="test" type='text' style='display:none' name='devices' value="無設備"/>
        <div class="col-lease">
        <input class='device' name="device" type="checkbox"  value='電視機' />
          <label>電視機</label>
           <input class='device'  name="device" type="checkbox" value='冰箱' />
          <label>冰箱</label>
           <input class='device'  name="device" type="checkbox" value='單人床' />
          <label>單人床</label>
         <input class='device'  name="device" type="checkbox" value='雙人床' class="phone_clear"/>
          <label>雙人床</label>
              <input class='device'  name="device" type="checkbox" value='衣櫃'/>
          <label>衣櫃</label>
     <input class='device'  name="device" type="checkbox" value='冷氣' />
          <label>冷氣</label>
           <input class='device'  name="device" type="checkbox" value='洗衣機'  />
          <label>洗衣機</label>
         
        </div>
      </div>
      <div class="col-md-12 clear">
        <label>訂金要求：</label>
        <div class="col-lease">
            <input  name='foregift' type="text">
        </div>
      </div>
      <div class="col-md-12 description clear">
        <label>房屋描述：</label>
        <textarea name="description" cols="" rows=""></textarea>
      </div>
      <div class="col-md-12 upload clear">
        <label>物業圖片：</label>
  
         <div class="file_li file_2">
           <?php echo hook('UploadImages', array('name'=>$field['name'],'value'=>$field['value']));?>
            
         </div> 
  </div>
       
       <div class="col_name col-md-12"> <label>委託人聯繫資料</label></div>
      <div class="col-md-12 col-contact clear col-xs-12">
        <label class="phone_clear">姓名:</label>
        <input type="text" name="name"  class="rantal_name"/>
        <label class="phone_clear">電話:</label>
        <input type="text" name="tel"  class="rantal_phone"/>
        <label class="phone_clear">郵箱:</label>
        <input type="text" name="email"  class="rantal_mail"/>
        <label class="clear">地址:</label>
        <input type="text" name="adds"  class="rantal_address"/>
      </div>
      <div class="confirmation col-md-12 clear"> <a id="sumbit" href="#">確認委託</a> </div>
    </div></form>
  </div>
</div>
<!--footer-->
    <script type="text/javascript">
                                                                
                                                           $('.nav_top li').removeClass('cur');
                                                           $(".nav_top li:eq(3)").addClass('cur');
								//上传图片
							    /* 初始化上传插件 */
								$("#upload_file_<?php echo ($field["name"]); ?>").uploadify({
							        "height"          : 30,
							        "swf"             : "/sf/Public/static/uploadify/uploadify.swf",
							        "fileObjName"     : "download",
							        "buttonText"      : "上传附件",
							        "uploader"        : "<?php echo U('File/upload',array('session_id'=>session_id()));?>",
							        "width"           : 120,
							        'removeTimeout'	  : 1,
							        "onUploadSuccess" : uploadFile<?php echo ($field["name"]); ?>,
							        'onFallback' : function() {
							            alert('未检测到兼容版本的Flash.');
							        }
							    });
								function uploadFile<?php echo ($field["name"]); ?>(file, data){
									var data = $.parseJSON(data);
							        if(data.status){
							        	var name = "<?php echo ($field["name"]); ?>";
							        	$("input[name="+name+"]").val(data.data);
							        	$("input[name="+name+"]").parent().find('.upload-img-box').html(
							        		"<div class=\"upload-pre-file\"><span class=\"upload_icon_all\"></span>" + data.info + "</div>"
							        	);
							        } else {
							        	updateAlert(data.info);
							        	setTimeout(function(){
							                $('#top-alert').find('button').click();
							                $(that).removeClass('disabled').prop('disabled',false);
							            },1500);
							        }
							    }
								</script>

        
        <script>
         $(function(){
             $('#sumbit').click(function(){
                
                 $('#form1').submit();

             })
             
             
         })
        </script>
       <div class="footer">
  <div class="footer_content">
   
      <div class="footer_top">
        <div class="footer_line"></div>
        <div class="footer_top_title">順發優勢</div>
        <div class="footer_line"></div>
      </div>
      <div class="footer_bottom">
        <div class="footer_bottom_content">
          <div class="footer_ico ico footer_ico"></div>
          <div class="footer_bottom_title">真實</div>
        </div>
        <div class="footer_bottom_content">
          <div class="footer_ico2 ico footer_ico"></div>
          <div class="footer_bottom_title">及時</div>
        </div>
        <div class="footer_bottom_content">
          <div class="footer_ico3 ico footer_ico"></div>
          <div class="footer_bottom_title">專業</div>
        </div>
      </div>
      <div class="copynight">Copynight @ 2014Soufun Holdings Limited,All Right Rwserved順發地產 版權所有</div>
    </div>

</div>
<!--footer end--> 
<!--goto top--> 
<a class="go_top" href="javascript:goTop();"><i class="fa fa-caret-up"></i></a> 

<!--goto top end--> 
 

<script src="/sf/Public/Index/js/bootstrap.min.js"></script> 
<script src="/sf/Public/Index/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script> 
<script src="/sf/Public/Index/js/sf_js.js"></script>
<script src="http://www.osblog.net/res/js/html5shiv.min.js"></script> 
<script>
 $(function(){
     var test = new Array();
     $('.device').change(function(){
        
         if($(this).is(':checked'))
         {
        
         test.push($(this).val());
             
         }
         else{
          test.splice($.inArray($(this),test),1);
         }
        
        $('.test').val(test);
         
     })
     
 })     
</script>
</body>
</html>