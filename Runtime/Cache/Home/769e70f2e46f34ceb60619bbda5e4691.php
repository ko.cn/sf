<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
<title><?php echo C('WEB_SITE_TITLE');?></title>
<meta http-equiv="X-UA-Compatible">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" type="text/css" href="/sf/Public/Index/css/bootstrap.min.css"/>
<link href="/sf/Public/Index/dist/css/swiper.min.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="/sf/Public/Index/css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto[gallery2] main stylesheet" charset="utf-8" />
<link rel="stylesheet" type="text/css" href="/sf/Public/Index/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="/sf/Public/Index/css/sf_style.css" >


</head>
<body>
	<!-- 头部 -->
	<!-- 导航条
================================================== -->
<div class="top">
  <div class="container">
    <div class="logo"><img src="/sf/Public/Index/images/Logo.png" /></div>
    <div class="nav_top">
      <div  class="phone_nav"><span class="phone_title">選單</span> <i class="fa fa-angle-down phone_ico"></i></div>
      <ul>
        <li class="cur"><a href="<?php echo U('Index/Index');?>">首頁</a></li>
        <li><a href="<?php echo U('Property/Index');?>">最新樓盤</a></li>
        <li><a href="<?php echo U('News/Index');?>">市場動態</a></li>
        <li><a href="<?php echo U('Rantal/Index');?>">委託租售</a></li>
        <li><a href="<?php echo U('Regulations/Index');?>">買賣須知</a></li>
        <li><a href="<?php echo U('Contact/Index');?>">聯繫我們</a></li>
      </ul>
        <div class="curBg"></div>
    </div>
  </div>
</div>
	<!-- /头部 -->
	
	<!-- 主体 -->
	<div class="add fleft gallery clearfix">
  <div class="add_img a-e-i"><img src="/sf<?php echo get_cover($bg['bgpic'],$field = path);?>" /></div>
  <div class="container">
    <div class="form-group search_li">
        <form action="<?php echo U('Property/search');?>" method="post">
      <input name='title' type="text" class="form-control" placeholder="最新樓盤">
      <button type="submit" class="btn btn-default">搜索<i class="fa fa-search search_ico"></i></button>
        </form>
    </div>
    <div class="estate_li">
       <div class="estate_title">快速搜查房產</div>
      <div class="estate_select estate_select_1">
          <form id="search" action="<?php echo U('Property/searchs');?>" method="post">
        <p class="select_title"><span>所在地</span><i class="fa fa-angle-down search_ico"></i></p>
        <input type="hidden" name="add" id='place_num' value=""/>
        <ul class="search">
       
          <?php if(is_array($search1)): $i = 0; $__LIST__ = $search1;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$data1): $mod = ($i % 2 );++$i;?><li name="add" onClick="$('#place_num').val('<?php echo ($data1); ?>')"><?php echo ($data1); ?></li><?php endforeach; endif; else: echo "" ;endif; ?>
        </ul>
      </div>
      <div class="estate_select">

        <p class="select_title"><span>物業類型</span><i class="fa fa-angle-down search_ico"></i></p>
        <input type="hidden" name="housestype" id='type_num' value=""/>
        <ul class="search">
  
          <?php if(is_array($search2)): $i = 0; $__LIST__ = $search2;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$data1): $mod = ($i % 2 );++$i;?><li name="'housestype" onClick="$('#type_num').val('<?php echo ($data1); ?>')"><?php echo ($data1); ?></li><?php endforeach; endif; else: echo "" ;endif; ?>
        </ul>
      </div>
      <div class="estate_select">
        <p class="select_title"><span>租/售</span><i class="fa fa-angle-down search_ico"></i></p>
        <input type="hidden" name="sell" id='rental_num' value="3"/>
        <ul class="search">
     
          <li  onClick="$('#rental_num').val('2')">出租</li>
             <li  onClick="$('#rental_num').val('1')">出售</li>
        </ul>
      </div>
      <div class="estate_select estate_select_4">
        <p class="select_title"><span>價格範圍</span><i class="fa fa-angle-down search_ico"></i></p>
        <input type="hidden" name="houses_prices" id='price_num' value="0-99999999"/>
        <ul class="search">

           <?php if(is_array($search4)): $i = 0; $__LIST__ = $search4;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$data): $mod = ($i % 2 );++$i;?><li name="houses_prices" onClick="$('#price_num').val('<?php echo ($data[0]); ?>-<?php echo ($data[1]); ?>')"><?php echo ($data[0]); ?>-<?php echo ($data[1]); ?></li><?php endforeach; endif; else: echo "" ;endif; ?>
        </ul>
      </from>
      </div>
    </div>
  </div>
</div>
<!--add end--> 

<!--news-->

 <div class="products_top container">
      <div class="top_line"></div>
      <div class="top_title">
      <i class="fa products_top_ico"></i>熱賣樓盤
      </div>
      <div class="top_line"></div>
      <a href="<?php echo U('Property/hot');?>">更多>></a> </div>
<div class="news">
  <div class="container">
    <div class="col-md-12">
       
       <div class="swiper-container swiper1">

  <!-- Carousel items -->
  <div class="swiper-wrapper">
      <?php if(is_array($listhot)): foreach($listhot as $key=>$datas): if($key == 0): ?><div class="swiper-slide">
      <div class="col-md-8 c-m-i c-e-i"><a href="<?php echo U('Property/detail','id='.$datas['id']);?>"><img  src="/sf<?php echo get_cover($datas['photo'],$field = path);?>" /></a> </div>
       <div class="col-md-4"><a href="<?php echo U('Property/detail','id='.$datas['id']);?>">
      <h3><?php echo ($datas['title']); ?></h3>
      </a>
      <p> 物業類型：<?php echo ($datas['housestype']); ?><br />
        裝修標準：<?php echo ($datas['attribute']); ?><br />
        特色描述：<?php echo ($datas['description']); ?> </p>
      <a href="<?php echo U('Property/detail','id='.$datas['id']);?>" class="products_btn">
      <input type="button" value="了解更多">
      </a> </div></div>
          <?php else: ?>
            <div class="swiper-slide">
      <div class="col-md-8 c-m-i c-e-i"><a href="/sf/Home/Property"><img  src="/sf<?php echo get_cover($datas['photo'],$field = path);?>" /></a> </div>
       <div class="col-md-4">   <a href="<?php echo U('Property/detail','id='.$datas['id']);?>">
      <h3><?php echo ($datas['title']); ?></h3>
      </a>
      <p> 物業類型：<?php echo ($datas['housestype']); ?><br />
        裝修標準：<?php echo ($datas['attribute']); ?><br />
        特色描述：<?php echo ($datas['description']); ?> </p>
      <a href="<?php echo U('Property/detail','id='.$datas['id']);?>" class="products_btn">
      <input type="button" value="了解更多">
      </a> </div></div><?php endif; endforeach; endif; ?>
  </div>
  <!-- Carousel nav -->
   <div class="swiper-button-prev swiper-button-prev1"><i class="fa fa-angle-left c-s-i"></i></div>
        <div class="swiper-button-next swiper-button-next1"><i class="fa fa-angle-right c-s-i"></i></div>
</div>
    </div>
   
  </div>
</div>
<!--products-->
<div class="products fleft gallery clearfix">
  <div class="container">
    <div class="products_top">
      <div class="top_line"></div>
      <div class="top_title">
        <i class="fa products_top_ico"></i>
        最新樓盤
      </div>
      <div class="top_line"></div>
      <a href="<?php echo U('Property/Index');?>">更多>></a> </div>
      <?php if(is_array($list)): $i = 0; $__LIST__ = array_slice($list,0,4,true);if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$data): $mod = ($i % 2 );++$i;?><div class="col-md-3 col-sm-3 col-xs-12">
      <div class="products_img col_img c-e-i"> <a href="#"><img src="/sf<?php echo get_cover($data['photo'],$field = path);?>" /></a>
        <div class="hover_img"><a href="/sf<?php echo get_cover($data['photo'],$field = path);?>"  rel="prettyPhoto[gallery2]"><i class="fa fa-picture-o img_ico"></i></a></div>
      </div>
      <a href="#">
      <h4><?php echo ($data['title']); ?></h4>
      </a>
      <p><?php echo ($data['title2']); ?></p>
      <a href="<?php echo U('Property/detail','id='.$data['id']);?>" class="products_btn">
      <input type="button" value="了解更多">
      </a> </div><?php endforeach; endif; else: echo "" ;endif; ?>
  </div>
</div>
<!--products end--> 
<!--searvices-->
<div class="products_top container">
      <div class="top_line"></div>
      <div class="top_title">
      <i class="fa products_top_ico"></i>市場動態
      </div>
      <div class="top_line"></div>
      <a href="<?php echo U('News/Index');?>">更多&gt;&gt;</a> </div>
<div class="searvice fleft gallery clearfix">

  <div class="container">
 
 
    <div class="col-md-8">
     
    <div class="col-sevice">
        <?php if(is_array($presell)): $i = 0; $__LIST__ = $presell;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$data): $mod = ($i % 2 );++$i;?><div class="searvice_content">
        <div class="col-md-12"><?php echo ($data['title']); ?></div>
        <div class="col-md-4 col-sm-4 col_img c-e-i"><img src="/sf<?php echo get_cover($data['ImageS'],$field = path);?>" />
          <div class="hover_img"><a href="/sf<?php echo get_cover($data['ImageS'],$field = path);?>"  rel="prettyPhoto[gallery2]"><i class="fa fa-picture-o img_ico"></i></a>
            <h4></h4>
          </div>
        </div>
        <div class="col-md-8 col-sm-8"> <a href="#" class="searvice_title_content"> <?php echo ($data['title2']); ?></a>
          <p><?php echo ($data['description']); ?></p>
          <a href="<?php echo U('News/Detail','id='.$data['id']);?>" class="products_btn">
          <input type="button" value="了解更多">
          </a> </div>
      </div><?php endforeach; endif; else: echo "" ;endif; ?>
     
   
      
     
      </div>
        <!--page_btn--> 
 <div class="page col-md-12">
   
   <?php echo ($_page); ?>
    </div>

<!--page_btn end--> 
   </div>
    <div class="col-md-3 col-xs-12 col-md-offset-1">
      <div class="searvice_right_img">
          <?php if(is_array($list3)): $i = 0; $__LIST__ = $list3;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><img src="/sf<?php echo get_cover($vo['cover_id'],$field = path);?>" /><?php endforeach; endif; else: echo "" ;endif; ?>
      </div>
      <div class="searvice_right_content"> <a>澳門房聞動態</a>
        <ul>
            <?php if(is_array($presell)): $i = 0; $__LIST__ = $presell;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><a href="<?php echo U('News/Detail','id='.$data['id']);?>"><li><?php echo ($vo['title']); ?></li></a><?php endforeach; endif; else: echo "" ;endif; ?>
        </ul>
      </div>
    </div>
  </div>
</div>
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<script>
$(function(){
    
   $(document).on('click','.search li',function(){
   $('#search').submit();    
   })
    
})  
</script>


	<!-- /主体 -->

	<!-- 底部 -->
	
    <!-- 底部
    ================================================== -->
<div class="footer">
  <div class="footer_content">
   
      <div class="footer_top">
        <div class="footer_line"></div>
        <div class="footer_top_title">順發優勢</div>
        <div class="footer_line"></div>
      </div>
      <div class="footer_bottom">
        <div class="footer_bottom_content">
          <div class="footer_ico ico footer_ico"></div>
          <div class="footer_bottom_title">真實</div>
        </div>
        <div class="footer_bottom_content">
          <div class="footer_ico2 ico footer_ico"></div>
          <div class="footer_bottom_title">及時</div>
        </div>
        <div class="footer_bottom_content">
          <div class="footer_ico3 ico footer_ico"></div>
          <div class="footer_bottom_title">專業</div>
        </div>
      </div>
      <div class="copynight"><?php echo C('COPYRIGHT');?></div>
    </div>

</div>
<!--footer end--> 
<!--goto top--> 
<a class="go_top" href="javascript:goTop();"><i class="fa fa-caret-up"></i></a> 

<!--goto top end--> 
 <?php echo C('ANALYTICS');?>
<script src="/sf/Public/Index/js/jquery.min.js"></script> 
<script src="/sf/Public/Index/js/bootstrap.min.js"></script> 
<script src="/sf/Public/Index/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script> 
<script src="/sf/Public/Index/dist/js/swiper.js" type="text/javascript"></script>
<script src="/sf/Public/Index/js/sf_js.js"></script>
<script src="http://www.osblog.net/res/js/html5shiv.min.js"></script> 
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55507f3d67ac0b77" async="async"></script>

<!-- 用于加载js代码 -->
<!-- 页面footer钩子，一般用于加载插件JS文件和JS代码 -->
<?php echo hook('pageFooter', 'widget');?>
<div class="hidden"><!-- 用于加载统计代码等隐藏元素 -->
	
</div>

	<!-- /底部 -->
</body>
</html>