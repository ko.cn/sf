<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<title><?php echo C('WEB_SITE_TITLE');?></title>
<meta http-equiv="X-UA-Compatible">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" type="text/css" href="/sf/Public/Index/css/bootstrap.min.css"/>
<link href="/sf/Public/Index/dist/css/swiper.min.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="/sf/Public/Index/css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto[gallery2] main stylesheet" charset="utf-8" />
<link rel="stylesheet" type="text/css" href="/sf/Public/Index/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="/sf/Public/Index/css/sf_style.css" >


<style>
.search_li a {
  width: 27%;
  height: 40px;
  color: #fff;
  font-size: 16px;
  background: #FF6633;
  border-radius: 0px;
  border-bottom-right-radius: 5px;
  border-top-right-radius: 5px;
  border: none;
}
</style>
</head>

<body>
<!--top-->
<!-- 导航条
================================================== -->
<div class="top">
  <div class="container">
    <div class="logo"><img src="/sf/Public/Index/images/Logo.png" /></div>
    <div class="nav_top">
      <div  class="phone_nav"><span class="phone_title">選單</span> <i class="fa fa-angle-down phone_ico"></i></div>
      <ul>
        <li class="cur"><a href="<?php echo U('Index/Index');?>">首頁</a></li>
        <li><a href="<?php echo U('Property/Index');?>">最新樓盤</a></li>
        <li><a href="<?php echo U('News/Index');?>">市場動態</a></li>
        <li><a href="<?php echo U('Rantal/Index');?>">委託租售</a></li>
        <li><a href="<?php echo U('Regulations/Index');?>">買賣須知</a></li>
        <li><a href="<?php echo U('Contact/Index');?>">聯繫我們</a></li>
      </ul>
        <div class="curBg"></div>
    </div>
  </div>
</div>
<!--top end-->
<div class="col-new-add">
<div class="new_property_add a-e-i"><img src="/sf<?php echo get_cover($bg['bgpic'],$field = path);?>"/></div>

<div class="container">
    <div class="form-group search_li">
        <form action="<?php echo U('Property/search');?>" method="post">
      <input name='title' type="text" class="form-control" placeholder="最新樓盤">
      <button type="submit" class="btn btn-default">搜索<i class="fa fa-search search_ico"></i></button>
        </form>
    </div>
    <div class="estate_li">
       <div class="estate_title">快速搜查房產</div>
      <div class="estate_select estate_select_1">
          <form id="search" action="<?php echo U('Property/searchs');?>" method="post">
              <p class="select_title"><span id="add">所在地</span><i class="fa fa-angle-down search_ico"></i></p>
        <input type="hidden" name="add" id='place_num' value=""/>
        <ul class="search">
       
          <?php if(is_array($search1)): $i = 0; $__LIST__ = $search1;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$data1): $mod = ($i % 2 );++$i;?><li name="add" onClick="$('#place_num').val('<?php echo ($data1); ?>')"><?php echo ($data1); ?></li><?php endforeach; endif; else: echo "" ;endif; ?>
        </ul>
      </div>
      <div class="estate_select">

        <p class="select_title"><span id='housestype'>物業類型</span><i class="fa fa-angle-down search_ico"></i></p>
        <input type="hidden" name="housestype" id='type_num' value=""/>
        <ul class="search">
  
          <?php if(is_array($search2)): $i = 0; $__LIST__ = $search2;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$data1): $mod = ($i % 2 );++$i;?><li name="housestype" onClick="$('#type_num').val('<?php echo ($data1); ?>')"><?php echo ($data1); ?></li><?php endforeach; endif; else: echo "" ;endif; ?>
        </ul>
      </div>
      <div class="estate_select">
        <p class="select_title"><span id='sell'>租/售</span><i class="fa fa-angle-down search_ico"></i></p>
        <input type="hidden" name="sell" id='rental_num' value=""/>
        <ul class="search">
     
          <li name='sell' data-type='2'  onClick="$('#rental_num').val('2')">出租</li>
             <li name='sell' data-type='1'  onClick="$('#rental_num').val('1')">出售</li>
        </ul>
      </div>
      <div class="estate_select estate_select_4">
        <p class="select_title"><span id='houses_prices'>價格範圍</span><i class="fa fa-angle-down search_ico"></i></p>
        <input type="hidden" name="houses_prices" id='price_num' value=""/>
        <ul class="search">

           <?php if(is_array($search4)): $i = 0; $__LIST__ = $search4;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$data1): $mod = ($i % 2 );++$i;?><li name="houses_prices" onClick="$('#price_num').val('<?php echo ($data1[0]); ?>-<?php echo ($data1[1]); ?>')"><?php echo ($data1[0]); ?>-<?php echo ($data1[1]); ?></li><?php endforeach; endif; else: echo "" ;endif; ?>
        </ul>
      </from>
      </div>
    </div>
  </div>
</div>
<div class="new_property">
<input type='hidden' class='current_page' />
	<input type='hidden' class='show_per_page' />
  <div class="container col_new_li"> 
 <?php if(is_array($all)): foreach($all as $key=>$data): ?><div class="row col-rantal">
        <a href="<?php echo U('Property/detail','id='.$data['id']);?>"><div class="col-md-4 col-sm-5 new_img c-e-i"><img src="/sf<?php echo get_cover($data['photo'],$field = path);?>" /></div></a>
    
      <div class="col-md-8 col-sm-7">
        <div class="new_property_title"><a href="Details_Property.html"><?php echo ($data['title']); ?></a>
            <?php if($data['sell'] == 0): ?><span class="col-title-icon">出售</sapn>
            <?php else: ?> <span class="col-title-icon">出租</sapn><?php endif; ?>
        </div>
        <div class="new_property_btn">
            <?php if(!empty($tag)): if(is_array($tag[$key])): foreach($tag[$key] as $key=>$datas): ?><input type="button" value="<?php echo ($datas); ?>" class="btn-<?php echo ($key+1); ?> btn tag" /><?php endforeach; endif; endif; ?>  
        </div>
        <div class="new_property_font">
               <?php if($data['sell'] == 0): ?><div class="price_title">出售價格：</div>
            <?php else: ?>  <div class="price_title">出租價格：</div><?php endif; ?>
        
<div class="price"><?php echo ($data['price']); ?></div>
          <div class="price_right">元/平方尺</div>
          <div class="price_bottom">首付<?php echo ($data['pay']); ?>萬起　<?php echo ($data['size']); ?>平方尺</div>
          <a href="<?php echo U('Property/detail','id='.$data['id']);?>"><input type="button" class="price_btn" value="詳細資料"/></a>
          
        </div>
      </div>
    </div><?php endforeach; endif; ?>
    </div>
  </div>

<!--page_btn-->
 <div class="page col-md-12">
    <?php echo ($_page); ?> 
    </div>

<!--page_btn end--> 
<!--footer-->

    <!-- 底部
    ================================================== -->
<div class="footer">
  <div class="footer_content">
   
      <div class="footer_top">
        <div class="footer_line"></div>
        <div class="footer_top_title">順發優勢</div>
        <div class="footer_line"></div>
      </div>
      <div class="footer_bottom">
        <div class="footer_bottom_content">
          <div class="footer_ico ico footer_ico"></div>
          <div class="footer_bottom_title">真實</div>
        </div>
        <div class="footer_bottom_content">
          <div class="footer_ico2 ico footer_ico"></div>
          <div class="footer_bottom_title">及時</div>
        </div>
        <div class="footer_bottom_content">
          <div class="footer_ico3 ico footer_ico"></div>
          <div class="footer_bottom_title">專業</div>
        </div>
      </div>
      <div class="copynight"><?php echo C('COPYRIGHT');?></div>
    </div>

</div>
<!--footer end--> 
<!--goto top--> 
<a class="go_top" href="javascript:goTop();"><i class="fa fa-caret-up"></i></a> 

<!--goto top end--> 
 <?php echo C('ANALYTICS');?>
<script src="/sf/Public/Index/js/jquery.min.js"></script> 
<script src="/sf/Public/Index/js/bootstrap.min.js"></script> 
<script src="/sf/Public/Index/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script> 
<script src="/sf/Public/Index/dist/js/swiper.js" type="text/javascript"></script>
<script src="/sf/Public/Index/js/sf_js.js"></script>
<script src="http://www.osblog.net/res/js/html5shiv.min.js"></script> 
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55507f3d67ac0b77" async="async"></script>

<!-- 用于加载js代码 -->
<!-- 页面footer钩子，一般用于加载插件JS文件和JS代码 -->
<?php echo hook('pageFooter', 'widget');?>
<div class="hidden"><!-- 用于加载统计代码等隐藏元素 -->
	
</div>

<script src="/sf/Public/Index/js/jquery.cookie.js" type="text/javascript"></script>
<script>
     $('.nav_top li').removeClass('cur');
    $(".nav_top li:eq(1)").addClass('cur');
$(function(){
    
    $('#add').html($.cookie("add"));
    $('#place_num').val($.cookie("add"));
     $('#housestype').html($.cookie("housestype"));
    $('#type_num').val($.cookie("housestype"));
     $('#sell').html($.cookie("sell2"));
    $('#rental_num').val($.cookie("sell"));
     $('#houses_prices').html($.cookie("houses_prices"));
    $('#rental_num').val()!=""? $('#rental_num').val($.cookie("sell")):$('#rental_num').val("3");   
  $('#price_num').val()!=""? $('#price_num').val($.cookie("houses_prices")):$('#price_num').val("0-999999");
     var cookietime = new Date(); 
     cookietime.setTime(cookietime.getTime() + (1*30*1000));//coockie保存一分鐘 
   $(document).on('click','.search li',function(){
   
     if($(this).attr('name')=='add')
     {
    $.cookie("add", $(this).html(),{expires:cookietime}); 
     }
        else if($(this).attr('name')=='houses_prices')
     {
    $.cookie("houses_prices", $(this).html(),{expires:cookietime}); 
     }
      else   if($(this).attr('name')=='housestype')
     {
    $.cookie("housestype", $(this).html(),{expires:cookietime}); 
     }
     else if($(this).attr('name')=='sell')
     {
         $.cookie("sell", $(this).attr('data-type'),{expires:cookietime}); 
           $.cookie("sell2", $(this).html(),{expires:cookietime}); 
     }

   $('#search').submit();    
   });
   $('.tag').each(function(){
       if($(this).val()=="")
       {
           $(this).remove();
       }    
   })
})  
</script>
</body>
</html>