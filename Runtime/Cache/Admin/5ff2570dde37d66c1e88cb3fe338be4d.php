<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo ($meta_title); ?> </title>
    <link href="/sf/Public/favicon.ico" type="image/x-icon" rel="shortcut icon">
    <link rel="stylesheet" type="text/css" href="/sf/Public/Admin/css/base.css" media="all">
    <link rel="stylesheet" type="text/css" href="/sf/Public/Admin/css/common.css" media="all">
    <link rel="stylesheet" type="text/css" href="/sf/Public/Admin/css/module.css">
    <link rel="stylesheet" type="text/css" href="/sf/Public/Admin/css/style.css" media="all">
	<link rel="stylesheet" type="text/css" href="/sf/Public/Admin/css/<?php echo (C("COLOR_STYLE")); ?>.css" media="all">
     <!--[if lt IE 9]>
    <script type="text/javascript" src="/sf/Public/static/jquery-1.10.2.min.js"></script>
    <![endif]--><!--[if gte IE 9]><!-->
    <script type="text/javascript" src="/sf/Public/static/jquery-2.0.3.min.js"></script>
    <script type="text/javascript" src="/sf/Public/Admin/js/jquery.mousewheel.js"></script>
    <style>
     .main-nav .current a{ background: #57cbc8}   
        
    </style>
    <!--<![endif]-->
    
</head>
<body>
    <!-- 頭部 -->
    <div class="header">
        <!-- Logo -->
        <span class="logo"><?php echo C('WEB_SITE_TITLE');?></span>
        <!-- /Logo -->

        <!-- 主導航 -->
        <ul class="main-nav">
            <?php if(is_array($__MENU__["main"])): $i = 0; $__LIST__ = $__MENU__["main"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu): $mod = ($i % 2 );++$i;?><li class="<?php echo ((isset($menu["class"]) && ($menu["class"] !== ""))?($menu["class"]):''); ?>"><a href="<?php echo (u($menu["url"])); ?>"><?php echo ($menu["title"]); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
        </ul>
        <!-- /主導航 -->

        <!-- 用戶欄 -->
        <div class="user-bar">
            <a href="javascript:;" class="user-entrance"><i class="icon-user"></i></a>
            <ul class="nav-list user-menu hidden">
                <li class="manager">妳好，<em title="<?php echo session('user_auth.username');?>"><?php echo session('user_auth.username');?></em></li>
                <li><a href="<?php echo U('User/updatePassword');?>">修改密碼</a></li>
                <li><a href="<?php echo U('User/updateNickname');?>">修改昵稱</a></li>
                <li><a href="<?php echo U('Public/logout');?>">退出</a></li>
            </ul>
        </div>
    </div>
    <!-- /頭部 -->

    <!-- 邊欄 -->
    <div class="sidebar">
        <!-- 子導航 -->
        
<div id="subnav" class="subnav">
    <?php if(!empty($_extra_menu)): ?>
        <?php echo extra_menu($_extra_menu,$__MENU__); endif; ?>
    <?php if(is_array($__MENU__["child"])): $i = 0; $__LIST__ = $__MENU__["child"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$sub_menu): $mod = ($i % 2 );++$i;?><!-- 子導航 -->
        <?php if(!empty($sub_menu)): if(!empty($key)): ?><h3><i class="icon icon-unfold"></i><?php echo ($key); ?></h3><?php endif; ?>
            <ul class="side-sub-menu">
                <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu): $mod = ($i % 2 );++$i;?><li>
                        <a class="item" href="<?php echo (u($menu["url"])); ?>"><?php echo ($menu["title"]); ?></a>
                    </li><?php endforeach; endif; else: echo "" ;endif; ?>
            </ul><?php endif; ?>
        <!-- /子導航 --><?php endforeach; endif; else: echo "" ;endif; ?>

 <h3>
 	<i class="icon <?php if(!in_array((ACTION_NAME), explode(',',"houseslist"))): ?>icon-fold<?php endif; ?>"></i>
 	最新樓盤
 </h3>
 	<ul class="side-sub-menu <?php if(!in_array((ACTION_NAME), explode(',',"houseslist"))): ?>subnav-off<?php endif; ?>">
 		<li <?php if((ACTION_NAME) == "houseslist"): ?>class="current"<?php endif; ?>><a class="item" href="<?php echo U('article/houseslist');?>">樓盤列表</a></li>
 		
 	</ul>
 <h3>
 	<i class="icon <?php if(!in_array((ACTION_NAME), explode(',',"rantel"))): ?>icon-fold<?php endif; ?>"></i>
 	委託租售
 </h3>
 	<ul class="side-sub-menu <?php if(!in_array((ACTION_NAME), explode(',',"rantel"))): ?>subnav-off<?php endif; ?>">
 		<li <?php if((ACTION_NAME) == "rantel"): ?>class="current"<?php endif; ?>><a class="item" href="<?php echo U('article/rantel');?>">樓盤出租</a></li>

 		<li <?php if((ACTION_NAME) == "sell"): ?>class="current"<?php endif; ?>><a class="item" href="<?php echo U('article/sell');?>">樓盤出售</a></li>	
 
 		
 		
		
 	</ul>
    <?php if(is_array($nodes)): $i = 0; $__LIST__ = $nodes;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$sub_menu): $mod = ($i % 2 );++$i;?><!-- 子導航 -->
        <?php if(!empty($sub_menu)): ?><h3>
            	<i class="icon <?php if(($sub_menu['current']) != "1"): ?>icon-fold<?php endif; ?>"></i>
            	<?php if(($sub_menu['allow_publish']) > "0"): ?><a class="item" href="<?php echo (u($sub_menu["url"])); ?>"><?php echo ($sub_menu["title"]); ?></a><?php else: echo ($sub_menu["title"]); endif; ?>
            </h3>
            <ul class="side-sub-menu <?php if(($sub_menu["current"]) != "1"): ?>subnav-off<?php endif; ?>">
                <?php if(is_array($sub_menu['_child'])): $i = 0; $__LIST__ = $sub_menu['_child'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu): $mod = ($i % 2 );++$i;?><li <?php if($menu['id'] == $cate_id or $menu['current'] == 1): ?>class="current"<?php endif; ?>>
                        <?php if(($menu['allow_publish']) > "0"): ?><a class="item" href="<?php echo (u($menu["url"])); ?>"><?php echo ($menu["title"]); ?></a><?php else: ?><a class="item" href="javascript:void(0);"><?php echo ($menu["title"]); ?></a><?php endif; ?>

                        <!-- 壹級子菜單 -->
                        <?php if(isset($menu['_child'])): ?><ul class="subitem">
                        	<?php if(is_array($menu['_child'])): foreach($menu['_child'] as $key=>$three_menu): ?><li>
                                <?php if(($three_menu['allow_publish']) > "0"): ?><a class="item" href="<?php echo (u($three_menu["url"])); ?>"><?php echo ($three_menu["title"]); ?></a><?php else: ?><a class="item" href="javascript:void(0);"><?php echo ($three_menu["title"]); ?></a><?php endif; ?>
                                <!-- 二級子菜單 -->
                                <?php if(isset($three_menu['_child'])): ?><ul class="subitem">
                                	<?php if(is_array($three_menu['_child'])): foreach($three_menu['_child'] as $key=>$four_menu): ?><li>
                                        <?php if(($four_menu['allow_publish']) > "0"): ?><a class="item" href="<?php echo U('index','cate_id='.$four_menu['id']);?>"><?php echo ($four_menu["title"]); ?></a><?php else: ?><a class="item" href="javascript:void(0);"><?php echo ($four_menu["title"]); ?></a><?php endif; ?>
                                        <!-- 三級子菜單 -->
                                        <?php if(isset($four_menu['_child'])): ?><ul class="subitem">
                                        	<?php if(is_array($four_menu['_child'])): $i = 0; $__LIST__ = $four_menu['_child'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$five_menu): $mod = ($i % 2 );++$i;?><li>
                                            	<?php if(($five_menu['allow_publish']) > "0"): ?><a class="item" href="<?php echo U('index','cate_id='.$five_menu['id']);?>"><?php echo ($five_menu["title"]); ?></a><?php else: ?><a class="item" href="javascript:void(0);"><?php echo ($five_menu["title"]); ?></a><?php endif; ?>
                                            </li><?php endforeach; endif; else: echo "" ;endif; ?>
                                        </ul><?php endif; ?>
                                        <!-- end 三級子菜單 -->
                                    </li><?php endforeach; endif; ?>
                                </ul><?php endif; ?>
                                <!-- end 二級子菜單 -->
                            </li><?php endforeach; endif; ?>
                        </ul><?php endif; ?>
                        <!-- end 壹級子菜單 -->
                    </li><?php endforeach; endif; else: echo "" ;endif; ?>
            </ul><?php endif; ?>
        <!-- /子導航 --><?php endforeach; endif; else: echo "" ;endif; ?>
  <ul class="side-sub-menu">
      <li></li>
 	</ul>
     <h3>
 	<i class="icon <?php if(!in_array((ACTION_NAME), explode(',',"rantel"))): ?>icon-fold<?php endif; ?>"></i>
        <a href="<?php echo U('article/message');?>">留言板</a>
 </h3>
 	<ul class="side-sub-menu <?php if(!in_array((ACTION_NAME), explode(',',"rantel"))): ?>subnav-off<?php endif; ?>">
 		
 
 		
 		
		
 	</ul>
 <h3>
<i class="icon <?php if(!in_array((ACTION_NAME), explode(',',"1"))): ?>icon-fold<?php endif; ?>"></i>
網站設置
</h3>
<ul data-id='4' class="side-sub-menu <?php if(!in_array((ACTION_NAME), explode(',',"inde"))): ?>subnav-off<?php endif; ?>">
    <li <?php if((ACTION_NAME) == "a"): ?>class="current"<?php endif; ?>><a class="item showlayer" href="<?php echo U('Article/edit',array('id'=>309,cate_id=>66));?>">logo設置</a></li>
<li <?php if((ACTION_NAME) == "a"): ?>class="current"<?php endif; ?>><a class="item" href="<?php echo U('Config/group');?>">網站基本資料</a></li>	
</ul>       
     <h3>
<i class="icon <?php if(!in_array((ACTION_NAME), explode(',',"1"))): ?>icon-fold<?php endif; ?>"></i>
<a href="<?php echo U('Google/Index');?>">google 
analytics</a>
</h3>
<ul class="side-sub-menu <?php if(!in_array((ACTION_NAME), explode(',',"inde"))): ?>subnav-off<?php endif; ?>">

</ul>    
    <!-- 回收站 -->
	<?php if(($show_recycle) == "1"): ?><h3>
        <em class="recycle"></em>
        <a href="<?php echo U('article/recycle');?>">回收站</a>
    </h3><?php endif; ?>
</div>
<script>
    $(function(){
      
        $(".side-sub-menu li").hover(function(){
            $(this).addClass("hover");
        },function(){
            $(this).removeClass("hover");
        });
    })
</script>


        <!-- /子導航 -->
    </div>
    <!-- /邊欄 -->

    <!-- 內容區 -->
    <div id="main-content">
        <div id="top-alert" class="fixed alert alert-error" style="display: none;">
            <button class="close fixed" style="margin-top: 4px;">&times;</button>
            <div class="alert-content">這是內容</div>
        </div>
        <div id="main" class="main">
            
            <!-- nav -->
            <?php if(!empty($_show_nav)): ?><div class="breadcrumb">
                <span>您的位置:</span>
                <?php $i = '1'; ?>
                <?php if(is_array($_nav)): foreach($_nav as $k=>$v): if($i == count($_nav)): ?><span><?php echo ($v); ?></span>
                    <?php else: ?>
                    <span><a href="<?php echo ($k); ?>"><?php echo ($v); ?></a>&gt;</span><?php endif; ?>
                    <?php $i = $i+1; endforeach; endif; ?>
            </div><?php endif; ?>
            <!-- nav -->
            

            
<!-- 標題 -->
<div class="main-title">
<h2>
出售列表    



</h2>
</div>

<!-- 按鈕工具欄 -->
<div class="cf">
<div class="fl">
<button class="btn ajax-post confirm" target-form="ids" url="<?php echo U("Article/setStatus",array("status"=>-1));?>">刪 除</button>


</div>

<!-- 高級搜索 -->
<div class="search-form fr cf">
<div class="sleft">
<div class="drop-down">
    <span id="sch-sort-txt" class="sort-txt" data="<?php echo ($status); ?>"><?php if(get_status_title($status) == ''): ?>所有<?php else: echo get_status_title($status); endif; ?></span>
    <i class="arrow arrow-down"></i>
    <ul id="sub-sch-menu" class="nav-list hidden">
            <li><a href="javascript:;" value="">所有</a></li>
            <li><a href="javascript:;" value="1">正常</a></li>
            <li><a href="javascript:;" value="0">禁用</a></li>
            <li><a href="javascript:;" value="2">待審核</a></li>
    </ul>
</div>
<input type="text" name="title" class="search-input" value="<?php echo I('title');?>" placeholder="請輸入標題文檔">
<a class="sch-btn" href="javascript:;" id="search" url="<?php echo U('article/mydocument','pid='.I('pid',0).'&cate_id='.$cate_id,false);?>"><i class="btn-search"></i></a>
</div>
<div class="btn-group-click adv-sch-pannel fl">
<button class="btn">高 級<i class="btn-arrowdown"></i></button>
<div class="dropdown cf">
<div class="row">
<label>創建時間：</label>
<input type="text" id="time-start" name="time-start" class="text input-2x" value="" placeholder="起始時間" /> -                		
<div class="input-append date" id="datetimepicker"  style="display:inline-block">
<input type="text" id="time-end" name="time-end" class="text input-2x" value="" placeholder="結束時間" />
<span class="add-on"><i class="icon-th"></i></span>
</div>
</div>
</div>
</div>
</div>
</div>


<!-- 數據表格 -->
<div class="data-table">
<table class="">
<thead>
<tr>
<th class="row-selected row-selected"><input class="check-all" type="checkbox"/></th>
<th class="">編號</th>
<th class="">標題</th>
<th class="">最後更新</th>
<th class="">操作</th>
</tr>
</thead>
<tbody>
<?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
<td><input class="ids" type="checkbox" name="ids[]" value="<?php echo ($vo["id"]); ?>" /></td>
<td><?php echo ($vo["id"]); ?> </td>
<td><a href="<?php echo U('Article/selledit?cate_id='.$vo['category_id'].'&id='.$vo['id']);?>"><?php echo ($vo["title"]); ?></a></td>




<td><span><?php echo (time_format($vo["update_time"])); ?></span></td>

<td>

<a href="<?php echo U('Article/setStatus?status=-1&ids='.$vo['id']);?>" class="confirm ajax-get">刪除</a>
</td>
</tr><?php endforeach; endif; else: echo "" ;endif; ?>
</tbody>
</table> 


</div>

<!-- 分頁 -->
<div class="page">
<?php echo ($_page); ?>
</div>
</div>


        </div>
        <div class="cont-ft">
            <div class="copyright">
                <div class="fl">感謝使用<a href="http://www.onethink.cn" target="_blank">OneThink</a>管理平臺</div>
                <div class="fr">V<?php echo (ONETHINK_VERSION); ?></div>
            </div>
        </div>
    </div>
    <!-- /內容區 -->
    <script type="text/javascript">
    (function(){
        var ThinkPHP = window.Think = {
            "ROOT"   : "/sf", //當前網站地址
            "APP"    : "/sf", //當前項目地址
            "PUBLIC" : "/sf/Public", //項目公共目錄地址
            "DEEP"   : "<?php echo C('URL_PATHINFO_DEPR');?>", //PATHINFO分割符
            "MODEL"  : ["<?php echo C('URL_MODEL');?>", "<?php echo C('URL_CASE_INSENSITIVE');?>", "<?php echo C('URL_HTML_SUFFIX');?>"],
            "VAR"    : ["<?php echo C('VAR_MODULE');?>", "<?php echo C('VAR_CONTROLLER');?>", "<?php echo C('VAR_ACTION');?>"]
        }
    })();
    </script>
    <script type="text/javascript" src="/sf/Public/static/think.js"></script>
    <script type="text/javascript" src="/sf/Public/Admin/js/common.js"></script>
    <script type="text/javascript">
        +function(){
            var $window = $(window), $subnav = $("#subnav"), url;
            $window.resize(function(){
                $("#main").css("min-height", $window.height() - 130);
            }).resize();

            /* 左邊菜單高亮 */
            url = window.location.pathname + window.location.search;
            url = url.replace(/(\/(p)\/\d+)|(&p=\d+)|(\/(id)\/\d+)|(&id=\d+)|(\/(group)\/\d+)|(&group=\d+)/, "");
            $subnav.find("a[href='" + url + "']").parent().addClass("current");

            /* 左邊菜單顯示收起 */
            $("#subnav").on("click", "h3", function(){
                var $this = $(this);
                $this.find(".icon").toggleClass("icon-fold");
                $this.next().slideToggle("fast").siblings(".side-sub-menu:visible").
                      prev("h3").find("i").addClass("icon-fold").end().end().hide();
            });

            $("#subnav h3 a").click(function(e){e.stopPropagation()});

            /* 頭部管理員菜單 */
            $(".user-bar").mouseenter(function(){
                var userMenu = $(this).children(".user-menu ");
                userMenu.removeClass("hidden");
                clearTimeout(userMenu.data("timeout"));
            }).mouseleave(function(){
                var userMenu = $(this).children(".user-menu");
                userMenu.data("timeout") && clearTimeout(userMenu.data("timeout"));
                userMenu.data("timeout", setTimeout(function(){userMenu.addClass("hidden")}, 100));
            });

	        /* 表單獲取焦點變色 */
	        $("form").on("focus", "input", function(){
		        $(this).addClass('focus');
	        }).on("blur","input",function(){
				        $(this).removeClass('focus');
			        });
		    $("form").on("focus", "textarea", function(){
			    $(this).closest('label').addClass('focus');
		    }).on("blur","textarea",function(){
			    $(this).closest('label').removeClass('focus');
		    });

            // 導航欄超出窗口高度後的模擬滾動條
            var sHeight = $(".sidebar").height();
            var subHeight  = $(".subnav").height();
            var diff = subHeight - sHeight; //250
            var sub = $(".subnav");
            if(diff > 0){
                $(window).mousewheel(function(event, delta){
                    if(delta>0){
                        if(parseInt(sub.css('marginTop'))>-10){
                            sub.css('marginTop','0px');
                        }else{
                            sub.css('marginTop','+='+10);
                        }
                    }else{
                        if(parseInt(sub.css('marginTop'))<'-'+(diff-10)){
                            sub.css('marginTop','-'+(diff-10));
                        }else{
                            sub.css('marginTop','-='+10);
                        }
                    }
                });
            }
        }();
    </script>
    
<link href="/sf/Public/static/datetimepicker/css/datetimepicker.css" rel="stylesheet" type="text/css">
<?php if(C('COLOR_STYLE')=='blue_color') echo '<link href="/sf/Public/static/datetimepicker/css/datetimepicker_blue.css" rel="stylesheet" type="text/css">'; ?>
<link href="/sf/Public/static/datetimepicker/css/dropdown.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/sf/Public/static/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="/sf/Public/static/datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js" charset="UTF-8"></script>
<script type="text/javascript">
$(function(){
//搜索功能
$("#search").click(function(){
var url = $(this).attr('url');
var status = $("#sch-sort-txt").attr("data");
var query  = $('.search-form').find('input').serialize();
query = query.replace(/(&|^)(\w*?\d*?\-*?_*?)*?=?((?=&)|(?=$))/g,'');
query = query.replace(/^&/g,'');
if(status != ''){
query += 'status=' + status + "&" + query;
}
if( url.indexOf('?')>0 ){
url += '&' + query;
}else{
url += '?' + query;
}
window.location.href = url;
});

/* 狀態搜索子菜單 */
$(".search-form").find(".drop-down").hover(function(){
$("#sub-sch-menu").removeClass("hidden");
},function(){
$("#sub-sch-menu").addClass("hidden");
});
$("#sub-sch-menu li").find("a").each(function(){
$(this).click(function(){
var text = $(this).text();
$("#sch-sort-txt").text(text).attr("data",$(this).attr("value"));
$("#sub-sch-menu").addClass("hidden");
})
});
//只有壹個模型時，點擊新增
	$('.document_add').click(function(){
		var url = $(this).attr('url');
		if(url != undefined && url != ''){
			window.location.href = url;
		}
	});
//回車自動提交
$('.search-form').find('input').keyup(function(event){
if(event.keyCode===13){
$("#search").click();
}
});

$('#time-start').datetimepicker({
format: 'yyyy-mm-dd',
language:"zh-CN",
minView:2,
autoclose:true
});

$('#datetimepicker').datetimepicker({
format: 'yyyy-mm-dd',
language:"zh-CN",
minView:2,
autoclose:true,
pickerPosition:'bottom-left'
})

})
</script>

</body>
</html>