<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo ($meta_title); ?> </title>
    <link href="/sf/Public/favicon.ico" type="image/x-icon" rel="shortcut icon">
    <link rel="stylesheet" type="text/css" href="/sf/Public/Admin/css/base.css" media="all">
    <link rel="stylesheet" type="text/css" href="/sf/Public/Admin/css/common.css" media="all">
    <link rel="stylesheet" type="text/css" href="/sf/Public/Admin/css/module.css">
    <link rel="stylesheet" type="text/css" href="/sf/Public/Admin/css/style.css" media="all">
	<link rel="stylesheet" type="text/css" href="/sf/Public/Admin/css/<?php echo (C("COLOR_STYLE")); ?>.css" media="all">
     <!--[if lt IE 9]>
    <script type="text/javascript" src="/sf/Public/static/jquery-1.10.2.min.js"></script>
    <![endif]--><!--[if gte IE 9]><!-->
    <script type="text/javascript" src="/sf/Public/static/jquery-2.0.3.min.js"></script>
    <script type="text/javascript" src="/sf/Public/Admin/js/jquery.mousewheel.js"></script>
    <style>
     .main-nav .current a{ background: #57cbc8}   
        
    </style>
    <!--<![endif]-->
    
</head>
<body>
    <!-- 頭部 -->
    <div class="header">
        <!-- Logo -->
        <span class="logo"><?php echo C('WEB_SITE_TITLE');?></span>
        <!-- /Logo -->

        <!-- 主導航 -->
        <ul class="main-nav">
            <?php if(is_array($__MENU__["main"])): $i = 0; $__LIST__ = $__MENU__["main"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu): $mod = ($i % 2 );++$i;?><li class="<?php echo ((isset($menu["class"]) && ($menu["class"] !== ""))?($menu["class"]):''); ?>"><a href="<?php echo (u($menu["url"])); ?>"><?php echo ($menu["title"]); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
        </ul>
        <!-- /主導航 -->

        <!-- 用戶欄 -->
        <div class="user-bar">
            <a href="javascript:;" class="user-entrance"><i class="icon-user"></i></a>
            <ul class="nav-list user-menu hidden">
                <li class="manager">妳好，<em title="<?php echo session('user_auth.username');?>"><?php echo session('user_auth.username');?></em></li>
                <li><a href="<?php echo U('User/updatePassword');?>">修改密碼</a></li>
                <li><a href="<?php echo U('User/updateNickname');?>">修改昵稱</a></li>
                <li><a href="<?php echo U('Public/logout');?>">退出</a></li>
            </ul>
        </div>
    </div>
    <!-- /頭部 -->

    <!-- 邊欄 -->
    <div class="sidebar">
        <!-- 子導航 -->
        
            <div id="subnav" class="subnav">

                <?php if(is_array($__MENU__["child"])): $i = 0; $__LIST__ = $__MENU__["child"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$sub_menu): $mod = ($i % 2 );++$i;?><!-- 子導航 -->
                    <?php if(!empty($sub_menu)): if(!empty($key)): ?><h3><i class="icon icon-unfold"></i><?php echo ($key); ?></h3><?php endif; ?>
                        <ul class="side-sub-menu">
                            <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu): $mod = ($i % 2 );++$i;?><li>
                                    <a class="item" href="<?php echo (u($menu["url"])); ?>"><?php echo ($menu["title"]); ?></a>
                                </li><?php endforeach; endif; else: echo "" ;endif; ?>
                        </ul><?php endif; ?>
                    <!-- /子導航 --><?php endforeach; endif; else: echo "" ;endif; ?>
            </div>
        
        <!-- /子導航 -->
    </div>
    <!-- /邊欄 -->

    <!-- 內容區 -->
    <div id="main-content">
        <div id="top-alert" class="fixed alert alert-error" style="display: none;">
            <button class="close fixed" style="margin-top: 4px;">&times;</button>
            <div class="alert-content">這是內容</div>
        </div>
        <div id="main" class="main">
            
            <!-- nav -->
            <?php if(!empty($_show_nav)): ?><div class="breadcrumb">
                <span>您的位置:</span>
                <?php $i = '1'; ?>
                <?php if(is_array($_nav)): foreach($_nav as $k=>$v): if($i == count($_nav)): ?><span><?php echo ($v); ?></span>
                    <?php else: ?>
                    <span><a href="<?php echo ($k); ?>"><?php echo ($v); ?></a>&gt;</span><?php endif; ?>
                    <?php $i = $i+1; endforeach; endif; ?>
            </div><?php endif; ?>
            <!-- nav -->
            

            
	<div class="main-title cf">
		<h2><?php echo ($info['id']?'編輯':'新增'); ?> [<?php echo get_model_by_id($info['model_id']);?>] 屬性 : <a href="<?php echo U('index','model_id='.$info['model_id']);?>">返回列表</a></h2>
	</div>

	<!-- 標簽頁導航 -->
	<div class="tab-wrap">
		<ul class="tab-nav nav">
			<li data-tab="tab1" class="current"><a href="javascript:void(0);">基 礎</a></li>
			<li data-tab="tab2"><a href="javascript:void(0);">高 級</a></li>
		</ul>
		<div class="tab-content">
			<!-- 表單 -->
			<form id="form" action="<?php echo U('update');?>" method="post" class="form-horizontal doc-modal-form">
				<!-- 基礎 -->
				<div id="tab1" class="tab-pane in tab1">
					<div class="form-item cf">
						<label class="item-label">字段名<span class="check-tips">（請輸入字段名 英文字母開頭，長度不超過30）</span></label>
						<div class="controls">
							<input type="text" class="text input-large" name="name" value="<?php echo ($info["name"]); ?>">
						</div>
					</div>
					<div class="form-item cf">
						<label class="item-label">字段標題<span class="check-tips">（請輸入字段標題，用於表單顯示）</span></label>
						<div class="controls">
							<input type="text" class="text input-large" name="title" value="<?php echo ($info["title"]); ?>">
						</div>
					</div>
					<div class="form-item cf">
						<label class="item-label">字段類型<span class="check-tips">（用於表單中的展示方式）</span></label>
						<div class="controls">
							<select name="type" id="data-type">
								<option value="">----請選擇----</option>
								<?php $_result=get_attribute_type();if(is_array($_result)): $i = 0; $__LIST__ = $_result;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$type): $mod = ($i % 2 );++$i;?><option value="<?php echo ($key); ?>" rule="<?php echo ($type[1]); ?>"><?php echo ($type[0]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
							</select>
						</div>
					</div>
					<div class="form-item cf">
						<label class="item-label">字段定義<span class="check-tips">（字段屬性的sql表示）</span></label>
						<div class="controls">
							<input type="text" class="text input-large" name="field" value="<?php echo ($info["field"]); ?>" id="data-field">
						</div>
					</div>
					<div class="form-item cf">
						<label class="item-label">參數<span class="check-tips">（布爾、枚舉、多選字段類型的定義數據）</span></label>
						<div class="controls">
							<label class="textarea input-large">
								<textarea name="extra"><?php echo ($info["extra"]); ?></textarea>
							</label>
						</div>
					</div>
					<div class="form-item cf">
						<label class="item-label">默認值<span class="check-tips">（字段的默認值）</span></label>
						<div class="controls">
							<input type="text" class="text input-large" name="value" value="<?php echo ($info["value"]); ?>">
						</div>
					</div>
					<div class="form-item cf">
						<label class="item-label">字段備註<span class="check-tips">(用於表單中的提示)</span></label>
						<div class="controls">
							<input type="text" class="text input-large" name="remark" value="<?php echo ($info["remark"]); ?>">
						</div>
					</div>
					<div class="form-item cf">
						<label class="item-label">是否顯示<span class="check-tips">（是否顯示在表單中）</span></label>
						<div class="controls">
							<select name="is_show">
								<option value="1">始終顯示</option>
								<option value="2">新增顯示</option>
								<option value="3">編輯顯示</option>
								<option value="0">不顯示</option>
							</select>
						</div>
					</div>
					<div class="form-item cf">
						<label class="item-label">是否必填<span class="check-tips">（用於自動驗證）</span></label>
						<div class="controls">
							<select name="is_must">
								<option value="0">否</option>
								<option value="1">是</option>
							</select>
						</div>
					</div>
                    </div>
                <div id="tab2" class="tab-pane tab2">
					<div class="form-item cf">
						<label class="item-label">驗證方式<span class="check-tips"></span></label>
						<div class="controls">
							<select name="validate_type">
								<option value="regex">正則驗證</option>
								<option value="function">函數驗證</option>
								<option value="unique">唯壹驗證</option>
								<option value="length">長度驗證</option>
                                <option value="in">驗證在範圍內</option>
                                <option value="notin">驗證不在範圍內</option>
                                <option value="between">區間驗證</option>
                                <option value="notbetween">不在區間驗證</option>
							</select>
						</div>
					</div>
					<div class="form-item cf">
						<label class="item-label">驗證規則<span class="check-tips">（根據驗證方式定義相關驗證規則）</span></label>
						<div class="controls">
							<input type="text" class="text input-large" name="validate_rule" value="<?php echo ($info["validate_rule"]); ?>">
						</div>
					</div>
					<div class="form-item cf">
						<label class="item-label">出錯提示<span class="check-tips"></span></label>
						<div class="controls">
							<input type="text" class="text input-large" name="error_info" value="<?php echo ($info["error_info"]); ?>">
						</div>
					</div>
					<div class="form-item cf">
						<label class="item-label">驗證時間<span class="check-tips"></span></label>
						<div class="controls">
							<select name="validate_time">
                                <option value="3">始 終</option>
								<option value="1">新 增</option>
								<option value="2">編 輯</option>
								</select>
						</div>
					</div>
					<div class="form-item cf">
						<label class="item-label">自動完成方式<span class="check-tips"></span></label>
						<div class="controls">
							<select name="auto_type">
								<option value="function">函數</option>
								<option value="field">字段</option>
								<option value="string">字符串</option>
							</select>
						</div>
					</div>
					<div class="form-item cf">
						<label class="item-label">自動完成規則<span class="check-tips">（根據完成方式訂閱相關規則）</span></label>
						<div class="controls">
							<input type="text" class="text input-large" name="auto_rule" value="<?php echo ($info["auto_rule"]); ?>">
						</div>
					</div>
					<div class="form-item cf">
						<label class="item-label">自動完成時間<span class="check-tips"></span></label>
						<div class="controls">
							<select name="auto_time">
								<option value="3">始 終</option>
								<option value="1">新 增</option>
								<option value="2">編 輯</option>
							</select>
						</div>
					</div>
				</div>

				<!-- 按鈕 -->
				<div class="form-item cf">
					<label class="item-label"></label>
					<div class="controls edit_sort_btn">
						<input type="hidden" name="id" value="<?php echo ($info['id']); ?>"/>
						<input type="hidden" name="model_id" value="<?php echo ($info['model_id']); ?>"/>
						<button class="btn submit-btn ajax-post no-refresh" type="submit" target-form="form-horizontal">確 定</button>
						<button class="btn btn-return" onclick="javascript:history.back(-1);return false;">返 回</button>
					</div>
				</div>
			</form>
		</div>
	</div>

        </div>
        <div class="cont-ft">
            <div class="copyright">
                <div class="fl">感謝使用<a href="http://www.onethink.cn" target="_blank">OneThink</a>管理平臺</div>
                <div class="fr">V<?php echo (ONETHINK_VERSION); ?></div>
            </div>
        </div>
    </div>
    <!-- /內容區 -->
    <script type="text/javascript">
    (function(){
        var ThinkPHP = window.Think = {
            "ROOT"   : "/sf", //當前網站地址
            "APP"    : "/sf", //當前項目地址
            "PUBLIC" : "/sf/Public", //項目公共目錄地址
            "DEEP"   : "<?php echo C('URL_PATHINFO_DEPR');?>", //PATHINFO分割符
            "MODEL"  : ["<?php echo C('URL_MODEL');?>", "<?php echo C('URL_CASE_INSENSITIVE');?>", "<?php echo C('URL_HTML_SUFFIX');?>"],
            "VAR"    : ["<?php echo C('VAR_MODULE');?>", "<?php echo C('VAR_CONTROLLER');?>", "<?php echo C('VAR_ACTION');?>"]
        }
    })();
    </script>
    <script type="text/javascript" src="/sf/Public/static/think.js"></script>
    <script type="text/javascript" src="/sf/Public/Admin/js/common.js"></script>
    <script type="text/javascript">
        +function(){
            var $window = $(window), $subnav = $("#subnav"), url;
            $window.resize(function(){
                $("#main").css("min-height", $window.height() - 130);
            }).resize();

            /* 左邊菜單高亮 */
            url = window.location.pathname + window.location.search;
            url = url.replace(/(\/(p)\/\d+)|(&p=\d+)|(\/(id)\/\d+)|(&id=\d+)|(\/(group)\/\d+)|(&group=\d+)/, "");
            $subnav.find("a[href='" + url + "']").parent().addClass("current");

            /* 左邊菜單顯示收起 */
            $("#subnav").on("click", "h3", function(){
                var $this = $(this);
                $this.find(".icon").toggleClass("icon-fold");
                $this.next().slideToggle("fast").siblings(".side-sub-menu:visible").
                      prev("h3").find("i").addClass("icon-fold").end().end().hide();
            });

            $("#subnav h3 a").click(function(e){e.stopPropagation()});

            /* 頭部管理員菜單 */
            $(".user-bar").mouseenter(function(){
                var userMenu = $(this).children(".user-menu ");
                userMenu.removeClass("hidden");
                clearTimeout(userMenu.data("timeout"));
            }).mouseleave(function(){
                var userMenu = $(this).children(".user-menu");
                userMenu.data("timeout") && clearTimeout(userMenu.data("timeout"));
                userMenu.data("timeout", setTimeout(function(){userMenu.addClass("hidden")}, 100));
            });

	        /* 表單獲取焦點變色 */
	        $("form").on("focus", "input", function(){
		        $(this).addClass('focus');
	        }).on("blur","input",function(){
				        $(this).removeClass('focus');
			        });
		    $("form").on("focus", "textarea", function(){
			    $(this).closest('label').addClass('focus');
		    }).on("blur","textarea",function(){
			    $(this).closest('label').removeClass('focus');
		    });

            // 導航欄超出窗口高度後的模擬滾動條
            var sHeight = $(".sidebar").height();
            var subHeight  = $(".subnav").height();
            var diff = subHeight - sHeight; //250
            var sub = $(".subnav");
            if(diff > 0){
                $(window).mousewheel(function(event, delta){
                    if(delta>0){
                        if(parseInt(sub.css('marginTop'))>-10){
                            sub.css('marginTop','0px');
                        }else{
                            sub.css('marginTop','+='+10);
                        }
                    }else{
                        if(parseInt(sub.css('marginTop'))<'-'+(diff-10)){
                            sub.css('marginTop','-'+(diff-10));
                        }else{
                            sub.css('marginTop','-='+10);
                        }
                    }
                });
            }
        }();
    </script>
    
<script type="text/javascript" charset="utf-8">
//導航高亮
highlight_subnav('<?php echo U('Model/index');?>');
Think.setValue('type', "<?php echo ((isset($info["type"]) && ($info["type"] !== ""))?($info["type"]):''); ?>");
Think.setValue('is_show', "<?php echo ((isset($info["is_show"]) && ($info["is_show"] !== ""))?($info["is_show"]):1); ?>");
Think.setValue('is_must', "<?php echo ((isset($info["is_must"]) && ($info["is_must"] !== ""))?($info["is_must"]):0); ?>");
Think.setValue('validate_time', "<?php echo ((isset($info["validate_time"]) && ($info["validate_time"] !== ""))?($info["validate_time"]):3); ?>");
Think.setValue('auto_time', "<?php echo ((isset($info["auto_time"]) && ($info["auto_time"] !== ""))?($info["auto_time"]):3); ?>");
Think.setValue('validate_type', "<?php echo ((isset($info["validate_type"]) && ($info["validate_type"] !== ""))?($info["validate_type"]):'regex'); ?>");
Think.setValue('auto_type', "<?php echo ((isset($info["auto_type"]) && ($info["auto_type"] !== ""))?($info["auto_type"]):'function'); ?>");
$(function(){
	showTab();
})
<?php if((ACTION_NAME) == "add"): ?>$(function(){
	$('#data-type').change(function(){
		$('#data-field').val($(this).find('option:selected').attr('rule'));
	});
})<?php endif; ?>
</script>

</body>
</html>