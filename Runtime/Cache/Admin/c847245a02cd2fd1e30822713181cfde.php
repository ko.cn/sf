<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo ($meta_title); ?> </title>
    <link href="/sf/Public/favicon.ico" type="image/x-icon" rel="shortcut icon">
    <link rel="stylesheet" type="text/css" href="/sf/Public/Admin/css/base.css" media="all">
    <link rel="stylesheet" type="text/css" href="/sf/Public/Admin/css/common.css" media="all">
    <link rel="stylesheet" type="text/css" href="/sf/Public/Admin/css/module.css">
    <link rel="stylesheet" type="text/css" href="/sf/Public/Admin/css/style.css" media="all">
	<link rel="stylesheet" type="text/css" href="/sf/Public/Admin/css/<?php echo (C("COLOR_STYLE")); ?>.css" media="all">
     <!--[if lt IE 9]>
    <script type="text/javascript" src="/sf/Public/static/jquery-1.10.2.min.js"></script>
    <![endif]--><!--[if gte IE 9]><!-->
    <script type="text/javascript" src="/sf/Public/static/jquery-2.0.3.min.js"></script>
    <script type="text/javascript" src="/sf/Public/Admin/js/jquery.mousewheel.js"></script>
    <style>
     .main-nav .current a{ background: #57cbc8}   
        
    </style>
    <!--<![endif]-->
    
</head>
<body>
    <!-- 頭部 -->
    <div class="header">
        <!-- Logo -->
        <span class="logo"><?php echo C('WEB_SITE_TITLE');?></span>
        <!-- /Logo -->

        <!-- 主導航 -->
        <ul class="main-nav">
            <?php if(is_array($__MENU__["main"])): $i = 0; $__LIST__ = $__MENU__["main"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu): $mod = ($i % 2 );++$i;?><li class="<?php echo ((isset($menu["class"]) && ($menu["class"] !== ""))?($menu["class"]):''); ?>"><a href="<?php echo (u($menu["url"])); ?>"><?php echo ($menu["title"]); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
        </ul>
        <!-- /主導航 -->

        <!-- 用戶欄 -->
        <div class="user-bar">
            <a href="javascript:;" class="user-entrance"><i class="icon-user"></i></a>
            <ul class="nav-list user-menu hidden">
                <li class="manager">妳好，<em title="<?php echo session('user_auth.username');?>"><?php echo session('user_auth.username');?></em></li>
                <li><a href="<?php echo U('User/updatePassword');?>">修改密碼</a></li>
                <li><a href="<?php echo U('User/updateNickname');?>">修改昵稱</a></li>
                <li><a href="<?php echo U('Public/logout');?>">退出</a></li>
            </ul>
        </div>
    </div>
    <!-- /頭部 -->

    <!-- 邊欄 -->
    <div class="sidebar">
        <!-- 子導航 -->
        
    <div id="subnav" class="subnav">
    <?php if(!empty($_extra_menu)): ?>
        <?php echo extra_menu($_extra_menu,$__MENU__); endif; ?>
    <?php if(is_array($__MENU__["child"])): $i = 0; $__LIST__ = $__MENU__["child"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$sub_menu): $mod = ($i % 2 );++$i;?><!-- 子導航 -->
        <?php if(!empty($sub_menu)): if(!empty($key)): ?><h3><i class="icon icon-unfold"></i><?php echo ($key); ?></h3><?php endif; ?>
            <ul class="side-sub-menu">
                <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu): $mod = ($i % 2 );++$i;?><li>
                        <a class="item" href="<?php echo (u($menu["url"])); ?>"><?php echo ($menu["title"]); ?></a>
                    </li><?php endforeach; endif; else: echo "" ;endif; ?>
            </ul><?php endif; ?>
        <!-- /子導航 --><?php endforeach; endif; else: echo "" ;endif; ?>

 <h3>
 	<i class="icon <?php if(!in_array((ACTION_NAME), explode(',',"houseslist"))): ?>icon-fold<?php endif; ?>"></i>
 	最新樓盤
 </h3>
 	<ul class="side-sub-menu <?php if(!in_array((ACTION_NAME), explode(',',"houseslist"))): ?>subnav-off<?php endif; ?>">
 		<li <?php if((ACTION_NAME) == "houseslist"): ?>class="current"<?php endif; ?>><a class="item" href="<?php echo U('article/houseslist');?>">樓盤列表</a></li>
 		
 	</ul>
 <h3>
 	<i class="icon <?php if(!in_array((ACTION_NAME), explode(',',"rantel"))): ?>icon-fold<?php endif; ?>"></i>
 	委託租售
 </h3>
 	<ul class="side-sub-menu <?php if(!in_array((ACTION_NAME), explode(',',"rantel"))): ?>subnav-off<?php endif; ?>">
 		<li <?php if((ACTION_NAME) == "rantel"): ?>class="current"<?php endif; ?>><a class="item" href="<?php echo U('article/rantel');?>">樓盤出租</a></li>

 		<li <?php if((ACTION_NAME) == "sell"): ?>class="current"<?php endif; ?>><a class="item" href="<?php echo U('article/sell');?>">樓盤出售</a></li>	
 
 		
 		
		
 	</ul>
    <?php if(is_array($nodes)): $i = 0; $__LIST__ = $nodes;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$sub_menu): $mod = ($i % 2 );++$i;?><!-- 子導航 -->
        <?php if(!empty($sub_menu)): ?><h3>
            	<i class="icon <?php if(($sub_menu['current']) != "1"): ?>icon-fold<?php endif; ?>"></i>
            	<?php if(($sub_menu['allow_publish']) > "0"): ?><a class="item" href="<?php echo (u($sub_menu["url"])); ?>"><?php echo ($sub_menu["title"]); ?></a><?php else: echo ($sub_menu["title"]); endif; ?>
            </h3>
            <ul class="side-sub-menu <?php if(($sub_menu["current"]) != "1"): ?>subnav-off<?php endif; ?>">
                <?php if(is_array($sub_menu['_child'])): $i = 0; $__LIST__ = $sub_menu['_child'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu): $mod = ($i % 2 );++$i;?><li <?php if($menu['id'] == $cate_id or $menu['current'] == 1): ?>class="current"<?php endif; ?>>
                        <?php if(($menu['allow_publish']) > "0"): ?><a class="item" href="<?php echo (u($menu["url"])); ?>"><?php echo ($menu["title"]); ?></a><?php else: ?><a class="item" href="javascript:void(0);"><?php echo ($menu["title"]); ?></a><?php endif; ?>

                        <!-- 壹級子菜單 -->
                        <?php if(isset($menu['_child'])): ?><ul class="subitem">
                        	<?php if(is_array($menu['_child'])): foreach($menu['_child'] as $key=>$three_menu): ?><li>
                                <?php if(($three_menu['allow_publish']) > "0"): ?><a class="item" href="<?php echo (u($three_menu["url"])); ?>"><?php echo ($three_menu["title"]); ?></a><?php else: ?><a class="item" href="javascript:void(0);"><?php echo ($three_menu["title"]); ?></a><?php endif; ?>
                                <!-- 二級子菜單 -->
                                <?php if(isset($three_menu['_child'])): ?><ul class="subitem">
                                	<?php if(is_array($three_menu['_child'])): foreach($three_menu['_child'] as $key=>$four_menu): ?><li>
                                        <?php if(($four_menu['allow_publish']) > "0"): ?><a class="item" href="<?php echo U('index','cate_id='.$four_menu['id']);?>"><?php echo ($four_menu["title"]); ?></a><?php else: ?><a class="item" href="javascript:void(0);"><?php echo ($four_menu["title"]); ?></a><?php endif; ?>
                                        <!-- 三級子菜單 -->
                                        <?php if(isset($four_menu['_child'])): ?><ul class="subitem">
                                        	<?php if(is_array($four_menu['_child'])): $i = 0; $__LIST__ = $four_menu['_child'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$five_menu): $mod = ($i % 2 );++$i;?><li>
                                            	<?php if(($five_menu['allow_publish']) > "0"): ?><a class="item" href="<?php echo U('index','cate_id='.$five_menu['id']);?>"><?php echo ($five_menu["title"]); ?></a><?php else: ?><a class="item" href="javascript:void(0);"><?php echo ($five_menu["title"]); ?></a><?php endif; ?>
                                            </li><?php endforeach; endif; else: echo "" ;endif; ?>
                                        </ul><?php endif; ?>
                                        <!-- end 三級子菜單 -->
                                    </li><?php endforeach; endif; ?>
                                </ul><?php endif; ?>
                                <!-- end 二級子菜單 -->
                            </li><?php endforeach; endif; ?>
                        </ul><?php endif; ?>
                        <!-- end 壹級子菜單 -->
                    </li><?php endforeach; endif; else: echo "" ;endif; ?>
            </ul><?php endif; ?>
        <!-- /子導航 --><?php endforeach; endif; else: echo "" ;endif; ?>
  <ul class="side-sub-menu">
      <li></li>
 	</ul>
     <h3>
 	<i class="icon <?php if(!in_array((ACTION_NAME), explode(',',"rantel"))): ?>icon-fold<?php endif; ?>"></i>
        <a href="<?php echo U('article/message');?>">留言板</a>
 </h3>
 	<ul class="side-sub-menu <?php if(!in_array((ACTION_NAME), explode(',',"rantel"))): ?>subnav-off<?php endif; ?>">
 		
 
 		
 		
		
 	</ul>
    <!-- 回收站 -->
	<?php if(($show_recycle) == "1"): ?><h3>
        <em class="recycle"></em>
        <a href="<?php echo U('article/recycle');?>">回收站</a>
    </h3><?php endif; ?>
</div>
<script>
    $(function(){
      
        $(".side-sub-menu li").hover(function(){
            $(this).addClass("hover");
        },function(){
            $(this).removeClass("hover");
        });
    })
</script>


        <!-- /子導航 -->
    </div>
    <!-- /邊欄 -->

    <!-- 內容區 -->
    <div id="main-content">
        <div id="top-alert" class="fixed alert alert-error" style="display: none;">
            <button class="close fixed" style="margin-top: 4px;">&times;</button>
            <div class="alert-content">這是內容</div>
        </div>
        <div id="main" class="main">
            
            <!-- nav -->
            <?php if(!empty($_show_nav)): ?><div class="breadcrumb">
                <span>您的位置:</span>
                <?php $i = '1'; ?>
                <?php if(is_array($_nav)): foreach($_nav as $k=>$v): if($i == count($_nav)): ?><span><?php echo ($v); ?></span>
                    <?php else: ?>
                    <span><a href="<?php echo ($k); ?>"><?php echo ($v); ?></a>&gt;</span><?php endif; ?>
                    <?php $i = $i+1; endforeach; endif; ?>
            </div><?php endif; ?>
            <!-- nav -->
            

            
 
<script type="text/javascript" src="/sf/Public/static/uploadify/jquery.uploadify.min.js"></script>
<script type="text/javascript" src="/sf/Addons/UploadImages/script/UploadImages.js"></script>
<link rel="stylesheet" href="/sf/Addons/UploadImages/style/UploadImages.css">
<style>
 .tags{ margin-bottom: 2px;width:80px; font-size:10px; height: 25px; border-radius: 3px; color:#fff; text-align:center; position:relative; float: left; margin-right: 5px; line-height: 25px}
.shut{ position:absolute; top: 0; right: 2px; font-size:10px;padding-TOP:6px; line-height: 0; font-size: 2px; color: #fff }
.c1{ background:#ec5e5e}
.c2{ background:#5ac991}
.c3{ background:#ff9999}
.shake{ animation:one 0.2s infinite; -webkit-animation:one 0.2s infinite; }
@keyframes one{
	0%{ transform:rotate(10deg);
	-webkit-transform:rotate(10deg);}
	50%{ transform:rotate(-10deg);
	-webkit-transform:rotate(-10deg);}
	100%{ transform:rotate(10deg);-webkit-transform:rotate(10deg);}
	}

@-webkit-keyframes one{
	0%{ transform:rotate(3deg);
	-webkit-transform:rotate(3deg);}
	50%{ transform:rotate(-3deg);
	-webkit-transform:rotate(-3deg);}
	100%{ transform:rotate(4deg);-webkit-transform:rotate(3deg);}
	}
</style> 
	<div class="main-title cf">
		<h2>
			基本資料 
		</h2>
	</div>
	<!-- 標簽頁導航 -->
<div class="tab-wrap">
<!--	<ul class="tab-nav nav">
		<?php $_result=parse_config_attr($model['field_group']);if(is_array($_result)): $i = 0; $__LIST__ = $_result;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$group): $mod = ($i % 2 );++$i;?><li data-tab="tab<?php echo ($key); ?>" <?php if(($key) == "1"): ?>class="current"<?php endif; ?>><a href="javascript:void(0);"><?php echo ($group); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
	</ul>-->
	<div class="tab-content">
	<!-- 表單 -->
	<form id="form" action="<?php echo U('update');?>" method="post" class="form-horizontal">
		<!-- 基礎文檔模型 -->
                <input type="text" value="<?php echo ($data["type"]); ?>" name="type"  style="display: none">
                      <input type="text" value="<?php echo ($data["cover_id"]); ?>" name="cover_id" style="display: none">
                          <input type="text" value="<?php echo ($data["link_id"]); ?>" name="link_id" style="display: none"> 
               <div class="form-item cf">
                    <label class="item-label">物業名稱<span class="check-tips">（標題）</span></label>
                    <div class="controls">
                        <input  type="text" class="text input-6x" name="title" value="<?php echo ($data["title"]); ?>" >                    </div>
                </div>
                             <div class="form-item cf">
                    <label class="item-label">副標題<span class="check-tips"></span></label>
                    <div class="controls">
                        <input  type="text" class="text input-6x" name="title2" value="<?php echo ($data["title2"]); ?>" >                    </div>
                </div>
                         <div class="form-item cf">
                    <label class="item-label">標籤<span class="check-tips">（點擊標籤列表即可添加）</span></label>
                    <div class="controls">
                        <input id="tags" type="text" class="text input-6x" name="tag" value="<?php echo ($data["tag"]); ?>" style="display:none">
                        <div class="tagbox">
                         
                          <!--動態添加標籤-->
                        </div>
                    </div>   
                </div>
                    
                 <div class="form-item cf">
                    
                    <div class="controls">
                        <input class="tagtxt" type="text" class="text input-2x " name="come" > <a class='tagmake' style="color:blue" href="#">[+]創建標籤</a>  <a href="#" class="tagmag" style="color:red">[-]管理標籤</a>                  </div>
                     <p id='tag'></p> 
                </div>
       <div class="form-item cf">
                   <label class="item-label">所在地<span class="check-tips"></span></label> 
                    <div class="controls">
                        <span class="add" style=" display: none"><?php echo ($data["add"]); ?></span>
                        <select name='add' id='select1'>
                            <option>所在地</option>
                            <?php if(is_array($add)): $i = 0; $__LIST__ = $add;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option><?php echo ($vo); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                        </select>                
                    </div>
                </div>
                                 <div class="form-item cf">
                   <label class="item-label">房屋類型<span class="check-tips"></span></label> 
                    <div class="controls">
                        <span class="type" style=" display: none"><?php echo ($data["housestype"]); ?></span>
                        <select name='housestype' id='select2'>    
                            <?php if(is_array($types)): $i = 0; $__LIST__ = $types;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option><?php echo ($vo); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                        </select>                
                    </div>
                </div>
                   <div class="form-item cf">
                    <label class="item-label">銷售類型<span class="check-tips"></span></label>
                   <div class="controls">
                        <label>
                                    <input class="radios" type="radio" value="0" name="sell" data-check="<?php echo ($data["sell"]); ?>">出售                                	</label><label >
                                    <input  class="radios" type="radio" value="1" name="sell" data-check="<?php echo ($data["sell"]); ?>">出租                                	</label>                    </div>
                </div>
                            <div class="form-item cf">
                    <label class="item-label">是否熱賣<span class="check-tips"></span></label>
                   <div class="controls">
                        <label>
                                    <input class="radios" type="radio" value="1" name="hots" data-check="<?php echo ($data["hots"]); ?>">是                             	</label><label >
                                    <input  class="radios" type="radio" value="0" name="hots" data-check="<?php echo ($data["hots"]); ?>">否                                	</label>                    </div>
                </div>
                 <div class="form-item cf"  style="float: left">
                    <label class="item-label">價格<span class="check-tips">（所有價格自定義單位）</span></label>
                    <div class="controls">
                        <input type="text" class="text input-2x" name="price" value="<?php echo ($data["price"]); ?>" ><input type="text" class="text input-2x" name="unit" value="<?php echo ($data["unit"]); ?>" >                     </div>
                </div>
                                <div class="form-item cf"  style="float: left">
                    <label class="item-label">預估交易費<span class="check-tips"></span></label>
                    <div class="controls">
                        <input type="text" class="text input-2x" name="fee" value="<?php echo ($data["fee"]); ?>" >                    </div>
                </div>
                                          <div class="form-item cf">
                    <label class="item-label">總價格<span class="check-tips"></span></label>
                    <div class="controls">
                        <input type="text" class="text input-2x" name="houses_prices" value="<?php echo ($data["houses_prices"]); ?>" >                    </div>
                </div>
                       <div class="form-item cf" style="float: left">
                    <label class="item-label">首付<span class="check-tips"></span></label>
                    <div class="controls">
                        <input type="text" class="text input-2x" name="pay" value="<?php echo ($data["pay"]); ?>" >                    </div>
                </div>
                 <div class="form-item cf" >
                    <label class="item-label">面積<span class="check-tips">（填寫單位）</span></label>
                    <div class="controls">
                        <input type="text" class="text input-2x" name="size" value="<?php echo ($data["size"]); ?>" > <input type="text" class="text input-2x" name="units" value="<?php echo ($data["units"]); ?>" >                       </div>
                </div>
                    <div class="form-item cf" >
                    <label class="item-label">房型<span class="check-tips"></span></label>
                    <div class="controls">
                        <input type="text" class="text input-2x" name="shi"  value="<?php echo ($data["shi"]); ?>">   室  <input type="text" class="text input-2x" name="ting" value="<?php echo ($data["ting"]); ?>" >  廳    
                     <input type="text" class="text input-2x" name="wei" value="<?php echo ($data["wei"]); ?>" >  衛  <input type="text" class="text input-2x" name="che" value="<?php echo ($data["che"]); ?>" > 車庫
                    </div>
               
                    </div>
                    <div class="form-item cf" >
                    <label class="item-label">屬性<span class="check-tips"></span></label>
                    <div class="controls">
                        <input type="text" class="text input-5x" name="attribute" value="<?php echo ($data["attribute"]); ?>" >                    </div>
                </div>
                       <div class="form-item cf" >
                    <label class="item-label">電話<span class="check-tips"></span></label>
                    <div class="controls">
                        <input type="text" class="text input-5x" name="tel"  value="<?php echo ($data["tel"]); ?>">                    </div>
                </div>
	<div class="main-title cf">
		<h2>
		其它資料
		</h2>
	</div>   
                          <div class="form-item cf" >
                    <label class="item-label">特色描述<span class="check-tips"></span></label>
                    <div class="controls">
                           <label class="textarea input-large">
                                <textarea name="description"><?php echo ($data["description"]); ?></textarea>
                                </label>     
                    </div>
                </div>
                <div class="form-item cf">
                    <label class="item-label">實景圖<span class="check-tips"></span></label>
                    <div class="controls">

    <div id="upload_picture_photo" class="uploadify" style="height: 30px; width: 120px;">
        <object id="SWFUpload_1" type="application/x-shockwave-flash" data="/sf/Public/static/uploadify/uploadify.swf?preventswfcaching=1430731466033" width="120" height="30" class="swfupload" style="position: absolute; z-index: 1;"><param name="wmode" value="transparent"><param name="movie" value="/sf/Public/static/uploadify/uploadify.swf?preventswfcaching=1430731466033"><param name="quality" value="high"><param name="menu" value="false"><param name="allowScriptAccess" value="always"><param name="flashvars" value="movieName=SWFUpload_1&amp;uploadURL=%2Fsf%2Findex.php%3Fs%3D%2FAdmin%2FFile%2FuploadPicture%2Fsession_id%2F2pieuumh72k3igkgkiruk66av4.html&amp;useQueryString=false&amp;requeueOnError=false&amp;httpSuccess=&amp;assumeSuccessTimeout=30&amp;params=&amp;filePostName=download&amp;fileTypes=*.jpg%3B%20*.png%3B%20*.gif%3B&amp;fileTypesDescription=All%20Files&amp;fileSizeLimit=0&amp;fileUploadLimit=0&amp;fileQueueLimit=999&amp;debugEnabled=false&amp;buttonImageURL=%2Fsf%2F&amp;buttonWidth=120&amp;buttonHeight=30&amp;buttonText=&amp;buttonTextTopPadding=0&amp;buttonTextLeftPadding=0&amp;buttonTextStyle=color%3A%20%23000000%3B%20font-size%3A%2016pt%3B&amp;buttonAction=-110&amp;buttonDisabled=false&amp;buttonCursor=-2"></object><div id="upload_picture_photo-button" class="uploadify-button " style="height: 30px; line-height: 30px; width: 120px;"><span class="uploadify-button-text">上传图片</span></div></div><div id="upload_picture_photo-queue" class="uploadify-queue"></div>
    <input type="hidden" name="photo" value="<?php echo ($data['photo']); ?>" class="icon photo">
    <div class="upload-img-box">
       <?php if(!empty($data["photo"])): if(is_array($photo)): $i = 0; $__LIST__ = $photo;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo2): $mod = ($i % 2 );++$i;?><div class="upload-pre-item">
                    <img src="/sf<?php echo get_cover($vo2,$field = path);?>" data-id="<?php echo ($vo2); ?>">
                    <span class="btn-close btn-close-photo" title="删除图片"></span>
                </div><?php endforeach; endif; else: echo "" ;endif; endif; ?>
 
    </div>
                            <script type="text/javascript">
    //上传图片
$(function(){
        /* 初始化上传插件*/
      
        $("#upload_picture_photo").uploadify({
            "height"          : 30,
            "swf"             : "/sf/Public/static/uploadify/uploadify.swf",
            "fileObjName"     : "download",
            "buttonText"      : "上传图片",
            "uploader"        : "/sf/index.php?s=/Admin/File/uploadPicture/session_id/2pieuumh72k3igkgkiruk66av4.html",
            "width"           : 120,
            'removeTimeout'   : 1,
            'fileTypeExts'    : '*.jpg; *.png; *.gif;',
            "onUploadSuccess" : uploadPicturephoto,
            'onFallback' : function() {
                alert('未检测到兼容版本的Flash.');
            }
        });
        $('.btn-close-photo').click(function(event) {
            
            event.preventDefault();
            $(this).parent().remove();
            picsbox = $("#upload_picture_photo").siblings('.upload-img-box');
            picArr = [];
            for (var i = 0; i < picsbox.children().length ; i++) {
                picArr.push(picsbox.children('.upload-pre-item:eq('+i+')').find('img').attr('data-id'));
            };
            picStr = picArr.join(',');
            $('.icon.photo').val(picStr);
        });
})
    function uploadPicturephoto(file, data){
        var data = $.parseJSON(data);
        var src = '';
        if(data.status){
            src = data.url || '/sf' + data.path;
            upload_img = "<div class='upload-pre-item'><img src=" + src +" title='点击显示大图' data-id="+data.id+"> <span class='btn-close btn-close-photo' title='删除图片'></span></div>";
            picsbox = $("#upload_picture_photo").siblings('.upload-img-box');
            picsbox.append(upload_img)
            picArr = [];
            for (var i = 0; i < picsbox.children().length ; i++) {
                picArr.push(picsbox.children('.upload-pre-item:eq('+i+')').find('img').attr('data-id'));
            };
            picStr = picArr.join(',');
            $('.icon.photo').val(picStr);
        } else {
            updateAlert(data.info);
            setTimeout(function(){
                $('#top-alert').find('button').click();
                $(that).removeClass('disabled').prop('disabled',false);
            },1500);
        }
    }
</script>                    </div>
                </div>
               
                <div class="form-item cf">
                    <label class="item-label">外景圖<span class="check-tips"></span></label>
                    <div class="controls">

    <div id="upload_picture_photo2" class="uploadify" style="height: 30px; width: 120px;"><object id="SWFUpload_2" type="application/x-shockwave-flash" data="/sf/Public/static/uploadify/uploadify.swf?preventswfcaching=1430810486915" width="120" height="30" class="swfupload" style="position: absolute; z-index: 1;"><param name="wmode" value="transparent"><param name="movie" value="/sf/Public/static/uploadify/uploadify.swf?preventswfcaching=1430810486915"><param name="quality" value="high"><param name="menu" value="false"><param name="allowScriptAccess" value="always"><param name="flashvars" value="movieName=SWFUpload_2&amp;uploadURL=%2Fsf%2Findex.php%3Fs%3D%2FAdmin%2FFile%2FuploadPicture%2Fsession_id%2F2pieuumh72k3igkgkiruk66av4.html&amp;useQueryString=false&amp;requeueOnError=false&amp;httpSuccess=&amp;assumeSuccessTimeout=30&amp;params=&amp;filePostName=download&amp;fileTypes=*.jpg%3B%20*.png%3B%20*.gif%3B&amp;fileTypesDescription=All%20Files&amp;fileSizeLimit=0&amp;fileUploadLimit=0&amp;fileQueueLimit=999&amp;debugEnabled=false&amp;buttonImageURL=%2Fsf%2F&amp;buttonWidth=120&amp;buttonHeight=30&amp;buttonText=&amp;buttonTextTopPadding=0&amp;buttonTextLeftPadding=0&amp;buttonTextStyle=color%3A%20%23000000%3B%20font-size%3A%2016pt%3B&amp;buttonAction=-110&amp;buttonDisabled=false&amp;buttonCursor=-2"></object><div id="upload_picture_photo2-button" class="uploadify-button " style="height: 30px; line-height: 30px; width: 120px;"><span class="uploadify-button-text">上传图片</span></div></div><div id="upload_picture_photo2-queue" class="uploadify-queue"></div>
    <input type="hidden" name="photo2" value="<?php echo ($data["photo2"]); ?>" class="icon photo2">
    <div class="upload-img-box">
     
        <?php if(!empty($data["photo2"])): if(is_array($photo2)): $i = 0; $__LIST__ = $photo2;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><div class="upload-pre-item">
                    <img src="/sf<?php echo get_cover($vo,$field = path);?>" data-id="<?php echo ($vo); ?>">
                    <span class="btn-close btn-close-photo2" title="删除图片"></span>
                </div><?php endforeach; endif; else: echo "" ;endif; endif; ?>

          
            </div>
                            <script type="text/javascript">
    //上传图片
$(function(){
        /* 初始化上传插件*/
        $("#upload_picture_photo2").uploadify({
            "height"          : 30,
            "swf"             : "/sf/Public/static/uploadify/uploadify.swf",
            "fileObjName"     : "download",
            "buttonText"      : "上传图片",
            "uploader"        : "/sf/index.php?s=/Admin/File/uploadPicture/session_id/2pieuumh72k3igkgkiruk66av4.html",
            "width"           : 120,
            'removeTimeout'   : 1,
            'fileTypeExts'    : '*.jpg; *.png; *.gif;',
            "onUploadSuccess" : uploadPicturephoto2,
            'onFallback' : function() {
                alert('未检测到兼容版本的Flash.');
            }
        });
        $('.btn-close-photo2').click(function(event) {
            
            event.preventDefault();
            $(this).parent().remove();
            picsbox = $("#upload_picture_photo2").siblings('.upload-img-box');
            picArr = [];
            for (var i = 0; i < picsbox.children().length ; i++) {
                picArr.push(picsbox.children('.upload-pre-item:eq('+i+')').find('img').attr('data-id'));
            };
            picStr = picArr.join(',');
            $('.icon.photo2').val(picStr);
        });
})
    function uploadPicturephoto2(file, data){
        var data = $.parseJSON(data);
        var src = '';
        if(data.status){
            src = data.url || '/sf' + data.path;
            upload_img = "<div class='upload-pre-item'><img src=" + src +" title='点击显示大图' data-id="+data.id+"> <span class='btn-close btn-close-photo2' title='删除图片'></span></div>";
            picsbox = $("#upload_picture_photo2").siblings('.upload-img-box');
            picsbox.append(upload_img)
            picArr = [];
            for (var i = 0; i < picsbox.children().length ; i++) {
                picArr.push(picsbox.children('.upload-pre-item:eq('+i+')').find('img').attr('data-id'));
            };
            picStr = picArr.join(',');
            $('.icon.photo2').val(picStr);
        } else {
            updateAlert(data.info);
            setTimeout(function(){
                $('#top-alert').find('button').click();
                $(that).removeClass('disabled').prop('disabled',false);
            },1500);
        }
    }
</script>                    </div>
                </div>
                     <div class="form-item cf" >
                         <label class="item-label">地理位置<span class="check-tips">（填寫坐標）</span></label>
                    <div class="controls">
                        <input type="text" class="text input-4x" name="place" value="<?php echo ($data["place"]); ?>" > <a target="_BLANK"  href="http://www.gpsspg.com/maps.htm">獲取坐標</a>                     </div>
                </div>
             
 </div>

		<div class="form-item cf">
				<a class="btn btn-return" id="submit" target-form="form-horizontal">確 定</a>
			<a class="btn btn-return" href="<?php echo (cookie('__forward__')); ?>">返 回</a>
			<input type="hidden" name="id" value="<?php echo ((isset($data["id"]) && ($data["id"] !== ""))?($data["id"]):''); ?>"/>
			<input type="hidden" name="pid" value="<?php echo ((isset($data["pid"]) && ($data["pid"] !== ""))?($data["pid"]):''); ?>"/>
			<input type="hidden" name="model_id" value="<?php echo ((isset($data["model_id"]) && ($data["model_id"] !== ""))?($data["model_id"]):''); ?>"/>
			<input type="hidden" name="category_id" value="<?php echo ((isset($data["category_id"]) && ($data["category_id"] !== ""))?($data["category_id"]):''); ?>">
		</div>
	</form>
	</div>
</div>

<script type="text/javascript">
        var tag = $.ajax({
               url:"<?php echo U('Tag/index');?>",
               async:true,
               success:function(data){
                  $('#tag').html(data);     
               },
               error:function(){
                   alert('錯誤');
               },
           })
        $('.tagmake').click(function(){
          var title= $('.tagtxt').val();
          if(title!='')
          {
            $.ajax({
               url:"<?php echo U('Tag/add');?>",
               type:"post",
               data:{title:title},
               async:true,
               success:function(data){
                  $('#tag').html(data);     
               },
               error:function(){
                   alert('錯誤');
               },
           });
          }
        }) ;   
</script>
 
<script type="text/javascript" src="/sf/Public/Admin/js/sf.js"></script>
    <script>
    $(function(){
         $('#select1').val($('.add').html()); 
            $('#select2').val($('.type').html());  
    })

    </script>


        </div>
        <div class="cont-ft">
            <div class="copyright">
                <div class="fl">感謝使用<a href="http://www.onethink.cn" target="_blank">OneThink</a>管理平臺</div>
                <div class="fr">V<?php echo (ONETHINK_VERSION); ?></div>
            </div>
        </div>
    </div>
    <!-- /內容區 -->
    <script type="text/javascript">
    (function(){
        var ThinkPHP = window.Think = {
            "ROOT"   : "/sf", //當前網站地址
            "APP"    : "/sf", //當前項目地址
            "PUBLIC" : "/sf/Public", //項目公共目錄地址
            "DEEP"   : "<?php echo C('URL_PATHINFO_DEPR');?>", //PATHINFO分割符
            "MODEL"  : ["<?php echo C('URL_MODEL');?>", "<?php echo C('URL_CASE_INSENSITIVE');?>", "<?php echo C('URL_HTML_SUFFIX');?>"],
            "VAR"    : ["<?php echo C('VAR_MODULE');?>", "<?php echo C('VAR_CONTROLLER');?>", "<?php echo C('VAR_ACTION');?>"]
        }
    })();
    </script>
    <script type="text/javascript" src="/sf/Public/static/think.js"></script>
    <script type="text/javascript" src="/sf/Public/Admin/js/common.js"></script>
    <script type="text/javascript">
        +function(){
            var $window = $(window), $subnav = $("#subnav"), url;
            $window.resize(function(){
                $("#main").css("min-height", $window.height() - 130);
            }).resize();

            /* 左邊菜單高亮 */
            url = window.location.pathname + window.location.search;
            url = url.replace(/(\/(p)\/\d+)|(&p=\d+)|(\/(id)\/\d+)|(&id=\d+)|(\/(group)\/\d+)|(&group=\d+)/, "");
            $subnav.find("a[href='" + url + "']").parent().addClass("current");

            /* 左邊菜單顯示收起 */
            $("#subnav").on("click", "h3", function(){
                var $this = $(this);
                $this.find(".icon").toggleClass("icon-fold");
                $this.next().slideToggle("fast").siblings(".side-sub-menu:visible").
                      prev("h3").find("i").addClass("icon-fold").end().end().hide();
            });

            $("#subnav h3 a").click(function(e){e.stopPropagation()});

            /* 頭部管理員菜單 */
            $(".user-bar").mouseenter(function(){
                var userMenu = $(this).children(".user-menu ");
                userMenu.removeClass("hidden");
                clearTimeout(userMenu.data("timeout"));
            }).mouseleave(function(){
                var userMenu = $(this).children(".user-menu");
                userMenu.data("timeout") && clearTimeout(userMenu.data("timeout"));
                userMenu.data("timeout", setTimeout(function(){userMenu.addClass("hidden")}, 100));
            });

	        /* 表單獲取焦點變色 */
	        $("form").on("focus", "input", function(){
		        $(this).addClass('focus');
	        }).on("blur","input",function(){
				        $(this).removeClass('focus');
			        });
		    $("form").on("focus", "textarea", function(){
			    $(this).closest('label').addClass('focus');
		    }).on("blur","textarea",function(){
			    $(this).closest('label').removeClass('focus');
		    });

            // 導航欄超出窗口高度後的模擬滾動條
            var sHeight = $(".sidebar").height();
            var subHeight  = $(".subnav").height();
            var diff = subHeight - sHeight; //250
            var sub = $(".subnav");
            if(diff > 0){
                $(window).mousewheel(function(event, delta){
                    if(delta>0){
                        if(parseInt(sub.css('marginTop'))>-10){
                            sub.css('marginTop','0px');
                        }else{
                            sub.css('marginTop','+='+10);
                        }
                    }else{
                        if(parseInt(sub.css('marginTop'))<'-'+(diff-10)){
                            sub.css('marginTop','-'+(diff-10));
                        }else{
                            sub.css('marginTop','-='+10);
                        }
                    }
                });
            }
        }();
    </script>
    
<link href="/sf/Public/static/datetimepicker/css/datetimepicker.css" rel="stylesheet" type="text/css">
<?php if(C('COLOR_STYLE')=='blue_color') echo '<link href="/sf/Public/static/datetimepicker/css/datetimepicker_blue.css" rel="stylesheet" type="text/css">'; ?>
<link href="/sf/Public/static/datetimepicker/css/dropdown.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/sf/Public/static/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="/sf/Public/static/datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js" charset="UTF-8"></script>
<script type="text/javascript">

Think.setValue("type", <?php echo ((isset($data["type"]) && ($data["type"] !== ""))?($data["type"]):'""'); ?>);
Think.setValue("display", <?php echo ((isset($data["display"]) && ($data["display"] !== ""))?($data["display"]):0); ?>);


$(document).on('click','.shake',function(){
    var title = $(this).html();
       $.ajax({
               url:"<?php echo U('Tag/del');?>",
               type:"post",
               data:{title:title},
               async:true,
               success:function(data){
                  alert('刪除成功');
                 $('#tag').html(data);   
               },
               error:function(){
                   alert('錯誤');
               },
           });
});
</script>

</body>
</html>