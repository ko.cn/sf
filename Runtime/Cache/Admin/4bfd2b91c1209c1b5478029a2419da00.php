<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo ($meta_title); ?> </title>
    <link href="/sf/Public/favicon.ico" type="image/x-icon" rel="shortcut icon">
    <link rel="stylesheet" type="text/css" href="/sf/Public/Admin/css/base.css" media="all">
    <link rel="stylesheet" type="text/css" href="/sf/Public/Admin/css/common.css" media="all">
    <link rel="stylesheet" type="text/css" href="/sf/Public/Admin/css/module.css">
    <link rel="stylesheet" type="text/css" href="/sf/Public/Admin/css/style.css" media="all">
	<link rel="stylesheet" type="text/css" href="/sf/Public/Admin/css/<?php echo (C("COLOR_STYLE")); ?>.css" media="all">
     <!--[if lt IE 9]>
    <script type="text/javascript" src="/sf/Public/static/jquery-1.10.2.min.js"></script>
    <![endif]--><!--[if gte IE 9]><!-->
    <script type="text/javascript" src="/sf/Public/static/jquery-2.0.3.min.js"></script>
    <script type="text/javascript" src="/sf/Public/Admin/js/jquery.mousewheel.js"></script>
    <style>
     .main-nav .current a{ background: #57cbc8}   
        
    </style>
    <!--<![endif]-->
    
</head>
<body>
    <!-- 頭部 -->
    <div class="header">
        <!-- Logo -->
        <span class="logo"><?php echo C('WEB_SITE_TITLE');?></span>
        <!-- /Logo -->

        <!-- 主導航 -->
        <ul class="main-nav">
            <?php if(is_array($__MENU__["main"])): $i = 0; $__LIST__ = $__MENU__["main"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu): $mod = ($i % 2 );++$i;?><li class="<?php echo ((isset($menu["class"]) && ($menu["class"] !== ""))?($menu["class"]):''); ?>"><a href="<?php echo (u($menu["url"])); ?>"><?php echo ($menu["title"]); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
        </ul>
        <!-- /主導航 -->

        <!-- 用戶欄 -->
        <div class="user-bar">
            <a href="javascript:;" class="user-entrance"><i class="icon-user"></i></a>
            <ul class="nav-list user-menu hidden">
                <li class="manager">妳好，<em title="<?php echo session('user_auth.username');?>"><?php echo session('user_auth.username');?></em></li>
                <li><a href="<?php echo U('User/updatePassword');?>">修改密碼</a></li>
                <li><a href="<?php echo U('User/updateNickname');?>">修改昵稱</a></li>
                <li><a href="<?php echo U('Public/logout');?>">退出</a></li>
            </ul>
        </div>
    </div>
    <!-- /頭部 -->

    <!-- 邊欄 -->
    <div class="sidebar">
        <!-- 子導航 -->
        
    <div id="subnav" class="subnav">
    <?php if(!empty($_extra_menu)): ?>
        <?php echo extra_menu($_extra_menu,$__MENU__); endif; ?>
    <?php if(is_array($__MENU__["child"])): $i = 0; $__LIST__ = $__MENU__["child"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$sub_menu): $mod = ($i % 2 );++$i;?><!-- 子導航 -->
        <?php if(!empty($sub_menu)): if(!empty($key)): ?><h3><i class="icon icon-unfold"></i><?php echo ($key); ?></h3><?php endif; ?>
            <ul class="side-sub-menu">
                <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu): $mod = ($i % 2 );++$i;?><li>
                        <a class="item" href="<?php echo (u($menu["url"])); ?>"><?php echo ($menu["title"]); ?></a>
                    </li><?php endforeach; endif; else: echo "" ;endif; ?>
            </ul><?php endif; ?>
        <!-- /子導航 --><?php endforeach; endif; else: echo "" ;endif; ?>

 <h3>
 	<i class="icon <?php if(!in_array((ACTION_NAME), explode(',',"houseslist"))): ?>icon-fold<?php endif; ?>"></i>
 	最新樓盤
 </h3>
 	<ul class="side-sub-menu <?php if(!in_array((ACTION_NAME), explode(',',"houseslist"))): ?>subnav-off<?php endif; ?>">
 		<li <?php if((ACTION_NAME) == "houseslist"): ?>class="current"<?php endif; ?>><a class="item" href="<?php echo U('article/houseslist');?>">樓盤列表</a></li>
 		
 	</ul>
 <h3>
 	<i class="icon <?php if(!in_array((ACTION_NAME), explode(',',"rantel"))): ?>icon-fold<?php endif; ?>"></i>
 	委託租售
 </h3>
 	<ul class="side-sub-menu <?php if(!in_array((ACTION_NAME), explode(',',"rantel"))): ?>subnav-off<?php endif; ?>">
 		<li <?php if((ACTION_NAME) == "rantel"): ?>class="current"<?php endif; ?>><a class="item" href="<?php echo U('article/rantel');?>">樓盤出租</a></li>

 		<li <?php if((ACTION_NAME) == "sell"): ?>class="current"<?php endif; ?>><a class="item" href="<?php echo U('article/sell');?>">樓盤出售</a></li>	
 
 		
 		
		
 	</ul>
    <?php if(is_array($nodes)): $i = 0; $__LIST__ = $nodes;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$sub_menu): $mod = ($i % 2 );++$i;?><!-- 子導航 -->
        <?php if(!empty($sub_menu)): ?><h3>
            	<i class="icon <?php if(($sub_menu['current']) != "1"): ?>icon-fold<?php endif; ?>"></i>
            	<?php if(($sub_menu['allow_publish']) > "0"): ?><a class="item" href="<?php echo (u($sub_menu["url"])); ?>"><?php echo ($sub_menu["title"]); ?></a><?php else: echo ($sub_menu["title"]); endif; ?>
            </h3>
            <ul class="side-sub-menu <?php if(($sub_menu["current"]) != "1"): ?>subnav-off<?php endif; ?>">
                <?php if(is_array($sub_menu['_child'])): $i = 0; $__LIST__ = $sub_menu['_child'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu): $mod = ($i % 2 );++$i;?><li <?php if($menu['id'] == $cate_id or $menu['current'] == 1): ?>class="current"<?php endif; ?>>
                        <?php if(($menu['allow_publish']) > "0"): ?><a class="item" href="<?php echo (u($menu["url"])); ?>"><?php echo ($menu["title"]); ?></a><?php else: ?><a class="item" href="javascript:void(0);"><?php echo ($menu["title"]); ?></a><?php endif; ?>

                        <!-- 壹級子菜單 -->
                        <?php if(isset($menu['_child'])): ?><ul class="subitem">
                        	<?php if(is_array($menu['_child'])): foreach($menu['_child'] as $key=>$three_menu): ?><li>
                                <?php if(($three_menu['allow_publish']) > "0"): ?><a class="item" href="<?php echo (u($three_menu["url"])); ?>"><?php echo ($three_menu["title"]); ?></a><?php else: ?><a class="item" href="javascript:void(0);"><?php echo ($three_menu["title"]); ?></a><?php endif; ?>
                                <!-- 二級子菜單 -->
                                <?php if(isset($three_menu['_child'])): ?><ul class="subitem">
                                	<?php if(is_array($three_menu['_child'])): foreach($three_menu['_child'] as $key=>$four_menu): ?><li>
                                        <?php if(($four_menu['allow_publish']) > "0"): ?><a class="item" href="<?php echo U('index','cate_id='.$four_menu['id']);?>"><?php echo ($four_menu["title"]); ?></a><?php else: ?><a class="item" href="javascript:void(0);"><?php echo ($four_menu["title"]); ?></a><?php endif; ?>
                                        <!-- 三級子菜單 -->
                                        <?php if(isset($four_menu['_child'])): ?><ul class="subitem">
                                        	<?php if(is_array($four_menu['_child'])): $i = 0; $__LIST__ = $four_menu['_child'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$five_menu): $mod = ($i % 2 );++$i;?><li>
                                            	<?php if(($five_menu['allow_publish']) > "0"): ?><a class="item" href="<?php echo U('index','cate_id='.$five_menu['id']);?>"><?php echo ($five_menu["title"]); ?></a><?php else: ?><a class="item" href="javascript:void(0);"><?php echo ($five_menu["title"]); ?></a><?php endif; ?>
                                            </li><?php endforeach; endif; else: echo "" ;endif; ?>
                                        </ul><?php endif; ?>
                                        <!-- end 三級子菜單 -->
                                    </li><?php endforeach; endif; ?>
                                </ul><?php endif; ?>
                                <!-- end 二級子菜單 -->
                            </li><?php endforeach; endif; ?>
                        </ul><?php endif; ?>
                        <!-- end 壹級子菜單 -->
                    </li><?php endforeach; endif; else: echo "" ;endif; ?>
            </ul><?php endif; ?>
        <!-- /子導航 --><?php endforeach; endif; else: echo "" ;endif; ?>
  <ul class="side-sub-menu">
      <li></li>
 	</ul>
     <h3>
 	<i class="icon <?php if(!in_array((ACTION_NAME), explode(',',"rantel"))): ?>icon-fold<?php endif; ?>"></i>
        <a href="<?php echo U('article/message');?>">留言板</a>
 </h3>
 	<ul class="side-sub-menu <?php if(!in_array((ACTION_NAME), explode(',',"rantel"))): ?>subnav-off<?php endif; ?>">
 		
 
 		
 		
		
 	</ul>
    <!-- 回收站 -->
	<?php if(($show_recycle) == "1"): ?><h3>
        <em class="recycle"></em>
        <a href="<?php echo U('article/recycle');?>">回收站</a>
    </h3><?php endif; ?>
</div>
<script>
    $(function(){
      
        $(".side-sub-menu li").hover(function(){
            $(this).addClass("hover");
        },function(){
            $(this).removeClass("hover");
        });
    })
</script>


        <!-- /子導航 -->
    </div>
    <!-- /邊欄 -->

    <!-- 內容區 -->
    <div id="main-content">
        <div id="top-alert" class="fixed alert alert-error" style="display: none;">
            <button class="close fixed" style="margin-top: 4px;">&times;</button>
            <div class="alert-content">這是內容</div>
        </div>
        <div id="main" class="main">
            
            <!-- nav -->
            <?php if(!empty($_show_nav)): ?><div class="breadcrumb">
                <span>您的位置:</span>
                <?php $i = '1'; ?>
                <?php if(is_array($_nav)): foreach($_nav as $k=>$v): if($i == count($_nav)): ?><span><?php echo ($v); ?></span>
                    <?php else: ?>
                    <span><a href="<?php echo ($k); ?>"><?php echo ($v); ?></a>&gt;</span><?php endif; ?>
                    <?php $i = $i+1; endforeach; endif; ?>
            </div><?php endif; ?>
            <!-- nav -->
            

            
<script type="text/javascript" src="/sf/Public/static/uploadify/jquery.uploadify.min.js"></script>
    
	<div class="main-title cf">
		<h2>
		出租
		</h2>
	</div>
	<!-- 標簽頁導航 -->
<div class="tab-wrap">
<!--	<ul class="tab-nav nav">
		<?php $_result=parse_config_attr($model['field_group']);if(is_array($_result)): $i = 0; $__LIST__ = $_result;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$group): $mod = ($i % 2 );++$i;?><li data-tab="tab<?php echo ($key); ?>" <?php if(($key) == "1"): ?>class="current"<?php endif; ?>><a href="javascript:void(0);"><?php echo ($group); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
	</ul>-->
	<div class="tab-content">
    <style>
table{ width:100%; border-collapse:collapse; font-size:13px; background: #fff; margin-bottom: 10px}
table tr{ width:100%; height:50px;}
table tr td{ border:1px #ccc solid; padding-left:15px;}
.text1{ width:70%; height:50px; border:0; padding-left:10px; line-height:50px;}
.text2{ width:70%; height:90px; border:0; vertical-align:middle; padding:10px; line-height:15px; overflow: hidden; font-size: 12px}
</style>
              <table  id="printContent">
  <tr>
    <td colspan="3" style="text-align:center;">出租委託</td>
  </tr>
  <tr>
    <td>街道：<input readonly  type="text" class="text1"  value="<?php echo ($data["street"]); ?>"></td>
    <td>區域：<input readonly type="text" class="text1"  value="<?php echo ($data["range"]); ?>"></td>
    <td>面積：<input readonly type="text" class="text1" value="<?php echo ($data["area"]); ?>平方米"></td>
  </tr>
  <tr>
    <td>戶型：<input readonly  type="text" class="text1" value="<?php echo ($data["house_type"]); ?>"></td>
    <td>類型：<input readonly  type="text" class="text1" value="<?php echo ($data["types"]); ?>"></td>
    <td>樓層：<input readonly  type="text" class="text1" value="<?php echo ($data["floor"]); ?>"></td>
  </tr>
  <tr>
    <td>租期：<input  readonly type="text" class="text1" value="<?php echo ($data["rantaltime"]); ?>"></td>
    <td colspan="2">現狀：<input readonly type="text" class="text1" value="<?php echo ($data["statused"]); ?>"></td>
  </tr>
  <tr>
    <td>價格：<input readonly type="text" class="text1" value="<?php echo ($data["fee"]); ?>MOP"></td>
    <td colspan="2">包含管理費：<input readonly type="text" class="text1"></td>
  </tr>
  <tr>
    <td colspan="3">物業地址：<input readonly type="text" class="text1" value="<?php echo ($data["address"]); ?>"></td>
  </tr>
  <tr>
    <td colspan="3">裝修程度：<input readonly type="text" class="text1" value="<?php echo ($data["finish"]); ?>"></td>
  </tr>
  <tr>
    <td colspan="3">配套設施：<input readonly type="text" class="text1" value="<?php echo ($data["device"]); ?>"></td>
  </tr>
  <tr>
    <td colspan="3">訂金要求：<input readonly type="text" class="text1" value="<?php echo ($data["foregift"]); ?>"></td>
  </tr>
  <tr>
    <td colspan="3">房屋描述：<textarea readonly class="text2" ><?php echo ($data["description"]); ?></textarea></td>
  </tr>
  <tr>
      <td colspan="3">物業圖片：
     <div class="form-item cf">
                 
                    <div class="controls">

    
 <div class="upload-img-box">
       <?php if(!empty($data["photo"])): if(is_array($photo)): $i = 0; $__LIST__ = $photo;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo2): $mod = ($i % 2 );++$i;?><div class="upload-pre-item" style=' float: left; border: 0'>
                    <img style=' margin: 5px; border: 0' src="/sf<?php echo get_cover($vo2,$field = path);?>" data-id="<?php echo ($vo2); ?>">
                </div><?php endforeach; endif; else: echo "" ;endif; endif; ?>
 
    </div>
                            <script type="text/javascript">
    //上传图片
$(function(){
        /* 初始化上传插件*/
      
        $("#upload_picture_photo").uploadify({
            "height"          : 30,
            "swf"             : "/sf/Public/static/uploadify/uploadify.swf",
            "fileObjName"     : "download",
            "buttonText"      : "上传图片",
            "uploader"        : "/sf/index.php?s=/Admin/File/uploadPicture/session_id/2pieuumh72k3igkgkiruk66av4.html",
            "width"           : 120,
            'removeTimeout'   : 1,
            'fileTypeExts'    : '*.jpg; *.png; *.gif;',
            "onUploadSuccess" : uploadPicturephoto,
            'onFallback' : function() {
                alert('未检测到兼容版本的Flash.');
            }
        });
        $('.btn-close-photo').click(function(event) {
            
            event.preventDefault();
            $(this).parent().remove();
            picsbox = $("#upload_picture_photo").siblings('.upload-img-box');
            picArr = [];
            for (var i = 0; i < picsbox.children().length ; i++) {
                picArr.push(picsbox.children('.upload-pre-item:eq('+i+')').find('img').attr('data-id'));
            };
            picStr = picArr.join(',');
            $('.icon.photo').val(picStr);
        });
})
    function uploadPicturephoto(file, data){
        var data = $.parseJSON(data);
        var src = '';
        if(data.status){
            src = data.url || '/sf' + data.path;
            upload_img = "<div class='upload-pre-item'><img src=" + src +" title='点击显示大图' data-id="+data.id+"> <span class='btn-close btn-close-photo' title='删除图片'></span></div>";
            picsbox = $("#upload_picture_photo").siblings('.upload-img-box');
            picsbox.append(upload_img)
            picArr = [];
            for (var i = 0; i < picsbox.children().length ; i++) {
                picArr.push(picsbox.children('.upload-pre-item:eq('+i+')').find('img').attr('data-id'));
            };
            picStr = picArr.join(',');
            $('.icon.photo').val(picStr);
        } else {
            updateAlert(data.info);
            setTimeout(function(){
                $('#top-alert').find('button').click();
                $(that).removeClass('disabled').prop('disabled',false);
            },1500);
        }
    }
</script>                    </div>
                </div>
      </td>
  </tr>
  <tr>
    <td colspan="3" style="border-bottom:0;">委託人聯繫方式：</td>
  </tr>
  <tr>
    <td style="border:0; border-left:1px #ccc solid;">姓名：<input readonly type="text" class="text1" value="<?php echo ($data["wname"]); ?>"></td>
    <td style="border:0;">電話：<input readonly type="text" class="text1" value="<?php echo ($data["tel"]); ?>"></td>
    <td style="border:0; border-right:1px #ccc solid;">郵箱：<input readonly type="text" class="text1" value="<?php echo ($data["email"]); ?>"></td>
  </tr>
  <tr>
    <td colspan="3" style="border-top:0;">地址：<input readonly type="text" class="text1" value="<?php echo ($data["adds"]); ?>"></td>
  </tr>
  <tr>
    <td colspan="3" style="border-top:0;">委託時間：<input readonly type="text" class="text1" value="<?php echo (date('Y-m-d',$data["create_time"])); ?>"></td>
  </tr>
  
</table>
<script>
//打印
(function($) {  
var printAreaCount = 0; 
$.fn.printArea = function() {  
var ele = $(this);  
var idPrefix = "printArea_";  
removePrintArea( idPrefix + printAreaCount ); 
printAreaCount++;  
var iframeId = idPrefix + printAreaCount; 
var iframeStyle = 'position:absolute;width:0px;height:0px; font-size:12px'; 
iframe = document.createElement('IFRAME'); 
$(iframe).attr({ style : iframeStyle, id    : iframeId });
document.body.appendChild(iframe); 
var doc = iframe.contentWindow.document;
$(document).find("link").filter(function(){  
return $(this).attr("rel").toLowerCase() == "stylesheet"; 
}).each(function(){
doc.write('<link type="text/css" rel="stylesheet" href="' +$(this).attr("href") + '" >'); 
});  
doc.write('<div class="' + $(ele).attr("class") + '">' + $(ele).html() + '</div>'); 
doc.close();  
var frameWindow = iframe.contentWindow; 
frameWindow.close(); 
frameWindow.focus(); 
frameWindow.print(); 
}  
var removePrintArea = function(id) 
{  
$( "iframe#" + id ).remove(); 
};  
})(jQuery);
</script>
  <script type="text/javascript"> 
    $(function(){ 
    $("#btnPrint").click(function() { 
        $(".tab-content").printArea(); 
    }); 
});
 </script>          
		<div class="form-item cf" style=" margin-left: 40%">
			<button class="btn  btn-return"  id="btnPrint">列印</button>
			<a class="btn btn-return" href="<?php echo (cookie('__forward__')); ?>">返 回</a>
			<?php if(C('OPEN_DRAFTBOX') and (ACTION_NAME == 'add' or $data['status'] == 3)): endif; ?>
			<input type="hidden" name="id" value="<?php echo ((isset($data["id"]) && ($data["id"] !== ""))?($data["id"]):''); ?>"/>
			<input type="hidden" name="pid" value="<?php echo ((isset($data["pid"]) && ($data["pid"] !== ""))?($data["pid"]):''); ?>"/>
			<input type="hidden" name="model_id" value="<?php echo ((isset($data["model_id"]) && ($data["model_id"] !== ""))?($data["model_id"]):''); ?>"/>
			<input type="hidden" name="category_id" value="<?php echo ((isset($data["category_id"]) && ($data["category_id"] !== ""))?($data["category_id"]):''); ?>">
		</div>
	</div>
</div>

        </div>
        <div class="cont-ft">
            <div class="copyright">
                <div class="fl">感謝使用<a href="http://www.onethink.cn" target="_blank">OneThink</a>管理平臺</div>
                <div class="fr">V<?php echo (ONETHINK_VERSION); ?></div>
            </div>
        </div>
    </div>
    <!-- /內容區 -->
    <script type="text/javascript">
    (function(){
        var ThinkPHP = window.Think = {
            "ROOT"   : "/sf", //當前網站地址
            "APP"    : "/sf", //當前項目地址
            "PUBLIC" : "/sf/Public", //項目公共目錄地址
            "DEEP"   : "<?php echo C('URL_PATHINFO_DEPR');?>", //PATHINFO分割符
            "MODEL"  : ["<?php echo C('URL_MODEL');?>", "<?php echo C('URL_CASE_INSENSITIVE');?>", "<?php echo C('URL_HTML_SUFFIX');?>"],
            "VAR"    : ["<?php echo C('VAR_MODULE');?>", "<?php echo C('VAR_CONTROLLER');?>", "<?php echo C('VAR_ACTION');?>"]
        }
    })();
    </script>
    <script type="text/javascript" src="/sf/Public/static/think.js"></script>
    <script type="text/javascript" src="/sf/Public/Admin/js/common.js"></script>
    <script type="text/javascript">
        +function(){
            var $window = $(window), $subnav = $("#subnav"), url;
            $window.resize(function(){
                $("#main").css("min-height", $window.height() - 130);
            }).resize();

            /* 左邊菜單高亮 */
            url = window.location.pathname + window.location.search;
            url = url.replace(/(\/(p)\/\d+)|(&p=\d+)|(\/(id)\/\d+)|(&id=\d+)|(\/(group)\/\d+)|(&group=\d+)/, "");
            $subnav.find("a[href='" + url + "']").parent().addClass("current");

            /* 左邊菜單顯示收起 */
            $("#subnav").on("click", "h3", function(){
                var $this = $(this);
                $this.find(".icon").toggleClass("icon-fold");
                $this.next().slideToggle("fast").siblings(".side-sub-menu:visible").
                      prev("h3").find("i").addClass("icon-fold").end().end().hide();
            });

            $("#subnav h3 a").click(function(e){e.stopPropagation()});

            /* 頭部管理員菜單 */
            $(".user-bar").mouseenter(function(){
                var userMenu = $(this).children(".user-menu ");
                userMenu.removeClass("hidden");
                clearTimeout(userMenu.data("timeout"));
            }).mouseleave(function(){
                var userMenu = $(this).children(".user-menu");
                userMenu.data("timeout") && clearTimeout(userMenu.data("timeout"));
                userMenu.data("timeout", setTimeout(function(){userMenu.addClass("hidden")}, 100));
            });

	        /* 表單獲取焦點變色 */
	        $("form").on("focus", "input", function(){
		        $(this).addClass('focus');
	        }).on("blur","input",function(){
				        $(this).removeClass('focus');
			        });
		    $("form").on("focus", "textarea", function(){
			    $(this).closest('label').addClass('focus');
		    }).on("blur","textarea",function(){
			    $(this).closest('label').removeClass('focus');
		    });

            // 導航欄超出窗口高度後的模擬滾動條
            var sHeight = $(".sidebar").height();
            var subHeight  = $(".subnav").height();
            var diff = subHeight - sHeight; //250
            var sub = $(".subnav");
            if(diff > 0){
                $(window).mousewheel(function(event, delta){
                    if(delta>0){
                        if(parseInt(sub.css('marginTop'))>-10){
                            sub.css('marginTop','0px');
                        }else{
                            sub.css('marginTop','+='+10);
                        }
                    }else{
                        if(parseInt(sub.css('marginTop'))<'-'+(diff-10)){
                            sub.css('marginTop','-'+(diff-10));
                        }else{
                            sub.css('marginTop','-='+10);
                        }
                    }
                });
            }
        }();
    </script>
    
<link href="/sf/Public/static/datetimepicker/css/datetimepicker.css" rel="stylesheet" type="text/css">
<?php if(C('COLOR_STYLE')=='blue_color') echo '<link href="/sf/Public/static/datetimepicker/css/datetimepicker_blue.css" rel="stylesheet" type="text/css">'; ?>
<link href="/sf/Public/static/datetimepicker/css/dropdown.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/sf/Public/static/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="/sf/Public/static/datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js" charset="UTF-8"></script>
<script type="text/javascript">

Think.setValue("type", <?php echo ((isset($data["type"]) && ($data["type"] !== ""))?($data["type"]):'""'); ?>);
Think.setValue("display", <?php echo ((isset($data["display"]) && ($data["display"] !== ""))?($data["display"]):0); ?>);



$(function(){
    $('.time').datetimepicker({
        format: 'yyyy-mm-dd hh:ii',
        language:"zh-CN",
        minView:2,
        autoclose:true
    });
    showTab();

	<?php if(C('OPEN_DRAFTBOX') and (ACTION_NAME == 'add' or $data['status'] == 3)): ?>//保存草稿
	var interval;
	$('#autoSave').click(function(){
        var target_form = $(this).attr('target-form');
        var target = $(this).attr('url')
        var form = $('.'+target_form);
        var query = form.serialize();
        var that = this;

        $(that).addClass('disabled').attr('autocomplete','off').prop('disabled',true);
        $.post(target,query).success(function(data){
            if (data.status==1) {
                updateAlert(data.info ,'alert-success');
                $('input[name=id]').val(data.data.id);
            }else{
                updateAlert(data.info);
            }
            setTimeout(function(){
                $('#top-alert').find('button').click();
                $(that).removeClass('disabled').prop('disabled',false);
            },1500);
        })

        //重新開始定時器
        clearInterval(interval);
        autoSaveDraft();
        return false;
    });

	//Ctrl+S保存草稿
	$('body').keydown(function(e){
		if(e.ctrlKey && e.which == 83){
			$('#autoSave').click();
			return false;
		}
	});

	//每隔壹段時間保存草稿
	function autoSaveDraft(){
		interval = setInterval(function(){
			//只有基礎信息填寫了，才會觸發
			var title = $('input[name=title]').val();
			var name = $('input[name=name]').val();
			var des = $('textarea[name=description]').val();
			if(title != '' || name != '' || des != ''){
				$('#autoSave').click();
			}
		}, 1000*parseInt(<?php echo C('DRAFT_AOTOSAVE_INTERVAL');?>));
	}
	autoSaveDraft();<?php endif; ?>

});
</script>


</body>
</html>