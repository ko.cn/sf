<?php

/* 
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
namespace Home\Controller;
use OT\DataDictionary;

class PropertyController extends HomeController {


public function index(){

$this->bg();
$data1=D('Document')->pagetest(60);
$this->assign('_page',$data1['data1']);
$data2=D('Document')->pagetest(60);
$val = D('Document')->tag($data2['data2']);
  $this->assign('tag', $val);
$this->assign('all',$data2['data2']);

//條件搜索
//條件搜索
$search = D('Document')->lists(67);
foreach($search as $key)
{
$search1 = explode(',',$key['search_add']);
$search2 = explode(',',$key['search_type']);
$search3 = explode(',',$key['search_prices']);
foreach($search3 as $key2)
{
$search4[] = explode('-',$key2);
}
}
$this->assign('search1',$search1);
$this->assign('search2',$search2);
$this->assign('search4',$search4);
$this->display('new_Property');

}

public function detail(){
 $this->bg();
$id = $_GET['id'];
if(isset($id))
{
$list = D('Document')->detail($id);
$tag = explode(',', $list['tag']);

$photo = explode(',', $list['photo']);
$photo2 = explode(',', $list['photo2']);
$price =  number_format($list['houses_prices']); 
$this->assign('photo2',$photo2);
$this->assign('photo',$photo);
$this->assign('tags',$tag);
$this->assign('list',$list);
$this->assign('price',$price);
$this->display('Details/Details');

}

}
public function search(){
 $this->bg();
$title = $_POST['title'];
if(isset($title)){
$list = D('Document')->likesearch($title,60);
$list2 = D('Document')->likesearch($title,60);
$search = D('Document')->lists(67);
foreach($search as $key)
{
$search1 = explode(',',$key['search_add']);
$search2 = explode(',',$key['search_type']);
$search3 = explode(',',$key['search_prices']);
foreach($search3 as $key2)
{
$search4[] = explode('-',$key2);
}
}
$this->assign('search1',$search1);
$this->assign('search2',$search2);
$this->assign('search4',$search4);
$val = D('Document')->tag($list['data2']);

$this->assign('tag',$val);
$this->assign('all',$list['data2']);
$this->assign('_page',$list2['data1']);
$this->display('new_Property');

}
}
public function searchs(){
  $this->bg();  
//條件搜索
//條件搜索
$search = D('Document')->lists(67);
foreach($search as $key)
{
$search1 = explode(',',$key['search_add']);
$search2 = explode(',',$key['search_type']);
$search3 = explode(',',$key['search_prices']);
foreach($search3 as $key2)
{
$search4[] = explode('-',$key2);
}
}
$this->assign('search1',$search1);
$this->assign('search2',$search2);
$this->assign('search4',$search4);
//POST
$add = $_POST['add'];
$sell = $_POST['sell'];
$housestype =$_POST['housestype'];
$price = $_POST['houses_prices'];
if(!empty($add)||!empty($sell)||!empty($housestype)||!empty($price))
{
$prices = explode('-', $price);
$list = D('Document')->searchs($add,$housestype,$sell, $prices[0],$prices[1]); 
$tag = D('Document')->tag($list);
$this->assign('tag',$tag);
$this->assign('all',$list);
$this->display('new_Property');

}

}

public  function gmap(){
  $this->bg();
   if(isset($_POST['place']))
   {
       $list = $_POST['place'];
   
       $this->assign('list',$list);
       $this->display('Details/gmap');
   }
  

}
public function hot(){
 $this->bg();   
$data1=D('Document')->pagetest(60,1);
$this->assign('_page',$data1['data1']);
$data2=D('Document')->pagetest(60,1);
$val = D('Document')->tag($data2['data2']);
$this->assign('tag', $val);
$this->assign('all',$data2['data2']);
$this->display('new_Property');
}

public function bg(){
     $list = D('Document')->getlist(70);
 $this->assign('bg',$list[1]);
}
}
