<?php
/* 
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
namespace Home\Controller;
use OT\DataDictionary;

class ContactController extends HomeController {
    
public function index(){
$this->bg();
$list = D('Document')->getlist(71);
$this->assign('cus',$list[0]);
$this->display('contact');
}
public function update(){
$categoryid = 63;    
$data = array();
$data['title'] = $_POST['contacts'];
$data['name']= $_POST['contacts'];
$data['tel'] = $_POST['phone'];
$data['email'] =$_POST['email'];
$data['description'] = $_POST['content'];
$data['category_id']=$categoryid;
$data['uid']=1;
$data['pid']=0;
$data['model_id']=11;
$data['type']=2;
$data['status'] =1; 
$data['create_time']=  time();
$data['update_time']=time();
$res = D('Document')->updates($data);
if(!$res){
$this->error(D('Document')->getError());
}else{
SendMail(C('MAIL_FROMU'),C('MAIL_FROMTITLE'),C('MAIL_CONTENT'));
$this->success($res['id']?'更新成功':'發佈成功', Cookie('__forward__'));
}     
}
public function bg(){
   $list = D('Document')->getlist(70);
   $this->assign('bg',$list[4]);
}

}