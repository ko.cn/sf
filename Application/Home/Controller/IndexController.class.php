<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Home\Controller;
use OT\DataDictionary;

/**
* 前台首页控制器
* 主要获取首页聚合数据
*/
class IndexController extends HomeController {

//系统首页
public function index(){

//非热卖楼盘
$list = D('Document')->getlist(60);
$this->assign('list',$list);
//市場動態
$data1=D('Document')->pagetest(2,Null,3);
$this->assign('_page',$data1['data1']);
$data2=D('Document')->pagetest(2,Null,3);
$this->assign('presell',$data2['data2']);
//热卖楼盘
$list2 = D('Document')->limit($list2)->getlist(60,1);
$this->assign('listhot',$list2);
//廣告
$list3 = D('Document')->lists(68);
$this->assign('list3',$list3);
//條件搜索
$search = D('Document')->lists(67);
foreach($search as $key)
{
$search1 = explode(',',$key['search_add']);
$search2 = explode(',',$key['search_type']);
$search3 = explode(',',$key['search_prices']);
foreach($search3 as $key2)
{
    $search4[] = explode('-',$key2);
   }
}
$this->bg();
$this->assign('search1',$search1);
$this->assign('search2',$search2);
$this->assign('search4',$search4);
$this->display();
}
public function bg(){
 $list = D('Document')->getlist(70);
 $this->assign('bg',$list[0]);
}


}