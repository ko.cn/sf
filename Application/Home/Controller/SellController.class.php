<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Home\Controller;
use OT\DataDictionary;

class SellController extends HomeController
{
    public  function Index(){
        $data=M('Screening');
        $info=$data->find(1);
        $area=explode(",",$info['area']);
        $widespread=explode(",",$info['widespread']);
        $type=explode(",",$info['type']);
        $floor=explode(",",$info['floor']);
        $price=  explode(",", $info['price']);
        $this->assign('area', $area);
        $this->assign('widespread', $widespread);
        $this->assign('type', $type);
        $this->assign('floor', $floor);
        $this->assign('price',$price);
        $this->display('sell');
        
    }
    
    
}