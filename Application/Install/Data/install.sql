-- -----------------------------
-- Think MySQL Data Transfer 
-- Date : 2013-12-17 15:14:46
-- -----------------------------

SET FOREIGN_KEY_CHECKS = 0;


-- -----------------------------
-- Table structure for `onethink_action`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_action`;
CREATE TABLE `onethink_action` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `name` char(30) NOT NULL DEFAULT '' COMMENT '行為唯壹標識',
  `title` char(80) NOT NULL DEFAULT '' COMMENT '行為說明',
  `remark` char(140) NOT NULL DEFAULT '' COMMENT '行為描述',
  `rule` text NOT NULL COMMENT '行為規則',
  `log` text NOT NULL COMMENT '日誌規則',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '類型',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '狀態',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '修改時間',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='系統行為表';

-- -----------------------------
-- Records of `onethink_action`
-- -----------------------------
INSERT INTO `onethink_action` VALUES ('1', 'user_login', '用戶登錄', '積分+10，每天壹次', 'table:member|field:score|condition:uid={$self} AND status>-1|rule:score+10|cycle:24|max:1;', '[user|get_nickname]在[time|time_format]登錄了後臺', '1', '1', '1387181220');
INSERT INTO `onethink_action` VALUES ('2', 'add_article', '發布文章', '積分+5，每天上限5次', 'table:member|field:score|condition:uid={$self}|rule:score+5|cycle:24|max:5', '', '2', '0', '1380173180');
INSERT INTO `onethink_action` VALUES ('3', 'review', '評論', '評論積分+1，無限制', 'table:member|field:score|condition:uid={$self}|rule:score+1', '', '2', '1', '1383285646');
INSERT INTO `onethink_action` VALUES ('4', 'add_document', '發表文檔', '積分+10，每天上限5次', 'table:member|field:score|condition:uid={$self}|rule:score+10|cycle:24|max:5', '[user|get_nickname]在[time|time_format]發表了壹篇文章。\r\n表[model]，記錄編號[record]。', '2', '0', '1386139726');
INSERT INTO `onethink_action` VALUES ('5', 'add_document_topic', '發表討論', '積分+5，每天上限10次', 'table:member|field:score|condition:uid={$self}|rule:score+5|cycle:24|max:10', '', '2', '0', '1383285551');
INSERT INTO `onethink_action` VALUES ('6', 'update_config', '更新配置', '新增或修改或刪除配置', '', '', '1', '1', '1383294988');
INSERT INTO `onethink_action` VALUES ('7', 'update_model', '更新模型', '新增或修改模型', '', '', '1', '1', '1383295057');
INSERT INTO `onethink_action` VALUES ('8', 'update_attribute', '更新屬性', '新增或更新或刪除屬性', '', '', '1', '1', '1383295963');
INSERT INTO `onethink_action` VALUES ('9', 'update_channel', '更新導航', '新增或修改或刪除導航', '', '', '1', '1', '1383296301');
INSERT INTO `onethink_action` VALUES ('10', 'update_menu', '更新菜單', '新增或修改或刪除菜單', '', '', '1', '1', '1383296392');
INSERT INTO `onethink_action` VALUES ('11', 'update_category', '更新分類', '新增或修改或刪除分類', '', '', '1', '1', '1383296765');

-- -----------------------------
-- Table structure for `onethink_action_log`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_action_log`;
CREATE TABLE `onethink_action_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `action_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '行為id',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '執行用戶id',
  `action_ip` bigint(20) NOT NULL COMMENT '執行行為者ip',
  `model` varchar(50) NOT NULL DEFAULT '' COMMENT '觸發行為的表',
  `record_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '觸發行為的數據id',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '日誌備註',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '狀態',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '執行行為的時間',
  PRIMARY KEY (`id`),
  KEY `action_ip_ix` (`action_ip`),
  KEY `action_id_ix` (`action_id`),
  KEY `user_id_ix` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='行為日誌表';


-- -----------------------------
-- Table structure for `onethink_addons`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_addons`;
CREATE TABLE `onethink_addons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `name` varchar(40) NOT NULL COMMENT '插件名或標識',
  `title` varchar(20) NOT NULL DEFAULT '' COMMENT '中文名',
  `description` text COMMENT '插件描述',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '狀態',
  `config` text COMMENT '配置',
  `author` varchar(40) DEFAULT '' COMMENT '作者',
  `version` varchar(20) DEFAULT '' COMMENT '版本號',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '安裝時間',
  `has_adminlist` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否有後臺列表',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='插件表';

-- -----------------------------
-- Records of `onethink_addons`
-- -----------------------------
INSERT INTO `onethink_addons` VALUES ('15', 'EditorForAdmin', '後臺編輯器', '用於增強整站長文本的輸入和顯示', '1', '{\"editor_type\":\"2\",\"editor_wysiwyg\":\"1\",\"editor_height\":\"500px\",\"editor_resize_type\":\"1\"}', 'thinkphp', '0.1', '1383126253', '0');
INSERT INTO `onethink_addons` VALUES ('2', 'SiteStat', '站點統計信息', '統計站點的基礎信息', '1', '{\"title\":\"\\u7cfb\\u7edf\\u4fe1\\u606f\",\"width\":\"1\",\"display\":\"1\",\"status\":\"0\"}', 'thinkphp', '0.1', '1379512015', '0');
INSERT INTO `onethink_addons` VALUES ('3', 'DevTeam', '開發團隊信息', '開發團隊成員信息', '1', '{\"title\":\"OneThink\\u5f00\\u53d1\\u56e2\\u961f\",\"width\":\"2\",\"display\":\"1\"}', 'thinkphp', '0.1', '1379512022', '0');
INSERT INTO `onethink_addons` VALUES ('4', 'SystemInfo', '系統環境信息', '用於顯示壹些服務器的信息', '1', '{\"title\":\"\\u7cfb\\u7edf\\u4fe1\\u606f\",\"width\":\"2\",\"display\":\"1\"}', 'thinkphp', '0.1', '1379512036', '0');
INSERT INTO `onethink_addons` VALUES ('5', 'Editor', '前臺編輯器', '用於增強整站長文本的輸入和顯示', '1', '{\"editor_type\":\"2\",\"editor_wysiwyg\":\"1\",\"editor_height\":\"300px\",\"editor_resize_type\":\"1\"}', 'thinkphp', '0.1', '1379830910', '0');
INSERT INTO `onethink_addons` VALUES ('6', 'Attachment', '附件', '用於文檔模型上傳附件', '1', 'null', 'thinkphp', '0.1', '1379842319', '1');
INSERT INTO `onethink_addons` VALUES ('9', 'SocialComment', '通用社交化評論', '集成了各種社交化評論插件，輕松集成到系統中。', '1', '{\"comment_type\":\"1\",\"comment_uid_youyan\":\"\",\"comment_short_name_duoshuo\":\"\",\"comment_data_list_duoshuo\":\"\"}', 'thinkphp', '0.1', '1380273962', '0');

-- -----------------------------
-- Table structure for `onethink_attachment`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_attachment`;
CREATE TABLE `onethink_attachment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用戶ID',
  `title` char(30) NOT NULL DEFAULT '' COMMENT '附件顯示名',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '附件類型',
  `source` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '資源ID',
  `record_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '關聯記錄ID',
  `download` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '下載次數',
  `size` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '附件大小',
  `dir` int(12) unsigned NOT NULL DEFAULT '0' COMMENT '上級目錄ID',
  `sort` int(8) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '狀態',
  PRIMARY KEY (`id`),
  KEY `idx_record_status` (`record_id`,`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='附件表';


-- -----------------------------
-- Table structure for `onethink_attribute`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_attribute`;
CREATE TABLE `onethink_attribute` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '字段名',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '字段註釋',
  `field` varchar(100) NOT NULL DEFAULT '' COMMENT '字段定義',
  `type` varchar(20) NOT NULL DEFAULT '' COMMENT '數據類型',
  `value` varchar(100) NOT NULL DEFAULT '' COMMENT '字段默認值',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '備註',
  `is_show` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否顯示',
  `extra` varchar(255) NOT NULL DEFAULT '' COMMENT '參數',
  `model_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '模型id',
  `is_must` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否必填',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '狀態',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  `validate_rule` varchar(255) NOT NULL,
  `validate_time` tinyint(1) unsigned NOT NULL,
  `error_info` varchar(100) NOT NULL,
  `validate_type` varchar(25) NOT NULL,
  `auto_rule` varchar(100) NOT NULL,
  `auto_time` tinyint(1) unsigned NOT NULL,
  `auto_type` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
    KEY `model_id` (`model_id`)
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COMMENT='模型屬性表';

-- -----------------------------
-- Records of `onethink_attribute`
-- -----------------------------
INSERT INTO `onethink_attribute` VALUES ('1', 'uid', '用戶ID', 'int(10) unsigned NOT NULL ', 'num', '0', '', '0', '', '1', '0', '1', '1384508362', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('2', 'name', '標識', 'char(40) NOT NULL ', 'string', '', '同壹根節點下標識不重復', '1', '', '1', '0', '1', '1383894743', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('3', 'title', '標題', 'char(80) NOT NULL ', 'string', '', '文檔標題', '1', '', '1', '0', '1', '1383894778', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('4', 'category_id', '所屬分類', 'int(10) unsigned NOT NULL ', 'string', '', '', '0', '', '1', '0', '1', '1384508336', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('5', 'description', '描述', 'char(140) NOT NULL ', 'textarea', '', '', '1', '', '1', '0', '1', '1383894927', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('6', 'root', '根節點', 'int(10) unsigned NOT NULL ', 'num', '0', '該文檔的頂級文檔編號', '0', '', '1', '0', '1', '1384508323', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('7', 'pid', '所屬ID', 'int(10) unsigned NOT NULL ', 'num', '0', '父文檔編號', '0', '', '1', '0', '1', '1384508543', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('8', 'model_id', '內容模型ID', 'tinyint(3) unsigned NOT NULL ', 'num', '0', '該文檔所對應的模型', '0', '', '1', '0', '1', '1384508350', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('9', 'type', '內容類型', 'tinyint(3) unsigned NOT NULL ', 'select', '2', '', '1', '1:目錄\r\n2:主題\r\n3:段落', '1', '0', '1', '1384511157', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('10', 'position', '推薦位', 'smallint(5) unsigned NOT NULL ', 'checkbox', '0', '多個推薦則將其推薦值相加', '1', '1:列表推薦\r\n2:頻道頁推薦\r\n4:首頁推薦', '1', '0', '1', '1383895640', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('11', 'link_id', '外鏈', 'int(10) unsigned NOT NULL ', 'num', '0', '0-非外鏈，大於0-外鏈ID,需要函數進行鏈接與編號的轉換', '1', '', '1', '0', '1', '1383895757', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('12', 'cover_id', '封面', 'int(10) unsigned NOT NULL ', 'picture', '0', '0-無封面，大於0-封面圖片ID，需要函數處理', '1', '', '1', '0', '1', '1384147827', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('13', 'display', '可見性', 'tinyint(3) unsigned NOT NULL ', 'radio', '1', '', '1', '0:不可見\r\n1:所有人可見', '1', '0', '1', '1386662271', '1383891233', '', '0', '', 'regex', '', '0', 'function');
INSERT INTO `onethink_attribute` VALUES ('14', 'deadline', '截至時間', 'int(10) unsigned NOT NULL ', 'datetime', '0', '0-永久有效', '1', '', '1', '0', '1', '1387163248', '1383891233', '', '0', '', 'regex', '', '0', 'function');
INSERT INTO `onethink_attribute` VALUES ('15', 'attach', '附件數量', 'tinyint(3) unsigned NOT NULL ', 'num', '0', '', '0', '', '1', '0', '1', '1387260355', '1383891233', '', '0', '', 'regex', '', '0', 'function');
INSERT INTO `onethink_attribute` VALUES ('16', 'view', '瀏覽量', 'int(10) unsigned NOT NULL ', 'num', '0', '', '1', '', '1', '0', '1', '1383895835', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('17', 'comment', '評論數', 'int(10) unsigned NOT NULL ', 'num', '0', '', '1', '', '1', '0', '1', '1383895846', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('18', 'extend', '擴展統計字段', 'int(10) unsigned NOT NULL ', 'num', '0', '根據需求自行使用', '0', '', '1', '0', '1', '1384508264', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('19', 'level', '優先級', 'int(10) unsigned NOT NULL ', 'num', '0', '越高排序越靠前', '1', '', '1', '0', '1', '1383895894', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('20', 'create_time', '創建時間', 'int(10) unsigned NOT NULL ', 'datetime', '0', '', '1', '', '1', '0', '1', '1383895903', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('21', 'update_time', '更新時間', 'int(10) unsigned NOT NULL ', 'datetime', '0', '', '0', '', '1', '0', '1', '1384508277', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('22', 'status', '數據狀態', 'tinyint(4) NOT NULL ', 'radio', '0', '', '0', '-1:刪除\r\n0:禁用\r\n1:正常\r\n2:待審核\r\n3:草稿', '1', '0', '1', '1384508496', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('23', 'parse', '內容解析類型', 'tinyint(3) unsigned NOT NULL ', 'select', '0', '', '0', '0:html\r\n1:ubb\r\n2:markdown', '2', '0', '1', '1384511049', '1383891243', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('24', 'content', '文章內容', 'text NOT NULL ', 'editor', '', '', '1', '', '2', '0', '1', '1383896225', '1383891243', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('25', 'template', '詳情頁顯示模板', 'varchar(100) NOT NULL ', 'string', '', '參照display方法參數的定義', '1', '', '2', '0', '1', '1383896190', '1383891243', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('26', 'bookmark', '收藏數', 'int(10) unsigned NOT NULL ', 'num', '0', '', '1', '', '2', '0', '1', '1383896103', '1383891243', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('27', 'parse', '內容解析類型', 'tinyint(3) unsigned NOT NULL ', 'select', '0', '', '0', '0:html\r\n1:ubb\r\n2:markdown', '3', '0', '1', '1387260461', '1383891252', '', '0', '', 'regex', '', '0', 'function');
INSERT INTO `onethink_attribute` VALUES ('28', 'content', '下載詳細描述', 'text NOT NULL ', 'editor', '', '', '1', '', '3', '0', '1', '1383896438', '1383891252', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('29', 'template', '詳情頁顯示模板', 'varchar(100) NOT NULL ', 'string', '', '', '1', '', '3', '0', '1', '1383896429', '1383891252', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('30', 'file_id', '文件ID', 'int(10) unsigned NOT NULL ', 'file', '0', '需要函數處理', '1', '', '3', '0', '1', '1383896415', '1383891252', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('31', 'download', '下載次數', 'int(10) unsigned NOT NULL ', 'num', '0', '', '1', '', '3', '0', '1', '1383896380', '1383891252', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('32', 'size', '文件大小', 'bigint(20) unsigned NOT NULL ', 'num', '0', '單位bit', '1', '', '3', '0', '1', '1383896371', '1383891252', '', '0', '', '', '', '0', '');

-- -----------------------------
-- Table structure for `onethink_auth_extend`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_auth_extend`;
CREATE TABLE `onethink_auth_extend` (
  `group_id` mediumint(10) unsigned NOT NULL COMMENT '用戶id',
  `extend_id` mediumint(8) unsigned NOT NULL COMMENT '擴展表中數據的id',
  `type` tinyint(1) unsigned NOT NULL COMMENT '擴展類型標識 1:欄目分類權限;2:模型權限',
  UNIQUE KEY `group_extend_type` (`group_id`,`extend_id`,`type`),
  KEY `uid` (`group_id`),
  KEY `group_id` (`extend_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用戶組與分類的對應關系表';

-- -----------------------------
-- Records of `onethink_auth_extend`
-- -----------------------------
INSERT INTO `onethink_auth_extend` VALUES ('1', '1', '1');
INSERT INTO `onethink_auth_extend` VALUES ('1', '1', '2');
INSERT INTO `onethink_auth_extend` VALUES ('1', '2', '1');
INSERT INTO `onethink_auth_extend` VALUES ('1', '2', '2');
INSERT INTO `onethink_auth_extend` VALUES ('1', '3', '1');
INSERT INTO `onethink_auth_extend` VALUES ('1', '3', '2');
INSERT INTO `onethink_auth_extend` VALUES ('1', '4', '1');
INSERT INTO `onethink_auth_extend` VALUES ('1', '37', '1');

-- -----------------------------
-- Table structure for `onethink_auth_group`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_auth_group`;
CREATE TABLE `onethink_auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '用戶組id,自增主鍵',
  `module` varchar(20) NOT NULL COMMENT '用戶組所屬模塊',
  `type` tinyint(4) NOT NULL COMMENT '組類型',
  `title` char(20) NOT NULL DEFAULT '' COMMENT '用戶組中文名稱',
  `description` varchar(80) NOT NULL DEFAULT '' COMMENT '描述信息',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '用戶組狀態：為1正常，為0禁用,-1為刪除',
  `rules` varchar(500) NOT NULL DEFAULT '' COMMENT '用戶組擁有的規則id，多個規則 , 隔開',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `onethink_auth_group`
-- -----------------------------
INSERT INTO `onethink_auth_group` VALUES ('1', 'admin', '1', '默認用戶組', '', '1', '1,2,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,79,80,81,82,83,84,86,87,88,89,90,91,92,93,94,95,96,97,100,102,103,105,106');
INSERT INTO `onethink_auth_group` VALUES ('2', 'admin', '1', '測試用戶', '測試用戶', '1', '1,2,5,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,79,80,82,83,84,88,89,90,91,92,93,96,97,100,102,103,195');

-- -----------------------------
-- Table structure for `onethink_auth_group_access`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_auth_group_access`;
CREATE TABLE `onethink_auth_group_access` (
  `uid` int(10) unsigned NOT NULL COMMENT '用戶id',
  `group_id` mediumint(8) unsigned NOT NULL COMMENT '用戶組id',
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- -----------------------------
-- Table structure for `onethink_auth_rule`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_auth_rule`;
CREATE TABLE `onethink_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '規則id,自增主鍵',
  `module` varchar(20) NOT NULL COMMENT '規則所屬module',
  `type` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1-url;2-主菜單',
  `name` char(80) NOT NULL DEFAULT '' COMMENT '規則唯壹英文標識',
  `title` char(20) NOT NULL DEFAULT '' COMMENT '規則中文描述',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否有效(0:無效,1:有效)',
  `condition` varchar(300) NOT NULL DEFAULT '' COMMENT '規則附加條件',
  PRIMARY KEY (`id`),
  KEY `module` (`module`,`status`,`type`)
) ENGINE=MyISAM AUTO_INCREMENT=217 DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `onethink_auth_rule`
-- -----------------------------
INSERT INTO `onethink_auth_rule` VALUES ('1', 'admin', '2', 'Admin/Index/index', '首頁', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('2', 'admin', '2', 'Admin/Article/mydocument', '內容', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('3', 'admin', '2', 'Admin/User/index', '用戶', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('4', 'admin', '2', 'Admin/Addons/index', '擴展', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('5', 'admin', '2', 'Admin/Config/group', '系統', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('7', 'admin', '1', 'Admin/article/add', '新增', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('8', 'admin', '1', 'Admin/article/edit', '編輯', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('9', 'admin', '1', 'Admin/article/setStatus', '改變狀態', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('10', 'admin', '1', 'Admin/article/update', '保存', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('11', 'admin', '1', 'Admin/article/autoSave', '保存草稿', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('12', 'admin', '1', 'Admin/article/move', '移動', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('13', 'admin', '1', 'Admin/article/copy', '復制', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('14', 'admin', '1', 'Admin/article/paste', '粘貼', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('15', 'admin', '1', 'Admin/article/permit', '還原', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('16', 'admin', '1', 'Admin/article/clear', '清空', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('17', 'admin', '1', 'Admin/article/index', '文檔列表', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('18', 'admin', '1', 'Admin/article/recycle', '回收站', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('19', 'admin', '1', 'Admin/User/addaction', '新增用戶行為', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('20', 'admin', '1', 'Admin/User/editaction', '編輯用戶行為', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('21', 'admin', '1', 'Admin/User/saveAction', '保存用戶行為', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('22', 'admin', '1', 'Admin/User/setStatus', '變更行為狀態', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('23', 'admin', '1', 'Admin/User/changeStatus?method=forbidUser', '禁用會員', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('24', 'admin', '1', 'Admin/User/changeStatus?method=resumeUser', '啟用會員', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('25', 'admin', '1', 'Admin/User/changeStatus?method=deleteUser', '刪除會員', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('26', 'admin', '1', 'Admin/User/index', '用戶信息', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('27', 'admin', '1', 'Admin/User/action', '用戶行為', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('28', 'admin', '1', 'Admin/AuthManager/changeStatus?method=deleteGroup', '刪除', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('29', 'admin', '1', 'Admin/AuthManager/changeStatus?method=forbidGroup', '禁用', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('30', 'admin', '1', 'Admin/AuthManager/changeStatus?method=resumeGroup', '恢復', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('31', 'admin', '1', 'Admin/AuthManager/createGroup', '新增', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('32', 'admin', '1', 'Admin/AuthManager/editGroup', '編輯', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('33', 'admin', '1', 'Admin/AuthManager/writeGroup', '保存用戶組', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('34', 'admin', '1', 'Admin/AuthManager/group', '授權', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('35', 'admin', '1', 'Admin/AuthManager/access', '訪問授權', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('36', 'admin', '1', 'Admin/AuthManager/user', '成員授權', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('37', 'admin', '1', 'Admin/AuthManager/removeFromGroup', '解除授權', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('38', 'admin', '1', 'Admin/AuthManager/addToGroup', '保存成員授權', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('39', 'admin', '1', 'Admin/AuthManager/category', '分類授權', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('40', 'admin', '1', 'Admin/AuthManager/addToCategory', '保存分類授權', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('41', 'admin', '1', 'Admin/AuthManager/index', '權限管理', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('42', 'admin', '1', 'Admin/Addons/create', '創建', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('43', 'admin', '1', 'Admin/Addons/checkForm', '檢測創建', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('44', 'admin', '1', 'Admin/Addons/preview', '預覽', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('45', 'admin', '1', 'Admin/Addons/build', '快速生成插件', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('46', 'admin', '1', 'Admin/Addons/config', '設置', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('47', 'admin', '1', 'Admin/Addons/disable', '禁用', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('48', 'admin', '1', 'Admin/Addons/enable', '啟用', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('49', 'admin', '1', 'Admin/Addons/install', '安裝', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('50', 'admin', '1', 'Admin/Addons/uninstall', '卸載', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('51', 'admin', '1', 'Admin/Addons/saveconfig', '更新配置', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('52', 'admin', '1', 'Admin/Addons/adminList', '插件後臺列表', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('53', 'admin', '1', 'Admin/Addons/execute', 'URL方式訪問插件', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('54', 'admin', '1', 'Admin/Addons/index', '插件管理', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('55', 'admin', '1', 'Admin/Addons/hooks', '鉤子管理', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('56', 'admin', '1', 'Admin/model/add', '新增', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('57', 'admin', '1', 'Admin/model/edit', '編輯', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('58', 'admin', '1', 'Admin/model/setStatus', '改變狀態', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('59', 'admin', '1', 'Admin/model/update', '保存數據', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('60', 'admin', '1', 'Admin/Model/index', '模型管理', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('61', 'admin', '1', 'Admin/Config/edit', '編輯', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('62', 'admin', '1', 'Admin/Config/del', '刪除', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('63', 'admin', '1', 'Admin/Config/add', '新增', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('64', 'admin', '1', 'Admin/Config/save', '保存', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('65', 'admin', '1', 'Admin/Config/group', '網站設置', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('66', 'admin', '1', 'Admin/Config/index', '配置管理', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('67', 'admin', '1', 'Admin/Channel/add', '新增', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('68', 'admin', '1', 'Admin/Channel/edit', '編輯', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('69', 'admin', '1', 'Admin/Channel/del', '刪除', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('70', 'admin', '1', 'Admin/Channel/index', '導航管理', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('71', 'admin', '1', 'Admin/Category/edit', '編輯', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('72', 'admin', '1', 'Admin/Category/add', '新增', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('73', 'admin', '1', 'Admin/Category/remove', '刪除', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('74', 'admin', '1', 'Admin/Category/index', '分類管理', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('75', 'admin', '1', 'Admin/file/upload', '上傳控件', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('76', 'admin', '1', 'Admin/file/uploadPicture', '上傳圖片', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('77', 'admin', '1', 'Admin/file/download', '下載', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('94', 'admin', '1', 'Admin/AuthManager/modelauth', '模型授權', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('79', 'admin', '1', 'Admin/article/batchOperate', '導入', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('80', 'admin', '1', 'Admin/Database/index?type=export', '備份數據庫', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('81', 'admin', '1', 'Admin/Database/index?type=import', '還原數據庫', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('82', 'admin', '1', 'Admin/Database/export', '備份', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('83', 'admin', '1', 'Admin/Database/optimize', '優化表', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('84', 'admin', '1', 'Admin/Database/repair', '修復表', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('86', 'admin', '1', 'Admin/Database/import', '恢復', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('87', 'admin', '1', 'Admin/Database/del', '刪除', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('88', 'admin', '1', 'Admin/User/add', '新增用戶', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('89', 'admin', '1', 'Admin/Attribute/index', '屬性管理', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('90', 'admin', '1', 'Admin/Attribute/add', '新增', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('91', 'admin', '1', 'Admin/Attribute/edit', '編輯', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('92', 'admin', '1', 'Admin/Attribute/setStatus', '改變狀態', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('93', 'admin', '1', 'Admin/Attribute/update', '保存數據', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('95', 'admin', '1', 'Admin/AuthManager/addToModel', '保存模型授權', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('96', 'admin', '1', 'Admin/Category/move', '移動', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('97', 'admin', '1', 'Admin/Category/merge', '合並', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('98', 'admin', '1', 'Admin/Config/menu', '後臺菜單管理', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('99', 'admin', '1', 'Admin/Article/mydocument', '內容', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('100', 'admin', '1', 'Admin/Menu/index', '菜單管理', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('101', 'admin', '1', 'Admin/other', '其他', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('102', 'admin', '1', 'Admin/Menu/add', '新增', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('103', 'admin', '1', 'Admin/Menu/edit', '編輯', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('104', 'admin', '1', 'Admin/Think/lists?model=article', '文章管理', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('105', 'admin', '1', 'Admin/Think/lists?model=download', '下載管理', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('106', 'admin', '1', 'Admin/Think/lists?model=config', '配置管理', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('107', 'admin', '1', 'Admin/Action/actionlog', '行為日誌', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('108', 'admin', '1', 'Admin/User/updatePassword', '修改密碼', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('109', 'admin', '1', 'Admin/User/updateNickname', '修改昵稱', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('110', 'admin', '1', 'Admin/action/edit', '查看行為日誌', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('205', 'admin', '1', 'Admin/think/add', '新增數據', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('111', 'admin', '2', 'Admin/article/index', '文檔列表', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('112', 'admin', '2', 'Admin/article/add', '新增', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('113', 'admin', '2', 'Admin/article/edit', '編輯', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('114', 'admin', '2', 'Admin/article/setStatus', '改變狀態', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('115', 'admin', '2', 'Admin/article/update', '保存', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('116', 'admin', '2', 'Admin/article/autoSave', '保存草稿', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('117', 'admin', '2', 'Admin/article/move', '移動', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('118', 'admin', '2', 'Admin/article/copy', '復制', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('119', 'admin', '2', 'Admin/article/paste', '粘貼', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('120', 'admin', '2', 'Admin/article/batchOperate', '導入', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('121', 'admin', '2', 'Admin/article/recycle', '回收站', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('122', 'admin', '2', 'Admin/article/permit', '還原', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('123', 'admin', '2', 'Admin/article/clear', '清空', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('124', 'admin', '2', 'Admin/User/add', '新增用戶', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('125', 'admin', '2', 'Admin/User/action', '用戶行為', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('126', 'admin', '2', 'Admin/User/addAction', '新增用戶行為', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('127', 'admin', '2', 'Admin/User/editAction', '編輯用戶行為', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('128', 'admin', '2', 'Admin/User/saveAction', '保存用戶行為', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('129', 'admin', '2', 'Admin/User/setStatus', '變更行為狀態', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('130', 'admin', '2', 'Admin/User/changeStatus?method=forbidUser', '禁用會員', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('131', 'admin', '2', 'Admin/User/changeStatus?method=resumeUser', '啟用會員', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('132', 'admin', '2', 'Admin/User/changeStatus?method=deleteUser', '刪除會員', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('133', 'admin', '2', 'Admin/AuthManager/index', '權限管理', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('134', 'admin', '2', 'Admin/AuthManager/changeStatus?method=deleteGroup', '刪除', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('135', 'admin', '2', 'Admin/AuthManager/changeStatus?method=forbidGroup', '禁用', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('136', 'admin', '2', 'Admin/AuthManager/changeStatus?method=resumeGroup', '恢復', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('137', 'admin', '2', 'Admin/AuthManager/createGroup', '新增', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('138', 'admin', '2', 'Admin/AuthManager/editGroup', '編輯', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('139', 'admin', '2', 'Admin/AuthManager/writeGroup', '保存用戶組', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('140', 'admin', '2', 'Admin/AuthManager/group', '授權', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('141', 'admin', '2', 'Admin/AuthManager/access', '訪問授權', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('142', 'admin', '2', 'Admin/AuthManager/user', '成員授權', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('143', 'admin', '2', 'Admin/AuthManager/removeFromGroup', '解除授權', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('144', 'admin', '2', 'Admin/AuthManager/addToGroup', '保存成員授權', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('145', 'admin', '2', 'Admin/AuthManager/category', '分類授權', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('146', 'admin', '2', 'Admin/AuthManager/addToCategory', '保存分類授權', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('147', 'admin', '2', 'Admin/AuthManager/modelauth', '模型授權', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('148', 'admin', '2', 'Admin/AuthManager/addToModel', '保存模型授權', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('149', 'admin', '2', 'Admin/Addons/create', '創建', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('150', 'admin', '2', 'Admin/Addons/checkForm', '檢測創建', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('151', 'admin', '2', 'Admin/Addons/preview', '預覽', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('152', 'admin', '2', 'Admin/Addons/build', '快速生成插件', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('153', 'admin', '2', 'Admin/Addons/config', '設置', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('154', 'admin', '2', 'Admin/Addons/disable', '禁用', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('155', 'admin', '2', 'Admin/Addons/enable', '啟用', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('156', 'admin', '2', 'Admin/Addons/install', '安裝', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('157', 'admin', '2', 'Admin/Addons/uninstall', '卸載', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('158', 'admin', '2', 'Admin/Addons/saveconfig', '更新配置', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('159', 'admin', '2', 'Admin/Addons/adminList', '插件後臺列表', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('160', 'admin', '2', 'Admin/Addons/execute', 'URL方式訪問插件', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('161', 'admin', '2', 'Admin/Addons/hooks', '鉤子管理', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('162', 'admin', '2', 'Admin/Model/index', '模型管理', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('163', 'admin', '2', 'Admin/model/add', '新增', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('164', 'admin', '2', 'Admin/model/edit', '編輯', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('165', 'admin', '2', 'Admin/model/setStatus', '改變狀態', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('166', 'admin', '2', 'Admin/model/update', '保存數據', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('167', 'admin', '2', 'Admin/Attribute/index', '屬性管理', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('168', 'admin', '2', 'Admin/Attribute/add', '新增', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('169', 'admin', '2', 'Admin/Attribute/edit', '編輯', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('170', 'admin', '2', 'Admin/Attribute/setStatus', '改變狀態', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('171', 'admin', '2', 'Admin/Attribute/update', '保存數據', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('172', 'admin', '2', 'Admin/Config/index', '配置管理', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('173', 'admin', '2', 'Admin/Config/edit', '編輯', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('174', 'admin', '2', 'Admin/Config/del', '刪除', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('175', 'admin', '2', 'Admin/Config/add', '新增', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('176', 'admin', '2', 'Admin/Config/save', '保存', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('177', 'admin', '2', 'Admin/Menu/index', '菜單管理', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('178', 'admin', '2', 'Admin/Channel/index', '導航管理', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('179', 'admin', '2', 'Admin/Channel/add', '新增', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('180', 'admin', '2', 'Admin/Channel/edit', '編輯', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('181', 'admin', '2', 'Admin/Channel/del', '刪除', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('182', 'admin', '2', 'Admin/Category/index', '分類管理', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('183', 'admin', '2', 'Admin/Category/edit', '編輯', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('184', 'admin', '2', 'Admin/Category/add', '新增', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('185', 'admin', '2', 'Admin/Category/remove', '刪除', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('186', 'admin', '2', 'Admin/Category/move', '移動', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('187', 'admin', '2', 'Admin/Category/merge', '合並', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('188', 'admin', '2', 'Admin/Database/index?type=export', '備份數據庫', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('189', 'admin', '2', 'Admin/Database/export', '備份', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('190', 'admin', '2', 'Admin/Database/optimize', '優化表', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('191', 'admin', '2', 'Admin/Database/repair', '修復表', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('192', 'admin', '2', 'Admin/Database/index?type=import', '還原數據庫', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('193', 'admin', '2', 'Admin/Database/import', '恢復', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('194', 'admin', '2', 'Admin/Database/del', '刪除', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('195', 'admin', '2', 'Admin/other', '其他', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('196', 'admin', '2', 'Admin/Menu/add', '新增', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('197', 'admin', '2', 'Admin/Menu/edit', '編輯', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('198', 'admin', '2', 'Admin/Think/lists?model=article', '應用', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('199', 'admin', '2', 'Admin/Think/lists?model=download', '下載管理', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('200', 'admin', '2', 'Admin/Think/lists?model=config', '應用', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('201', 'admin', '2', 'Admin/Action/actionlog', '行為日誌', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('202', 'admin', '2', 'Admin/User/updatePassword', '修改密碼', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('203', 'admin', '2', 'Admin/User/updateNickname', '修改昵稱', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('204', 'admin', '2', 'Admin/action/edit', '查看行為日誌', '-1', '');
INSERT INTO `onethink_auth_rule` VALUES ('206', 'admin', '1', 'Admin/think/edit', '編輯數據', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('207', 'admin', '1', 'Admin/Menu/import', '導入', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('208', 'admin', '1', 'Admin/Model/generate', '生成', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('209', 'admin', '1', 'Admin/Addons/addHook', '新增鉤子', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('210', 'admin', '1', 'Admin/Addons/edithook', '編輯鉤子', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('211', 'admin', '1', 'Admin/Article/sort', '文檔排序', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('212', 'admin', '1', 'Admin/Config/sort', '排序', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('213', 'admin', '1', 'Admin/Menu/sort', '排序', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('214', 'admin', '1', 'Admin/Channel/sort', '排序', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('215', 'admin', '1', 'Admin/Category/operate/type/move', '移動', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('216', 'admin', '1', 'Admin/Category/operate/type/merge', '合並', '1', '');

-- -----------------------------
-- Table structure for `onethink_category`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_category`;
CREATE TABLE `onethink_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '分類ID',
  `name` varchar(30) NOT NULL COMMENT '標誌',
  `title` varchar(50) NOT NULL COMMENT '標題',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上級分類ID',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序（同級有效）',
  `list_row` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '列表每頁行數',
  `meta_title` varchar(50) NOT NULL DEFAULT '' COMMENT 'SEO的網頁標題',
  `keywords` varchar(255) NOT NULL DEFAULT '' COMMENT '關鍵字',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  `template_index` varchar(100) NOT NULL COMMENT '頻道頁模板',
  `template_lists` varchar(100) NOT NULL COMMENT '列表頁模板',
  `template_detail` varchar(100) NOT NULL COMMENT '詳情頁模板',
  `template_edit` varchar(100) NOT NULL COMMENT '編輯頁模板',
  `model` varchar(100) NOT NULL DEFAULT '' COMMENT '關聯模型',
  `type` varchar(100) NOT NULL DEFAULT '' COMMENT '允許發布的內容類型',
  `link_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '外鏈',
  `allow_publish` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否允許發布內容',
  `display` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '可見性',
  `reply` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否允許回復',
  `check` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '發布的文章是否需要審核',
  `reply_model` varchar(100) NOT NULL DEFAULT '',
  `extend` text NOT NULL COMMENT '擴展設置',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '數據狀態',
  `icon` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '分類圖標',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`name`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COMMENT='分類表';

-- -----------------------------
-- Records of `onethink_category`
-- -----------------------------
INSERT INTO `onethink_category` VALUES ('1', 'blog', '博客', '0', '0', '10', '', '', '', '', '', '', '', '2', '2,1', '0', '0', '1', '0', '0', '1', '', '1379474947', '1382701539', '1', '0');
INSERT INTO `onethink_category` VALUES ('2', 'default_blog', '默認分類', '1', '1', '10', '', '', '', '', '', '', '', '2', '2,1,3', '0', '1', '1', '0', '1', '1', '', '1379475028', '1386839751', '1', '31');

-- -----------------------------
-- Table structure for `onethink_channel`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_channel`;
CREATE TABLE `onethink_channel` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '頻道ID',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上級頻道ID',
  `title` char(30) NOT NULL COMMENT '頻道標題',
  `url` char(100) NOT NULL COMMENT '頻道連接',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '導航排序',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '狀態',
  `target` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '新窗口打開',
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `onethink_channel`
-- -----------------------------
INSERT INTO `onethink_channel` VALUES ('1', '0', '首頁', 'Index/index', '1', '1379475111', '1379923177', '1', '0');
INSERT INTO `onethink_channel` VALUES ('2', '0', '博客', 'Article/index?category=blog', '2', '1379475131', '1379483713', '1', '0');
INSERT INTO `onethink_channel` VALUES ('3', '0', '官網', 'http://www.onethink.cn', '3', '1379475154', '1387163458', '1', '0');

-- -----------------------------
-- Table structure for `onethink_config`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_config`;
CREATE TABLE `onethink_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '配置ID',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '配置名稱',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '配置類型',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '配置說明',
  `group` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '配置分組',
  `extra` varchar(255) NOT NULL DEFAULT '' COMMENT '配置值',
  `remark` varchar(100) NOT NULL COMMENT '配置說明',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '狀態',
  `value` text NOT NULL COMMENT '配置值',
  `sort` smallint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`name`),
  KEY `type` (`type`),
  KEY `group` (`group`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `onethink_config`
-- -----------------------------
INSERT INTO `onethink_config` VALUES ('1', 'WEB_SITE_TITLE', '1', '網站標題', '1', '', '網站標題前臺顯示標題', '1378898976', '1379235274', '1', 'OneThink內容管理框架', '0');
INSERT INTO `onethink_config` VALUES ('2', 'WEB_SITE_DESCRIPTION', '2', '網站描述', '1', '', '網站搜索引擎描述', '1378898976', '1379235841', '1', 'OneThink內容管理框架', '1');
INSERT INTO `onethink_config` VALUES ('3', 'WEB_SITE_KEYWORD', '2', '網站關鍵字', '1', '', '網站搜索引擎關鍵字', '1378898976', '1381390100', '1', 'ThinkPHP,OneThink', '8');
INSERT INTO `onethink_config` VALUES ('4', 'WEB_SITE_CLOSE', '4', '關閉站點', '1', '0:關閉,1:開啟', '站點關閉後其他用戶不能訪問，管理員可以正常訪問', '1378898976', '1379235296', '1', '1', '1');
INSERT INTO `onethink_config` VALUES ('9', 'CONFIG_TYPE_LIST', '3', '配置類型列表', '4', '', '主要用於數據解析和頁面表單的生成', '1378898976', '1379235348', '1', '0:數字\r\n1:字符\r\n2:文本\r\n3:數組\r\n4:枚舉', '2');
INSERT INTO `onethink_config` VALUES ('10', 'WEB_SITE_ICP', '1', '網站備案號', '1', '', '設置在網站底部顯示的備案號，如“滬ICP備12007941號-2', '1378900335', '1379235859', '1', '', '9');
INSERT INTO `onethink_config` VALUES ('11', 'DOCUMENT_POSITION', '3', '文檔推薦位', '2', '', '文檔推薦位，推薦到多個位置KEY值相加即可', '1379053380', '1379235329', '1', '1:列表頁推薦\r\n2:頻道頁推薦\r\n4:網站首頁推薦', '3');
INSERT INTO `onethink_config` VALUES ('12', 'DOCUMENT_DISPLAY', '3', '文檔可見性', '2', '', '文章可見性僅影響前臺顯示，後臺不收影響', '1379056370', '1379235322', '1', '0:所有人可見\r\n1:僅註冊會員可見\r\n2:僅管理員可見', '4');
INSERT INTO `onethink_config` VALUES ('13', 'COLOR_STYLE', '4', '後臺色系', '1', 'default_color:默認\r\nblue_color:紫羅蘭', '後臺顏色風格', '1379122533', '1379235904', '1', 'default_color', '10');
INSERT INTO `onethink_config` VALUES ('20', 'CONFIG_GROUP_LIST', '3', '配置分組', '4', '', '配置分組', '1379228036', '1384418383', '1', '1:基本\r\n2:內容\r\n3:用戶\r\n4:系統', '4');
INSERT INTO `onethink_config` VALUES ('21', 'HOOKS_TYPE', '3', '鉤子的類型', '4', '', '類型 1-用於擴展顯示內容，2-用於擴展業務處理', '1379313397', '1379313407', '1', '1:視圖\r\n2:控制器', '6');
INSERT INTO `onethink_config` VALUES ('22', 'AUTH_CONFIG', '3', 'Auth配置', '4', '', '自定義Auth.class.php類配置', '1379409310', '1379409564', '1', 'AUTH_ON:1\r\nAUTH_TYPE:2', '8');
INSERT INTO `onethink_config` VALUES ('23', 'OPEN_DRAFTBOX', '4', '是否開啟草稿功能', '2', '0:關閉草稿功能\r\n1:開啟草稿功能\r\n', '新增文章時的草稿功能配置', '1379484332', '1379484591', '1', '1', '1');
INSERT INTO `onethink_config` VALUES ('24', 'DRAFT_AOTOSAVE_INTERVAL', '0', '自動保存草稿時間', '2', '', '自動保存草稿的時間間隔，單位：秒', '1379484574', '1386143323', '1', '60', '2');
INSERT INTO `onethink_config` VALUES ('25', 'LIST_ROWS', '0', '後臺每頁記錄數', '2', '', '後臺數據每頁顯示記錄數', '1379503896', '1380427745', '1', '10', '10');
INSERT INTO `onethink_config` VALUES ('26', 'USER_ALLOW_REGISTER', '4', '是否允許用戶註冊', '3', '0:關閉註冊\r\n1:允許註冊', '是否開放用戶註冊', '1379504487', '1379504580', '1', '1', '3');
INSERT INTO `onethink_config` VALUES ('27', 'CODEMIRROR_THEME', '4', '預覽插件的CodeMirror主題', '4', '3024-day:3024 day\r\n3024-night:3024 night\r\nambiance:ambiance\r\nbase16-dark:base16 dark\r\nbase16-light:base16 light\r\nblackboard:blackboard\r\ncobalt:cobalt\r\neclipse:eclipse\r\nelegant:elegant\r\nerlang-dark:erlang-dark\r\nlesser-dark:lesser-dark\r\nmidnight:midnight', '詳情見CodeMirror官網', '1379814385', '1384740813', '1', 'ambiance', '3');
INSERT INTO `onethink_config` VALUES ('28', 'DATA_BACKUP_PATH', '1', '數據庫備份根路徑', '4', '', '路徑必須以 / 結尾', '1381482411', '1381482411', '1', './Data/', '5');
INSERT INTO `onethink_config` VALUES ('29', 'DATA_BACKUP_PART_SIZE', '0', '數據庫備份卷大小', '4', '', '該值用於限制壓縮後的分卷最大長度。單位：B；建議設置20M', '1381482488', '1381729564', '1', '20971520', '7');
INSERT INTO `onethink_config` VALUES ('30', 'DATA_BACKUP_COMPRESS', '4', '數據庫備份文件是否啟用壓縮', '4', '0:不壓縮\r\n1:啟用壓縮', '壓縮備份文件需要PHP環境支持gzopen,gzwrite函數', '1381713345', '1381729544', '1', '1', '9');
INSERT INTO `onethink_config` VALUES ('31', 'DATA_BACKUP_COMPRESS_LEVEL', '4', '數據庫備份文件壓縮級別', '4', '1:普通\r\n4:壹般\r\n9:最高', '數據庫備份文件的壓縮級別，該配置在開啟壓縮時生效', '1381713408', '1381713408', '1', '9', '10');
INSERT INTO `onethink_config` VALUES ('32', 'DEVELOP_MODE', '4', '開啟開發者模式', '4', '0:關閉\r\n1:開啟', '是否開啟開發者模式', '1383105995', '1383291877', '1', '1', '11');
INSERT INTO `onethink_config` VALUES ('33', 'ALLOW_VISIT', '3', '不受限控制器方法', '0', '', '', '1386644047', '1386644741', '1', '0:article/draftbox\r\n1:article/mydocument\r\n2:Category/tree\r\n3:Index/verify\r\n4:file/upload\r\n5:file/download\r\n6:user/updatePassword\r\n7:user/updateNickname\r\n8:user/submitPassword\r\n9:user/submitNickname\r\n10:file/uploadpicture', '0');
INSERT INTO `onethink_config` VALUES ('34', 'DENY_VISIT', '3', '超管專限控制器方法', '0', '', '僅超級管理員可訪問的控制器方法', '1386644141', '1386644659', '1', '0:Addons/addhook\r\n1:Addons/edithook\r\n2:Addons/delhook\r\n3:Addons/updateHook\r\n4:Admin/getMenus\r\n5:Admin/recordList\r\n6:AuthManager/updateRules\r\n7:AuthManager/tree', '0');
INSERT INTO `onethink_config` VALUES ('35', 'REPLY_LIST_ROWS', '0', '回復列表每頁條數', '2', '', '', '1386645376', '1387178083', '1', '10', '0');
INSERT INTO `onethink_config` VALUES ('36', 'ADMIN_ALLOW_IP', '2', '後臺允許訪問IP', '4', '', '多個用逗號分隔，如果不配置表示不限制IP訪問', '1387165454', '1387165553', '1', '', '12');
INSERT INTO `onethink_config` VALUES ('37', 'SHOW_PAGE_TRACE', '4', '是否顯示頁面Trace', '4', '0:關閉\r\n1:開啟', '是否顯示頁面Trace信息', '1387165685', '1387165685', '1', '0', '1');

-- -----------------------------
-- Table structure for `onethink_document`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_document`;
CREATE TABLE `onethink_document` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文檔ID',
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用戶ID',
  `name` char(40) NOT NULL DEFAULT '' COMMENT '標識',
  `title` char(80) NOT NULL DEFAULT '' COMMENT '標題',
  `category_id` int(10) unsigned NOT NULL COMMENT '所屬分類',
  `description` char(140) NOT NULL DEFAULT '' COMMENT '描述',
  `root` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '根節點',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '所屬ID',
  `model_id` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '內容模型ID',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '2' COMMENT '內容類型',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '推薦位',
  `link_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '外鏈',
  `cover_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '封面',
  `display` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '可見性',
  `deadline` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '截至時間',
  `attach` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '附件數量',
  `view` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '瀏覽量',
  `comment` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '評論數',
  `extend` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '擴展統計字段',
  `level` int(10) NOT NULL DEFAULT '0' COMMENT '優先級',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '數據狀態',
  PRIMARY KEY (`id`),
  KEY `idx_category_status` (`category_id`,`status`),
  KEY `idx_status_type_pid` (`status`,`uid`,`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='文檔模型基礎表';

-- -----------------------------
-- Records of `onethink_document`
-- -----------------------------
INSERT INTO `onethink_document` VALUES ('1', '1', '', 'OneThink1.0正式版發布', '2', '大家期待的OneThink正式版發布', '0', '0', '2', '2', '0', '0', '0', '1', '0', '0', '8', '0', '0', '0', '1387260660', '1387263112', '1');

-- -----------------------------
-- Table structure for `onethink_document_article`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_document_article`;
CREATE TABLE `onethink_document_article` (
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文檔ID',
  `parse` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '內容解析類型',
  `content` text NOT NULL COMMENT '文章內容',
  `template` varchar(100) NOT NULL DEFAULT '' COMMENT '詳情頁顯示模板',
  `bookmark` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '收藏數',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文檔模型文章表';

-- -----------------------------
-- Records of `onethink_document_article`
-- -----------------------------
INSERT INTO `onethink_document_article` VALUES ('1', '0', '<h1>\r\n	OneThink1.0正式版發布&nbsp;\r\n</h1>\r\n<p>\r\n	<br />\r\n</p>\r\n<p>\r\n	<strong>OneThink是壹個開源的內容管理框架，基於最新的ThinkPHP3.2版本開發，提供更方便、更安全的WEB應用開發體驗，采用了全新的架構設計和命名空間機制，融合了模塊化、驅動化和插件化的設計理念於壹體，開啟了國內WEB應用傻瓜式開發的新潮流。&nbsp;</strong> \r\n</p>\r\n<h2>\r\n	主要特性：\r\n</h2>\r\n<p>\r\n	1. 基於ThinkPHP最新3.2版本。\r\n</p>\r\n<p>\r\n	2. 模塊化：全新的架構和模塊化的開發機制，便於靈活擴展和二次開發。&nbsp;\r\n</p>\r\n<p>\r\n	3. 文檔模型/分類體系：通過和文檔模型綁定，以及不同的文檔類型，不同分類可以實現差異化的功能，輕松實現諸如資訊、下載、討論和圖片等功能。\r\n</p>\r\n<p>\r\n	4. 開源免費：OneThink遵循Apache2開源協議,免費提供使用。&nbsp;\r\n</p>\r\n<p>\r\n	5. 用戶行為：支持自定義用戶行為，可以對單個用戶或者群體用戶的行為進行記錄及分享，為您的運營決策提供有效參考數據。\r\n</p>\r\n<p>\r\n	6. 雲端部署：通過驅動的方式可以輕松支持平臺的部署，讓您的網站無縫遷移，內置已經支持SAE和BAE3.0。\r\n</p>\r\n<p>\r\n	7. 雲服務支持：即將啟動支持雲存儲、雲安全、雲過濾和雲統計等服務，更多貼心的服務讓您的網站更安心。\r\n</p>\r\n<p>\r\n	8. 安全穩健：提供穩健的安全策略，包括備份恢復、容錯、防止惡意攻擊登錄，網頁防篡改等多項安全管理功能，保證系統安全，可靠、穩定的運行。&nbsp;\r\n</p>\r\n<p>\r\n	9. 應用倉庫：官方應用倉庫擁有大量來自第三方插件和應用模塊、模板主題，有眾多來自開源社區的貢獻，讓您的網站“One”美無缺。&nbsp;\r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<p>\r\n	<strong>&nbsp;OneThink集成了壹個完善的後臺管理體系和前臺模板標簽系統，讓妳輕松管理數據和進行前臺網站的標簽式開發。&nbsp;</strong> \r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<h2>\r\n	後臺主要功能：\r\n</h2>\r\n<p>\r\n	1. 用戶Passport系統\r\n</p>\r\n<p>\r\n	2. 配置管理系統&nbsp;\r\n</p>\r\n<p>\r\n	3. 權限控制系統\r\n</p>\r\n<p>\r\n	4. 後臺建模系統&nbsp;\r\n</p>\r\n<p>\r\n	5. 多級分類系統&nbsp;\r\n</p>\r\n<p>\r\n	6. 用戶行為系統&nbsp;\r\n</p>\r\n<p>\r\n	7. 鉤子和插件系統\r\n</p>\r\n<p>\r\n	8. 系統日誌系統&nbsp;\r\n</p>\r\n<p>\r\n	9. 數據備份和還原\r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<p>\r\n	&nbsp;[ 官方下載：&nbsp;<a href=\"http://www.onethink.cn/download.html\" target=\"_blank\">http://www.onethink.cn/download.html</a>&nbsp;&nbsp;開發手冊：<a href=\"http://document.onethink.cn/\" target=\"_blank\">http://document.onethink.cn/</a>&nbsp;]&nbsp;\r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<p>\r\n	<strong>OneThink開發團隊 2013</strong> \r\n</p>', '', '0');

-- -----------------------------
-- Table structure for `onethink_document_download`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_document_download`;
CREATE TABLE `onethink_document_download` (
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文檔ID',
  `parse` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '內容解析類型',
  `content` text NOT NULL COMMENT '下載詳細描述',
  `template` varchar(100) NOT NULL DEFAULT '' COMMENT '詳情頁顯示模板',
  `file_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件ID',
  `download` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '下載次數',
  `size` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文檔模型下載表';


-- -----------------------------
-- Table structure for `onethink_file`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_file`;
CREATE TABLE `onethink_file` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文件ID',
  `name` char(30) NOT NULL DEFAULT '' COMMENT '原始文件名',
  `savename` char(20) NOT NULL DEFAULT '' COMMENT '保存名稱',
  `savepath` char(30) NOT NULL DEFAULT '' COMMENT '文件保存路徑',
  `ext` char(5) NOT NULL DEFAULT '' COMMENT '文件後綴',
  `mime` char(40) NOT NULL DEFAULT '' COMMENT '文件mime類型',
  `size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `md5` char(32) NOT NULL DEFAULT '' COMMENT '文件md5',
  `sha1` char(40) NOT NULL DEFAULT '' COMMENT '文件 sha1編碼',
  `location` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '文件保存位置',
  `create_time` int(10) unsigned NOT NULL COMMENT '上傳時間',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_md5` (`md5`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文件表';


-- -----------------------------
-- Table structure for `onethink_hooks`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_hooks`;
CREATE TABLE `onethink_hooks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `name` varchar(40) NOT NULL DEFAULT '' COMMENT '鉤子名稱',
  `description` text NOT NULL COMMENT '描述',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '類型',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `addons` varchar(255) NOT NULL DEFAULT '' COMMENT '鉤子掛載的插件 ''，''分割',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `onethink_hooks`
-- -----------------------------
INSERT INTO `onethink_hooks` VALUES ('1', 'pageHeader', '頁面header鉤子，壹般用於加載插件CSS文件和代碼', '1', '0', '');
INSERT INTO `onethink_hooks` VALUES ('2', 'pageFooter', '頁面footer鉤子，壹般用於加載插件JS文件和JS代碼', '1', '0', 'ReturnTop');
INSERT INTO `onethink_hooks` VALUES ('3', 'documentEditForm', '添加編輯表單的 擴展內容鉤子', '1', '0', 'Attachment');
INSERT INTO `onethink_hooks` VALUES ('4', 'documentDetailAfter', '文檔末尾顯示', '1', '0', 'Attachment,SocialComment');
INSERT INTO `onethink_hooks` VALUES ('5', 'documentDetailBefore', '頁面內容前顯示用鉤子', '1', '0', '');
INSERT INTO `onethink_hooks` VALUES ('6', 'documentSaveComplete', '保存文檔數據後的擴展鉤子', '2', '0', 'Attachment');
INSERT INTO `onethink_hooks` VALUES ('7', 'documentEditFormContent', '添加編輯表單的內容顯示鉤子', '1', '0', 'Editor');
INSERT INTO `onethink_hooks` VALUES ('8', 'adminArticleEdit', '後臺內容編輯頁編輯器', '1', '1378982734', 'EditorForAdmin');
INSERT INTO `onethink_hooks` VALUES ('13', 'AdminIndex', '首頁小格子個性化顯示', '1', '1382596073', 'SiteStat,SystemInfo,DevTeam');
INSERT INTO `onethink_hooks` VALUES ('14', 'topicComment', '評論提交方式擴展鉤子。', '1', '1380163518', 'Editor');
INSERT INTO `onethink_hooks` VALUES ('16', 'app_begin', '應用開始', '2', '1384481614', '');

-- -----------------------------
-- Table structure for `onethink_member`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_member`;
CREATE TABLE `onethink_member` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用戶ID',
  `nickname` char(16) NOT NULL DEFAULT '' COMMENT '昵稱',
  `sex` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '性別',
  `birthday` date NOT NULL DEFAULT '0000-00-00' COMMENT '生日',
  `qq` char(10) NOT NULL DEFAULT '' COMMENT 'qq號',
  `score` mediumint(8) NOT NULL DEFAULT '0' COMMENT '用戶積分',
  `login` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '登錄次數',
  `reg_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '註冊IP',
  `reg_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '註冊時間',
  `last_login_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '最後登錄IP',
  `last_login_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最後登錄時間',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '會員狀態',
  PRIMARY KEY (`uid`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='會員表';

-- -----------------------------
-- Table structure for `onethink_menu`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_menu`;
CREATE TABLE `onethink_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文檔ID',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '標題',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上級分類ID',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序（同級有效）',
  `url` char(255) NOT NULL DEFAULT '' COMMENT '鏈接地址',
  `hide` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否隱藏',
  `tip` varchar(255) NOT NULL DEFAULT '' COMMENT '提示',
  `group` varchar(50) DEFAULT '' COMMENT '分組',
  `is_dev` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否僅開發者模式可見',
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=122 DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `onethink_menu`
-- -----------------------------
INSERT INTO `onethink_menu` VALUES ('1', '首頁', '0', '1', 'Index/index', '0', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('2', '內容', '0', '2', 'Article/mydocument', '0', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('3', '文檔列表', '2', '0', 'article/index', '1', '', '內容', '0');
INSERT INTO `onethink_menu` VALUES ('4', '新增', '3', '0', 'article/add', '0', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('5', '編輯', '3', '0', 'article/edit', '0', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('6', '改變狀態', '3', '0', 'article/setStatus', '0', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('7', '保存', '3', '0', 'article/update', '0', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('8', '保存草稿', '3', '0', 'article/autoSave', '0', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('9', '移動', '3', '0', 'article/move', '0', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('10', '復制', '3', '0', 'article/copy', '0', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('11', '粘貼', '3', '0', 'article/paste', '0', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('12', '導入', '3', '0', 'article/batchOperate', '0', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('13', '回收站', '2', '0', 'article/recycle', '1', '', '內容', '0');
INSERT INTO `onethink_menu` VALUES ('14', '還原', '13', '0', 'article/permit', '0', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('15', '清空', '13', '0', 'article/clear', '0', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('16', '用戶', '0', '3', 'User/index', '0', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('17', '用戶信息', '16', '0', 'User/index', '0', '', '用戶管理', '0');
INSERT INTO `onethink_menu` VALUES ('18', '新增用戶', '17', '0', 'User/add', '0', '添加新用戶', '', '0');
INSERT INTO `onethink_menu` VALUES ('19', '用戶行為', '16', '0', 'User/action', '0', '', '行為管理', '0');
INSERT INTO `onethink_menu` VALUES ('20', '新增用戶行為', '19', '0', 'User/addaction', '0', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('21', '編輯用戶行為', '19', '0', 'User/editaction', '0', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('22', '保存用戶行為', '19', '0', 'User/saveAction', '0', '\"用戶->用戶行為\"保存編輯和新增的用戶行為', '', '0');
INSERT INTO `onethink_menu` VALUES ('23', '變更行為狀態', '19', '0', 'User/setStatus', '0', '\"用戶->用戶行為\"中的啟用,禁用和刪除權限', '', '0');
INSERT INTO `onethink_menu` VALUES ('24', '禁用會員', '19', '0', 'User/changeStatus?method=forbidUser', '0', '\"用戶->用戶信息\"中的禁用', '', '0');
INSERT INTO `onethink_menu` VALUES ('25', '啟用會員', '19', '0', 'User/changeStatus?method=resumeUser', '0', '\"用戶->用戶信息\"中的啟用', '', '0');
INSERT INTO `onethink_menu` VALUES ('26', '刪除會員', '19', '0', 'User/changeStatus?method=deleteUser', '0', '\"用戶->用戶信息\"中的刪除', '', '0');
INSERT INTO `onethink_menu` VALUES ('27', '權限管理', '16', '0', 'AuthManager/index', '0', '', '用戶管理', '0');
INSERT INTO `onethink_menu` VALUES ('28', '刪除', '27', '0', 'AuthManager/changeStatus?method=deleteGroup', '0', '刪除用戶組', '', '0');
INSERT INTO `onethink_menu` VALUES ('29', '禁用', '27', '0', 'AuthManager/changeStatus?method=forbidGroup', '0', '禁用用戶組', '', '0');
INSERT INTO `onethink_menu` VALUES ('30', '恢復', '27', '0', 'AuthManager/changeStatus?method=resumeGroup', '0', '恢復已禁用的用戶組', '', '0');
INSERT INTO `onethink_menu` VALUES ('31', '新增', '27', '0', 'AuthManager/createGroup', '0', '創建新的用戶組', '', '0');
INSERT INTO `onethink_menu` VALUES ('32', '編輯', '27', '0', 'AuthManager/editGroup', '0', '編輯用戶組名稱和描述', '', '0');
INSERT INTO `onethink_menu` VALUES ('33', '保存用戶組', '27', '0', 'AuthManager/writeGroup', '0', '新增和編輯用戶組的\"保存\"按鈕', '', '0');
INSERT INTO `onethink_menu` VALUES ('34', '授權', '27', '0', 'AuthManager/group', '0', '\"後臺 \\ 用戶 \\ 用戶信息\"列表頁的\"授權\"操作按鈕,用於設置用戶所屬用戶組', '', '0');
INSERT INTO `onethink_menu` VALUES ('35', '訪問授權', '27', '0', 'AuthManager/access', '0', '\"後臺 \\ 用戶 \\ 權限管理\"列表頁的\"訪問授權\"操作按鈕', '', '0');
INSERT INTO `onethink_menu` VALUES ('36', '成員授權', '27', '0', 'AuthManager/user', '0', '\"後臺 \\ 用戶 \\ 權限管理\"列表頁的\"成員授權\"操作按鈕', '', '0');
INSERT INTO `onethink_menu` VALUES ('37', '解除授權', '27', '0', 'AuthManager/removeFromGroup', '0', '\"成員授權\"列表頁內的解除授權操作按鈕', '', '0');
INSERT INTO `onethink_menu` VALUES ('38', '保存成員授權', '27', '0', 'AuthManager/addToGroup', '0', '\"用戶信息\"列表頁\"授權\"時的\"保存\"按鈕和\"成員授權\"裏右上角的\"添加\"按鈕)', '', '0');
INSERT INTO `onethink_menu` VALUES ('39', '分類授權', '27', '0', 'AuthManager/category', '0', '\"後臺 \\ 用戶 \\ 權限管理\"列表頁的\"分類授權\"操作按鈕', '', '0');
INSERT INTO `onethink_menu` VALUES ('40', '保存分類授權', '27', '0', 'AuthManager/addToCategory', '0', '\"分類授權\"頁面的\"保存\"按鈕', '', '0');
INSERT INTO `onethink_menu` VALUES ('41', '模型授權', '27', '0', 'AuthManager/modelauth', '0', '\"後臺 \\ 用戶 \\ 權限管理\"列表頁的\"模型授權\"操作按鈕', '', '0');
INSERT INTO `onethink_menu` VALUES ('42', '保存模型授權', '27', '0', 'AuthManager/addToModel', '0', '\"分類授權\"頁面的\"保存\"按鈕', '', '0');
INSERT INTO `onethink_menu` VALUES ('43', '擴展', '0', '7', 'Addons/index', '0', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('44', '插件管理', '43', '1', 'Addons/index', '0', '', '擴展', '0');
INSERT INTO `onethink_menu` VALUES ('45', '創建', '44', '0', 'Addons/create', '0', '服務器上創建插件結構向導', '', '0');
INSERT INTO `onethink_menu` VALUES ('46', '檢測創建', '44', '0', 'Addons/checkForm', '0', '檢測插件是否可以創建', '', '0');
INSERT INTO `onethink_menu` VALUES ('47', '預覽', '44', '0', 'Addons/preview', '0', '預覽插件定義類文件', '', '0');
INSERT INTO `onethink_menu` VALUES ('48', '快速生成插件', '44', '0', 'Addons/build', '0', '開始生成插件結構', '', '0');
INSERT INTO `onethink_menu` VALUES ('49', '設置', '44', '0', 'Addons/config', '0', '設置插件配置', '', '0');
INSERT INTO `onethink_menu` VALUES ('50', '禁用', '44', '0', 'Addons/disable', '0', '禁用插件', '', '0');
INSERT INTO `onethink_menu` VALUES ('51', '啟用', '44', '0', 'Addons/enable', '0', '啟用插件', '', '0');
INSERT INTO `onethink_menu` VALUES ('52', '安裝', '44', '0', 'Addons/install', '0', '安裝插件', '', '0');
INSERT INTO `onethink_menu` VALUES ('53', '卸載', '44', '0', 'Addons/uninstall', '0', '卸載插件', '', '0');
INSERT INTO `onethink_menu` VALUES ('54', '更新配置', '44', '0', 'Addons/saveconfig', '0', '更新插件配置處理', '', '0');
INSERT INTO `onethink_menu` VALUES ('55', '插件後臺列表', '44', '0', 'Addons/adminList', '0', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('56', 'URL方式訪問插件', '44', '0', 'Addons/execute', '0', '控制是否有權限通過url訪問插件控制器方法', '', '0');
INSERT INTO `onethink_menu` VALUES ('57', '鉤子管理', '43', '2', 'Addons/hooks', '0', '', '擴展', '0');
INSERT INTO `onethink_menu` VALUES ('58', '模型管理', '68', '3', 'Model/index', '0', '', '系統設置', '0');
INSERT INTO `onethink_menu` VALUES ('59', '新增', '58', '0', 'model/add', '0', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('60', '編輯', '58', '0', 'model/edit', '0', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('61', '改變狀態', '58', '0', 'model/setStatus', '0', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('62', '保存數據', '58', '0', 'model/update', '0', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('63', '屬性管理', '68', '0', 'Attribute/index', '1', '網站屬性配置。', '', '0');
INSERT INTO `onethink_menu` VALUES ('64', '新增', '63', '0', 'Attribute/add', '0', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('65', '編輯', '63', '0', 'Attribute/edit', '0', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('66', '改變狀態', '63', '0', 'Attribute/setStatus', '0', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('67', '保存數據', '63', '0', 'Attribute/update', '0', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('68', '系統', '0', '4', 'Config/group', '0', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('69', '網站設置', '68', '1', 'Config/group', '0', '', '系統設置', '0');
INSERT INTO `onethink_menu` VALUES ('70', '配置管理', '68', '4', 'Config/index', '0', '', '系統設置', '0');
INSERT INTO `onethink_menu` VALUES ('71', '編輯', '70', '0', 'Config/edit', '0', '新增編輯和保存配置', '', '0');
INSERT INTO `onethink_menu` VALUES ('72', '刪除', '70', '0', 'Config/del', '0', '刪除配置', '', '0');
INSERT INTO `onethink_menu` VALUES ('73', '新增', '70', '0', 'Config/add', '0', '新增配置', '', '0');
INSERT INTO `onethink_menu` VALUES ('74', '保存', '70', '0', 'Config/save', '0', '保存配置', '', '0');
INSERT INTO `onethink_menu` VALUES ('75', '菜單管理', '68', '5', 'Menu/index', '0', '', '系統設置', '0');
INSERT INTO `onethink_menu` VALUES ('76', '導航管理', '68', '6', 'Channel/index', '0', '', '系統設置', '0');
INSERT INTO `onethink_menu` VALUES ('77', '新增', '76', '0', 'Channel/add', '0', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('78', '編輯', '76', '0', 'Channel/edit', '0', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('79', '刪除', '76', '0', 'Channel/del', '0', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('80', '分類管理', '68', '2', 'Category/index', '0', '', '系統設置', '0');
INSERT INTO `onethink_menu` VALUES ('81', '編輯', '80', '0', 'Category/edit', '0', '編輯和保存欄目分類', '', '0');
INSERT INTO `onethink_menu` VALUES ('82', '新增', '80', '0', 'Category/add', '0', '新增欄目分類', '', '0');
INSERT INTO `onethink_menu` VALUES ('83', '刪除', '80', '0', 'Category/remove', '0', '刪除欄目分類', '', '0');
INSERT INTO `onethink_menu` VALUES ('84', '移動', '80', '0', 'Category/operate/type/move', '0', '移動欄目分類', '', '0');
INSERT INTO `onethink_menu` VALUES ('85', '合並', '80', '0', 'Category/operate/type/merge', '0', '合並欄目分類', '', '0');
INSERT INTO `onethink_menu` VALUES ('86', '備份數據庫', '68', '0', 'Database/index?type=export', '0', '', '數據備份', '0');
INSERT INTO `onethink_menu` VALUES ('87', '備份', '86', '0', 'Database/export', '0', '備份數據庫', '', '0');
INSERT INTO `onethink_menu` VALUES ('88', '優化表', '86', '0', 'Database/optimize', '0', '優化數據表', '', '0');
INSERT INTO `onethink_menu` VALUES ('89', '修復表', '86', '0', 'Database/repair', '0', '修復數據表', '', '0');
INSERT INTO `onethink_menu` VALUES ('90', '還原數據庫', '68', '0', 'Database/index?type=import', '0', '', '數據備份', '0');
INSERT INTO `onethink_menu` VALUES ('91', '恢復', '90', '0', 'Database/import', '0', '數據庫恢復', '', '0');
INSERT INTO `onethink_menu` VALUES ('92', '刪除', '90', '0', 'Database/del', '0', '刪除備份文件', '', '0');
INSERT INTO `onethink_menu` VALUES ('93', '其他', '0', '5', 'other', '1', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('96', '新增', '75', '0', 'Menu/add', '0', '', '系統設置', '0');
INSERT INTO `onethink_menu` VALUES ('98', '編輯', '75', '0', 'Menu/edit', '0', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('104', '下載管理', '102', '0', 'Think/lists?model=download', '0', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('105', '配置管理', '102', '0', 'Think/lists?model=config', '0', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('106', '行為日誌', '16', '0', 'Action/actionlog', '0', '', '行為管理', '0');
INSERT INTO `onethink_menu` VALUES ('108', '修改密碼', '16', '0', 'User/updatePassword', '1', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('109', '修改昵稱', '16', '0', 'User/updateNickname', '1', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('110', '查看行為日誌', '106', '0', 'action/edit', '1', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('112', '新增數據', '58', '0', 'think/add', '1', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('113', '編輯數據', '58', '0', 'think/edit', '1', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('114', '導入', '75', '0', 'Menu/import', '0', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('115', '生成', '58', '0', 'Model/generate', '0', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('116', '新增鉤子', '57', '0', 'Addons/addHook', '0', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('117', '編輯鉤子', '57', '0', 'Addons/edithook', '0', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('118', '文檔排序', '3', '0', 'Article/sort', '1', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('119', '排序', '70', '0', 'Config/sort', '1', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('120', '排序', '75', '0', 'Menu/sort', '1', '', '', '0');
INSERT INTO `onethink_menu` VALUES ('121', '排序', '76', '0', 'Channel/sort', '1', '', '', '0');

-- -----------------------------
-- Table structure for `onethink_model`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_model`;
CREATE TABLE `onethink_model` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '模型ID',
  `name` char(30) NOT NULL DEFAULT '' COMMENT '模型標識',
  `title` char(30) NOT NULL DEFAULT '' COMMENT '模型名稱',
  `extend` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '繼承的模型',
  `relation` varchar(30) NOT NULL DEFAULT '' COMMENT '繼承與被繼承模型的關聯字段',
  `need_pk` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '新建表時是否需要主鍵字段',
  `field_sort` text NOT NULL COMMENT '表單字段排序',
  `field_group` varchar(255) NOT NULL DEFAULT '1:基礎' COMMENT '字段分組',
  `attribute_list` text NOT NULL COMMENT '屬性列表（表的字段）',
  `template_list` varchar(100) NOT NULL DEFAULT '' COMMENT '列表模板',
  `template_add` varchar(100) NOT NULL DEFAULT '' COMMENT '新增模板',
  `template_edit` varchar(100) NOT NULL DEFAULT '' COMMENT '編輯模板',
  `list_grid` text NOT NULL COMMENT '列表定義',
  `list_row` smallint(2) unsigned NOT NULL DEFAULT '10' COMMENT '列表數據長度',
  `search_key` varchar(50) NOT NULL DEFAULT '' COMMENT '默認搜索字段',
  `search_list` varchar(255) NOT NULL DEFAULT '' COMMENT '高級搜索的字段',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '狀態',
  `engine_type` varchar(25) NOT NULL DEFAULT 'MyISAM' COMMENT '數據庫引擎',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='文檔模型表';

-- -----------------------------
-- Records of `onethink_model`
-- -----------------------------
INSERT INTO `onethink_model` VALUES ('1', 'document', '基礎文檔', '0', '', '1', '{\"1\":[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\",\"10\",\"11\",\"12\",\"13\",\"14\",\"15\",\"16\",\"17\",\"18\",\"19\",\"20\",\"21\",\"22\"]}', '1:基礎', '', '', '', '', 'id:編號\r\ntitle:標題:article/index?cate_id=[category_id]&pid=[id]\r\ntype|get_document_type:類型\r\nlevel:優先級\r\nupdate_time|time_format:最後更新\r\nstatus_text:狀態\r\nview:瀏覽\r\nid:操作:[EDIT]&cate_id=[category_id]|編輯,article/setstatus?status=-1&ids=[id]|刪除', '0', '', '', '1383891233', '1384507827', '1', 'MyISAM');
INSERT INTO `onethink_model` VALUES ('2', 'article', '文章', '1', '', '1', '{\"1\":[\"3\",\"24\",\"2\",\"5\"],\"2\":[\"9\",\"13\",\"19\",\"10\",\"12\",\"16\",\"17\",\"26\",\"20\",\"14\",\"11\",\"25\"]}', '1:基礎,2:擴展', '', '', '', '', 'id:編號\r\ntitle:標題:article/edit?cate_id=[category_id]&id=[id]\r\ncontent:內容', '0', '', '', '1383891243', '1387260622', '1', 'MyISAM');
INSERT INTO `onethink_model` VALUES ('3', 'download', '下載', '1', '', '1', '{\"1\":[\"3\",\"28\",\"30\",\"32\",\"2\",\"5\",\"31\"],\"2\":[\"13\",\"10\",\"27\",\"9\",\"12\",\"16\",\"17\",\"19\",\"11\",\"20\",\"14\",\"29\"]}', '1:基礎,2:擴展', '', '', '', '', 'id:編號\r\ntitle:標題', '0', '', '', '1383891252', '1387260449', '1', 'MyISAM');

-- -----------------------------
-- Table structure for `onethink_picture`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_picture`;
CREATE TABLE `onethink_picture` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵id自增',
  `path` varchar(255) NOT NULL DEFAULT '' COMMENT '路徑',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '圖片鏈接',
  `md5` char(32) NOT NULL DEFAULT '' COMMENT '文件md5',
  `sha1` char(40) NOT NULL DEFAULT '' COMMENT '文件 sha1編碼',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '狀態',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- -----------------------------
-- Table structure for `onethink_ucenter_admin`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_ucenter_admin`;
CREATE TABLE `onethink_ucenter_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '管理員ID',
  `member_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '管理員用戶ID',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '管理員狀態',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='管理員表';


-- -----------------------------
-- Table structure for `onethink_ucenter_app`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_ucenter_app`;
CREATE TABLE `onethink_ucenter_app` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '應用ID',
  `title` varchar(30) NOT NULL COMMENT '應用名稱',
  `url` varchar(100) NOT NULL COMMENT '應用URL',
  `ip` char(15) NOT NULL COMMENT '應用IP',
  `auth_key` varchar(100) NOT NULL COMMENT '加密KEY',
  `sys_login` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '同步登陸',
  `allow_ip` varchar(255) NOT NULL COMMENT '允許訪問的IP',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '應用狀態',
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='應用表';


-- -----------------------------
-- Table structure for `onethink_ucenter_member`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_ucenter_member`;
CREATE TABLE `onethink_ucenter_member` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用戶ID',
  `username` char(16) NOT NULL COMMENT '用戶名',
  `password` char(32) NOT NULL COMMENT '密碼',
  `email` char(32) NOT NULL COMMENT '用戶郵箱',
  `mobile` char(15) NOT NULL COMMENT '用戶手機',
  `reg_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '註冊時間',
  `reg_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '註冊IP',
  `last_login_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最後登錄時間',
  `last_login_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '最後登錄IP',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `status` tinyint(4) DEFAULT '0' COMMENT '用戶狀態',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='用戶表';

-- -----------------------------
-- Table structure for `onethink_ucenter_setting`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_ucenter_setting`;
CREATE TABLE `onethink_ucenter_setting` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '設置ID',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '配置類型（1-用戶配置）',
  `value` text NOT NULL COMMENT '配置數據',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='設置表';


-- -----------------------------
-- Table structure for `onethink_url`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_url`;
CREATE TABLE `onethink_url` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '鏈接唯壹標識',
  `url` char(255) NOT NULL DEFAULT '' COMMENT '鏈接地址',
  `short` char(100) NOT NULL DEFAULT '' COMMENT '短網址',
  `status` tinyint(2) NOT NULL DEFAULT '2' COMMENT '狀態',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_url` (`url`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='鏈接表';


-- -----------------------------
-- Table structure for `onethink_userdata`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_userdata`;
CREATE TABLE `onethink_userdata` (
  `uid` int(10) unsigned NOT NULL COMMENT '用戶id',
  `type` tinyint(3) unsigned NOT NULL COMMENT '類型標識',
  `target_id` int(10) unsigned NOT NULL COMMENT '目標id',
  UNIQUE KEY `uid` (`uid`,`type`,`target_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
